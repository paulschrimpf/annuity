# annuity likelihood
######################################################################
# external functions (compiled in c), in c/amplfunc.dll
function normpdf; # takes 1 argument, returns std normal pdf

function normcdf; # takes 1 argument, returns std normal cdf

function gampdf; # takes 3 arguments, (x,a,binv)
                 #  returns pdf at x with shape a and inverse scale binv
                 
function gamcdf; # takes 3 arguments, (x,a,binv)
                 #  returns cdf at x with shape a and inverse scale binv
                 
function gaucheb; # takes 3 arguments, (i,flag,N)
                  #  if flag=0, returns ith Gauss-Chebyshev integration
                  #  point from N point rule on (-infty,infty).
                  #  if flag=1, returns ith weight

function gaulag;  # takes 3 arguments, (i,flag,N)
                  #  if flag=0, returns ith Gauss-Laguerre integration
                  #  point from N point rule on [0,infty).
                  #  for integrating f(x)
                  #  if flag=1, returns ith weight
 
function cutoff; # take 2 arguments, (logA,c) returns cutoff value of 
                 # log beta at logA, c in the cutoff index
                 # works by linear interpolation. 
                 # setCut() must be called first

function setCut; # 3 arguments (nCut,logA{gridpts},logB{gridpts},c)
                 # stores grid of cutoff values used by cutoff() for 
                 # interpolation.  must be called before cutoff()
                 #  nCut is total number of cutoffs
                 #  logA{gridpts} are log alpha grid points
                 #  logB{gridpts} are log beta grid points
                 #  c (between 1 and nCut) is a cutoff index
######################################################################

######################################################################
# Parameter declarations.  These are constants to the solver.
# syntax is:
# "param name [{indexing set}] [type] [:= value];"
# values not assigned here, are assigned in the .dat file

# data related parameters
param nObs integer; # number of observations
param nG integer := 4;
set people=1..nObs; # indexing set of observations
param group{i in people};
param fage{i in people}; # age entered sample
param lage{i in people}; # age exited sample
param died{i in people} binary; # whether each person died
param g{i in people} integer; # guarantee choice
param nX; # number of x's
param X{i in people, k in 1..nX}; # x's
# these next three are just b/c we can't use logical statements in the
# objective function
param g0 {i in people} binary := if g[i]=0 then 1 else 0;
param g5 {i in people} binary:= if g[i]=5 then 1 else 0;
param g10{i in people} binary:= if g[i]=10 then 1 else 0;

# cutoffs
param cutN integer; # number of gridpoints
param nCut integer := 3; # number of cutoffs
param cutLogB{i in 1..cutN, c in 1..nCut, d in 1..nG};
param cutLogA{i in 1..cutN};


# parameters related to integration
param nInt := 24; # number of integration points
                  # (as long as at 12 or so, it does not make much difference)
param x{i in 1..nInt} := gaucheb(i,0,nInt); # integration points
param w{i in 1..nInt} := gaucheb(i,1,nInt); # integration weights

# limits on variables
#  Note: there are apparently at least two ways to put constant constraints
#        on variables.  We can declare them as 
#	   "var name <=upBound,>=loBound;"
#        or we can later write
#          "subject to constraintName: loBound<=varName<=upBound"
#        I'm not sure if it makes a difference.  Both are done here.
param minSig; 
param maxCorr := 1 - 1e-6;
param minCorr := -1 + 1e-6;
######################################################################
param pMid;

param lambda;

######################################################################
# variables: these are what we will maximize with respect to
var muA{1..nG};
var muB{1..nG};
var gA{1..nX};
var gB{1..nX};
var sigA>=minSig;
var sigB>=minSig;
var corr<=maxCorr,>=minCorr; 
######################################################################

######################################################################
# problem statement
maximize logLikelihood:
 sum{i in people} log( max(1e-300, # to ensure we don't take log(0)
  # integral        # integration weight 
  sum{j in 1..nInt} w[j]*normpdf(x[j])*
 # mortality portion of liklelihood
  # note: alpha[j] = exp(x[j]*sigA+muA[group[i]])
  (died[i]*exp(exp(x[j]*sigA+muA[group[i]]+(sum{k in 1..nX} gA[k]*X[i,k]))/lambda*
              (exp(lambda*fage[i])-exp(lambda*lage[i])))
         *exp(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]))*exp(lambda*lage[i])
   +
   (1-died[i])*exp(exp(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]))/lambda*
               (exp(lambda*fage[i])-exp(lambda*lage[i])))
   )
 # annuity choice portion of likelihood
 # note: condmu = muB[group[i]] + corr*sigB*(logalpha[j]-muA[group[i]])/sigA
 #              = muB[group[i]] + corr*sigB*x[j]
 #       condsig = sigB*sqrt(1-corr^2)
   *(g0[i]* normcdf((cutoff(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]),
               1+nCut*(group[i]-1))-(muB[group[i]]+corr*sigB*x[j] + (sum{k in 1..nX} gB[k]*X[i,k])))
                   /(sigB*sqrt(1-corr^2)))*(1-pMid) 
    +g5[i]*( (normcdf((cutoff(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]),
               2+nCut*(group[i]-1))-(muB[group[i]]+corr*sigB*x[j] + (sum{k in 1..nX} gB[k]*X[i,k])))
                    /(sigB*sqrt(1-corr^2))) 
           -normcdf((cutoff(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]),
               1+nCut*(group[i]-1))-(muB[group[i]]+corr*sigB*x[j] + (sum{k in 1..nX} gB[k]*X[i,k])))
                    /(sigB*sqrt(1-corr^2)))) *(1-pMid) + pMid)
    +g10[i]*(1-normcdf((cutoff(x[j]*sigA+muA[group[i]] + (sum{k in 1..nX} gA[k]*X[i,k]),
               2+nCut*(group[i]-1))-(muB[group[i]]+corr*sigB*x[j] + (sum{k in 1..nX} gB[k]*X[i,k])))
                       /(sigB*sqrt(1-corr^2))))*(1-pMid) 
    )
  ))
 ;

subject to # domain constraints 
 sigBoundA: sigA>=minSig;
 sigBoundB: sigB>=minSig;
 corrBound: minCorr<=corr<=maxCorr;
# muBoundA: -8<=muA<=-4;
# muBoundB: 5<=muB<=15;
