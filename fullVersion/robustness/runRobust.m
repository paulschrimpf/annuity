% array of robustness specs
% re-runs counterfactuals and calculates percent vol annuitize for the
% cells that do not have it now

if (exist('wd'))
  cd(wd);
end
clear;

base.year = 92;
base.gam = 3;
base.bgam = 3;
base.frac = 0.2;
base.zeta = 1;
base.dist = 'normal';
base.withX = false;
base.ext = false;
base.r = 0;
base.pub = 0;
base.lambda = 0.1102206337978725; % pooled value
base.dumbp = 0;
base.nonh.w0 = 100;
base.nonh.p = 1;
base.fasthazard = 1;
base.zeta = 1;
base.gammaHetero.beta = 0;
base.alphaUncertainty = 0;
base.alphaBias = 1;
base.twodh = [];

% robustness specs
robust{1} = base;
robust{1}.gam = 5;
robust{1}.bgam = 5;

robust{2} = base;
robust{2}.gam = 1.5;
robust{2}.bgam = 1.5;

robust{3} = base;
robust{3}.bgam = 1.5;

robust{4} = base;
robust{4}.bgam = 5;

robust{5} = base;
robust{5}.frac = 0.1;

robust{6} = base;
robust{6}.frac=.3;

robust{7} = base;
robust{7}.year = 90;

robust{8} = base;
robust{8}.dist = 'gamma-normal';

robust{9} = base;
robust{9}.dist = 'gamma';

robust{10} = base;
robust{10}.r = 0.06;

robust{11} = base;
robust{11}.pub = 0.5;

robust{12} = base;
robust{12}.withX = true;

robust{13} = base;
robust{13}.ext = true;

robust{14} = base;
robust{14}.r = 0.05;

robust{15} = base;
robust{15}.dumbp = 0.5;

robust{16} = base;
robust{16}.nonh.w0 = [1750 8950 24900];
robust{16}.nonh.p = [.375 .25 .375];
robust{16}.bgam = 1.5;

robust{17} = base;
robust{17}.fasthazard = 2;
robust{17}.lambda = 0.001523088419505936;

robust{18} = base;
robust{18}.fasthazard = 3;
robust{18}.lambda = 3.578765549617122e-05;

robust{19} = base;
robust{19}.frac=.5;

robust{20} = base;
robust{20}.frac=.7;

robust{21} = base;
robust{21}.zeta=1/(1.05);

robust{22} = base;
robust{22}.zeta=1/(1.1);

robust{23} = base;
robust{23}.zeta=1/2;

robust{24} = base;
robust{24}.fasthazard = 1.5;
robust{24}.lambda = 0.01340732327124827;

% consumption ra hetero, w0 = 100
robust{25} = base;
robust{25}.gammaHetero.beta = exp([9.769910335744829 ...
                    9.425006169276971 ...
                    9.647593475141907 ...
                    9.868793665937353 ...
                   ]);
robust{25}.gammaHetero.cons = 1;
robust{25}.gammaHetero.beq = 0;
robust{25}.gammaHetero.w0 = 100;

% bequest ra hetero, w0 = 100
robust{26} = base;
robust{26}.gammaHetero.beta = exp(9.7);
robust{26}.gammaHetero.cons = 0;
robust{26}.gammaHetero.beq = 1;
robust{26}.gammaHetero.w0 = 100;

% consumption ra hetero, w0 = mean = 12231
robust{27} = base;
robust{27}.gammaHetero.beta = exp([ -1.0716   -1.7828   -3.2859   -3.1478]);
   % mean beta 
robust{27}.gammaHetero.cons = 1;
robust{27}.gammaHetero.beq = 0;
robust{27}.gammaHetero.w0 = 12231;

% consumption ra hetero, w0 = median = 8950
robust{28} = base
robust{28}.gammaHetero.beta = exp([-2.4891   -3.2092   -2.1856   -3.9401]);
% beta at median
robust{28}.gammaHetero.cons = 1;
robust{28}.gammaHetero.beq = 0;
robust{28}.gammaHetero.w0 = 8950;

% alpha uncertainty (std deviation of uncertainty about alpha)
siga = 0.054;
robust{29} = base;
robust{29}.alphaUncertainty = 0.2*siga;
robust{30} = base;
robust{30}.alphaUncertainty = 0.5*siga;
robust{31} = base;
robust{31}.alphaUncertainty = siga;
robust{32} = base;
robust{32}.alphaUncertainty = 2*siga;

% alpha bias: alphaHat = mu + alphaBias*(log alpha - mu)
robust(33:36) = {base};
robust{33}.alphaBias = 0.5;
robust{34}.alphaBias = 0.9;
robust{35}.alphaBias = 1.1;
robust{36}.alphaBias = 2;

% two dimensions of heterogeneity
robust{37} = base; 
robust{37}.twodh.gamma = [1.5 3 4.5]; 

% fraction annuitized correlated with preferences
robust{38} = base;
robust{38}.frac = [0.1 0.2 0.4];
robust{38}.pfrac = [0.5 0.25 0.25];

%robust{27} = base;
%robust{27}.dist = 'mixed-normal';

robust{39} = base;

SEPARATE = true; % separate by year
SIMULATE = true;
USEOLDSIM = true;
OBJECTIVE = 'mle';
READLAMBDA = true;
ESTIMATE = false;
DEGREE = 1;
EXTRALAGRID = 0;
NINT = 24;
PENALTY = -1;

path(path,'../src');
path(path,'../src/cutoffs');
path(path,'../bootstrap');
path(path,'../report');

for rb=39:39 %rb=31:32
  clear annuityLike;
  clear step2Like;
  clear mortLike;
  clear transformParam;
  
  
  GAM = robust{rb}.gam;
  BGAM = robust{rb}.bgam;
  FRAC = robust{rb}.frac;
  DIST = robust{rb}.dist;
  ZETA = robust{rb}.zeta;
  YEAR = robust{rb}.year;
  EXTERNAL = robust{rb}.ext;
  PUBLICANNUITY = robust{rb}.pub;
  INCLUDEX = robust{rb}.withX;
  RATE = robust{rb}.r;
  LAMBDA = robust{rb}.lambda
  DUMB.p = robust{rb}.dumbp;
  DUMB.estP = 0;
  NONH = robust{rb}.nonh;
  FASTHAZARD = robust{rb}.fasthazard;
  ZETA = robust{rb}.zeta;
  GAMMAHETERO = robust{rb}.gammaHetero;  
  ALPHAUN = robust{rb}.alphaUncertainty;
  ALPHABI = robust{rb}.alphaBias;
  TWODH = robust{rb}.twodh;
  
  configName = 'confPooled';

  %if (DIST ~= 'normal') 
  %  continue;
  %end

  eval(['cfg = ' configName '();']);
  mortOnly = strcmp(OBJECTIVE,'mlmort'); 
  if (mortOnly)  % change config to just do mortality
    READLAMBDA = false;
  end
  cfg.est.objective = OBJECTIVE;

  switch(OBJECTIVE)
   case '2step'
    cfg.outprefix = [cfg.outprefix '_2step'];
   case 'mlmort'
    cfg.outprefix = [cfg.outprefix 'Mort'];
  end

  cfg.cut.gam = GAM;
  cfg.cut.bgam = BGAM;
  cfg.cut.fracAnnuitized = FRAC;
  if (numel(FRAC)>1)
    cfg.cut.pfrac = robust{rb}.pfrac;
  else
    cfg.cut.pfrac = 1;
  end
  cfg.est.dist = DIST;
  cfg.cut.zeta = ZETA;
  cfg.mort.fasthazard = FASTHAZARD;
  cfg.cut.fasthazard = FASTHAZARD;
  cfg.cut.public = PUBLICANNUITY;
  cfg.cut.alphaUncertainty = ALPHAUN;
  cfg.cut.alphaBias = ALPHABI;
  cfg.est.dumb = DUMB;
  
  if (SEPARATE) 
    cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
  else
    if (YEAR~=92)
      cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
    end
  end
  if (GAM~=3)
    cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
  end
  if (BGAM~=GAM)
    cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
  end
  if (numel(FRAC)>1) 
    cfg.outprefix = [cfg.outprefix '_varfrac'];
  elseif (FRAC~=0.2)
    cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
  end
  if (~strcmp(DIST,'normal'))
    cfg.outprefix = [cfg.outprefix,'_' DIST];
  end
  if(ZETA~=1)
    cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
  end
  if(INCLUDEX)
    cfg.outprefix = [cfg.outprefix, '_X'];
    cfg.data.lalphaX = [-1 5 7];
    cfg.data.alphaXName(2).s = 'Premium';
    cfg.data.alphaXName(3).s = 'Percqua';

    cfg.data.lbetaX = [-1 5 7];
    cfg.data.betaXName(2).s = 'Premium';
    cfg.data.betaXName(3).s = 'Percqua';
  end
  if(FASTHAZARD~=1)
    cfg.outprefix = [cfg.outprefix, '_t' num2str(FASTHAZARD)];
    if (FASTHAZARD==2)
      cfg.cut.la = (-12:0.1:-2)';
    elseif (FASTHAZARD==3)
      cfg.cut.la = (-15:0.1:-5)';
    end
  end
  if(EXTERNAL)
    cfg.outprefix = [cfg.outprefix '_ext'];
    cfg.data.okay = 15;
    cfg.data.okeep = 1;
  end
  if(PUBLICANNUITY)
    cfg.outprefix = [cfg.outprefix '_pub' num2str(PUBLICANNUITY*100)];
  end
  if(exist('RATE','var') && RATE>0)
    cfg.outprefix = [cfg.outprefix '_r' num2str(RATE*100)];
    cfg.cut.rho = 1/(1+RATE);
    cfg.cut.delta = 1/(1+RATE);
    cfg.data = rmfield(cfg.data,'lrate');
  end
  if (DUMB.p>0 || DUMB.estP)
    if (DUMB.estP)
      cfg.outprefix = [cfg.outprefix '_dumbEst'];
    else
      cfg.outprefix = [cfg.outprefix '_dumb' num2str(DUMB.p*100)];
    end
  end
  if (exist('NONH','var') && length(NONH.w0)>1) 
    cfg.outprefix = [cfg.outprefix '_nonh'];
    cfg.cut.w0 = NONH.w0;
    cfg.cut.wp = NONH.p;
  elseif numel(FRAC)>1
    cfg.cut.wp = robust{rb}.pfrac;
  else
    cfg.cut.wp = 1;
  end
  if (exist('GAMMAHETERO','var') && any(GAMMAHETERO.beta>0))
    cfg.cut.raHetero = GAMMAHETERO;
    cfg.cut.file = 'cutoffs/gcut';
    if (GAMMAHETERO.cons && ~GAMMAHETERO.beq);
      cfg.outprefix = [cfg.outprefix '_raH'];
    elseif (GAMMAHETERO.cons && GAMMAHETERO.beq)
      cfg.outprefix = [cfg.outprefix '_raH_both'];
    elseif (~GAMMAHETERO.cons && GAMMAHETERO.beq)
      cfg.outprefix = [cfg.outprefix '_raH_beq'];
    end
    cfg.cut.w0 = GAMMAHETERO.w0;
    if (GAMMAHETERO.w0 ~= 100)
      cfg.outprefix = [cfg.outprefix '_w' num2str(GAMMAHETERO.w0)];
    end
  end
  if (exist('ALPHAUN','var') && ALPHAUN>0)
    cfg.outprefix = [cfg.outprefix '_aUnc' num2str(ALPHAUN)];
  end
  if (ALPHABI ~= 1)
    cfg.outprefix = [cfg.outprefix '_aBias' num2str(ALPHABI)];
  end
  if isstruct(TWODH) 
    cfg.outprefix = [cfg.outprefix '_twodh'];
    cfg.cut.twodh = TWODH;
  end
  
  cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');

  if (exist('LAMBDA','var'))
    %cfg.outprefix = [cfg.outprefix '_lambda' num2str(LAMBDA)];    
    cfg.mort.lambda=LAMBDA;
    cfg.cut.lambda = cfg.mort.lambda;
  end

  % read data
  if (SEPARATE) 
    switch YEAR
     case {92,95}
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9295.txt'];
     case {90,97} 
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9097.txt']; 
     otherwise
      error(['no appropriate data set for separate and ' num2str(YEAR)]);
    end
      cfg.data.keepYear = 1900 + YEAR;
  else
    if (YEAR<100)
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_19' num2str(YEAR) ...
                       '.txt'];
    else
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_' num2str(YEAR) ...
                       '.txt'];
    end
    cfg.data.allDummies=false;
  end
  
  % load data
  [data cfg] = readData(cfg); % see readData for details of data struc
  if (~strcmp(cfg.est.objective,'mlmort'))
    %cfg.cut.redo = true;
    % load cutoffs
    if (EXTRALAGRID)
      cfg.cut.la = sort([cfg.cut.la; EXTRALAGRID]);
    end
    cfg.cut.lambda = cfg.mort.lambda;
    wd = pwd;
    cd ../src;
    cut = loadcutoffs(cfg);
    cd(wd);
    which cleanCut
    cut = cleanCut(cut,cfg,data);
    est.cut = cut;
  end
  if (isfield(cfg,'dropBadGroups') && cfg.dropBadGroups)
    [data cut cfg] = dropBadGroups(data,cut,cfg);
    est.cut = cut;
  end 

  if (INCLUDEX) % change order of x's so that x's are last
    X = data.alphaX;
    name = data.name.alphaX;
    data.alphaX(:,1:4) = X(:,[1 4 5 6]);
    data.alphaX(:,5:6) = X(:,2:3);
    data.name.alphaX(2:4) = name(4:6);
    data.name.alphaX(5:6) = name(2:3);
    data.name.betaX = data.name.alphaX;
    data.betaX = data.alphaX;
  end
  
  est.lambda = cfg.mort.lambda;
  est.fasthazard = cfg.mort.fasthazard;
  est.inc = cfg.mort.inc;
  est.disp = cfg.est.disp;
  est.extraMoments = cfg.est.extraMoments;
  est.dist = cfg.est.dist;
  est.dumb = cfg.est.dumb;
  
  % initialize integration points
  cfg.est.nint = NINT;
  switch(est.dist)
   case {'normal'}
    est.int = gaussChebyshevInt(cfg.est.nint);
   case {'normal-flex'}
    est.int = gaussChebyshevInt(cfg.est.nint);
    est.parm.var(1,2:(DEGREE+1)) = 0;
   case {'mixed-normal'}
    est.int = gaussChebyshevInt(cfg.est.nint);
   case {'gamma','gamma-normal'}
    [est.int.x est.int.w] = gaussLaguerre(cfg.est.nint,0);
    est.int.n = cfg.est.nint;
  end

  i = findstr('/',cfg.outprefix);
  prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
 
  % call ampl to estimate the model
  if (ESTIMATE) 
    writeAMPLdata(data,cfg,cut);
    fprintf('Wrote an AMPL data set\n');
    i = findstr('/',cfg.outprefix);
    dataname = [cfg.outprefix(i(length(i))+1:length(cfg.outprefix)) '.dat'];
    system(['perl renameDat.pl ' dataname]);
    fprintf('calling ampltrial ...\n');
    if (DUMB.p)
      if(DUMB.p~=0.5)
        error('dumb.p');
      end
      [status output] = system('amplTrial pooledD50.ampl | tee ampl.out');
    elseif (INCLUDEX)
      [status output] = system('amplTrial pooledWithX.ampl | tee ampl.out');
    elseif (length(NONH.w0)>1)
      %[status output] = system('amplTrial poolednonh.ampl | tee
      %ampl.out');
    elseif (ALPHAUN>0) 
      [status output] = system(['amplTrial pooledAlphaUn.ampl | tee' ...
                    ' ampl.out']);
    elseif (ALPHABI~=1) 
      [status output] = system(['amplTrial pooledAlphaBias.ampl | tee' ...
                    ' ampl.out']);
    elseif (numel(FRAC)>1)
      [status output] = system(['amplTrial pooledVarFrac.ampl | tee' ...
                    ' ampl.out']);      
    elseif isstruct(TWODH)
      [status output] = system(['amplTrial pooled2dh.ampl | tee' ...
                    ' ampl.out']);            
    else
      if (strcmp(DIST,'normal'))
        [status output] = system('amplTrial pooled.ampl | tee ampl.out');
      elseif (strcmp(DIST,'gamma'))
        [status output] = system('amplTrial pooledgamma.ampl | tee ampl.out');
      elseif (strcmp(DIST,'gamma-normal'))
        [status output] = system('amplTrial pooledGN.ampl | tee ampl.out');
      elseif (strcmp(DIST,'mixed-normal'))
        %[status output] = system('amplTrial pooledMN.ampl | tee ampl.out');
      end
    end
    
    system(['perl writeEstimates.pl estimates/' prefix '.out'])
  end
  % load estimates
  amplEst = load(['estimates/' prefix '.out']);
  amplLike= amplEst(1);
  
  if isstruct(TWODH)
    k = length(TWODH.gamma);
  elseif numel(FRAC)>1
    k = numel(FRAC);
    est.parm.frac = FRAC;
  else 
    k = length(cfg.cut.w0);
  end
  est.w0 = cfg.cut.w0;
  est.wp = cfg.cut.wp;
  est.parm.gAlpha = zeros(k,size(data.alphaX,2));
  est.parm.gBeta = zeros(k,size(data.betaX,2));
  j = 2;
  est.parm.gAlpha(:,1) = amplEst(j:j+k-1);
  j = j+k;
  est.parm.gAlpha(:,2) = amplEst(j:j+k-1);
  j = j+k;
  est.parm.gAlpha(:,3) = amplEst(j:j+k-1);
  j = j+k;  
  est.parm.gAlpha(:,4) = amplEst(j:j+k-1);
  j = j+k;
  switch(DIST)
   case 'normal'
    est.parm.var(1,1) = amplEst(j)^2;
    M = 1;
   case {'gamma','gamma-normal'}
    est.parm.ashape = amplEst(j);
    M = 1;
   case 'mixed-normal'
    est.parm.var(1,1) = amplEst(j)^2;
    M = 2;    
  end
  j = j+1;
  for m=1:M
    if (m>1)
      est.parm(m).gAlpha = est.parm(1).gAlpha;
      est.parm(m).var = est.parm(1).var;
    end
    est.parm(m).gBeta(:,1) = amplEst(j:j+k-1);
    j = j+k;
    est.parm(m).gBeta(:,2) = amplEst(j:j+k-1);
    j = j+k;
    est.parm(m).gBeta(:,3) = amplEst(j:j+k-1);
    j = j+k;  
    est.parm(m).gBeta(:,4) = amplEst(j:j+k-1);
    j = j+k;
  end

  switch(DIST)
   case {'normal','gamma-normal'} 
    est.parm.var(2,2) = amplEst(j)^2;
    j = j+1;  
   case {'gamma'}
    est.parm(m).var = eye(2);
    est.parm(m).bshape = amplEst(j);
    j = j+1;
   case 'mixed-normal'
    for m=1:M
      est.parm(m).var(2,2) = amplEst(j)^2;
      j = j+1;
    end    
  end
  for m=1:M
    est.parm(m).corr = amplEst(j);  
    est.parm(m).var(1,2) = est.parm(m).corr*sqrt(est.parm(m).var(1,1)* ...
                                              est.parm(m).var(2,2));
    j = j+1;
  end

  if (INCLUDEX)
    est.parm.gAlpha(5:6) = amplEst(j:j+1);
    j = j+2;
    est.parm.gBeta(5:6) = amplEst(j:j+1);
    j = j+2;
  end
  if (DUMB.p)
    est.parm.dumb.p = DUMB.p;
  end
  if (M>1)
    for m=1:M
      est.parm(m).p = amplEst(j);
      j = j+1;
    end
  end
  if isstruct(TWODH)
    est.parm.twodh = TWODH;
    for m = 1:length(TWODH.gamma)
      est.parm.twodh.p(m) = amplEst(j);
      j = j+1;
    end
  end
  % convert data.alphaX and data.betaX into group dummies 
  for g=1:length(cfg.data.group)
    x(:,g) = data.group==g;
    data.name.alphaX(g).s = cfg.data.group(g).name
    data.name.betaX(g).s = cfg.data.group(g).name
  end
  data.alphaX(:,1:4) = x;
  data.betaX(:,1:4) = x;

  estSave = est;
 
  % get std errors 
  % pack parameters
  switch(cfg.est.objective)
   case 'mle'
    objFunc = 'annuityLike';
    parm = transformParam(est.parm,'pack',cfg.est.objective,cfg.est.dist);
   case 'mlmort'
    objFunc = 'mortLike';
    k = length(est.parm.gAlpha);
    parm = zeros(k+2,1);
    parm(1:k) = est.parm.gAlpha;
    k = k+1;
    switch (est.dist)
     case {'normal','exponential'}
      parm(k) = log(sqrt(est.parm.var(1,1)));
      k = k+1;
     case 'gamma'
      parm(k) = log(sqrt(est.parm.var(1,1)));
      k = k+1;
      parm(k) = log(1);
      k = k+1;
     case 'mixed-normal'
      error('not implemented');
    end
    parm(k) = log(est.lambda);
   case '2step'
    objFunc = 'step2Like';
    cfg2 = cfg;
    est2 = est;
    load(cfg.est.mortResults);
    cfg = cfg2;
    est.dist = cfg.est.dist;
    est.lambda = cfg.mort.lambda;
    est.inc = cfg.mort.inc;
    est.disp = cfg.est.disp;
    est.extraMoments = cfg.est.extraMoments;
    est.int = est2.int;
    est.cut = est2.cut;
    est.parm.var(2,2) = est2.parm.var(2,2);
    est.parm.gBeta = est2.parm.gBeta;
    clear cfg2 est2;
    parm = transformParam(est.parm,'pack',cfg.est.objective,est.dist);
   otherwise
    error('Unrecognized objective function\n');
  end
  bootEst = [];
  est.penalty = PENALTY;
  if (length(NONH.w0)==1 && isempty(TWODH) && numel(FRAC)==1) 
    eval(['[f g h gi] = ' objFunc '(parm,est,data);']);
  else
    f = 0;
    g = zeros(size(parm));
    h = eye(length(parm));
    gi = ones(data.N,length(parm));
  end
  fprintf(['amplLike= %.16g\n'...
           'logLike = %.16g\n'...
           'norm(grad)=%g\n'],amplLike,f,norm(g));

  % unpack parameters
  if (~strcmp(cfg.est.objective,'mlmort') && isempty(TWODH))
    [est.parm der] = transformParam(parm,'unpack',cfg.est.objective,est.dist);
    % standard errors5A
    if(size(gi,2)==data.N)
      gi = gi' - ones(data.N,1)*sum(gi');
    else 
      gi = gi - ones(data.N,1)*sum(gi);
    end
    v = inv(gi'*gi);
    switch(est.dist)
     case 'normal'
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      jac = reshape(der.var(1,1,:),1,length(parm));
      std.var(1,1) = sqrt(jac*v*jac');
      jac = reshape(der.var(1,2,:),1,length(parm));
      std.var(1,2) = sqrt(jac*v*jac');
      jac = reshape(der.var(2,2,:),1,length(parm));
      std.var(2,2) = sqrt(jac*v*jac');
      std.corr = sqrt(der.corr*v*der.corr');

      std.lambda = 0;
      est.parm.std = std;          
     case 'normal-flex'
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      std.var = est.parm.var;
      std.corr = 0;
      
      est.parm.std = std;          
     case 'mixed-normal'
      std = est.parm;
      for m=1:length(est.parm)
        std(m).gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
        std(m).gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
        std(m).corr = 0;
        std(m).var = zeros(2,2);
        est.parm(m).std = std(m);                    
      end
     case 'gamma'
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      jac = reshape(der.ashape,1,length(parm));
      std.ashape = sqrt(jac*v*jac');
      jac = reshape(der.bshape,1,length(parm));
      std.bshape = sqrt(jac*v*jac');
      std.corr = sqrt(der.corr*v*der.corr');
      std.var = zeros(2,2);
      
      est.parm.std = std;          
     case 'gamma-normal'
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      jac = reshape(der.ashape,1,length(parm));
      std.ashape = sqrt(jac*v*jac');
      std.corr = sqrt(der.corr*v*der.corr');
      jac = reshape(der.var(2,2,:),1,length(parm));
      std.var(2,2) = sqrt(jac*v*jac');
      
      est.parm.std = std;          
    end     
  elseif ~isempty(TWODH) || numel(FRAC)>1
    est.parm.std.gAlpha = zeros(size(est.parm.gAlpha));
    est.parm.std.var = zeros(size(est.parm.var));
    est.parm.std.gBeta = zeros(size(est.parm.gBeta));  
    est.parm.std.corr = 0;
    est.parm.std.lambda=0;    
  else
    % unpack parameters
    k = length(est.parm.gAlpha);
    est.parm.gAlpha = parm(1:k);
    k = k+1;
    stdalpha = exp(parm(k));
    est.parm.var(1,1) = stdalpha^2;
    k = k+1;
    est.lambda = exp(parm(k));
    est.parm.std.gAlpha = zeros(size(est.parm.gAlpha));
    est.parm.std.var = zeros(size(est.parm.var));
    est.parm.var(1,2) = 0;
    est.parm.var(2,2) = 0;
    est.parm.var(2,1) = 0;  
    est.parm.std.gBeta = zeros(size(est.parm.gBeta));  
    est.parm.gBeta = zeros(size(est.parm.gBeta));
    est.parm.std.corr = 0;
    est.parm.corr = 0;
    est.parm.std.lambda=0;
  end

 

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  i = findstr('/',cfg.outprefix);
  prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
  
  % run counterfactuals
  % reduced form
  if (mortOnly)
    cfnsim = 0;
  else
    cfnsim = 300;
  end
  
  mkdir(prefix);
  mkdir(prefix,'tables');
  mkdir(prefix,'figures');
  %eval(['delete ' prefix '/tables/*']);
  %eval(['delete ' prefix '/figures/*']);
  %eval(['delete ' prefix '/*']);
  if(cfg.mort.lambda~=est.lambda)
    warning('cfg lambda ~= est.lambda, using est.lambda');
    cfg.mort.lambda = est.lambda;
  end
    
  % table of estimates  est.parm.std.lambda = 0;
  %estReport(data,est,prefix);

  if (exist('GAMMAHETERO','var') && any(GAMMAHETERO.beta>0))
    graphCutRA(data,est,cut,prefix);
  elseif isempty(TWODH) && numel(FRAC)==1
    graphCutoffs(data,est,cut,prefix)
  end
  
  % counterfactuals
  if(cfnsim)
    cfdata = simCF(data,est,cfg,cfnsim,prefix,true,true,USEOLDSIM);
    cfdata=mmsTable(cfdata,cfg);
    cfTables(data,est,cfdata,cfg,prefix);
    fit(data,est,cfdata,cfg,prefix);
    robustTable(data,est,cfg,cfdata);

    % eliminate cross subsidies
    %  nosubData = crossSub(data,est,cfdata,cfg,prefix);
    % interest rate stuff
    %if (~USEOLDSIM)
    %nosubData2 = crossSub2(data,est,cfdata,cfg,prefix);
    %end
  end
%  eval(['!perl createTex.pl ' num2str(data.ngroup) ' ' ...
%        prefix ' > ' prefix '/slideTables.tex']);
%  eval(['!cp Makefile ' prefix '/.']);
%  if (~exist([prefix '/notes.tex']))
    % create blank file
%    eval(['!touch ' prefix '/notes.tex']);
%  end
  %eval(['!cd ' prefix ' && make clean ps']);

end % loop over conf
