# ampl command file for step 2 estimation
load amplfunc.dll; # load library of external functions
# declare model and data, these tell the solver which problem to solve and
# what parameter values to use
model annuityLikeSP.mod; # model file
include "settings.ampl";
option solver snopt; # set the solver
                     # available choices are snopt,minos,loqo, and minos
                     # snopt seems most robust so far
option snopt_options 'timing 1 outlev=3'; # some solver options
option minos_options 'timing 1 outlev=3';
option loqo_options 'timing 1 outlev=3'; # some solver options
option minos_options 'outlev=3';

# intial values
fix muA[1] := -5.76034419918545;
fix muA[2] :=        -4.736268677499399 ;
fix muA[3] :=       -5.677953398732058 ;
fix muA[4] :=       -5.007287704451158;
fix sigA :=       0.05358837279656161;
let {gr in 1..nG,j in 1..nInt, k in 1..3} Pga[gr,j,k] := 1/3;
solve;
display Pga;
printf '' > Pga.csv;
for{gr in 1..nG} {
  printf '\n\n' >> Pga.csv;
  for{j in 1..nInt} {
    printf '%25.16g, %25.16g, %25.16g\n', Pga[gr,j,1],Pga[gr,j,2],Pga[gr,j,3] 
       >> Pga.csv;
  }
}
