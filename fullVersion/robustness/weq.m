function [res] = weq(v,wlo,whi,q,beta,vp);

  T = vp.T;
  vp.frac = 0;
  wTOL = 1.0e-8; % absolute tolerance
  
  %res = fzero(@(x) vf2(x,0,q,beta,0)-v,(whi+wlo)/2);
  %return;
  % find the weq by bisection
  vhi = vf3(whi,0,q,beta,0,vp); 
  its = 0;
  while (v>vhi && its<10) % try to gracefully recover
      its = its+1;
      warning('v(whi)<v, doubling whi');
      fprintf('%12.8g %12.8g\n',vhi,v);
      wlo = whi;
      vlo = vhi;
      whi = whi*2
      vhi = vf3(whi,0,q,beta,0,vp);
  end;
  if(its==10)
      error('bad whi');
  end;
  
  vlo = vf3(wlo,0,q,beta,0,vp);
  its = 0;
  while(vlo>v && its<10)
      its = its+1;
      warning('v(wlog)>v, halving wlo');
      whi = wlo; vhi = vlo;
      wlo = wlo*0.5;
      vlo = vf3(wlo,0,q,beta,0,vp);
  end;
  if(its==10)
      error('bad wlo');
  end;
  
  its = 0;
  w = 0.5*(whi+wlo);
  while(whi-wlo>wTOL)
    if(mod(its,2)==0 & vlo>-realmin)
        w = (v - vlo)*(whi-wlo)/(vhi-vlo)+wlo;  % trial w
        % this w should be close to the solution
    else
        % but with a concave objective (like the value function), the above
        % w will always be greater than the zero, so don't use it all the
        % time       
        w= (whi+wlo)*0.5;
    end;
    V = vf3(w,0,q,beta,0,vp);
    if(V<v) 
        vlo = V;
        wlo = w;
    elseif(V>v)
        vhi = V;
        whi = w;
    else % V==V
        whi = w;
        wlo = w;
    end;
    its = its+1;
  end;
  res = w;
