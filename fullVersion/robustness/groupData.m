%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function gData = groupData(data,g)
  gData = data;
  gData.N = sum(data.group==g);
  gi = data.group==g;
  vecs = {'g','died','male','year','quarter','cage','t','group', ...
          'lage','fage','agemin','baseage','maxAge'};
  for v=1:length(vecs)
    gData.(vecs{v}) = [];
    gData.(vecs{v}) = data.(vecs{v})(gi);
  end
  mats = {'betaX','alphaX'};
  for v=1:length(mats)
    gData.(mats{v}) = [];
    gData.(mats{v}) = data.(mats{v})(gi,:);
  end
end
