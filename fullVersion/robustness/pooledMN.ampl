# ampl command file for step 2 estimation
load amplfunc.dll; # load library of external functions
# declare model and data, these tell the solver which problem to solve and
# what parameter values to use
model annuityLikePooledMN.mod; # model file
let minSig := 1e-6;
let pMid := 0; 
data pooled.dat; # data file

let M := 2;

# initialize cutoff grid
call {c in 1..nCut,d in 1..nG} 
 setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,c,d],c+nCut*(d-1)); 

#display cutLogA, cutLogB;
option solver snopt; # set the solver
                     # available choices are snopt,minos,loqo, and minos
                     # snopt seems most robust so far
option snopt_options 'timing 1 outlev=3'; # some solver options
option minos_options 'timing 1 outlev=3';
option loqo_options 'timing 1 outlev=3'; # some solver options
option minos_options 'outlev=3';

# intial values
set sigAinit := {0.1};
set muAinit := {-5.3};
set sigBinit := {0.01,0.1};
set muBinit := {10.25};

display {t in 1..nCut,d in 1..nG} t+nCut*(d-1);

# begin file of output
printf 'pooledNormal minos results\n' > pooled.csv;
printf ' ,initial,f60,m65,f65,m60,initial,final,initial,f60,m65,f65,m60,initial,final,initial,final\n' >> pooled.csv;
printf 'loglike, muA, , , , , sigA, , muB, , , , , sigB, , corr, ,\n' >> pooled.csv;
for {c in {-9,-5,5,9}} { 
 for {sa in sigAinit} {
  for {ma in muAinit} {
   for {sb in sigBinit} {
    for {mb in muBinit} {

#param c := -1.109;
  call {t in 1..nCut,d in 1..nG} 
   setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,t,d],t+nCut*(d-1)); 

  let{m in 1..M} p[m] := 1/M;
  let sigA := sa;
  let{d in 1..nG} muA[d] := ma;
  let{m in 1..M} sigB[m] := sb;
  # initialize muB to fit b/t cutoffs at muA
  let{d in 1..nG, m in 1..M} muB[d,m] := (0.4+m/10)*(cutoff(muA[d],1+nCut*(d-1))+cutoff(muA[d],2+nCut*(d-1)));
  let{m in 1..M} corr[m] := c/10;
          
  solve;

  display _solve_time;
  display logLikelihood,lambda,muA,sigA,muB,sigB,corr;
  # printf results
  printf '%25.16g,%25.16g', 
         logLikelihood,ma >> pooled.csv;
  for{k in 1..nG} printf ',%.16g',muA[k] >> pooled.csv;
  printf ',%.16g,%.16g,%.16g', 
         sa,sigA,mb >> pooled.csv;
  for{m in 1..M} {
    for{k in 1..nG} {
      printf ',%.16g', muB[k,m] >> pooled.csv;
  }}
  printf ',%.16g', sb >> pooled.csv;
  for{m in 1..M} printf ',%.16g',sigB[m] >> pooled.csv;
  printf ',%.16g', c/10 >> pooled.csv;
  for{m in 1..M} printf ',%.16g',corr[m] >> pooled.csv;
  for{m in 1..M} printf ',%.16g',p[m] >> pooled.csv;
  printf '\n' >> pooled.csv;
}}}}}
