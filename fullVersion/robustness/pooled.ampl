# ampl command file for step 2 estimation
load amplfunc.dll; # load library of external functions
# declare model and data, these tell the solver which problem to solve and
# what parameter values to use
model annuityLikePooled.mod; # model file
let minSig := 1e-6;
let pMid := 0; 
include "settings.ampl";
#data pooled.dat; # data file

# initialize cutoff grid
call {c in 1..nCut,d in 1..nG} 
 setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,c,d],c+nCut*(d-1)); 

#display cutLogA, cutLogB;
option solver snopt; # set the solver
                     # available choices are snopt,minos,loqo, and minos
                     # snopt seems most robust so far
option snopt_options 'timing 1 outlev=3'; # some solver options
option minos_options 'timing 1 outlev=3';
option loqo_options 'timing 1 outlev=3'; # some solver options
option minos_options 'outlev=3';

# intial values
# results are admittedly sensitive to initial values
# try a large set to be confident of ours
set muAinit := {-5};
set sigAinit := {0.1};
set sigBinit := {0.1};
set muBinit := {9};

display {t in 1..nCut,d in 1..nG} t+nCut*(d-1);

# begin file of output
printf 'pooledNormal snopt results\n' > pooled.csv;
printf ' ,initial,f60,m65,f65,m60,initial,final,initial,f60,m65,f65,m60,initial,final,initial,final\n' >> pooled.csv;
printf 'loglike, muA, , , , , sigA, , muB, , , , , sigB, , corr, ,\n' >> pooled.csv;

# initial values from matlab 
include "initial.ampl";
let corr := 0;
solve;
printf '%25.16g,,%25.16g,%25.16g,%25.16g,%25.16g,,%25.16g,,%25.16g,%25.16g,%25.16g,%25.16g,,%25.16g,,%25.16g\n', 
       logLikelihood,muA[1],muA[2],muA[3],muA[4],sigA,muB[1],muB[2],muB[3],muB[4],sigB,corr >> pooled.csv;

# a larger set of initial values
for {c in {-9,-5,0,5,9}} { 
 for {sa in sigAinit} {
  for {ma in muAinit} {
   for {sb in sigBinit} {
    for {mb in muBinit} {

  call {t in 1..nCut,d in 1..nG} 
   setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,t,d],t+nCut*(d-1)); 


  let sigA := sa;
  let{d in 1..nG} muA[d] := ma;        
  let sigB := sb;
  # initialize muB to fit b/t cutoffs at muA        
  let{d in 1..nG} muB[d] := 0.5*(cutoff(muA[d],1+nCut*(d-1))+cutoff(muA[d],2+nCut*(d-1)));
  let corr := c/10;

  display logLikelihood;

  solve;

  display _solve_time;
  display logLikelihood,lambda,muA,sigA,muB,sigB,corr;
  # printf results
  printf '%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g\n', 
         logLikelihood,ma,muA[1],muA[2],muA[3],muA[4],sa,sigA,mb,muB[1],muB[2],muB[3],muB[4],sb,sigB,c/10,corr >> pooled.csv;
}}}}}
