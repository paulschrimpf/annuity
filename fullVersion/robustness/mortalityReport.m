function mortalityReport(data,est,cfg,prefix)
% create table and graphs summarizing the mortality estimates
   
  %----------------------------------------------------------------------
  % life expectancy and P(live)
  centile = [.05 .50 .95];
  % do separately for each group
  for (gr=1:data.ngroup)
    gData = groupData(data,gr);
    [elife{gr} pl{gr}] = lifeExpectancy(gData,est,centile);
    elife{gr} = elife{gr} + gData.baseage(1);
  end
    
  % write the table
  out = [prefix '/tables/elife.tex'];
  of = fopen(out,'w');
  fprintf(of,'Percentile ');
  for (gr=1:data.ngroup) 
    i = strfind(cfg.data.group(gr).name,'1992');
    name = cfg.data.group(gr).name(1:i-1);
    fprintf(of,'& %s ',name);
  end
  fprintf(of,' \\\\ \\hline \n');

  for c = 1:length(centile),
    fprintf(of,'%4g ',100*centile(c));
    for gr=1:data.ngroup
      fprintf(of,' & %8.4g ',elife{gr}(c));
    end
    fprintf(of,' \\\\ \n');
  end;
  % add institute of actuaries data
  mtable = [82.47 78.87  83.26 79.98];
  fprintf(of,'\\\\ \n From Mortality Table & %8.4g & %8.4g & %8.4g & %8.4g \\\\ \n', ...
          mtable);
  fclose(of);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [elife plive] = lifeExpectancy(data,est,centile)
% compute life expectancy and P(live<5) & P(live<10)
 
  % monte carlo integration make this easier
  logAlpha = [];
  dup = ceil(1e5/data.N);
  for i = 1:dup
    la = sampleAlphaBeta(data,est);
    logAlpha = [logAlpha; la];
  end
  if(isfield(est,'fasthazard'))
    h = est.fasthazard;
  else 
    h = 1;
  end
  alpha = exp(quantile(logAlpha,centile));
  lambda = est.lambda;
  t = (min(data.agemin)+[5 10]);
  Pobs = exp(alpha/lambda.*(1-exp(lambda*min(data.agemin).^h)));
  for j = 1:length(t)
    plive(1:length(centile),j) = 1-exp(alpha/lambda*(1-exp(lambda*t(j).^h))) ./ ... 
        Pobs;
  end
  dt = 0.1;
  t = (0.0:dt:200.0);
  elife(1:length(centile)) = sum( exp(kron(alpha'/lambda,(1 - exp(lambda*t.^h))))*dt,2);
end
  
