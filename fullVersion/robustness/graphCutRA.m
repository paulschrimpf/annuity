function graphCutoffs(data,est,cut,prefix)
% Make a graph of the cutoffs with a plot of alpha and beta on top
  
  % plot cutoffs
  % logs
  [logAlpha logBeta] = sampleAlphaBeta(data,est);  

  ngroup = data.ngroup;
  logBeta = exp(logBeta);

  cmd = 'plot(logAlpha,logBeta,''.''';
  for g=1:ngroup
    cut(g).lb = exp(cut(g).lb);
    cmd = [cmd sprintf([',cut(%d).la,cut(%d).lb(:,1),''g'',' ...
                        'cut(%d).la,cut(%d).lb(:,2),''r'''],g,g,g,g)];
  end
  cmd = [cmd ');'];
  out = [prefix '/figures/dist1.eps'];
  figure;
  hist(logAlpha);
  figure;
  hist(logBeta);
  figure;
  eval(cmd);
  axis([quantile(logAlpha,[0 1]) ...
        quantile(logBeta, [0 1]) ]);    
  ax = axis;
  %axis([-5.5 -3.5 0 16]);
  xlabel('log(\alpha)');
  ylabel('\gamma');
  print('-depsc2',out);

  cmd = '';
  for g=1:ngroup
    %cut(g).lb = exp(cut(g).lb);
    if (isempty(cmd))
      cmd =  sprintf(['plot(cut(%d).la,cut(%d).lb(:,1),''g'',' ...
                      'cut(%d).la,cut(%d).lb(:,2),''r'''],g,g,g,g);
    else
      cmd = [cmd sprintf([',cut(%d).la,cut(%d).lb(:,1),''g'',' ...
                          'cut(%d).la,cut(%d).lb(:,2),''r'''],g,g,g,g)];
    end
  end
  x = [ax(1) ax(2) ax(2) ax(1) ax(1)];
  y = [ax(3) ax(3) ax(4) ax(4) ax(3)];
  cmd = [cmd ',x,y,''k--'');'];
  figure;
  eval(cmd);
  axis([-9 -3 0 10]);
  xlabel('log(\alpha)');
  ylabel('\gamma');
  print('-depsc2',[prefix '/figures/cut.eps']);
  
  % levels
  %   out = [prefix '/figures/distLevel.eps'];
  %   figure;
  %   plot(exp(logAlpha),exp(logBeta),'.', ...
  %       exp(cut.la),exp(cut.lb(:,1)),exp(cut.la),exp(cut.lb(:,2)) );
  % %  axis([quantile(exp(logAlpha),[0.01 0.99]) ...
  % %        quantile(exp(logBeta), [0.01 0.99]) ]);    
  %   axis(exp([-5.5 -4 3 20]));
  %   xlabel('alpha');
  %   ylabel('beta');
  %   print('-depsc2',out);
  
  %----------------------------------------------------------------------
  if(getenv('DISPLAY'))
   for g=1:ngroup
     out = [prefix '/figures/cut' num2str(g) '.eps'];
     figure;
     m = data.group==g;
     la = logAlpha(m);
     lb = logBeta(m);
     plot(la,lb,'.',cut(g).la,cut(g).lb(:,1),cut(g).la,cut(g).lb(:,2));
     axis([quantile(logAlpha,[0.0 1]) ...
           quantile(logBeta, [0.0 1]) ]);    
 %     axis([-5.5 -4 3 20]);
     xlabel('log(alpha)');
     ylabel('log(beta)');
%     print('-depsc2',out);
   end
  end
  % levels
  %   out = [prefix '/figures/cutLevel.eps'];
  %   figure;
  %   plot(exp(cut.la),exp(cut.lb(:,1)),exp(cut.la),exp(cut.lb(:,2)) );
  %   axis(exp([-5.5 -4 3 20]));
  %   xlabel('alpha');
  %   ylabel('beta');
  %   print('-depsc2',out);
  
