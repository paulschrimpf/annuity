% estimate model on a grid of zeta values
clear all;
confs= {'confFemale60', ...
        'confFemale65', ...
        'confMale60', ...
        'confMale65'};

YEAR = 92;
SEPARATE = true; % separate by year
GAM = 3;
BGAM = 3;
OBJECTIVE = 'mle';
READLAMBDA = true;
FRAC = 0.2;
DIST = 'normal';

zgrid = 0.1:0.1:1;

for c = 1:length(confs) 
  for z=1:length(zgrid)
    ZETA = zgrid(z);
   
    clear annuityLike;
    clear step2Like;
    clear mortLike;
    clear transformParam;
    configName = confs{c};
    eval(['cfg = ' configName '();']);
    mortOnly = strcmp(OBJECTIVE,'mlmort'); 
    if (mortOnly)  % change config to just do mortality
      READLAMBDA = false;
      cfg.est.objective = 'mlmort';
      cfg.outprefix = [cfg.outprefix 'Mort'];
      cfg.est.opt.DerivativeCheck='off';
      %    cfg.est.opt.LargeScale='off';
    end
    cfg.est.objective = OBJECTIVE;
    cfg.est.opt.LargeScale = 'off';
    cfg.est.opt.MaxIter = 50000;
    cfg.est.opt.MaxFunEvals = 50000;
    cfg.est.opt.DerivativeCheck = 'off';
    cfg.est.disp = 50;
    
    switch(OBJECTIVE)
     case '2step'
      cfg.outprefix = [cfg.outprefix '_2step'];
     case 'mlmort'
      cfg.outprefix = [cfg.outprefix 'Mort'];
    end
    
    cfg.cut.gam = GAM;
    cfg.cut.bgam = BGAM;
    cfg.cut.fracAnnuitized = FRAC;
    cfg.est.dist = DIST;
    cfg.cut.zeta = ZETA;
    
    if (SEPARATE) 
      cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
    else
      if (YEAR~=92)
        cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
      end
    end
    if (GAM~=3)
      cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
    end
    if (BGAM~=GAM)
      cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
    end
    if (FRAC~=0.2)
      cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
    end
    if (~strcmp(DIST,'normal'))
      cfg.outprefix = [cfg.outprefix,'_' DIST];
    end
    if(ZETA~=1)
      cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
    end
    cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');
    
    % read data
    if (SEPARATE) 
      switch YEAR
       case {92,95}
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9295.txt'];
       case {90,97} 
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9097.txt']; 
       otherwise
        error(['no appropriate data set for separate and ' num2str(YEAR)]);
      end
      cfg.data.keepYear = 1900 + YEAR;
    else
      if (YEAR<100)
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_19' num2str(YEAR) ...
                         '.txt'];
      else
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_' num2str(YEAR) ...
                         '.txt'];
      end
      cfg.data.allDummies=false;
    end
    
    if (READLAMBDA)
      if(SEPARATE)
        if (YEAR<=94)
          ystr = '92';
        else
          ystr = '95';
        end
        filename = [confs{c} '_' ystr];
      else
        filename = confs{c};
      end
      load ([filename '.lambda'])
      eval(['cfg.mort.lambda = ' filename]);
    end


    %  cfg.outprefix = [cfg.outprefix '_95'];
    [data cfg] = readData(cfg); % see readData for details of data struc
    if (~strcmp(cfg.est.objective,'mlmort'))
      % load cutoffs
      cut = loadcutoffs(cfg);
      cut = cleanCut(cut,cfg,data);
      est.cut = cut;
    end
    if (isfield(cfg,'dropBadGroups') && cfg.dropBadGroups)
      [data cut cfg] = dropBadGroups(data,cut,cfg);
      est.cut = cut;
    end  
    
    est.lambda = cfg.mort.lambda;
    est.inc = cfg.mort.inc;
    est.disp = cfg.est.disp;
    est.extraMoments = cfg.est.extraMoments;
    est.dist = cfg.est.dist;
    
    if(isfield(cfg.est,'restart') && ~isempty(cfg.est.restart))
      % load restart deck
      scfg=cfg;
      eval(['load ' cfg.est.restart]);
      % restart deck consists of 'est' struc containing the complete state
      % of the previous estimates
      cfg=scfg;
      est.lambda = cfg.mort.lambda;
      est.inc = cfg.mort.inc;
      est.disp = cfg.est.disp;
      est.extraMoments = cfg.est.extraMoments;
      est.dist = cfg.est.dist;
    else
      % initialize estimates (also document it's structure)
      % current parameters
      est.lambda = cfg.mort.lambda;
      if (z==1) % default initial values
        est.parm.gAlpha = [-5; zeros(size(data.alphaX,2)-1,1)];
        b = 0; % initialize mean and std of beta to rougly fit cutoffs 
        s = 0;
        if (~mortOnly)
          for k=1:length(cut)
            [m f] = min(abs(cut(k).la-est.parm.gAlpha(1)));
            b = b + (cut(k).lb(f,1)+cut(k).lb(f,2))/2
            s = s + (cut(k).lb(f,2)-cut(k).lb(f,1));
          end
          b = b/length(cut);
          s = s/length(cut);
        end
        est.parm.gBeta = [b; zeros(size(data.betaX,2)-1,1)];
        est.parm.var = [s 0.0; 0.0 s];
        est.parm.ashape = 5;
        est.parm.bshape = 1;
        est.parm.corr = 0;
        est.int.x = []; % integration points and weights
        est.int.w = []; 
      else
        % keep values from last zeta
        % OR could update beta to match new cutoffs ...
      end
    end
    
    % initialize integration points
    switch(est.dist)
     case 'normal'
      est.int = gaussChebyshevInt(cfg.est.nint);
     case {'gamma','gamma-normal'}
      [est.int.x est.int.w] = gaussLaguerre(cfg.est.nint,0);
      est.int.n = cfg.est.nint;
    end
    
    
    % pack parameters
    switch(cfg.est.objective)
     case 'mle'
      objFunc = 'annuityLike';
      parm = transformParam(est.parm,'pack',cfg.est.objective,cfg.est.dist);
     case 'mlmort'
      objFunc = 'mortLike';
      k = length(est.parm.gAlpha);
      parm = zeros(k+2,1);
      parm(1:k) = est.parm.gAlpha;
      k = k+1;
      switch (est.dist)
       case {'normal','exponential'}
        parm(k) = log(sqrt(est.parm.var(1,1)));
        k = k+1;
       case 'gamma'
        parm(k) = log(sqrt(est.parm.var(1,1)));
        k = k+1;
        parm(k) = log(1);
        k = k+1;
      end
      parm(k) = log(est.lambda);
     case '2step'
      objFunc = 'step2Like';
      cfg2 = cfg;
      est2 = est;
      load(cfg.est.mortResults);
      cfg = cfg2;
      est.dist = cfg.est.dist;
      est.lambda = cfg.mort.lambda;
      est.inc = cfg.mort.inc;
      est.disp = cfg.est.disp;
      est.extraMoments = cfg.est.extraMoments;
      est.int = est2.int;
      est.cut = est2.cut;
      est.parm.var(2,2) = est2.parm.var(2,2);
      est.parm.gBeta = est2.parm.gBeta;
      clear cfg2 est2;
      parm = transformParam(est.parm,'pack',cfg.est.objective,est.dist);
     otherwise
      error('Unrecognized objective function\n');
    end
    bootEst = [];
%    switch(cfg.est.optmethod)
      eval(['[parm fval exit output optgrad opthess] =' ...
            'fminunc(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
%     otherwise
%      error('optimization method "%s" unrecognized.\n',cfg.est.optmethod);
%    end
    % we really want to make sure we've optimized, so restart, try
    % alternate methods, etc
    
    cfg.est.opt.LargeScale = 'off';
    % just restart the estimator
    eval(['[parm2 fval2 exit output optgrad opthess] =' ...
          'fminunc(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
    if(~isequal(parm2,parm) || fval~=fval2)
      fprintf('WARNING: optimization improved by %g by restart\n',fval- ...
              fval2);
    end
    parm = parm2; fval = fval2;
    % do a simplex search
    eval(['[parm fval exit output] = ' ...
          'fminsearch(@(x) ' objFunc '(x,est,data),parm,' ...
                    ' cfg.est.opt)']);
    if(~isequal(parm2,parm) || fval~=fval2)
      fprintf('WARNING: optimization improved by %g by simplex\n',fval- ...
              fval2);
      parm = parm2; fval = fval2;
    end
    eval(['[parm2 fval2 exit output optgrad opthess] =' ...
          'fminunc(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
    fprintf('Second Gradient search changed likelihood by %g\n',fval- ...
            fval2);
    parm = parm2; fval = fval2;

    eval(['[f g h gi] = ' objFunc '(parm,est,data);']);
    likeGrid(z,c) = f;
    % unpack parameters
    if (~strcmp(cfg.est.objective,'mlmort'))
      [est.parm der] = transformParam(parm,'unpack',cfg.est.objective,est.dist);
      % standard errors
      if(size(gi,2)==data.N)
        gi = gi' - ones(data.N,1)*mean(gi');
      else 
        gi = gi - ones(data.N,1)*mean(gi);
      end
      v = inv(gi'*gi)/data.N;
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      est.parm.std.var = zeros(2,2);
      switch(est.dist)
       case 'normal'
        jac = reshape(der.var(1,1,:),1,length(parm));
        std.var(1,1) = sqrt(jac*v*jac');
        jac = reshape(der.var(1,2,:),1,length(parm));
        std.var(1,2) = sqrt(jac*v*jac');
        jac = reshape(der.var(2,2,:),1,length(parm));
        std.var(2,2) = sqrt(jac*v*jac');
        std.corr = sqrt(der.corr*v*der.corr');
       case 'gamma'
        jac = reshape(der.ashape,1,length(parm));
        std.ashape = sqrt(jac*v*jac');
        jac = reshape(der.bshape,1,length(parm));
        std.bshape = sqrt(jac*v*jac');
        std.corr = sqrt(der.corr*v*der.corr');
       case 'gamma-normal'
        jac = reshape(der.ashape,1,length(parm));
        std.ashape = sqrt(jac*v*jac');
        std.corr = sqrt(der.corr*v*der.corr');
        jac = reshape(der.var(2,2,:),1,length(parm));
        std.var(2,2) = sqrt(jac*v*jac');
      end     
      est.parm.std = std;          
    else
      % unpack parameters
      k = length(est.parm.gAlpha);
      est.parm.gAlpha = parm(1:k);
      k = k+1;
      stdalpha = exp(parm(k));
      est.parm.var(1,1) = stdalpha^2;
      k = k+1;
      est.lambda = exp(parm(k));
      est.parm.std.gAlpha = zeros(size(est.parm.gAlpha));
      est.parm.std.var = zeros(size(est.parm.var));
      est.parm.var(1,2) = 0;
      est.parm.var(2,2) = 0;
      est.parm.var(2,1) = 0;  
      est.parm.std.gBeta = zeros(size(est.parm.gBeta));  
      est.parm.gBeta = zeros(size(est.parm.gBeta));
      est.parm.std.corr = 0;
      est.parm.corr = 0;
    end

    fprintf('parameter values:\n');
    fprintf(' lambda         = %8.6g\n',est.lambda);
    if (~strcmp(cfg.est.objective,'mlmort'))
      est.parm = transformParam(parm,'unpack',cfg.est.objective,est.dist);
      est.parm.std = std;
      pgmm = transformParam(est.parm,'pack','gmm',est.dist);
      for k=1:length(est.parm.gAlpha)
        fprintf(' gamma alpha %d  = %8.6g (%8.6g)\n',k,est.parm.gAlpha(k), ...
                est.parm.std.gAlpha(k));
      end
      for k=1:length(est.parm.gBeta)
        fprintf(' gamma beta %d  = %8.6g (%8.6g)\n',k,est.parm.gBeta(k), ...
                est.parm.std.gBeta(k));
      end
      switch(est.dist)
       case 'normal'
        fprintf(' var log alpha  = %8.6g (%8.6g)\n',est.parm.var(1,1), ...
                est.parm.std.var(1,1));
        fprintf(' var log beta   = %8.6g (%8.6g)\n',est.parm.var(2,2), ...
                est.parm.std.var(2,2));
        fprintf(' cov log a,b    = %8.6g (%8.6g)\n',est.parm.var(1,2), ...
                est.parm.std.var(1,2));
        fprintf(' corr log a,b   = %8.6g (%8.6g)\n',est.parm.corr, ...
                est.parm.std.corr);
       case 'gamma'
        fprintf(' alpha shape    = %8.6g (%8.6g)\n',est.parm.ashape, ...
                est.parm.std.ashape);
        fprintf(' beta shape     = %8.6g (%8.6g)\n',est.parm.bshape, ...
                est.parm.std.bshape);
        fprintf(' coeff          = %8.6g (%8.6g)\n',est.parm.corr, ...
                est.parm.std.corr);
       case 'gamma-normal'
        fprintf(' alpha shape    = %8.6g (%8.6g)\n',est.parm.ashape, ...
                est.parm.std.ashape);
        fprintf(' var log beta   = %8.6g (%8.6g)\n',est.parm.var(2,2), ...
                est.parm.std.var(2,2));
        fprintf(' coeff          = %8.6g (%8.6g)\n',est.parm.corr, ...
                est.parm.std.corr);
      end
      clear annuityGMM;
      [f g h gi moments] = annuityGMM(pgmm,est,data);
      moments
    end
    if(exist('optgrad'))
      fprintf('gradient = %.8g  %.8g  %.8g  %.8g\n',optgrad);
      fprintf('\n');
    end

    % save results
    eval(['save ',cfg.outprefix,'Results est cfg bootEst']);
  end % loop over zeta
  
  fprintf([' ############################################################' ...
           ' ##########\n']);
  fprintf('Finished with %d of %d values for zeta for config %\n', ...
          z,length(zgrid),c,length(confs));
  fprintf([' ############################################################' ...
           ' ##########\n']);
end % loop over confs

save zGrid likeGrid zgrid;
