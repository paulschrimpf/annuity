% array of robustness specs
% re-runs counterfactuals and calculates percent vol annuitize for the
% cells that do not have it now

if (exist('wd'))
  cd(wd);
end
clear;

base.year = 92;
base.gam = 3;
base.bgam = 3;
base.frac = 0.2;
base.zeta = 1;
base.dist = 'normal';
base.withX = false;
base.ext = false;
base.r = 0;
base.pub = 0;
base.lambda = 0.1102206337978725; % pooled value
base.dumbp = 0;
base.nonh.w0 = 100;
base.nonh.p = 1;
base.fasthazard = 1;
base.zeta = 1;
base.gammaHetero.beta = 0;
base.alphaUncertainty = 0;
base.alphaBias = 1;
base.twodh = [];

% robustness specs
robust{1} = base;
robust{1}.gam = 5;
robust{1}.bgam = 5;

robust{2} = base;
robust{2}.gam = 1.5;
robust{2}.bgam = 1.5;

robust{3} = base;
robust{3}.bgam = 1.5;

robust{4} = base;
robust{4}.bgam = 5;

robust{5} = base;
robust{5}.frac = 0.1;

robust{6} = base;
robust{6}.frac=.3;

robust{7} = base;
robust{7}.year = 90;

robust{8} = base;
robust{8}.dist = 'gamma-normal';

robust{9} = base;
robust{9}.dist = 'gamma';

robust{10} = base;
robust{10}.r = 0.06;

robust{11} = base;
robust{11}.pub = 0.5;

robust{12} = base;
robust{12}.withX = true;

robust{13} = base;
robust{13}.ext = true;

robust{14} = base;
robust{14}.r = 0.05;

robust{15} = base;
robust{15}.dumbp = 0.5;

robust{16} = base;
robust{16}.nonh.w0 = [1750 8950 24900];
robust{16}.nonh.p = [.375 .25 .375];
robust{16}.bgam = 1.5;

robust{17} = base;
robust{17}.fasthazard = 2;
robust{17}.lambda = 0.001523088419505936;

robust{18} = base;
robust{18}.fasthazard = 3;
robust{18}.lambda = 3.578765549617122e-05;

robust{19} = base;
robust{19}.frac=.5;

robust{20} = base;
robust{20}.frac=.7;

robust{21} = base;
robust{21}.zeta=1/(1.05);

robust{22} = base;
robust{22}.zeta=1/(1.1);

robust{23} = base;
robust{23}.zeta=1/2;

robust{24} = base;
robust{24}.fasthazard = 1.5;
robust{24}.lambda = 0.01340732327124827;

% consumption ra hetero, w0 = 100
robust{25} = base;
robust{25}.gammaHetero.beta = exp([9.769910335744829 ...
                    9.425006169276971 ...
                    9.647593475141907 ...
                    9.868793665937353 ...
                   ]);
robust{25}.gammaHetero.cons = 1;
robust{25}.gammaHetero.beq = 0;
robust{25}.gammaHetero.w0 = 100;

% bequest ra hetero, w0 = 100
robust{26} = base;
robust{26}.gammaHetero.beta = exp(9.7);
robust{26}.gammaHetero.cons = 0;
robust{26}.gammaHetero.beq = 1;
robust{26}.gammaHetero.w0 = 100;

% consumption ra hetero, w0 = mean = 12231
robust{27} = base;
robust{27}.gammaHetero.beta = exp([ -1.0716   -1.7828   -3.2859   -3.1478]);
   % mean beta 
robust{27}.gammaHetero.cons = 1;
robust{27}.gammaHetero.beq = 0;
robust{27}.gammaHetero.w0 = 12231;

% consumption ra hetero, w0 = median = 8950
robust{28} = base
robust{28}.gammaHetero.beta = exp([-2.4891   -3.2092   -2.1856   -3.9401]);
% beta at median
robust{28}.gammaHetero.cons = 1;
robust{28}.gammaHetero.beq = 0;
robust{28}.gammaHetero.w0 = 8950;

% alpha uncertainty (std deviation of uncertainty about alpha)
siga = 0.054;
robust{29} = base;
robust{29}.alphaUncertainty = 0.2*siga;
robust{30} = base;
robust{30}.alphaUncertainty = 0.5*siga;
robust{31} = base;
robust{31}.alphaUncertainty = siga;
robust{32} = base;
robust{32}.alphaUncertainty = 2*siga;

% alpha bias: alphaHat = mu + alphaBias*(log alpha - mu)
robust(33:36) = {base};
robust{33}.alphaBias = 0.5;
robust{34}.alphaBias = 0.9;
robust{35}.alphaBias = 1.1;
robust{36}.alphaBias = 2;

% two dimensions of heterogeneity
robust{37} = base; 
robust{37}.twodh.gamma = [1.5 3 4.5]; 

% fraction annuitized correlated with preferences
robust{38} = base;
robust{38}.frac = [0.1 0.2 0.4];
robust{38}.pfrac = [0.5 0.25 0.25];

%robust{27} = base;
%robust{27}.dist = 'mixed-normal';

robust{39} = base;

SEPARATE = true; % separate by year
SIMULATE = true;
USEOLDSIM = true;
OBJECTIVE = 'mle';
READLAMBDA = true;
ESTIMATE = false;
DEGREE = 1;
EXTRALAGRID = 0;
NINT = 24;
PENALTY = -1;

path(path,'../../src');
path(path,'../../src/cutoffs');
path(path,'../../bootstrap');
path(path,'../../report');

of = fopen('mms.csv','w');
fprintf(of,'%20s , %10s , %10s , %10s , %10s , %10s , %10s\n',...
        '','Mean','10%','25%','50%','75%','90%');

for rb=1:39 %rb=31:32
  clear annuityLike;
  clear step2Like;
  clear mortLike;
  clear transformParam;
  
  
  GAM = robust{rb}.gam;
  BGAM = robust{rb}.bgam;
  FRAC = robust{rb}.frac;
  DIST = robust{rb}.dist;
  ZETA = robust{rb}.zeta;
  YEAR = robust{rb}.year;
  EXTERNAL = robust{rb}.ext;
  PUBLICANNUITY = robust{rb}.pub;
  INCLUDEX = robust{rb}.withX;
  RATE = robust{rb}.r;
  LAMBDA = robust{rb}.lambda
  DUMB.p = robust{rb}.dumbp;
  DUMB.estP = 0;
  NONH = robust{rb}.nonh;
  FASTHAZARD = robust{rb}.fasthazard;
  ZETA = robust{rb}.zeta;
  GAMMAHETERO = robust{rb}.gammaHetero;  
  ALPHAUN = robust{rb}.alphaUncertainty;
  ALPHABI = robust{rb}.alphaBias;
  TWODH = robust{rb}.twodh;
  
  configName = 'confPooled';

  %if (DIST ~= 'normal') 
  %  continue;
  %end

  eval(['cfg = ' configName '();']);
  mortOnly = strcmp(OBJECTIVE,'mlmort'); 
  if (mortOnly)  % change config to just do mortality
    READLAMBDA = false;
  end
  cfg.est.objective = OBJECTIVE;

  switch(OBJECTIVE)
   case '2step'
    cfg.outprefix = [cfg.outprefix '_2step'];
   case 'mlmort'
    cfg.outprefix = [cfg.outprefix 'Mort'];
  end

  cfg.cut.gam = GAM;
  cfg.cut.bgam = BGAM;
  cfg.cut.fracAnnuitized = FRAC;
  if (numel(FRAC)>1)
    cfg.cut.pfrac = robust{rb}.pfrac;
  else
    cfg.cut.pfrac = 1;
  end
  cfg.est.dist = DIST;
  cfg.cut.zeta = ZETA;
  cfg.mort.fasthazard = FASTHAZARD;
  cfg.cut.fasthazard = FASTHAZARD;
  cfg.cut.public = PUBLICANNUITY;
  cfg.cut.alphaUncertainty = ALPHAUN;
  cfg.cut.alphaBias = ALPHABI;
  cfg.est.dumb = DUMB;
  
  if (SEPARATE) 
    cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
  else
    if (YEAR~=92)
      cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
    end
  end
  if (GAM~=3)
    cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
  end
  if (BGAM~=GAM)
    cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
  end
  if (numel(FRAC)>1) 
    cfg.outprefix = [cfg.outprefix '_varfrac'];
  elseif (FRAC~=0.2)
    cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
  end
  if (~strcmp(DIST,'normal'))
    cfg.outprefix = [cfg.outprefix,'_' DIST];
  end
  if(ZETA~=1)
    cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
  end
  if(INCLUDEX)
    cfg.outprefix = [cfg.outprefix, '_X'];
    cfg.data.lalphaX = [-1 5 7];
    cfg.data.alphaXName(2).s = 'Premium';
    cfg.data.alphaXName(3).s = 'Percqua';

    cfg.data.lbetaX = [-1 5 7];
    cfg.data.betaXName(2).s = 'Premium';
    cfg.data.betaXName(3).s = 'Percqua';
  end
  if(FASTHAZARD~=1)
    cfg.outprefix = [cfg.outprefix, '_t' num2str(FASTHAZARD)];
    if (FASTHAZARD==2)
      cfg.cut.la = (-12:0.1:-2)';
    elseif (FASTHAZARD==3)
      cfg.cut.la = (-15:0.1:-5)';
    end
  end
  if(EXTERNAL)
    cfg.outprefix = [cfg.outprefix '_ext'];
    cfg.data.okay = 15;
    cfg.data.okeep = 1;
  end
  if(PUBLICANNUITY)
    cfg.outprefix = [cfg.outprefix '_pub' num2str(PUBLICANNUITY*100)];
  end
  if(exist('RATE','var') && RATE>0)
    cfg.outprefix = [cfg.outprefix '_r' num2str(RATE*100)];
    cfg.cut.rho = 1/(1+RATE);
    cfg.cut.delta = 1/(1+RATE);
    cfg.data = rmfield(cfg.data,'lrate');
  end
  if (DUMB.p>0 || DUMB.estP)
    if (DUMB.estP)
      cfg.outprefix = [cfg.outprefix '_dumbEst'];
    else
      cfg.outprefix = [cfg.outprefix '_dumb' num2str(DUMB.p*100)];
    end
  end
  if (exist('NONH','var') && length(NONH.w0)>1) 
    cfg.outprefix = [cfg.outprefix '_nonh'];
    cfg.cut.w0 = NONH.w0;
    cfg.cut.wp = NONH.p;
  elseif numel(FRAC)>1
    cfg.cut.wp = robust{rb}.pfrac;
  else
    cfg.cut.wp = 1;
  end
  if (exist('GAMMAHETERO','var') && any(GAMMAHETERO.beta>0))
    cfg.cut.raHetero = GAMMAHETERO;
    cfg.cut.file = 'cutoffs/gcut';
    if (GAMMAHETERO.cons && ~GAMMAHETERO.beq);
      cfg.outprefix = [cfg.outprefix '_raH'];
    elseif (GAMMAHETERO.cons && GAMMAHETERO.beq)
      cfg.outprefix = [cfg.outprefix '_raH_both'];
    elseif (~GAMMAHETERO.cons && GAMMAHETERO.beq)
      cfg.outprefix = [cfg.outprefix '_raH_beq'];
    end
    cfg.cut.w0 = GAMMAHETERO.w0;
    if (GAMMAHETERO.w0 ~= 100)
      cfg.outprefix = [cfg.outprefix '_w' num2str(GAMMAHETERO.w0)];
    end
  end
  if (exist('ALPHAUN','var') && ALPHAUN>0)
    cfg.outprefix = [cfg.outprefix '_aUnc' num2str(ALPHAUN)];
  end
  if (ALPHABI ~= 1)
    cfg.outprefix = [cfg.outprefix '_aBias' num2str(ALPHABI)];
  end
  if isstruct(TWODH) 
    cfg.outprefix = [cfg.outprefix '_twodh'];
    cfg.cut.twodh = TWODH;
  end
  
  cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');

  if (exist('LAMBDA','var'))
    %cfg.outprefix = [cfg.outprefix '_lambda' num2str(LAMBDA)];    
    cfg.mort.lambda=LAMBDA;
    cfg.cut.lambda = cfg.mort.lambda;
  end

  % read data
  if (SEPARATE) 
    switch YEAR
     case {92,95}
      cfg.data.name = ['../../../data/newCompB2_9295.txt'];
     case {90,97} 
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9097.txt']; 
     otherwise
      error(['no appropriate data set for separate and ' num2str(YEAR)]);
    end
      cfg.data.keepYear = 1900 + YEAR;
  else
    if (YEAR<100)
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_19' num2str(YEAR) ...
                       '.txt'];
    else
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_' num2str(YEAR) ...
                       '.txt'];
    end
    cfg.data.allDummies=false;
  end
  
  % load data
  [data cfg] = readData(cfg); % see readData for details of data struc
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  i = findstr('/',cfg.outprefix);
  prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
  
  datafile = [prefix '/cfdata'];
  try 
    load(datafile);
  catch
    warning('%s does not exist.  Skipping.',datafile)
    continue;
  end
  cfdata=mmsTable(cfdata,cfg);    

  % print results
  fprintf(of,'%20s',prefix);
  fprintf(of,' , %10.4g',[mean(cfdata.s.mms) ...
                      quantile(cfdata.s.mms,[.1 .25 .5 .75 .9])]);
  fprintf(of,'\n');

end % loop over conf
fclose(of);
