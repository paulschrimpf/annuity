
str = {'60 f','60 m','65 f','65 m'}
for g=1:4
    figure;
    %cut(g).lb_orig = log(cut(g).lb_orig);
    lb = exp(cut(g).lb_orig);
    plot(cut(g).la,lb(:,1),'r',cut(g).la,lb(:,2),'b', ...
        cut(g).la,lb(:,3),'k')
    %plot(cut(g).la,cut(g).lb{1}(:,1),'r',cut(g).la,cut(g).lb{1}(:,2),'r'
    %...
    %    cut(g).la,cut(g).lb{2}(:,1),'b--',cut(g).la,cut(g).lb{2}(:,2),'b--', ...
    %    cut(g).la,cut(g).lb{3}(:,1),'k:',cut(g).la,cut(g).lb{3}(:,2),'k:')
    legend('0-5','5-10','0-10')
    title(str{g});
    print('-depsc2','cut.eps');
    
    for c=1:1
        figure;
        ma = 1:length(cut(g).grid.la);
        mg = 1:length(cut(g).grid.g);
        ga = cut(g).grid.g(mg);
        la = cut(g).grid.la(ma);
        [mv ch] = max(cut(g).grid.val(:,ma,mg));
        v = squeeze(cut(g).grid.val(c,ma,mg)) - ...
            squeeze(cut(g).grid.val(c+1,ma,mg));
        ch = squeeze(ch);
        mesh(ga,la,ch);
        title(sprintf('g = %d',g));
    end
end 


