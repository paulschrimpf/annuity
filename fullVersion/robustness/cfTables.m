function cfTables(data,est,cfdata,cfg,prefix)
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %----------------------------------------------------------------------
  if (data.ngroup>5)
    ngroup = 0;
    dg = zeros(data.N,1);
    for s = 0:1
      for cage = min(data.cage):max(data.cage)
        m = data.male==s & data.cage==cage;
        if (sum(m)>0)
          ngroup = ngroup+1;
          dg(m) = ngroup;
        end
      end
    end
  else
    dg = data.group;
    ngroup = data.ngroup;
    for g=1:ngroup
      gname(g).s = cfg.data.group(g).name;
    end
  end
  
  if (cfdata.ngroup>5)
    ngroup = 0;
    cfdg = zeros(cfdata.N,1);
    for s = 0:1
      for cage = min(cfdata.cage):max(cfdata.cage)
        m = cfdata.male==s & cfdata.cage==cage;
        if (sum(m)>0)
          ngroup = ngroup+1;
          cfdg(m) = ngroup;
          if (s)
            gname(ngroup).s = [num2str(cage) ' Male'];
          else
            gname(ngroup).s = [num2str(cage) ' Female'];
          end
        end
      end
    end
  else
    cfdg = cfdata.group;
    ngroup = cfdata.ngroup;
    for g=1:ngroup
      gname(g).s = cfg.data.group(g).name;
    end
  end
  % create datasets for each group
  for g=1:ngroup
    m = cfdg ==g;
    gdta(g) = cfdata;
    gdta(g).N = sum(m);
    gdta(g).fage = cfdata.fage(m);
    gdta(g).lage = cfdata.lage(m);
    gdta(g).died = cfdata.died(m);
    gdta(g).g = cfdata.g(m);
    gdta(g).maxAge = cfdata.maxAge(m);
    gdta(g).alphaX = cfdata.alphaX(m,:);
    gdta(g).betaX = cfdata.betaX(m,:);
    gdta(g).group = cfdata.group(m);
    gdta(g).beta = cfdata.beta(m);
    gdta(g).alpha = cfdata.alpha(m);
    gdta(g).val = cfdata.val(m,:);
    gdta(g).weq = cfdata.weq(m,:);
    gdta(g).epay = cfdata.epay(m,:);
    gdta(g).m.val = cfdata.m.val(m,:);
    gdta(g).m.weq = cfdata.m.weq(m,:);
    gdta(g).m.epay = cfdata.m.epay(m,:);
    gdta(g).s.val = cfdata.s.val(m,:);
    gdta(g).s.weq = cfdata.s.weq(m,:);
    gdta(g).s.epay = cfdata.s.epay(m,:);
    gdta(g).s.g = cfdata.s.g(m);

    m = dg==g;
    rgdta(g) = data;
    rgdta(g).N = sum(m);
    rgdta(g).fage = data.fage(m);
    rgdta(g).lage = data.lage(m);
    rgdta(g).died = data.died(m);
    rgdta(g).g = data.g(m);
    rgdta(g).maxAge = data.maxAge(m);
    rgdta(g).alphaX = data.alphaX(m,:);
    rgdta(g).betaX = data.betaX(m,:);
    rgdta(g).group = data.group(m);
  end

  % print some tables
  [str table] = condMoments(cfdata,data);
  filename = [prefix '/tables/condmoments.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Guarantee Choice} \\\\ \n');
  fprintf(of,' & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.4g & %.4g & %.4g & %.4g \\\\ \n',str(r).s,table(r,:));
  end
  if(ngroup>1)
    for g=1:ngroup
      [str table] = condMoments(gdta(g),rgdta(g));
      fprintf(of,'\\hline \\multicolumn{5}{c}{%s} \\\\ \\hline \n',gname(g).s);
      for r = 1:length(str)
        fprintf(of,'%s & %.4g & %.4g & %.4g & %.4g \\\\ \n',str(r).s,table(r,:));
      end
    end
  end
  fclose(of);
  
  % symmetric info
  clear str table;
  [str table] = symInfo(cfdata);
  filename = [prefix '/tables/symmetric.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Equilibrium Guarantee Choice} \\\\ \n');
  fprintf(of,' & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.4g & %.4g & %.4g & %.4g \\\\ \n',str(r).s,table(r,:));
  end
  fclose(of);
  
  % mandates
  clear str table;
  [str table] = mandate(cfdata);
  filename = [prefix '/tables/mandate.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Equilibrium Guarantee Choice} \\\\ \n');
  fprintf(of,'Mandated & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.4g & %.4g & %.4g & %.4g \\\\ \n',str(r).s,table(r,:));
  end
  fclose(of);
  
  % summary of wealth equivalents
  clear str table;
  [str table] = weqSum(cfdata);
  filename = [prefix '/tables/weqSummary.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Equilibrium Guarantee Choice} \\\\ \n');
  fprintf(of,' & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
  end
  if(ngroup>1)
    for g=1:ngroup
      [str table] = weqSum(gdta(g));
      fprintf(of,'\\hline \\multicolumn{5}{c}{%s} \\\\ \\hline \n',gname(g).s);
      for r = 1:length(str)
        fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
      end
    end
  end  
  fclose(of);

  % voluntary annuitization
  clear str table;
  [str table] = volAnn(cfdata);
  filename = [prefix '/tables/volAnn.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Equilibrium Guarantee Choice} \\\\ \n');
  fprintf(of,' & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
  end
  if(ngroup>1)
    for g=1:ngroup
      [str table] = weqSum(gdta(g));
      fprintf(of,'\\hline \\multicolumn{5}{c}{%s} \\\\ \\hline \n',gname(g).s);
      for r = 1:length(str)
        fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
      end
    end
  end  
  fclose(of);
   
  % expected payments
  clear str table;
  [str table] = epay(cfdata);
  filename = [prefix '/tables/epay.tex'];
  of = fopen(filename,'w');
  fprintf(of,' & \\multicolumn{4}{c}{Equilibrium Guarantee Choice} \\\\ \n');
  fprintf(of,' & All & 0 & 5 & 10 \\\\ \n');
  for r = 1:length(str)
    fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
  end
  if(ngroup>1)
    for g=1:ngroup
      [str table] = epay(gdta(g));
      fprintf(of,'\\hline \\multicolumn{5}{c}{%s} \\\\ \\hline \n',gname(g).s);
      for r = 1:length(str)
        fprintf(of,'%s & %.5g & %.5g & %.5g & %.5g \\\\ \n',str(r).s,table(r,:));
      end
    end
  end  
  fprintf(of,['\\hline \\multicolumn{5}{l}{Interest rate such that company' ...
              ' breaks even = %.5g} \\\\ \n'],evenInterest(cfdata,cfg));
  fclose(of);
end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [str table] = condMoments(cfdata,data)
  oc = cfdata.oc;
  % Conditional moments
  str(1).s = 'Obs. Proportion';
  table(1,:) = [1 mean(data.g==0) mean(data.g==5) mean(data.g==10)];
  str(2).s = 'Est. Proportion';
  table(2,:) = [1 mean(cfdata.g==0) mean(cfdata.g==5) mean(cfdata.g==10)];
  str(3).s = 'Weatlh Equiv.'; 
  ow = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    ow(i) = cfdata.weq(i,oc(i));
  end
  table(3,:) = [mean(ow) mean(cfdata.weq(cfdata.g==0,1)) ...
                mean(cfdata.weq(cfdata.g==5,2)) ...
                mean(cfdata.weq(cfdata.g==10,3))];
  str(4).s = ['$E\left(\frac{\log(\alpha) - \mu_\alpha}{\sigma_\alpha} |' ...
              ' g \right)$'];
  sla = log(cfdata.alpha);
  sla = (sla - mean(sla))/std(sla);
  slb = log(cfdata.beta);
  slb = (slb - mean(slb))/std(slb);
  table(4,:) = [0 mean(sla(cfdata.g==0)) ...
                mean(sla(cfdata.g==5)) ...
                mean(sla(cfdata.g==10))];
  str(5).s = ['$E\left(\frac{\log(\beta) - \mu_\beta}{\sigma_\beta} |' ...
              ' g \right)$'];
  table(5,:) = [0 mean(slb(cfdata.g==0)) ...
                mean(slb(cfdata.g==5)) ...
                mean(slb(cfdata.g==10))];
end
%----------------------------------------------------------------------


%----------------------------------------------------------------------
function [str table] = symInfo(cfdata)
  soc = cfdata.s.oc;
  clear str;
  str(1).s = 'Wealth Equiv.';
  sow = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    sow(i) = cfdata.s.weq(i,soc(i));
  end
  table(1,:) = [mean(sow) mean(sow(cfdata.g==0)) ...
                mean(sow(cfdata.g==5)) ...
                mean(sow(cfdata.g==10))];
  str(2).s = 'Choose 0';
  table(2,:) = [mean(cfdata.s.g==0) mean(cfdata.s.g(cfdata.g==0)==0) ...
                mean(cfdata.s.g(cfdata.g==5)==0) ...
                mean(cfdata.s.g(cfdata.g==10)==0) ];
  str(3).s = 'Choose 5';
  table(3,:) = [mean(cfdata.s.g==5) mean(cfdata.s.g(cfdata.g==0)==5) ...
                mean(cfdata.s.g(cfdata.g==5)==5) ...
                mean(cfdata.s.g(cfdata.g==10)==5) ];
  str(4).s = 'Choose 10';
  table(4,:) = [mean(cfdata.s.g==10) mean(cfdata.s.g(cfdata.g==0)==10) ...
                mean(cfdata.s.g(cfdata.g==5)==10) ...
                mean(cfdata.s.g(cfdata.g==10)==10) ];
end
%----------------------------------------------------------------------

%----------------------------------------------------------------------
function [str table] = mandate(cfdata)
  str(1).s = '0';
  table(1,:) = [mean(cfdata.m.weq(:,1)) ...
                mean(cfdata.m.weq(cfdata.g==0,1)) ...
                mean(cfdata.m.weq(cfdata.g==5,1)) ...
                mean(cfdata.m.weq(cfdata.g==10,1)) ];
  str(2).s = '5';
  table(2,:) = [mean(cfdata.m.weq(:,2)) ...
                mean(cfdata.m.weq(cfdata.g==0,2)) ...
                mean(cfdata.m.weq(cfdata.g==5,2)) ...
                mean(cfdata.m.weq(cfdata.g==10,2)) ];
  str(3).s = '10';
  table(3,:) = [mean(cfdata.m.weq(:,3)) ...
                mean(cfdata.m.weq(cfdata.g==0,3)) ...
                mean(cfdata.m.weq(cfdata.g==5,3)) ...
                mean(cfdata.m.weq(cfdata.g==10,3)) ];
end
%----------------------------------------------------------------------

%----------------------------------------------------------------------
function [str table] = weqSum(cfdata)
  oc = cfdata.oc;
  ow = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    ow(i) = cfdata.weq(i,oc(i));
    op(i) = cfdata.epay(i,oc(i));
  end
  mepay = mean(op);
  soc = cfdata.s.oc;
  sow = zeros(cfdata.N,1);
  mR = sow;
  for i = 1:cfdata.N
    sow(i) = cfdata.s.weq(i,soc(i));
    mR(i) = cfdata.maxRisk(cfdata.group(i));
  end  
  k = 1;
  str(k).s = 'Maximal Money at Risk';
  table(k,:) = ones(1,4)*mean(mR);
  k = k+1;
  str(k).s = 'Current (asymm. info)';
  table(k,:) = [mean(ow) mean(cfdata.weq(cfdata.g==0,1)) ...
                mean(cfdata.weq(cfdata.g==5,2)) ...
                mean(cfdata.weq(cfdata.g==10,3))];
  k = k+1;
  str(k).s = 'Symm. Info.';
  table(k,:) = [mean(sow) mean(sow(cfdata.g==0)) ...
                mean(sow(cfdata.g==5)) ...
                mean(sow(cfdata.g==10))];
  k = k+1;
  str(k).s = 'Relative Difference';
  ti = -([ow cfdata.weq] - [sow sow sow sow])./(mR*ones(1,4));
  table(k,:) = [mean(ti(:,1)) mean(ti(cfdata.g==0,2)) ...
                mean(ti(cfdata.g==5,3)) ...
                mean(ti(cfdata.g==10,4))];
  k = k+1;

  str(k).s = 'Mandated 0';
  table(k,:) = [mean(cfdata.m.weq(:,1)) ...
                mean(cfdata.m.weq(cfdata.g==0,1)) ...
                mean(cfdata.m.weq(cfdata.g==5,1)) ...
                mean(cfdata.m.weq(cfdata.g==10,1)) ];
  k = k+1;
  str(k).s = 'Relative Difference';
  ti = -([ow cfdata.weq] - cfdata.m.weq(:,1)*ones(1,4))./(mR*ones(1,4));
  table(k,:) = [mean(ti(:,1)) mean(ti(cfdata.g==0,2)) ...
                mean(ti(cfdata.g==5,3)) ...
                mean(ti(cfdata.g==10,4))];
  k = k+1;
  str(k).s = 'Mandated 5';
  table(k,:) = [mean(cfdata.m.weq(:,2)) ...
                mean(cfdata.m.weq(cfdata.g==0,2)) ...
                mean(cfdata.m.weq(cfdata.g==5,2)) ...
                mean(cfdata.m.weq(cfdata.g==10,2)) ];
  k = k+1;
  str(k).s = 'Relative Difference';
  ti = -([ow cfdata.weq] - cfdata.m.weq(:,2)*ones(1,4))./(mR*ones(1,4));
  table(k,:) = [mean(ti(:,1)) mean(ti(cfdata.g==0,2)) ...
                mean(ti(cfdata.g==5,3)) ...
                mean(ti(cfdata.g==10,4))];
  k = k+1;
  str(k).s = 'Mandated 10';
  table(k,:) = [mean(cfdata.m.weq(:,3)) ...
                mean(cfdata.m.weq(cfdata.g==0,3)) ...
                mean(cfdata.m.weq(cfdata.g==5,3)) ...
                mean(cfdata.m.weq(cfdata.g==10,3)) ];
  k = k+1;
  str(k).s = 'Relative Difference';
  ti = -([ow cfdata.weq] - cfdata.m.weq(:,3)*ones(1,4))./(mR*ones(1,4));
  table(k,:) = [mean(ti(:,1)) mean(ti(cfdata.g==0,2)) ...
                mean(ti(cfdata.g==5,3)) ...
                mean(ti(cfdata.g==10,4))];
  k = k+1;
end
%----------------------------------------------------------------------

%----------------------------------------------------------------------
function [str table] = volAnn(cfdata)
  oc = cfdata.oc;
  ow = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    ow(i) = cfdata.weq(i,oc(i));
    op(i) = cfdata.epay(i,oc(i));
  end
  k = 1;
  str(k).s = 'Current (asymm. info)';
  table(k,:) = [mean(ow) mean(cfdata.weq(cfdata.g==0,1)) ...
                mean(cfdata.weq(cfdata.g==5,2)) ...
                mean(cfdata.weq(cfdata.g==10,3))];
  k = k+1;
  str(k).s = '\% Vol. Annuitize';
  table(k,:) = [mean(ow>100) mean(cfdata.weq(cfdata.g==0,1)>100) ...
                mean(cfdata.weq(cfdata.g==5,2)>100) ...
                mean(cfdata.weq(cfdata.g==10,3)>100)];
  k = k+1;
end
%----------------------------------------------------------------------

%----------------------------------------------------------------------
function [str table] = epay(cfdata)
  str(1).s = 'Current (asymm. info)';
  oc = cfdata.oc;
  soc = cfdata.s.oc;
  op = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    op(i) = cfdata.epay(i,oc(i));
  end
  table(1,:) = [mean(op) mean(cfdata.epay(cfdata.g==0,1)) ...
                mean(cfdata.epay(cfdata.g==5,2)) ...
                mean(cfdata.epay(cfdata.g==10,3))];
  str(2).s = 'Symm. Info.';
  sop = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    sop(i) = cfdata.s.epay(i,soc(i));
  end
  table(2,:) = [mean(sop) mean(sop(cfdata.g==0)) ...
                mean(sop(cfdata.g==5)) ...
                mean(sop(cfdata.g==10))];
  str(3).s = 'Mandated 0';
  table(3,:) = [mean(cfdata.m.epay(:,1)) ...
                mean(cfdata.m.epay(cfdata.g==0,1)) ...
                mean(cfdata.m.epay(cfdata.g==5,1)) ...
                mean(cfdata.m.epay(cfdata.g==10,1)) ];
  str(4).s = 'Mandated 5';
  table(4,:) = [mean(cfdata.m.epay(:,2)) ...
                mean(cfdata.m.epay(cfdata.g==0,2)) ...
                mean(cfdata.m.epay(cfdata.g==5,2)) ...
                mean(cfdata.m.epay(cfdata.g==10,2)) ];
  str(5).s = 'Mandated 10';
  table(5,:) = [mean(cfdata.m.epay(:,3)) ...
                mean(cfdata.m.epay(cfdata.g==0,3)) ...
                mean(cfdata.m.epay(cfdata.g==5,3)) ...
                mean(cfdata.m.epay(cfdata.g==10,3)) ];
end
