%% make nice output from beta bounds calculation
% Makes tables of the bound on the difference in welfare under the
% observed allocation and each counterfactual.  Also draws graphs showing
% the alpha, beta distribution that gives rise to the bounds.  These
% results are not included in the paper, but they are refered to in
% section 6.3.  
clear;
%int = gaussChebyshevInt(24);
%wf = int.w.*normpdf(int.x);
groupNames = {'60 Female','60 male','65 Female','65 Male'};
file = fopen('bb/bbTable.tex','w');
ghi = zeros(4,3);
glo = ghi;
for g=1:4 % loop over groups
  load(sprintf('bbSym%d.mat',g));
  sdwhi = dwhi;
  sdwlo = dwlo;
  load(sprintf('bb%d.mat',g));
  dwlo(:,4,:) = sdwlo(:,4,:);
  dwhi(:,4,:) = sdwhi(:,4,:);
  % Make table of bounds conditional on eq choice
  % integrate over alpha
  idwlo = -squeeze(sum(dwhi.*repmat(wf,[1 4 3]),1))'; 
  idwhi = -squeeze(sum(dwlo.*repmat(wf,[1 4 3]),1))'; 
  caption = sprintf(['Effect of Counterfactuals conditional on Equilibrium Choice for ' ... 
                     '%s\n'],groupNames{g});
  % begin a latex formatted table
  fprintf(file,'\\begin{table}[htpb] \\caption{%s}\n',caption);
  fprintf(file,'\\centering \\begin{tabular}{rl|ccc c}\n');
  fprintf(file,['& & \\multicolumn{3}{c}{Mandate} \\\\ \n' ...
                'Eq.& & 0 & 5 & 10 & Sym\\\\ \\hline \n']);
  for e=1:3
    fprintf(file,'%d & low',(e-1)*5);
    fprintf(file,' & %6.3g ',idwlo(e,:));
    fprintf(file,'\\\\ \n & high');
    fprintf(file,'& %6.3g ',idwhi(e,:));
    fprintf(file,'\\\\ \\hline \n');
  end
  for gm=1:4
    hi(gm) = 0;
    lo(gm) = 0;
    for a=1:numel(wf)
      for ge=1:3
        hi(gm) = hi(gm) - wf(a)*Pga(a,ge)*dwlo(a,gm,ge);
        lo(gm) = lo(gm) - wf(a)*Pga(a,ge)*dwhi(a,gm,ge);
      end
      if (gm<4)
        for k=1:3
          ghi(g,k) = ghi(g,k) + wf(a)*(gsMin(a,gm)==k)*Pga(a,gm);
          glo(g,k) = glo(g,k) + wf(a)*(gsMax(a,gm)==k)*Pga(a,gm);
        end
      end
    end
  end
  % Make table of unconditional bounds
  fprintf(file,'All & low');
  fprintf(file,'& %6.3g ',lo);
  fprintf(file,'\\\\ \n & high');
  fprintf(file,'& %6.3g ',hi);
  fprintf(file,['\\\\ \\hline\\hline\n' ...
                '\\end{tabular} \\end{table}\n\n']);
  

  % Plot betaMin,betaMax, dwhi, and dwlo as functions of alpha
  la = log(alpha);
  figs = 0;
  tmp = dwhi;
  dwhi = -dwlo;
  dwlo = -tmp;
  tmp=bMax;
  bMax = bMin;
  bMin = tmp;
  if (figs)
  for m=1:3 % loop over mandates  
    for e=1:3 % loop over eq
      figure;
      line(la,dwhi(:,m,e),'Color','r','LineStyle','v');
      line(la,dwlo(:,m,e),'Color','r','LineStyle','^');
      ax1 = gca;
      set(ax1,'YColor','r');
      legend('\Delta w^H', '\Delta w^L','Location','NorthWest');
      ax2 = axes('Position',get(ax1,'Position'),...
                 'XAxisLocation','bottom',...
                 'YAxisLocation','right',...
                 'Color','none',...
                 'YColor','b');
      set(ax2,'XLim',get(ax1,'XLim'));
      set(ax2,'XTick',get(ax1,'XTick'));
      line(la,bMax(:,m,e),'Color','b','LineStyle','--');
      line(la,bMin(:,m,e),'Color','b','LineStyle',':');
      legend('\beta^H','\beta^L');
      title(sprintf('%s Mandate %d Eq %d',groupNames{g},m,e));
      print('-depsc2',sprintf('bb/bb_g%dm%de%d.eps',g,m,e));
      system(sprintf('cd bb && epstopdf bb_g%dm%de%d.eps && cd ..',g,m,e));
    end
  end      
  end
end % loop over groups
fclose(file);

for g=1:4
  fprintf('%s ',groupNames{g});
  fprintf('& %.3g',ghi(g,:))
  fprintf('\\\\ \n');
  fprintf('& %.3g',glo(g,:))
  fprintf('\\\\ \n');
end
