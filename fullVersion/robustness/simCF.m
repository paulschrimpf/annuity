function cfdata = simCF(data,est,cfg,nsim,prefix,doMandates,savedata,loadData)
% Run counterfactual simulations given data, est, and cfg.
% Does nsim monte carlo simulations

  if(nargin<6)
    doMandates=true;
  end
  if(nargin<7)
    savedata = true;
  end
  if(nargin<8)
    loadData = false;
  end

  datafile = [prefix '/cfdata'];
  
  exist([datafile '.mat'])

  if(loadData && exist([datafile '.mat'])~= 2)
    loadData = false;
  end

  if (loadData)
    load(datafile);
  else
    global T gam delta rho;
    guar = 0:5:10; % guarantee lengths
    
    w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
    agemax = cfg.cut.agemax; % Maximal age alive
    alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
    gam  = cfg.cut.gam;      % CRRA coefficient
    infl  = cfg.cut.inflation;% Inflation rate                               
    rho   = cfg.cut.rho;     % Riskless interest rate                       
    delta = cfg.cut.delta;   % Utility discount rate                        
    nchoice = length(guar);
    disp = 10;

    % allocate data structure
    cfdata.N = nsim;
    cfdata.ngroup = data.ngroup;
    cfdata.fage = zeros(nsim,1);
    cfdata.lage = zeros(nsim,1);
    cfdata.died = ones(nsim,1); % everyone will die
    cfdata.maxAge = 1000*ones(nsim,1); % no censoring
    cfdata.g = zeros(nsim,1);
    cfdata.alphaX = zeros(nsim,size(data.alphaX,2));
    cfdata.betaX = zeros(nsim,size(data.betaX,2));
    % stuff not observed in the real data
    cfdata.alpha = zeros(nsim,1);
    cfdata.beta = zeros(nsim,1);
    % observed equilibrium
    cfdata.val = zeros(nsim,nchoice); % E(utility | each guarantee)
    cfdata.weq = zeros(nsim,nchoice); % wealth equivalents
    cfdata.epay = zeros(nsim,nchoice); % expected payment  
                                       % under mandated length
    cfdata.m.val = cfdata.val;
    cfdata.m.weq = cfdata.weq;
    cfdata.m.epay = cfdata.epay;
    % under symmetric info
    cfdata.s.val = cfdata.val;
    cfdata.s.weq = cfdata.weq;
    cfdata.s.epay = cfdata.epay;
    cfdata.s.g = cfdata.g;
  
    dataIndex= zeros(nsim,1);
    cfdata.group = zeros(nsim,1);
    for n = 1:nsim
      % draw x's
      i = ceil(rand()*data.N);
      cfdata.alphaX(n,:) = data.alphaX(i,:);
      cfdata.betaX(n,:) = data.betaX(i,:);
      cfdata.group(n) = data.group(i);
    
      if (length(cfg.cut.w0)>1)
        u = rand();
        cp = cumsum(cfg.cut.wp);
        w0 = cfg.cut.w0(u<cp);
        cfdata.w0(n) = w0(1);
      end
    end
    % draw alpha, beta
    if (isfield(est,'w0') && length(est.w0)>1)
      [logAlpha logBeta w0] = sampleAlphaBeta(cfdata,est);
      cfdata.w0 = w0;
      cfdata.bgam = ones(cfdata.N,1)*cfg.cut.bgam;
      cfdata.gam = ones(cfdata.N,1)*cfg.cut.gam;
    elseif isfield(est.parm,'twodh')
      [logAlpha logBeta gamma] = sampleAlphaBeta(cfdata,est);
      gamma = reshape(gamma,numel(gamma),1); % make column vector
      cfdata.gam = gamma;
      cfdata.bgam = gamma;
      cfdata.w0 = w0*ones(cfdata.N,1);
    elseif isfield(est.parm,'frac')
      [logAlpha logBeta frac] = sampleAlphaBeta(cfdata,est);
      frac = reshape(frac,numel(frac),1); % make column vector
      cfdata.frac = frac;
      cfdata.w0 = w0*ones(cfdata.N,1);      
      cfdata.bgam = ones(cfdata.N,1)*cfg.cut.bgam;
      cfdata.gam = ones(cfdata.N,1)*cfg.cut.gam;
    else
      [logAlpha logBeta] = sampleAlphaBeta(cfdata,est);
      cfdata.w0 = w0*ones(cfdata.N,1);
      cfdata.bgam = ones(cfdata.N,1)*cfg.cut.bgam;
      cfdata.gam = ones(cfdata.N,1)*cfg.cut.gam;
    end
    cfdata.alpha = exp(logAlpha);

    if isfield(cfg.cut,'raHetero') 
      if (length(cfg.cut.raHetero.beta)==1)
        cfdata.beta = cfg.cut.raHetero.beta*ones(cfdata.N,1);
      else
        cfdata.beta = zeros(cfdata.N,1);
        for n=1:cfdata.N
          cfdata.beta(n) = cfg.cut.raHetero.beta(cfdata.group(n));
        end
      end
      cfdata.gam = exp(logBeta);
      if (isfield(cfg.cut.raHetero,'cons') && cfg.cut.raHetero.cons)
        cfdata.bgam = ones(cfdata.N,1)*cfg.cut.bgam;
      else
        cfdata.bgam = cfdata.gam;
      end
    else
      cfdata.beta = exp(logBeta);
    end    
    
    if (isfield(cfg.cut,'alphaBias'))
      cfdata.alphaBias = cfg.cut.alphaBias;
    else
      cfdata.alphaBias = 1;
    end
    lambda = est.lambda;
    
      
    cfdata = runCF(cfdata,est,cfg,doMandates);
  end
  z = cfg.cut.z;
  T = cfg.cut.agemax - cfg.cut.agemin;
  for g=1:data.ngroup
    if (length(cfg.cut.w0)>1)
      cfdata.maxRisk(g) = (cfg.cut.wp*cfg.cut.w0')*cfg.cut.fracAnnuitized* ...
          (z(g,1)-z(g,3))/z(g,3);    
    elseif (numel(cfg.cut.fracAnnuitized)>1)
      cfdata.maxRisk(g) = cfg.cut.w0*(cfg.cut.fracAnnuitized*cfg.cut.wp')* ...
          (z(g,1)-z(g,3))/z(g,3);
    else 
      cfdata.maxRisk(g) = cfg.cut.w0*cfg.cut.fracAnnuitized* ...
          (z(g,1)-z(g,3))/z(g,3);
    end
  end
  if(savedata);
    % save the data
    eval(['save ' datafile ' cfdata']);
  end
  
  %----------------------------------------------------------------------
end % function simCF
