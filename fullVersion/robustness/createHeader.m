function createHeader(data,cfg,prefix,cfnsim)
% Print a header describing the estimates
  
  of = fopen([prefix '/header.tex'],'w');
  fprintf(of,['These estimates come from a data set of ' ...
              '%d observations consisting of %d groups.  ' ...
              'Each group faces a different price.  '], ...
          data.N,data.ngroup);
  if(data.ngroup<5)
    fprintf(of,['The groups are:\n' ...
                '\\begin{enumerate}\n']);
    for g=1:data.ngroup
      fprintf(of,'\\item %s \n',cfg.data.group(g).name);
    end
  else
    fprintf(of,['The first 5 groups are:\n' ...
                '\\begin{enumerate}\n']);
    for g=1:5
      fprintf(of,'\\item %s \n',cfg.data.group(g).name);
    end
  end
  fprintf(of,'\\end{enumerate}\n');
  
  fprintf(of,'%d regressors are used for $\\alpha$.  They are named: ' ...
          ,size(data.alphaX,2));
  for k=1:size(data.alphaX,2)-1
    fprintf(of,' %s,',data.name.alphaX(k).s);
  end
  k = size(data.alphaX,2);
  fprintf(of,' and %s.\n\n',data.name.alphaX(k).s);

  fprintf(of,'%d regressors are used for $\\beta$.  They are named: ' ...
          ,size(data.betaX,2));
  for k=1:size(data.betaX,2)-1
    fprintf(of,' %s,',data.name.betaX(k).s);
  end
  k = size(data.betaX,2);
  fprintf(of,' and %s.\n\n',data.name.betaX(k).s);
  
  switch(cfg.est.dist)
   case 'normal'
    fprintf(of,['$\\alpha$ and $\\beta$ are log-normally distributed.\n\' ...
                'n']);
   case 'gamma'
    fprintf(of,['$\\alpha$ and $\\beta$ are gamma distributed.\n\' ...
                'n']);
   case 'gamma-normal'
    fprintf(of,['$\\alpha$ is gamma distributed, and $\\beta$ is log-normally distributed.\n\' ...
                'n']);
  end
  if(cfnsim>0)
    fprintf(of,'%d simulations were used for the counterfactuals.\n\n', ...
            cfnsim);
  else
    fprintf(of,'Counterfactual simulations were not run.\n\n');
  end
  if (isfield(cfg.mort,'fasthazard'))
    fprintf(of,['The survival function is $\\exp(\\lambda/\\alpha (1 -' ...
                ' e^{\\lambda t^%g}))$'],cfg.mort.fasthazard);
  end

  
  fprintf(of,['\\begin{table}[htbp]\\centering' ...
              '\\caption{Model Parameters}\n' ...
              '\\begin{tabular}{c|c} \\hline\\hline \n' ...
              'Parameter & Value \\\\ \\hline \n' ...
              '$\\lambda$ & %g \\\\ \n' ...
              'Utility $\\gamma$ & %g \\\\ \n' ...
              'Bequest $\\gamma$ & %g \\\\ \n'], ...
          cfg.mort.lambda,cfg.cut.gam,cfg.cut.bgam);
  if (length(cfg.cut.inflation)==1)
    fprintf(of,'Inflation & %g \\\\ \n',cfg.cut.inflation);
  else
    fprintf(of,'Inflation & %g - %g \\\\ \n', ...
            min(cfg.cut.inflation),max(cfg.cut.inflation));
  end
  if (length(cfg.cut.rho)==1)
    fprintf(of,'Interest Rate & %g \\\\ \n',1./cfg.cut.rho-1);
  else
    fprintf(of,'Interest Rates & %g - %g \\\\ \n', ...
            min(1./cfg.cut.rho-1),max(1./cfg.cut.rho-1));
  end
  if (length(cfg.cut.delta)==1)
    fprintf(of,'Discount Rate & %g \\\\ \n',1./cfg.cut.delta-1);
  else
    fprintf(of,'Discount Rates & %g - %g \\\\ \n', ...
            min(1./cfg.cut.delta-1),max(1./cfg.cut.delta-1));
  end
  fprintf(of,'\\end{tabular} \\end{table} \n');
  fclose(of);
  
