%% Make nice output from bounds on pairwise differences in the welfare
% effects of policies.  Must be run after betaBounds.m.  These tables are
% not included in the published paper, but the results are referred to in
% section 6.3
clear;
%int = gaussChebyshevInt(24);
%wf = int.w.*normpdf(int.x);
groupNames = {'60 Female','60 male','65 Female','65 Male'};
file = fopen('bb/bbTable3.tex','w');
ghi = zeros(4,3);
glo = ghi;
for g=1:4 % loop over groups
  load(sprintf('bb3%d.mat',g));
  % Make table of bounds 
  % integrate over alpha
  if (any(abs(dwlo2(:))>10)) 
    warning(['some dwlo2 are large, setting to zero -- double check that ' ...
             'this is a numeric error']);
    dwlo2(abs(dwlo2)>10) = 0;
  end
  if (any(abs(dwhi2(:))>10)) 
    warning(['some dwhi2 are large, setting to zero -- double check that ' ...
             'this is a numeric error']);
    dwhi2(abs(dwhi2)>10) = 0;
  end
  idwlo = zeros(size(dwlo2,2),size(dwlo2,3)); idwhi = idwlo;
  for ge=1:3
    idwlo = idwlo + squeeze(sum(dwlo2(:,:,:,ge).* ...
                                repmat(wf.*Pga(:,ge),[1,size(idwlo),1]),1));
    idwhi = idwhi + squeeze(sum(dwhi2(:,:,:,ge).* ...
                                repmat(wf.*Pga(:,ge),[1,size(idwhi),1]),1));
  end
  caption = sprintf(['Difference of policies for %s\n'],groupNames{g});
  % begin a latex formatted table
  fprintf(file,'\\begin{table}[htpb] \\caption{%s}\n',caption);
  fprintf(file,'\\centering \\begin{tabular}{rl|ccc c}\n');
  fprintf(file,['& & \\multicolumn{3}{c}{Mandate 1} \\\\ \n' ...
                'Mandate 2& & 0 & 5 & 10 & Sym\\\\ \\hline \n']);
  for e=1:3
    fprintf(file,'%d & low',(e-1)*5);
    fprintf(file,' & %6.3g ',[idwlo(e,:) -idwhi(4,e)]);
    fprintf(file,'\\\\ \n & high');
    fprintf(file,'& %6.3g ',[idwhi(e,:) -idwlo(4,e)]);
    fprintf(file,'\\\\ \\hline \n');
  end
  fprintf(file,['\\hline \\end{tabular} \\end{table}\n\n']);

end
