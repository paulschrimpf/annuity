# ampl command file for step 2 estimation
load amplfunc.dll; # load library of external functions
# declare model and data, these tell the solver which problem to solve and
# what parameter values to use
model annuityLikeGamma.mod; # model file
data pooled.dat; # data file

# initialize cutoff grid
call {c in 1..nCut,d in 1..nG} 
 setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,c,d],c+nCut*(d-1)); 

#display cutLogA, cutLogB;
option solver snopt; # set the solver
                     # available choices are snopt,minos,loqo, and minos
                     # snopt seems most robust so far
option snopt_options 'timing 1 outlev=3'; # some solver options
option minos_options 'timing 1 outlev=3';
option loqo_options 'timing 1 outlev=3'; # some solver options
option minos_options 'outlev=3';

# intial values
set shapeAinit := {0.1,1,4};
set muAinit := {-5.3,-7,-4,-3};
set shapeBinit := {0.01,0.1,1,10,30};
set muBinit := {10.25,6,8,15};

display {t in 1..nCut,d in 1..nG} t+nCut*(d-1);

# begin file of output
printf 'pooledNormal minos results\n' > pooled.csv;
printf ' ,initial,f60,m65,f65,m60,initial,final,initial,f60,m65,f65,m60,initial,final,initial,final\n' >> pooled.csv;
printf 'loglike, muA, , , , , shapeA, , muB, , , , , shapeB, , corr, ,\n' >> pooled.csv;
for {c in {-9,-5,5,9}} { 
 for {sa in shapeAinit} {
  for {ma in muAinit} {
   for {sb in shapeBinit} {
    for {mb in muBinit} {

#param c := -1.109;
  call {t in 1..nCut,d in 1..nG} 
   setCut(nCut*nG,{i in 1..cutN} cutLogA[i],{i in 1..cutN} cutLogB[i,t,d],t+nCut*(d-1)); 


  let shapeA := sa;
  let{d in 1..nG} muA[d] := ma;        
  let shapeB := sb;
  # initialize muB to fit b/t cutoffs at muA        
  let{d in 1..nG} muB[d] := 0.5*(cutoff(muA[d],1+nCut*(d-1))+cutoff(muA[d],2+nCut*(d-1)));
  let corr := c;
          
  solve;

  display _solve_time;
  display logLikelihood,lambda,muA,shapeA,muB,shapeB,corr;
  # printf results
  printf '%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g,%25.16g\n', 
         logLikelihood,ma,muA[1],muA[2],muA[3],muA[4],sa,shapeA,mb,muB[1],muB[2],muB[3],muB[4],sb,shapeB,c/10,corr >> pooled.csv;
}}}}}
