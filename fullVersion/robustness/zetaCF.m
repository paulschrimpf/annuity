% estimate model on a grid of zeta values
clear all;
confs= {'confFemale60', ...
        'confFemale65', ...
        'confMale60', ...
        'confMale65'};

YEAR = 92;
SEPARATE = true; % separate by year
GAM = 3;
BGAM = 3;
OBJECTIVE = 'mle';
READLAMBDA = true;
FRAC = 0.2;
DIST = 'normal';
USEOLDSIM = false;

zgrid = 0.1:0.1:0.9;

for z=1:length(zgrid)
  ZETA = zgrid(z);
  for c = 1:length(confs) 
    clear annuityLike;
    clear step2Like;
    clear mortLike;
    clear transformParam;
    configName = confs{c};
    eval(['cfg = ' configName '();']);
    mortOnly = strcmp(OBJECTIVE,'mlmort'); 
    if (mortOnly)  % change config to just do mortality
      READLAMBDA = false;
      cfg.est.objective = 'mlmort';
      cfg.outprefix = [cfg.outprefix 'Mort'];
      cfg.est.opt.DerivativeCheck='off';
      %    cfg.est.opt.LargeScale='off';
    end
    cfg.est.objective = OBJECTIVE;
    cfg.est.opt.LargeScale = 'Off';
    cfg.est.opt.MaxIter = 50000;
    cfg.est.opt.DerivativeCheck = 'Off';
    cfg.est.disp = 50;
    
    switch(OBJECTIVE)
     case '2step'
      cfg.outprefix = [cfg.outprefix '_2step'];
     case 'mlmort'
      cfg.outprefix = [cfg.outprefix 'Mort'];
    end
    
    cfg.cut.gam = GAM;
    cfg.cut.bgam = BGAM;
    cfg.cut.fracAnnuitized = FRAC;
    cfg.est.dist = DIST;
    cfg.cut.zeta = ZETA;
    
    if (SEPARATE) 
      cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
    else
      if (YEAR~=92)
        cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
      end
    end
    if (GAM~=3)
      cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
    end
    if (BGAM~=GAM)
      cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
    end
    if (FRAC~=0.2)
      cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
    end
    if (~strcmp(DIST,'normal'))
      cfg.outprefix = [cfg.outprefix,'_' DIST];
    end
    if(ZETA~=1)
      cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
    end
    cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');

    % read data
    if (SEPARATE) 
      switch YEAR
       case {92,95}
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9295.txt'];
       case {90,97} 
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9097.txt']; 
       otherwise
        error(['no appropriate data set for separate and ' num2str(YEAR)]);
      end
      cfg.data.keepYear = 1900 + YEAR;
    else
      if (YEAR<100)
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_19' num2str(YEAR) ...
                         '.txt'];
      else
        cfg.data.name = ['/proj/paul/annuities/data/newCompB2_' num2str(YEAR) ...
                         '.txt'];
      end
      cfg.data.allDummies=false;
    end
    
    if (READLAMBDA)
      if(SEPARATE)
        if (YEAR<=94)
          ystr = '92';
        else
          ystr = '95';
        end
        filename = [confs{c} '_' ystr];
      else
        filename = confs{c};
      end
      load ([filename '.lambda'])
      eval(['cfg.mort.lambda = ' filename]);
    end


    %  cfg.outprefix = [cfg.outprefix '_95'];
    [data cfg] = readData(cfg); % see readData for details of data struc
    if (~strcmp(cfg.est.objective,'mlmort'))
      % load cutoffs
      cut = loadcutoffs(cfg);
      cut = cleanCut(cut,cfg,data);
      est.cut = cut;
    end
    if (isfield(cfg,'dropBadGroups') && cfg.dropBadGroups)
      [data cut cfg] = dropBadGroups(data,cut,cfg);
      est.cut = cut;
    end  
    
    est.lambda = cfg.mort.lambda;
    est.inc = cfg.mort.inc;
    est.disp = cfg.est.disp;
    est.extraMoments = cfg.est.extraMoments;
    est.dist = cfg.est.dist;
    
    if(isfield(cfg.est,'restart') && ~isempty(cfg.est.restart))
      % load restart deck
      scfg=cfg;
      eval(['load ' cfg.est.restart]);
      % restart deck consists of 'est' struc containing the complete state
      % of the previous estimates
      cfg=scfg;
      est.lambda = cfg.mort.lambda;
      est.inc = cfg.mort.inc;
      est.disp = cfg.est.disp;
      est.extraMoments = cfg.est.extraMoments;
      est.dist = cfg.est.dist;
    else
      % initialize estimates (also document it's structure)
      % current parameters
      est.lambda = cfg.mort.lambda;
      est.parm.gAlpha = [-5; zeros(size(data.alphaX,2)-1,1)];
      b = 0; % initialize mean and std of beta to rougly fit cutoffs 
      s = 0;
      if (~mortOnly)
        for k=1:length(cut)
          [m f] = min(abs(cut(k).la-est.parm.gAlpha(1)));
          b = b + (cut(k).lb(f,1)+cut(k).lb(f,2))/2
          s = s + (cut(k).lb(f,2)-cut(k).lb(f,1));
        end
        b = b/length(cut);
        s = s/length(cut);
      end
      est.parm.gBeta = [b; zeros(size(data.betaX,2)-1,1)];
      est.parm.var = [s 0.0; 0.0 s];
      est.parm.ashape = 5;
      est.parm.bshape = 1;
      est.parm.corr = 0;
      est.int.x = []; % integration points and weights
      est.int.w = []; 
    end
    
    % initialize integration points
    switch(est.dist)
     case 'normal'
      est.int = gaussChebyshevInt(cfg.est.nint);
     case {'gamma','gamma-normal'}
      [est.int.x est.int.w] = gaussLaguerre(cfg.est.nint,0);
      est.int.n = cfg.est.nint;
    end
    
    
    % pack parameters
    switch(cfg.est.objective)
     case 'mle'
      objFunc = 'annuityLike';
      parm = transformParam(est.parm,'pack',cfg.est.objective,cfg.est.dist);
     case 'mlmort'
      objFunc = 'mortLike';
      k = length(est.parm.gAlpha);
      parm = zeros(k+2,1);
      parm(1:k) = est.parm.gAlpha;
      k = k+1;
      switch (est.dist)
       case {'normal','exponential'}
        parm(k) = log(sqrt(est.parm.var(1,1)));
        k = k+1;
       case 'gamma'
        parm(k) = log(sqrt(est.parm.var(1,1)));
        k = k+1;
        parm(k) = log(1);
        k = k+1;
      end
      parm(k) = log(est.lambda);
     case '2step'
      objFunc = 'step2Like';
      cfg2 = cfg;
      est2 = est;
      load(cfg.est.mortResults);
      cfg = cfg2;
      est.dist = cfg.est.dist;
      est.lambda = cfg.mort.lambda;
      est.inc = cfg.mort.inc;
      est.disp = cfg.est.disp;
      est.extraMoments = cfg.est.extraMoments;
      est.int = est2.int;
      est.cut = est2.cut;
      est.parm.var(2,2) = est2.parm.var(2,2);
      est.parm.gBeta = est2.parm.gBeta;
      clear cfg2 est2;
      parm = transformParam(est.parm,'pack',cfg.est.objective,est.dist);
     otherwise
      error('Unrecognized objective function\n');
    end
    bootEst = [];

    % load results
    eval(['load ',cfg.outprefix,'Results']);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % run counterfactuals
    % SIMULATIONS
    cd ../report;
    % reduced form
    if (mortOnly)
      cfnsim = 0;
    else
      cfnsim = 200;
    end
    path(path,'../src/');
    path(path,'../src/cutoffs/');
    
    i = findstr('/',cfg.outprefix);
    prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
    mkdir(prefix);
    mkdir(prefix,'tables');
    mkdir(prefix,'figures');
    %eval(['delete ' prefix '/tables/*']);
    %eval(['delete ' prefix '/figures/*']);
    %eval(['delete ' prefix '/*']);
    ncfg =cfg;
    eval(['load ' cfg.outprefix 'Results']);
    cfg = ncfg;
    
    if(cfg.mort.lambda~=est.lambda)
      warning('cfg lambda ~= est.lambda, using est.lambda');
      cfg.mort.lambda = est.lambda;
    end
    
    % header
    createHeader(data,cfg,prefix,cfnsim);
    reducedForm(data,cfg,prefix);
    
    % table of estimates
    estReport(data,est,prefix);
    
    % mortality
    mortalityReport(data,est,cfg,prefix);
    pdie(data,est,cfg,prefix);
    
    % cutoffs
    if (~strcmp(cfg.est.objective,'mlmort'))
      graphCutoffs(data,est,cut,prefix);
      % consumption trajectories
      consumption(data,est,cfg,prefix);
      fullAnnuity(data,est,cfg,prefix);
    end
    
    % counterfactuals
    if(cfnsim)
      cfdata = simCF(data,est,cfg,cfnsim,prefix,true,true,USEOLDSIM);
      cfTables(data,est,cfdata,cfg,prefix);
      fit(data,est,cfdata,cfg,prefix);
      % eliminate cross subsidies
      %  nosubData = crossSub(data,est,cfdata,cfg,prefix);
      % interest rate stuff
      %if (~USEOLDSIM)
      %nosubData2 = crossSub2(data,est,cfdata,cfg,prefix);
      %end
    end
    eval(['!perl createTex.pl ' num2str(data.ngroup) ' ' ...
          prefix ' > ' prefix '/slideTables.tex']);
    eval(['!cp Makefile ' prefix '/.']);
    if (~exist([prefix '/notes.tex']))
      % create blank file
      eval(['!touch ' prefix '/notes.tex']);
    end
    %eval(['!cd ' prefix ' && make clean ps']);
    
    cd ../src;
    
  end % loop over confs
  fprintf([' ############################################################' ...
           ' ##########\n']);
  fprintf('Finished with %d of %d values for zeta\n',z,length(zgrid));
  fprintf([' ############################################################' ...
           ' ##########\n']);
end % loop over zeta
