function betaBounds(data,est,cfg,prefix) 
% Computes bounds on welfare estimates without assuming a distribution
% for beta.  See section 6.3.  
  for g=1:data.ngroup
    sigAlpha = sqrt(est.parm.var(1,1));
    muAlpha = est.parm.gAlpha(g);
    wf = est.int.w.*normpdf(est.int.x);
    alpha = exp(est.int.x*sigAlpha + muAlpha);
    nt = 2*numel(alpha); % number of mortality times to use to solve
    tlo = min(data.fage(data.group==g));
    thi = max(data.lage(data.group==g)); 
    %mt = rand(numel(alpha),nt)*(thi-tlo)+tlo;
    %mt = ones(numel(alpha),1)*data.lage(data.group==g & data.died==1)'; 
    mt = ones(numel(alpha),1)*(tlo:(thi-tlo)/nt:thi);
    nt = size(mt,2)-1;
    expAL = 1-exp((alpha*ones(1,nt))/est.lambda .* ...
                  (exp(est.lambda*mt(:,1:(end-1)))-exp(est.lambda*mt(:,2:end))));
    expAL = expAL .*(wf*ones(1,nt));
    Pgt = zeros(size(mt,2)-1,3); % Pr(guar<g & m<t)
    for guar=0:5:10
      for t=1:(size(mt,2)-1)
        Pgt(t,guar/5+1) = sum(data.g(data.group==g)==guar & ...
                            data.lage(data.group==g) <= mt(1,t+1) & ...
                            data.died(data.group==g)==1 & ...
                            data.fage(data.group==g) <=  mt(1,t) & ...
                            data.lage(data.group==g) >= mt(1,t)) ...
            /  max(sum(data.g(data.group==g)==guar & ... 
                       data.fage(data.group==g) <=  mt(1,t) & ...
                       data.lage(data.group==g) >= mt(1,t)),1);
      end
      % covariance matrix
      %W{guar/5} = diag(Pgt(:,guar/5).*(1-Pgt(:,guar/5)))+eye(size(Pgt,1))*1e-6;
      %Pgt(:,guar/5) = Pg - Pgt(:,guar/5);
    end
    rank(expAL)
    %Pga = expAL' \ Pgt; % P(guar<=g | alpha)
    Pga(:,1) = ones(size(alpha))/3; %*mean(data.g(data.group==g)==0);
    Pga(:,2) = ones(size(alpha))/3 ; %*mean(data.g(data.group==g)==5);
    Pga(:,3) = ones(size(alpha))/3; %*mean(data.g(data.group==g)==10);
    norm(expAL'*Pga - Pgt)
    % stacked version
    sPga = Pga(:);
    sPgt = Pgt(:);
    seAL = blkdiag(expAL',expAL',expAL');
    %stop
    Pga(:,1) = lsqlin(expAL',Pgt(:,1),[],[], wf',mean(data.g(data.group==g)==0), ...
                      zeros(size(alpha)),ones(size(alpha)),Pga(:,1));
    Pga(:,2) = lsqlin(expAL',Pgt(:,2),[],[], wf',mean(data.g(data.group==g)==5), ...
                      zeros(size(alpha)),ones(size(alpha)),Pga(:,2));    
    Pga
    [sum(Pga(:,1).*wf) mean(data.g(data.group==g)<5) ]
    [sum(Pga(:,2).*wf) mean(data.g(data.group==g)==5)]
    % stacked
    conMat = zeros(length(alpha),length(sPga));
    for i=1:length(alpha)
      conMat(i,i+(0:2)*length(alpha)) = 1;
    end
    conVec = ones(length(alpha),1);
    %seAL = [seAL; wf' zeros(1,2*numel(wf)); zeros(1,numel(wf)) wf' ...
    %        zeros(1,numel(wf)); zeros(1,2*numel(wf)) wf'];
    %sPgt = [sPgt; mean(data.g(data.group==g)==0); ...
    %        mean(data.g(data.group==g)==5);  mean(data.g(data.group==g)==10)];
    %sPga = lsqlin(seAL,sPgt,[],[], conMat, conVec, ...
    %              zeros(size(sPga)),ones(size(sPga)),sPga);    
    %Pga = reshape(sPga,size(Pga))
    sPga = csvread('Pga.csv');
    s = (g-1)*numel(alpha)+1;
    Pga = sPga(s:(s+numel(alpha)-1),:)
    lt=[0 sum(Pga(:,1).*wf) mean(data.g(data.group==g)==0); ...
        5 sum(Pga(:,2).*wf) mean(data.g(data.group==g)==5); ...
        10 sum(Pga(:,3).*wf) mean(data.g(data.group==g)==10)]
    fprintf('%d & %.4f & %.4f \\\\ \n',lt');
    % Plot Pga to check plausibility
    figure;
    plot(log(alpha),Pga(:,1),'-',log(alpha),Pga(:,2),'--',log(alpha),Pga(:,3),':');
    title(cfg.data.group(g).name);
    xlabel('log \alpha');
    ylabel('Probability');
    legend('P(g=0|\alpha)','P(g=5|\alpha)', 'P(g=10|\alpha)');
    print('-depsc2',sprintf('pga%d',g'));    

    % maximize welfare change as fn of beta
    % 1. compute epay, use to find prices
    % 2. for each alpha:
    %   - maximize over beta weq(eq) - weq(sym) 
    %      - find val(beta,alpha) in eq
    %      - compute weq(val,eq), weq(val,sym)
    %      - use fminbnd
    % do same for sym info and each mandate

    % value function parameters
    vp.rho = cfg.cut.rho(g);
    vp.delta = cfg.cut.delta(g);
    vp.T = cfg.cut.agemax-cfg.cut.agemin(g);
    vp.zeta = cfg.cut.zeta;
    vp.gam = cfg.cut.gam;
    vp.bgam = cfg.cut.bgam;
    vp.pub = 0;
    vp.infl = cfg.cut.inflation(g);
    vp.frac = cfg.cut.fracAnnuitized;
    w0 = cfg.cut.w0;
    lambda = cfg.cut.lambda;
    z = cfg.cut.z(g,:); % annuity payment rates
    t0 = cfg.cut.agemin(g) - cfg.mort.baseage;
    t1 = cfg.cut.agemax - cfg.mort.baseage;
    % expected payment -- needed to compute payment rates for mandates
    % and symmetric info
    totalPay = 0;
    for gr=1:5  % a referee was interested in 15 and 20 year mandates, so
                % we include them here
      if gr>length(z)
        z(gr) = z(end)
      end
      for i=1:numel(alpha)
        paya(i,gr) = epay(lambda,alpha(i),(gr-1)*5, z(gr), t0, t1, vp.delta/(1+vp.infl)); 
        if (gr<=3)
          totalPay = totalPay + wf(i)*paya(i,gr)*Pga(i,gr);
        end
      end
    end
    
    %% bounds on change in welfare compared to equilibrium
    for gm=1:3
      % mandates
      zmand = z(gm) * totalPay/(sum(paya(:,gm).*wf));
      z(gm);
      for ge=1:3
        for i=1:numel(alpha) 
          % cutoffs 
          switch(ge)
           case {1}
            cut0 = -15;
            cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                            log(alpha(i)),'linear','extrap')); 
           case {2}
            cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                            log(alpha(i)),'linear','extrap')); 
            cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                            log(alpha(i)),'linear','extrap'));
           case {3}
            cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                            log(alpha(i)),'linear','extrap'));
            cut1 = 30;
          end % end switch
          % mortality hazard
          q = exp(alpha(i)/lambda*(1-exp(lambda*(t0:t1))));
          q = (q - exp(alpha(i)/lambda*(1-exp(lambda*(1+(t0:t1))))))./q;
          [bMax(i,gm,ge) dwhi(i,gm,ge)] = ...
              fminbnd(@(b) -wdiff(exp(b),z(ge),(ge-1)*5,zmand,(gm-1)*5,q, ...
                                  vp,w0),cut0,cut1);
          fprintf('.');
          [bMin(i,gm,ge) dwlo(i,gm,ge)] = ...
              fminbnd(@(b) wdiff(exp(b),z(ge),(ge-1)*5,zmand,(gm-1)*5,q, ...
                                 vp,w0),cut0,cut1);
          fprintf('.');
        end % loop over alpha
        fprintf('\n');
      end % loop over ge
      fprintf('finished group %d m %d\n',g,gm);
      squeeze(dwhi(:,gm,:))
      squeeze(dwlo(:,gm,:))
    end % loop over gm 

    % symmetric info
    for ge=1:3
      for i=1:numel(alpha)
        % payment rates
        zsym = z* totalPay./paya(i,1:3);
        % cutoffs 
        switch(ge)
         case {1}
          cut0 = -15;
          cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                          log(alpha(i)),'linear','extrap')); 
         case {2}
          cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                          log(alpha(i)),'linear','extrap')); 
          cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                          log(alpha(i)),'linear','extrap'));
         case {3}
          cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                          log(alpha(i)),'linear','extrap'));
          cut1 = 30;
        end % end switch
        q = exp(alpha(i)/lambda*(1-exp(lambda*(t0:t1))));
        q = (q - exp(alpha(i)/lambda*(1-exp(lambda*(1+(t0:t1))))))./q;
        [bMax(i,4,ge) dwhi(i,4,ge)] = ...
            fminbnd(@(b) -wdiff(exp(b),z(ge),(ge-1)*5,zsym,[0 5 10], q, ...
                                vp,w0),cut0,cut1);
        [w gsMax(i,ge)] = wdiff(exp(bMax(i,4,ge)),z(ge),(ge-1)*5, ...
                                    zsym,[0 5 10], q,vp,w0)
        fprintf('.');
        [bMin(i,4,ge) dwlo(i,4,ge)] = ...
            fminbnd(@(b) wdiff(exp(b),z(ge),(ge-1)*5,zsym,[0 5 10], q, ...
                               vp,w0),cut0,cut1);
        [w gsMin(i,ge)] = wdiff(exp(bMin(i,4,ge)),z(ge),(ge-1)*5, ...
                                    zsym,[0 5 10], q,vp,w0)
        fprintf('.');
      end % loop over alpha
      fprintf('\n');
    end % loop over ge
    fprintf('finished group %d symmetric\n',g);    
    eval(['save bbSym' num2str(g) '.mat alpha Pga dwhi dwlo bMax bMin wf ' ...
          'gsMin gsMax;']);

    %% bounds on pairwise differences between policies
    % differences between mandates 
    for gm=1:4 
      zmand = z(gm) * totalPay/(sum(paya(:,gm).*wf))
      for gm2=(gm+1):5
        zmand2 = z(gm2) * totalPay/(sum(paya(:,gm2).*wf))
        fprintf('Beginning %d-%d-%d\n',g,gm,gm2);
        for ge=1:3
          for i=1:numel(alpha) 
            % cutoffs 
            switch(ge)
             case {1}
              cut0 = -15;
              cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                              log(alpha(i)),'linear','extrap')); 
             case {2}
              cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                              log(alpha(i)),'linear','extrap')); 
              cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                              log(alpha(i)),'linear','extrap'));
             case {3}
              cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                              log(alpha(i)),'linear','extrap'));
              cut1 = 30;
            end % end switch
                % mortality hazard
            q = exp(alpha(i)/lambda*(1-exp(lambda*(t0:t1))));
            q = (q - exp(alpha(i)/lambda*(1-exp(lambda*(1+(t0:t1))))))./q;
            
            [bMax2(i,gm,gm2,ge) dwhi2(i,gm,gm2,ge)] = ...
                fminbnd(@(b) -wdiff(exp(b),zmand2,(gm2-1)*5,zmand,(gm-1)*5,q, ...
                                    vp,w0),cut0,cut1);
            fprintf('.');
            [bMin2(i,gm,gm2,ge) dwlo2(i,gm,gm2,ge)] = ...
                fminbnd(@(b) wdiff(exp(b),zmand2,(gm2-1)*5,zmand,(gm-1)*5,q, ...
                                   vp,w0),cut0,cut1);
            fprintf('.');
          end % loop over alpha
          fprintf('\n');
        end % loop over ge
      end % loop over gm2
      fprintf('finished group %d mandate %d\n',g,gm);
    end % loop over gm 

    % symmetric info vs mandates
    gsMin2 = [];
    gsMax2 = [];
    %{
    for gm=1:3
      zmand = z(gm) * totalPay/(sum(paya(:,gm).*wf))      
      for ge=1:3
        for i=1:numel(alpha)
          % payment rates
          zsym = z* totalPay./paya(i,1:3);
          % cutoffs 
          switch(ge)
           case {1}
            cut0 = -15;
            cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                            log(alpha(i)),'linear','extrap')); 
           case {2}
            cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                            log(alpha(i)),'linear','extrap')); 
            cut1 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                            log(alpha(i)),'linear','extrap'));
           case {3}
            cut0 = (interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                            log(alpha(i)),'linear','extrap'));
            cut1 = 30;
          end % end switch
          q = exp(alpha(i)/lambda*(1-exp(lambda*(t0:t1))));
          q = (q - exp(alpha(i)/lambda*(1-exp(lambda*(1+(t0:t1))))))./q;
          [bMax2(i,6,gm,ge) dwhi2(i,6,gm,ge)] = ...
              fminbnd(@(b) -wdiff(exp(b),zmand,(gm-1)*5,zsym,[0 5 10], q, ...
                                  vp,w0),cut0,cut1);
          [w gsMax2(i,gm,ge)] = wdiff(exp(bMax2(i,4,gm,ge)),zmand,(gm-1)*5, ...
                                  zsym,[0 5 10], q,vp,w0);
          fprintf('.');
          [bMin2(i,6,gm,ge) dwlo2(i,6,gm,ge)] = ...
              fminbnd(@(b) wdiff(exp(b),zmand,(gm-1)*5,zsym,[0 5 10], q, ...
                                 vp,w0),cut0,cut1);
          [w gsMin2(i,gm,ge)] = wdiff(exp(bMin2(i,4,gm,ge)),zmand,(gm-1)*5, ...
                                  zsym,[0 5 10], q,vp,w0);
          fprintf('.');
        end % loop over alpha
        fprintf('\n');
      end % loop over ge
    end % loop over gm
    %}
    fprintf('finished group %d symmetric\n',g);    
    
    dwhi2 = -dwhi2;
    eval(['save bb3' num2str(g) '.mat alpha Pga dwhi2 dwlo2 bMax2 bMin2 wf ' ...
          'gsMin2 gsMax2;']);
    fprintf('finished group %d and saved results\n',g);
  end % loop over groups
end % function betaBounds

% Expected payment per pound annuitized given lambda and alpha for a
% guarantee of length g with payment rate z beginning at age t0, dying
% with certainty by t1 and discount rate discount
function pay = epay(lambda,alpha,g,z,t0,t1,discount) 
  t = t0:t1;
  pdiet = exp(alpha/lambda*(1-exp(lambda*(t)))) ...
          - exp(alpha/lambda*(1-exp(lambda*(t+1))));
  %pdiet(end) = 1-sum(pdiet(1:end-1));
  cumz = cumsum(z*discount.^(t-t0));
  if (g>0)
    pay = cumz(g)*sum(pdiet(1:g)) + sum(cumz((g+1):end).*pdiet((g+1):end));
  else
    pay = sum(cumz.*pdiet);
  end
  pay = pay/sum(pdiet);
  return;
end % function epay

% Returns difference in wealth equivalents between a contract z0,g0 and z1,g1
function [dw gdw]= wdiff(beta,z0,g0,z1,g1,q,vp,w0 )
  wlo = w0*(1-vp.frac);
  whi = w0*1.1;

  v0 = vf3(w0,z0,q,beta,g0,vp);
  weq0 = weq(v0,wlo,whi,q,beta,vp);
  for i=1:length(z1) 
    v1(i) = vf3(w0,z1(i),q,beta,g1(i),vp);
    weq1(i) = weq(v1(i),wlo,whi,q,beta,vp);
  end
  dw = weq0 - max(weq1);
  if (nargout>1)
    [w gdw] = max(weq1);
  end
end % function wdiff

