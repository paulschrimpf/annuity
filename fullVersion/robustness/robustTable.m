function robustTable(data,est,cfg,cfdata);

  % weq under asym info
  wa = zeros(size(cfdata.w0));
  for i=1:cfdata.N
    wa(i) = cfdata.weq(i,cfdata.oc(i));
  end
  if (isfield(cfdata,'frac'))
    frac = cfdata.frac;
  else 
    frac = cfg.cut.fracAnnuitized;
  end
  fprintf('average welfare under asym = %g\n',mean(wa));
  fprintf('  scaled so that w0 = 100, = %g\n',mean(wa./cfdata.w0)*100);
  [ws cs] = max(cfdata.s.weq,[],2);
  mean(cs==1) 
  mean(cs==2)
  mean(cs==3)
  fprintf('average welfare under symm = %g\n',mean(ws));
  fprintf('  scaled so that w0 = 100, = %g\n',mean(ws./cfdata.w0)*100);
  fprintf('abs diff = %g\n',mean((ws-wa)./(frac.*cfdata.w0))*6000); 
  fprintf('---------------------------------\n');
  lg = [0 5 10];
  for c=1:length(lg)
    fprintf('average welfare mandate %d = %g\n',lg(c),mean(cfdata.m.weq(: ...
                                                  ,c)));
    fprintf('   abs diff %d = %g\n',lg(c),mean((cfdata.m.weq(:,c)-wa)./ ...
                                                (frac.*cfdata.w0))*6000); 
  end
  
end
  
