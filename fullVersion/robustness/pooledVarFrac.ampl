# ampl command file for step 2 estimation
load amplfunc.dll; # load library of external functions
# declare model and data, these tell the solver which problem to solve and
# what parameter values to use
model annuityLike2dh.mod; # model file
let minSig := 1e-6;
let pMid := 0; 
data pooled.dat; # data file

# initialize cutoff grid
call {c in 1..nCut,d in 1..nG,m in 1..nCutMix} 
 setCut(nCut*nG*nCutMix,{i in 1..cutN} cutLogA[i],
        {i in 1..cutN} cutLogB[i,c,d,m],c+nCut*(d-1)+(m-1)*nCut*nG); 

#display cutLogA, cutLogB;
option solver snopt; # set the solver
                     # available choices are snopt,minos,loqo, and minos
                     # snopt seems most robust so far
option snopt_options 'timing 1 outlev=3'; # some solver options
option minos_options 'timing 1 outlev=3';
option loqo_options 'timing 1 outlev=3'; # some solver options
option minos_options 'outlev=3';

# intial values
set sigAinit := {0.1};
set muAinit := {-5.3};
set sigBinit := {0.01,0.1};
set muBinit := {10.25};

display {t in 1..nCut,d in 1..nG} t+nCut*(d-1);

# begin file of output
printf 'pooledNormal minos results\n' > pooled.csv;
printf ' ,initial,f60,m65,f65,m60,initial,final,initial,f60,m65,f65,m60,initial,final,initial,final\n' >> pooled.csv;
printf 'loglike, muA, , , , , sigA, , muB, , , , , sigB, , corr, ,\n' >> pooled.csv;
for {c in {-9,-5,5,9}} { 
 for {sa in sigAinit} {
  for {ma in muAinit} {
   for {sb in sigBinit} {
    for {mb in muBinit} {

  display pCut;
  fix{m in 1..nCutMix} pCut[m];
  let sigA := sa;
  let{d in 1..nG,m in 1..nCutMix} muA[d,m] := ma;        
  let sigB := sb;
  # initialize muB to fit b/t cutoffs at muA        
  let{d in 1..nG,m in 1..nCutMix} muB[d,m] 
   := 0.5*(cutoff(muA[d,m],1+nCut*(d-1)+(m-1)*nG*nCut)
          +cutoff(muA[d,m],2+nCut*(d-1)+(m-1)*nG*nCut));
  let corr := c/10;
          
  solve;

  display _solve_time;
  display logLikelihood,lambda,muA,sigA,muB,sigB,corr,pCut;
  # printf results
  printf '%25.16g,%25.16g', logLikelihood,ma >> pooled.csv;
  printf{d in 1..nG, m in 1..nCutMix} ',%25.16g', muA[d,m] >> pooled.csv;
  printf ',%25.16g,%25.16g,%25.16g', sa,sigA,mb >> pooled.csv;
  printf{d in 1..nG, m in 1..nCutMix} ',%25.16g', muB[d,m] >> pooled.csv;
  printf ',%25.16g,%25.16g,%25.16g,%25.16g', sb,sigB,c/10,corr >> pooled.csv; 
  printf{m in 1..nCutMix} ',%.16g', pCut[m] >> pooled.csv;
  printf '\n' >> pooled.csv;
}}}}}


