function [cfdata mms] = mmsTable(cfdata,cfg)
  % compute equilibrium total payments (used for calculating symmetric
  % info prices)
  
  % fill in missing fields of cfdata (needed for backwards compatibility
  % with old results)
  if ~isfield(cfdata,'w0')
    cfdata.w0 = ones(cfdata.N,1)*cfg.cut.w0;
  end
  
  for g=1:cfdata.ngroup
    totalPay(g) = 0.0; % total payments made in equilibrium
    for n=1:cfdata.N
      if(cfdata.group(n)==g)
        totalPay(g) = totalPay(g) + cfdata.epay(n,cfdata.oc(n));
      end
    end
  end

  for n=1:cfdata.N
    z_slope = cfg.cut.z(cfdata.group(n),:);
    % compute symmetric info price
    if isfield(cfdata,'frac')
      wa = cfdata.w0.*cfdata.frac;
    else
      wa = cfdata.w0;
    end
    zi = z_slope.*totalPay(g)./ ...
         (sum(cfdata.group==g)*cfdata.epay(n,:)) ...
         * wa(n)/mean(wa(cfdata.group==g));
    cfdata.s.mms(n) = 20*(zi(1)/zi(3)-1);
  end

  for g=1:cfdata.ngroup
    z = cfg.cut.z(g,:);
    mms(g) = 20*(z(1)/z(3)-1);
  end
  cfdata.mms = mms(cfdata.group);
end
