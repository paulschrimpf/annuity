function graphCutoffs(data,est,cut,prefix)
% Make a graph of the cutoffs with a plot of alpha and beta on top
  
  % plot cutoffs
  % logs
  [logAlpha logBeta] = sampleAlphaBeta(data,est);  
  

  ngroup = data.ngroup;
  
  color = 'rgbk';
  style = '.*x+';
  lstyle = {'-','--','-.',':'};
  cmd = sprintf('plot(logAlpha(data.group==1),logBeta(data.group==1),''%s%s''', ...
                color(1),style(1));
  g=1;
  cmd = [cmd sprintf([',cut(%d).la,cut(%d).lb(:,1),''%s%s'',' ...
                      'cut(%d).la,cut(%d).lb(:,2),''%s%s'''], ...
                     g,g,color(g),lstyle{g},g,g,color(g),lstyle{g})];
  for g=2:ngroup
    cmd = [cmd sprintf(',logAlpha(data.group==%d),logBeta(data.group==%d),''%s%s''', ...
                       g,g,color(g),style(g))];
    cmd = [cmd sprintf([',cut(%d).la,cut(%d).lb(:,1),''%s%s'',' ...
                        'cut(%d).la,cut(%d).lb(:,2),''%s%s'''], ...
                       g,g,color(g),lstyle{g},g,g,color(g),lstyle{g})];
  end
  cmd = [cmd ');'];
  out = [prefix '/figures/dist1.eps'];
  figure;
  hist(logAlpha);
  figure;
  hist(logBeta);
  figure;
  eval(cmd);
  axis([quantile(logAlpha,[0 1]) ...
        quantile(logBeta, [0 1]) ]);    
  ax = axis;
  %axis([-5.5 -3.5 0 16]);
  xlabel('log(\alpha)');
  ylabel('log(\beta)');
  print('-depsc2',out);


  cmd = '';
  for g=1:ngroup
    if (isempty(cmd))
      cmd =  sprintf(['plot(cut(%d).la,cut(%d).lb(:,1),''g'',' ...
                      'cut(%d).la,cut(%d).lb(:,2),''r'''],g,g,g,g);
    else
      cmd = [cmd sprintf([',cut(%d).la,cut(%d).lb(:,1),''g'',' ...
                          'cut(%d).la,cut(%d).lb(:,2),''r'''],g,g,g,g)];
    end
  end
  x = [ax(1) ax(2) ax(2) ax(1) ax(1)];
  y = [ax(3) ax(3) ax(4) ax(4) ax(3)];
  cmd = [cmd ',x,y,''k--'');'];
  figure;
  eval(cmd);
  axis([-9 -3 0 20]);
  xlabel('log(\alpha)');
  ylabel('log(\beta)');
  print('-depsc2',[prefix '/figures/cut.eps']);
  
  % levels
  %   out = [prefix '/figures/distLevel.eps'];
  %   figure;
  %   plot(exp(logAlpha),exp(logBeta),'.', ...
  %       exp(cut.la),exp(cut.lb(:,1)),exp(cut.la),exp(cut.lb(:,2)) );
  % %  axis([quantile(exp(logAlpha),[0.01 0.99]) ...
  % %        quantile(exp(logBeta), [0.01 0.99]) ]);    
  %   axis(exp([-5.5 -4 3 20]));
  %   xlabel('alpha');
  %   ylabel('beta');
  %   print('-depsc2',out);
  
  %----------------------------------------------------------------------
  for g=1:ngroup
    out = [prefix '/figures/cutLevel' num2str(g) '.eps'];
    figure;
    m = data.group==g;
    la = exp(logAlpha(m));
    lb = exp(logBeta(m));
    plot(la,lb,'.',exp(cut(g).la),exp(cut(g).lb(:,1)),exp(cut(g).la), ...
         exp(cut(g).lb(:,2)) );
    axis(exp([quantile(logAlpha,[0.0 1]) ...
          quantile(logBeta, [0.0 1]) ]));    
    xlabel('\alpha');
    ylabel('\beta');
    print('-depsc2',out);
    out

    out = [prefix '/figures/cutLevelNo' num2str(g) '.eps'];
    figure;
    m = data.group==g;
    la = exp(logAlpha(m));
    lb = exp(logBeta(m));
    plot(exp(cut(g).la),exp(cut(g).lb(:,1)),exp(cut(g).la), ...
         exp(cut(g).lb(:,2)) );
    axis(exp([quantile(logAlpha,[0.0 1]) ...
              quantile(logBeta, [0.0 1]) ]));    
    xlabel('\alpha');
    ylabel('\beta');
    print('-depsc2',out);
  end
end
