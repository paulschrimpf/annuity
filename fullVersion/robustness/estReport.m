function estReport(data,estin,prefix)
  % column labels
  i = 1;
  j = i;
  %%%%%%
  est = estin;
  if (strcmp(est.dist,'gamma') | strcmp(est.dist,'gamma-normal')) 
    [la lb] = sampleAlphaBeta(data,est);
    for g=1:size(data.alphaX,2)
      est.parm.gAlpha(g) = mean(la(data.alphaX(:,g)==1))
      est.parm.gBeta(g) =  mean(lb(data.alphaX(:,g)==1))
      est.parm.var = cov(la,lb);
      est.parm.corr = est.parm.var(1,2)/sqrt(est.parm.var(1,1)*est.parm.var(2,2));
    end
  end

  if (~strcmp(est.dist,'mixed-normal'))
    for k=1:length(est.parm.gAlpha)
      if (findstr('60 M',data.name.alphaX(k).s))
        str(i).s = '$\mu_{\alpha}^{m60}$';
      elseif (findstr('65 M',data.name.alphaX(k).s))
        str(i).s = '$\mu_{\alpha}^{m65}$';
      elseif (findstr('65 F',data.name.alphaX(k).s))
        str(i).s = '$\mu_{\alpha}^{f65}$';
      elseif (findstr('60 F',data.name.alphaX(k).s))
        str(i).s = '$\mu_{\alpha}^{f60}$';
      end
      i = i+1;    
    end
    i = j+numel(est.parm.gAlpha);
    parm(j:i-1) = est.parm.gAlpha(:);
    se(j:i-1) = est.parm.std.gAlpha(:);
    switch(est.dist)
     case {'normal','normal-flex','gamma','gamma-normal'}
      str(i).s = '$\sigma_{\alpha}$';
      parm(i) = sqrt(est.parm.var(1,1));
      if (isfield(est.parm.std,'stdDev'))
        se(i) = est.parm.std.stdDev(1);  
      else 
        % apply delta method to change std. err of variance into std err of
        % std dev
        se(i) = sqrt(0.25/est.parm.var(1,1))*est.parm.std.var(1,1);  
      end
     case {'gamma','gamma-normal'}
      str(i).s = '$\alpha$ shape';
      parm(i) = est.parm.ashape;
      se(i) = est.parm.std.ashape;
    end
    i = i+1;
    j = i;
    for k=1:length(est.parm.gBeta)
      if (findstr('60 M',data.name.alphaX(k).s))
        str(i).s = '$\mu_{\beta}^{m60}$';
      elseif (findstr('65 M',data.name.betaX(k).s))
        str(i).s = '$\mu_{\beta}^{m65}$';
      elseif (findstr('65 F',data.name.betaX(k).s))
        str(i).s = '$\mu_{\beta}^{f65}$';
      elseif (findstr('60 F',data.name.betaX(k).s))
        str(i).s = '$\mu_{\beta}^{f60}$';
      end
      i = i+1;
    end
    str(j).s = '$\mu_{\beta}$';
    i = j+numel(est.parm.gBeta);
    parm(j:i-1) = est.parm.gBeta(:);
    se(j:i-1) = est.parm.std.gBeta(:);
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','gamma'}
      str(i).s = '$\sigma_{\beta}$';
      parm(i) = sqrt(est.parm.var(2,2));
      if (isfield(est.parm.std,'stdDev'))
        se(i) = est.parm.std.stdDev(2);  
      else 
        % apply delta method to change std. err of variance into std err of
        % std dev
        se(i) = sqrt(0.25/est.parm.var(1,1))*est.parm.std.var(2,2);  
      end
     case 'gamma'
      str(i).s = '$\beta$ shape';
      parm(i) = est.parm.bshape;
      se(i) = est.parm.std.bshape;
    end
    i = i+1;
    switch(est.dist)
     case {'normal','gamma-normal','gamma'}
      str(i).s = '$\rho$';
      parm(i) = est.parm.corr;
      se(i) = est.parm.std.corr;
     case 'normal-flex'
      str(i).s = 'bla';
      parm(i) = est.parm.var(1,2);
      se(i) = 0;
    end
    i = i+1;          
  else % dist = 'mixed-normal'
    for m =1:length(est.parm)
      for k=1:length(est.parm(m).gAlpha)
        str(i).s = data.name.alphaX(k).s;
        i = i+1;
      end
      str(j).s = '$\mu_{\alpha}$';
      parm(j:i-1) = est.parm(m).gAlpha;
      se(j:i-1) = est.parm(m).std.gAlpha;
      
      str(i).s = '$\sigma_{\alpha}$';
      parm(i) = sqrt(est.parm(m).var(1,1));
      se(i) = 0.25/est.parm(m).var(1,1)*est.parm(m).std.var(1,1);  
      i = i+1;
      j = i;
      for k=1:length(est.parm(m).gBeta)
        str(i).s = data.name.betaX(k).s;
        i = i+1;
      end
      str(j).s = '$\mu_{\beta}$';
      parm(j:i-1) = est.parm(m).gBeta;
      se(j:i-1) = est.parm(m).std.gBeta;
      str(i).s = '$\sigma_{\beta}$';
      parm(i) = sqrt(est.parm(m).var(2,2));
      se(i) = 0.25/est.parm(m).var(2,2)*est.parm(m).std.var(2,2);
      i = i+1;
      str(i).s = '$\rho$';
      parm(i) = est.parm(m).corr;
      se(i) = est.parm(m).std.corr;
      i = i+1;
    end
  end
  str(i).s = '$\lambda$';
  parm(i) = est.lambda;
  if (isfield(est.parm.std,'lambda'))
    se(i) = est.parm.std.lambda;
  else
    % open first stage estimates to get lambda se
    firstStage = [prefix(1:3) 'Mort'];
    k = strfind(prefix,'_just');
    if (~isempty(k))
      firstStage = [firstStage prefix(k:k+4) '92'];
    end
    est2 = est;
    load(['../results/' firstStage 'Results']);
    est1 = est;
    est = est2;
    k = length(est1.parm.gAlpha);
    pm = zeros(k+2,1);
    pm(1:k) = est1.parm.gAlpha;
    k = k+1;
    pm(k) = log(sqrt(est1.parm.var(1,1)));
    k = k+1;
    pm(k) = log(est1.lambda);
    data1 = data;
    data1.alphaX = data.alphaX(:,1:length(est1.parm.gAlpha));
    [like g hess gi] = mortLike(pm,est1,data1);
    gi = gi - ones(data.N,1)*mean(gi);
    v = inv(gi'*gi);
    se(i) = sqrt(est.lambda*v(k,k)*est.lambda);
  end
  i = i+1;
  if (isfield(est.parm,'twodh'))
    for g=1:length(est.parm.twodh.gamma)
      str(i).s = sprintf('P \gamma = %.1g',est.parm.twodh.gamma(g));
      parm(i) = est.parm.twodh.p(g);
      se(i) = 0;
      i = i+1;
    end
  end
    
  out = [prefix '/tables/estimates.tex'];
  of = fopen(out,'w');
  fprintf(of,'Parameter & Estimate \\\\ \\hline \n');
  for c = 1:length(str),
    fprintf(of,'%s & %.4g',str(c).s, ...
            parm(c));
    if(se(c)>0)
      fprintf(of,' & (%.4g) \\\\ \n',se(c));
    else
      fprintf(of,' & \\\\ \n');
    end
  end;
  if(isfield(est,'cut'))
    fprintf(of,['\\hline \\multicolumn{3}{c}{\\footnotesize' ...
                '{Note: $N = %d$, $\\gamma = %.2g$, bequest $\\gamma = %.2g$'], ...
            data.N,est.cut(1).cfg.cut(1).gam,est.cut(1).cfg.cut(1).bgam);
    if(length(est.cut(1).cfg.cut.rho)==1)
      fprintf(of,',$r=%.2g$',1/est.cut(1).cfg.cut.rho-1);
    else
      lo = min(1./est.cut(1).cfg.cut.rho-1);
      hi = max(1./est.cut(1).cfg.cut.rho-1);
      fprintf(of,',$r=%.2g$-$%.2g$',lo,hi);
    end

    if(length(est.cut(1).cfg.cut.delta)==1)
      fprintf(of,',$\\delta=%.2g$',1/est.cut(1).cfg.cut.delta-1);
    else
      lo = min(1./est.cut(1).cfg.cut.delta-1);
      hi = max(1./est.cut(1).cfg.cut.delta-1);
      fprintf(of,',$\\delta=%.2g$-$%.2g$',lo,hi);
    end
    
    if(length(est.cut(1).cfg.cut.inflation)==1)
      fprintf(of,',$\\pi=%.2g$',est.cut(1).cfg.cut.inflation);
    else
      lo = min(est.cut(1).cfg.cut.inflation);
      hi = max(est.cut(1).cfg.cut.inflation);
      fprintf(of,',$\\pi=%.2g$-$%.2g$',lo,hi);
    end
    if(isfield(est.cut(1).cfg.cut,'alphaUncertainty') ...
       && est.cut(1).cfg.cut.alphaUncertainty>0) 
      fprintf(of,',$\\sigma_\\epsilon = %.2g$', ...
              est.cut(1).cfg.cut.alphaUncertainty)
    end
    if(isfield(est.cut(1).cfg.cut,'alphaBias') ...
       && est.cut(1).cfg.cut.alphaBias~=1) 
      fprintf(of,',$\\alpha$ bias $ = %.2g$', ...
              est.cut(1).cfg.cut.alphaBias)
    end
    
    fprintf(of,'}} \\\\ \n');
  end    
end
