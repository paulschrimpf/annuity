function cfdata = runCF(dataIn,est,cfg,doMandate)
  
  if (nargin<4)
    doMandate = true;
  end

  %global T bgam gam delta rho;
  cfdata = dataIn;
  guar = 0:5:10; % guarantee lengths
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  nchoice = length(guar);
  disp = 10;
  lambda = est.lambda;
  logAlpha = log(cfdata.alpha);
  if (isfield(cfg.cut,'raHetero'));
    logBeta = log(cfdata.gam);
  else
    logBeta = log(cfdata.beta);
  end


  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end


  % simulate choices, wealth equivalents, etc
  warning('Off','MATLAB:divideByZero');
  for n=1:cfdata.N
    if(isfield(cfdata,'w0'))
      w0 = cfdata.w0(n);
    end
    if isfield(cfdata,'gam') && length(cfdata.gam)>1
      gam = cfdata.gam(n);
      bgam = cfdata.bgam(n);
    end
    if isfield(cfdata,'frac')
      alpha2 = cfdata.frac(n);
    end

    z_slope = cfg.cut.z(cfdata.group(n),:);
    agemin = cfg.cut.agemin(cfdata.group(n));
    T = agemax - agemin;
    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(cfdata.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(cfdata.group(n));
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(cfdata.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(cfdata.group(n));
    end
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                  % rate factors    
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    galpha = exp(logAlpha(n));
    if isfield(cfdata,'alphaUncertainty');
      t1 = (agemin-base):(agemax-base+1);
      % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
      % compute by gauss-hermite quadrature
      n = 200; % make it very precise
      [n x w] = gqzero(n);
      x = x*(sqrt(2)*cfdata.alphaUncertainty);
      w = w/sqrt(pi);
      if (size(x,2)==1) %  change to row5D vectors
        x = x';
        w = w';
      end
      j = 1;
      for t=agemin-base:agemax-base+1;
        Mt(:,j) = sum(exp(galpha*exp(x)/lambda*(1 - exp(lambda*(t^h)))) ...
                      .*(ones(size(galpha))*w),2);
        j = j+1;
      end
      L = size(Mt,2);
      part1 = Mt(:,1:L-1);
      part2 = Mt(:,2:L);
    else 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    end    
    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    if (isfield(cfdata,'alphaBias') && cfdata.alphaBias ~= 1)
      mu = cfdata.alphaX(n,:)*est.parm.gAlpha;
      logAHat(n) = mu + cfdata.alphaBias*(logAlpha(n)-mu);
      alph = exp(logAHat(n));
      part22  = exp(alph/lambda*(1 - exp(lambda*(tp1.^h))));
      part12  = exp(alph/lambda*(1 - exp(lambda*(t.^h  ))));     
      qPerceived = (part12 - part22)./part12;
      qPerceived(part12==0) = 0;
    else
      logAHat(n) = logAlpha(n);
      qPerceived = q;
    end
    fr     = (part1(1:T) - part2(1:T))';          % density  

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;
    vp.alphaUncertainty=cfg.cut.alphaUncertainty;

    for c = 1:nchoice,        % guarantee choice
      Z= w0*(alpha2*z_slope(c)); % for computing e(pay)
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cfdata.val(n,c) = vf3(w0,z_slope(c),qPerceived,cfdata.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      cfdata.weq(n,c) = weq(cfdata.val(n,c),wlo,whi,qPerceived,cfdata.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        cfdata.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        cfdata.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    cfdata.epay(n,:) = cfdata.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Equilibrium: Done with %d of %d\n',n,cfdata.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');  
  lb = est.cut(1).lb;
  t = whos('lb');
  if (strcmp(t.class,'cell'))
    for g=1:cfdata.ngroup
      for w=1:length(est.cut(g).lb)
        if isfield(est.parm,'frac')
          m = (cfdata.group==g & cfdata.frac==est.cut(g).fracAnnuitized(w));
        elseif ~isfield(est.cut(g),'twodh') || isempty(est.cut(g).twodh)
          m = (cfdata.group==g & cfdata.w0==est.cut(g).w0(w)); 
        else
          m = (cfdata.group==g & cfdata.gam'==est.cut(g).twodh.gamma(w)); 
        end
        e(m,1) = interp1(est.cut(g).la,est.cut(g).lb{w}(:,1), ...
                         logAlpha(m),'linear','extrap');
        e(m,2) = interp1(est.cut(g).la,est.cut(g).lb{w}(:,2), ...
                         logAlpha(m),'linear','extrap');
        e(isnan(e)) = Inf;
      end   
    end
  else
    for g=1:cfdata.ngroup
      m = cfdata.group==g;
      e(m,1) = interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                       logAHat(m),'linear','extrap');
      e(m,2) = interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                       logAHat(m),'linear','extrap');
      e(isnan(e)) = Inf;
    end
  end
  if (size(logBeta,1)==1)
    logBeta = logBeta'
  end
  oc = 1*(logBeta<=e(:,1)) + 2*(logBeta>e(:,1) & logBeta<e(:,2)) + ...
       3*(logBeta>=e(:,2));
  [m moc] = max(cfdata.val,[],2);

  fprintf('Choice messed up %d times\n',sum(oc~=moc));
  fprintf('   %10d %10d %10d\n',1,2,3)
  fprintf('%2d %10d %10d %10d\n',1,sum(oc==1 & moc==1), ...
          sum(oc==1 & moc==2),sum(oc==1 & moc==3));
  fprintf('%2d %10d %10d %10d\n',2,sum(oc==2 & moc==1), ...
          sum(oc==2 & moc==2),sum(oc==2 & moc==3));
  fprintf('%2d %10d %10d %10d\n',3,sum(oc==3 & moc==1), ... 
          sum(oc==3 & moc==2),sum(oc==3 & moc==3));

  cfdata.oc=oc;
  for n =1:cfdata.N
    cfdata.g(n) = guar(oc(n));
  end

  %----------------------------------------------------------------------
  % simulate mandatory guarantee lengths
  
  % determine annuity payment rates
  for g=1:cfdata.ngroup
    totalPay(g) = 0.0; % total payments made in equilibrium
    for n=1:cfdata.N
      if(cfdata.group(n)==g)
        totalPay(g) = totalPay(g) + cfdata.epay(n,oc(n));
      end
    end
    mand_zslope(g,:) = cfg.cut.z(g,:).* ...
        totalPay(g)./sum(cfdata.epay(cfdata.group==g,:),1);
  end
if(doMandate)
  mand_zslope
  % simulate choices, wealth equivalents, etc
  warning('Off','MATLAB:divideByZero');
  for n=1:cfdata.N
    if(isfield(cfdata,'w0'))
      w0 = cfdata.w0(n);
    end
    if isfield(cfdata,'gam') && length(cfdata.gam)>1
      gam = cfdata.gam(n);
      bgam = cfdata.bgam(n);
    end
    if isfield(cfdata,'frac')
      alpha2 = cfdata.frac(n);
    end

    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(cfdata.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(cfdata.group(n));
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(cfdata.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(cfdata.group(n));
    end

    agemin = cfg.cut.agemin(cfdata.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    galpha = exp(logAlpha(n));
    if isfield(cfdata,'alphaUncertainty');
      t1 = (agemin-base):(agemax-base+1);
      % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
      % compute by gauss-hermite quadrature
      n = 200; % make it very precise
      [n x w] = gqzero(n);
      x = x*(sqrt(2)*cfdata.alphaUncertainty);
      w = w/sqrt(pi);
      if (size(x,2)==1) %  change to row5D vectors
        x = x';
        w = w';
      end
      j = 1;
      for t=agemin-base:agemax-base+1;
        Mt(:,j) = sum(exp(galpha*exp(x)/lambda*(1 - exp(lambda*(t^h)))) ...
                      .*(ones(size(galpha))*w),2);
        j = j+1;
      end
      L = size(Mt,2);
      part1 = Mt(:,1:L-1);
      part2 = Mt(:,2:L);
    else 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    end    

    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    if (isfield(cfdata,'alphaBias') && cfdata.alphaBias ~= 1)
      mu = cfdata.alphaX(n,:)*est.parm.gAlpha;
      logAHat(n) = mu + cfdata.alphaBias*(logAlpha(n)-mu);
      alph = exp(logAHat(n));
      part22  = exp(alph/lambda*(1 - exp(lambda*(tp1.^h))));
      part12  = exp(alph/lambda*(1 - exp(lambda*(t.^h  ))));     
      qPerceived = (part12 - part22)./part12;
      qPerceived(part12==0) = 0;
    else
      logAHat(n)  = logAlpha(n);
      qPerceived = q;
    end

    fr     = (part1(1:T) - part2(1:T))';          % density  

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = cfdata.gam(n);
    vp.bgam = cfdata.bgam(n);
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;

    pubrate = pubAnnuityRate(infl,rho,q);
    for c = 1:nchoice,        % guarantee choice
      Z = w0*(alpha2*mand_zslope(cfdata.group(n),c));
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cfdata.m.val(n,c) = vf3(w0,mand_zslope(cfdata.group(n),c),qPerceived, ...
                              cfdata.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      cfdata.m.weq(n,c) = weq(cfdata.m.val(n,c),wlo,whi,qPerceived,cfdata.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        cfdata.m.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        cfdata.m.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    cfdata.m.epay(n,:) = cfdata.m.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Mandate: Done with %d of %d\n',n,cfdata.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
end % if doMandate

  %----------------------------------------------------------------------
  % symmetric information
  % simulate choices, wealth equivalents, etc
  warning('Off','MATLAB:divideByZero');
  %sumPay = sum(totalPay);
  for n=1:cfdata.N
    if(isfield(cfdata,'w0'))
      w0 = cfdata.w0(n);
    end
    if isfield(cfdata,'gam') && length(cfdata.gam)>1
      gam = cfdata.gam(n);
      bgam = cfdata.bgam(n);
    end
    if isfield(cfdata,'frac')
      alpha2 = cfdata.frac(n);
    end

    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(cfdata.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(cfdata.group(n));
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(cfdata.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(cfdata.group(n));
    end

    z_slope = cfg.cut.z(cfdata.group(n),:);
    agemin = cfg.cut.agemin(cfdata.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    galpha = exp(logAlpha(n));
    if isfield(cfdata,'alphaUncertainty');
      t1 = (agemin-base):(agemax-base+1);
      % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
      % compute by gauss-hermite quadrature
      n = 200; % make it very precise
      [n x w] = gqzero(n);
      x = x*(sqrt(2)*cfdata.alphaUncertainty);
      w = w/sqrt(pi);
      if (size(x,2)==1) %  change to row5D vectors
        x = x';
        w = w';
      end
      j = 1;
      for t=agemin-base:agemax-base+1;
        Mt(:,j) = sum(exp(galpha*exp(x)/lambda*(1 - exp(lambda*(t^h)))) ...
                      .*(ones(size(galpha))*w),2);
        j = j+1;
      end
      L = size(Mt,2);
      part1 = Mt(:,1:L-1);
      part2 = Mt(:,2:L);
    else 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    end    

    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    if (isfield(cfdata,'alphaBias') && cfdata.alphaBias ~= 1)
      mu = cfdata.alphaX(n,:)*est.parm.gAlpha;
      logAHat(n) = mu + cfdata.alphaBias*(logAlpha(n)-mu);
      alph = exp(logAHat(n));
      part22  = exp(alph/lambda*(1 - exp(lambda*(tp1.^h))));
      part12  = exp(alph/lambda*(1 - exp(lambda*(t.^h  ))));     
      qPerceived = (part12 - part22)./part12;
      qPerceived(part12==0) = 0;
    else
      logAHat(n) = logAlpha(n);
      qPerceived = q;
    end

    fr     = (part1(1:T) - part2(1:T))';          % density  
    g = cfdata.group(n);
    if isfield(cfdata,'frac')
      wa = cfdata.w0.*cfdata.frac;
    else
      wa = cfdata.w0;
    end
    zi = z_slope.*totalPay(g)./ ...
         (sum(cfdata.group==g)*cfdata.epay(n,:)) ...
         * wa(n)/mean(wa(cfdata.group==g));
    clear wa;
    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = cfdata.gam(n);
    vp.bgam = cfdata.bgam(n);
    vp.pub = pub;
    vp.frac = alpha2;
    vp.infl = infl;
    
    for c = 1:nchoice,        % guarantee choice
      Z = w0*(alpha2*zi(c)); % formula for annuity pricing
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cfdata.s.val(n,c) = vf3(w0,zi(c),qPerceived,cfdata.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      cfdata.s.weq(n,c) = weq(cfdata.s.val(n,c),wlo,whi,qPerceived,cfdata.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        cfdata.s.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        cfdata.s.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    cfdata.s.epay(n,:) = cfdata.s.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Symmetric: Done with %d of %d\n',n,cfdata.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
  [m soc] = max(cfdata.s.val,[],2);
  cfdata.s.oc = soc;
  for n =1:cfdata.N
    cfdata.s.g(n) = guar(soc(n));
  end
end % function runCF()

