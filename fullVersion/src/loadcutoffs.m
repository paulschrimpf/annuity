function cutout = loadcutoffs(cfg)
% Load/create cutoffs.
  ngroup = length(cfg.data.group);
  found = false*ones(ngroup,1);
  
  if (isfield(cfg.cut,'twodh') && isstruct(cfg.cut.twodh))
    pcfg = cfg;
    pcfg.cut.twodh = [];
    for g=1:length(cfg.cut.twodh.gamma)
      pcfg.cut.gam = cfg.cut.twodh.gamma(g);
      pcfg.cut.bgam = cfg.cut.twodh.gamma(g);
      pcut{g} = loadcutoffs(pcfg);
    end
    cutout = pcut{1};
    for c=1:length(cutout) 
      cutout(c).lb = cell(1,length(pcut));
      cutout(c).lb_orig = cell(1,length(pcut));
      for g=1:length(pcut)
        cutout(c).lb{g} = pcut{g}(c).lb;
        cutout(c).lb_orig{g} = pcut{g}(c).lb_orig;
      end
      cutout(c).twodh = cfg.cut.twodh;
    end
    return;
  elseif (numel(cfg.cut.fracAnnuitized)>1)
    pcfg = cfg;
    pcfg.cut.wp = 1;
    for g=1:length(cfg.cut.fracAnnuitized)
      pcfg.cut.fracAnnuitized = cfg.cut.fracAnnuitized(g);
      pcut{g} = loadcutoffs(pcfg);
    end
    cutout = pcut{1};
    for c=1:length(cutout) 
      cutout(c).lb = cell(1,length(pcut));
      cutout(c).lb_orig = cell(1,length(pcut));
      for g=1:length(pcut)
        cutout(c).lb{g} = pcut{g}(c).lb;
        cutout(c).lb_orig{g} = pcut{g}(c).lb_orig;
      end
      cutout(c).fracAnnuitized = cfg.cut.fracAnnuitized;
    end
    return;
  end

    
  if (cfg.mort.lambda ~= cfg.cut.lambda) 
    error('cfg.mort.lambda ~= cfg.cut.lambda');
  end
  
  if (exist(cfg.cut.file)==2) 
    eval(['load ' cfg.cut.file]);
    % look for correct prices
    fc = false*ones(length(cut),1);
    for c = 1:length(cut)
      if(~isfield(cut(c),'public') || isempty(cut(c).public))
        cut(c).public = 0;
      end
      for g=1:ngroup
        if (cutEqualcfg(cut(c),cfg,g))
          cutout(g) = cut(c);
          found(g) = true;
          fc(c) = true;
        end
      end
    end
  else
    cut = [];
  end

  if(isfield(cfg.cut,'redo') && cfg.cut.redo)
    fprintf('WARNING: forcing recalculation of cutoffs\n');
    cut = cut(~fc);
    found(:) = false;
  end
  
  if (sum(found)<ngroup) % need to calculate new cutoffs
    fprintf('%d cutoffs not found in the file, "%s"\n', ...
            sum(~found),cfg.cut.file);
    fprintf(['Calculating the missing cutoffs and appending them to the' ...
             ' file\n']);
    cutcfg = cfg;
    cutcfg.cut.z = cfg.cut.z(~found,:);
    cutcfg.cut.agemin = cfg.cut.agemin(~found);
    cutcfg.cut.rho = cfg.cut.rho(~found);
    cutcfg.cut.delta = cfg.cut.delta(~found);
    cutcfg.cut.inflation = cfg.cut.inflation(~found);
    path(path,'cutoffs');
    if (isfield(cutcfg.cut,'raHetero'))
      newcut = findgcut(cutcfg);
    else
      newcut = findbcut(cutcfg);
    end
    % save new cutoffs
    if (exist(cfg.cut.file)==2) 
      eval(['load ' cfg.cut.file]);
      if(isfield(cfg.cut,'redo') && cfg.cut.redo)
        cut = cut(~fc);
      end
    else
      cut = [];
    end
    % add zeta field to existing cutoffs
    for c=1:length(cut)
      if (~isfield(cut(c),'zeta'))
        cut(c).zeta = 1;
      end
      if(~isfield(cut(c),'public') || isempty(cut(c).public))
        cut(c).public = 0;
      end
      if(~isfield(cut(c),'alphaUncertainty') || ...
         isempty(cut(c).alphaUncertainty))
        cut(c).alphaUncertainty = 0;
      end
    end
    cut = [cut reshape(newcut,1,size(newcut,1)*size(newcut,2))];
    newcut
    save tempcut newcut;
    %cutEqualcfg(newcut,cfg,1)
    eval(['save ' cfg.cut.file ' cut']);
    % finish looking up cutoffs
    % look for correct prices
    for c = 1:length(cut)
      for g=1:ngroup
        if (cutEqualcfg(cut(c),cfg,g))
          cutout(g) = cut(c);
          found(g) = true;
        end
      end
    end
  end
  
  if(sum(found)<ngroup)
    sum(found)
    ngroup
    error('Failed to calculate all cutoffs');
  end
  
  for g=1:ngroup
    lb = cutout(g).lb;
    t = whos('lb');
    if (strcmp(t.class,'cell') && length(cutout(g).w0)==1)
      cutout(g).lb = cutout(g).lb{1};
      cutout(g).lb_orig = cutout(g).lb_orig{1};
    end            
  end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
function eq = cutEqualcfg(cut,cfg,g)
  % returns true if cut matches cfg for group g
  eq = ((cut.lambda == cfg.cut.lambda) && ...
        (sum(cut.z==cfg.cut.z(g,:))==3) && ...
        (length(cut.la)==length(cfg.cut.la)) && ...
        (sum(cut.la==cfg.cut.la))==length(cfg.cut.la) && ...
        (cut.agemin == cfg.cut.agemin(g)) && ...
        (cut.cfg.cut.gam == cfg.cut.gam) && ...
        (cut.cfg.cut.bgam == cfg.cut.bgam) && ...
        (cut.cfg.cut.fracAnnuitized == cfg.cut.fracAnnuitized) && ...
        (cut.public == cfg.cut.public));
   if (length(cfg.cut.rho)>1)
     eq = (eq &&  ...
           cut.rho == cfg.cut.rho(g));
   else
     eq = (eq &&  ...
           cut.rho == cfg.cut.rho);
   end
   if (length(cfg.cut.delta)>1)
     eq = (eq &&  ...
           cut.delta == cfg.cut.delta(g));
   else
     eq = (eq &&  ...
           cut.delta == cfg.cut.delta);
   end
   if (length(cfg.cut.inflation)>1)
     eq = (eq &&  ...
           cut.inflation == cfg.cut.inflation(g));
   else
     eq = (eq &&  ...
           cut.inflation == cfg.cut.inflation);
   end
   if(isfield(cfg.mort,'fasthazard') && ~isfield(cut.cfg.mort, ...
                                                 'fasthazard'))
     cut.cfg.mort.fasthazard=1;
   end
   eq = eq && isequal(cut.cfg.mort,cfg.mort);
   if (~isfield(cut,'zeta'))
     cut.zeta = 1;
   end
   if (~isfield(cfg.cut,'zeta'))
     cfg.cut.zeta = 1;
   end
%   if (eq)
%     [cut.zeta cfg.cut.zeta]
%   end
   eq = eq && (cut.zeta==cfg.cut.zeta);

   if(isfield(cfg.cut,'raHetero'))
     if (length(cfg.cut.raHetero.beta)>1)
       eq = eq && isfield(cut,'beta') && ...
            cut.beta == cfg.cut.raHetero.beta(g);
     else
       eq = eq && isfield(cut,'beta') && ...
            cut.beta == cfg.cut.raHetero.beta;
     end
     %eq = 1;
   end

   if (~isfield(cut,'w0') || isempty(cut.w0))
     cut.w0 = 100;
     cut.wp = 1;
   end
   if (~isfield(cfg.cut,'wp'))
     cfg.cut.wp = 1;
   end
   if (eq)
     cut.w0
     cfg.cut.w0
     cut.wp 
     cfg.cut.wp
   end

   if(~isfield(cut,'alphaUncertainty') || ...
      isempty(cut.alphaUncertainty))
     cut.alphaUncertainty = 0;
   end

  if(~isfield(cfg.cut,'alphaUncertainty') || ...
      isempty(cfg.cut.alphaUncertainty))
    cfg.cut.alphaUncertainty = 0;
  end
  eq = eq && cut.alphaUncertainty==cfg.cut.alphaUncertainty;

  if (~isfield(cut,'twodh'))
    cut.twodh = [];
  end
  if (~isfield(cfg.cut,'twodh'))
    cfg.cut.twodh = [];
  end
  eq = eq && isequal(cut.twodh,cfg.cut.twodh);
  
  eq = eq && (length(cut.w0)==length(cfg.cut.w0) ...
              && all(cut.w0==cfg.cut.w0) ...
              && all(cut.wp==cfg.cut.wp));
end     
%--- END function cutEqualcfg() --------------------------------------
