function [like grad hess gi] = annuityLike(parm,est,data)
% this function is just a frontend that organizes stuff
  persistent neval likeLast gradLast ngradeval;
  if (nargin==2) % for compatibility with ucminf.
    % est is a cell array 
    data = est{2};
    est = est{1};
  end
  if (isempty(neval))
    neval = 0;
    likeLast = 0;
    gradLast = zeros(size(parm));
    ngradeval = 0;
  end
    
  switch (est.dist)
   case 'mixed-normal'
    nmix = length(est.parm);
    like_i = zeros(data.N,1);
    gi = zeros(length(parm),data.N);
    est.parm = transformParam(parm,'unpack','mle',est.dist);
    for m=1:nmix
      em = est;
      em.dist = 'normal';
      em.parm = []; em.parm=est.parm(m);
      pm = transformParam(em.parm,'pack','mle','normal');
      np = length(pm);
      [lm gim] = likelihood(pm,em,data);
      pm = est.parm(m).p;
      like_i = like_i + lm*pm;
      gi( ((m-1)*np+1):m*np,:) = gim*pm;
      if (m<nmix)
        gi(length(parm)-nmix+m+1,:) = gi(length(parm)-nmix+m+1,:) ...
            + lm'*pm;            
      end
      gi((length(parm)-nmix+2):length(parm),:) =  ...
          gi((length(parm)-nmix+2):length(parm),:) - ...
          (pm*[est.parm(1:(nmix-1)).p]'*lm');
    end

   otherwise
    est.parm = transformParam(parm,'unpack','mle',est.dist);
    if(nargout<2) % call that actually calculates the likelihood
      like_i = likelihood(parm,est,data);
    else 
      [like_i gi] = likelihood(parm,est,data);
    end
    
    % add middle pickers 
    if (isfield(est,'dumb') && est.dumb.p>0)
      dumbest = est;
      xtraparm = 0;
      if(isfield(est.parm,'dumb'))
        if (isfield(est.parm.dumb,'gAlpha'))
          xtraparm = xtraparm + length(est.parm.dumb.gAlpha);
          dumbest.parm.gAlpha = est.parm.dumb.gAlpha;
        end
        if (isfield(est.parm.dumb,'var'))
          xtraparm = xtraparm + 1;
          dumbest.parm.var(1,1) = est.parm.dumb.var;
        end
        if (isfield(est.parm.dumb,'p'))
          xtraparm = xtraparm + 1;
          est.dumb.p = est.parm.dumb.p;
        end
      end 
      if (nargout<2)
        dlike_i = likelihoodMort(parm,dumbest,data);
        like_i = like_i*(1-est.dumb.p);
        m = data.g==5;
        like_i(m) = like_i(m) + est.dumb.p*dlike_i(m);
      else 
        slike_i = like_i;
        [dlike_i dgi] = likelihoodMort(parm,dumbest,data);
        like_i = like_i*(1-est.dumb.p);
        m = data.g==5;
        like_i(m) = like_i(m) + est.dumb.p*dlike_i(m);
        
        if(isfield(est.parm,'dumb') && isfield(est.parm.dumb,'p'))
          gi = gi(1:length(parm)-1,:);
          dgi = dgi(1:length(parm)-1,:);
        end

        if(xtraparm>0)
          np = length(parm)-xtraparm;
          if (isfield(est.parm.dumb,'gAlpha'))
            j = length(est.parm.dumb.gAlpha);
            dgAi = dgi(1:j,:);
            dgi(1:j,:) = 0;
          else
            dgAi = [];
          end
          if (isfield(est.parm.dumb,'var'))
            j = np-2;
            dgVi = dgi(j,:);
            dgi(j,:) = 0;
          else 
            dgVi = [];
          end
          dgi = [dgi(1:np,:); dgAi; dgVi];
        end % xtraparm > 0
        gi = gi*(1-est.dumb.p);
        gi(:,m) = gi(:,m) + est.dumb.p*dgi(:,m);
        
        if (isfield(est.parm.dumb,'p'))
          ex = exp(parm(length(parm)));
          dpdx = 2*ex/(ex+1)^2;
          gi = [gi; (-slike_i + (data.g==5).*dlike_i)'*dpdx ];
        end
      end
    end
  end

  like = -sum(log(like_i));
  if (isnan(like))
    fprintf('ERROR: log likelihood isnan\n');
  end
  
  if(nargout>1) % the gradient
    ngradeval = ngradeval+1;
    gi = gi./(ones(size(gi,1),1)*like_i'); 
    grad = -sum(gi,2); 
    % since we're finding min(-loglikelihood)
  else
    grad = [];
  end% if nargout>1
  
  if(nargout>2)
    warning('Hessian not implemented');
  end
  hess = [];
  
  if(~isreal(like) || sum(isnan(grad))>0)
    est.parm.var
    grad
    like
    like = likeLast*2;
    grad = gradLast*0.5;
  else
    likeLast = like;
    gradLast = grad;
  end
  
  neval = neval + 1;
  % penalize for high corr on early evaluations - makes more stable
  if (strcmp(est.dist,'normal') && ngradeval<est.penalty)
    lp = length(parm);
    d = max(ngradeval^2,1);
    cp = parm(lp); % correlation parameter
    fprintf('penalty = %g\n',exp(cp^2)/d);
    like = like + exp(cp^2)/d;
    if (nargout>1)
      grad(lp) = grad(lp) + exp(cp^2)*2*cp/d;
    end
  end
  
  if(mod(neval,est.disp)==0)
    est.parm = transformParam(parm,'unpack','mle',est.dist);
    fprintf('%d function evaluations\n',neval);
    fprintf('f = %20.16g\n',like);
    fprintf('parameter values:\n');
    switch (est.dist) 
     case {'normal'}
      fprintf(' gamma alpha = %8.6g\n',est.parm.gAlpha);
      fprintf(' gamma beta  = %8.6g\n',est.parm.gBeta);
      fprintf(' var log alpha  = %8.6g\n',est.parm.var(1,1));
      fprintf(' var log beta   = %8.6g\n',est.parm.var(2,2));
      fprintf(' cov log a,b    = %8.6g\n',est.parm.var(1,2));
      fprintf(' corr log a,b   = %8.6g\n',est.parm.corr);
      if(isfield(est.parm,'dumb'))
        if(isfield(est.parm.dumb,'gAlpha'))
          fprintf(' dumb g alpha = %8.6g\n',est.parm.dumb.gAlpha);
        end
        if(isfield(est.parm.dumb,'var'))
          fprintf(' dumb var a   = %8.6g\n',est.parm.dumb.var);
        end
        if(isfield(est.parm.dumb,'p'))
          fprintf(  'dumb p      = %8.6g\n',est.parm.dumb.p);
        end
      end
     case 'normal-flex'
      fprintf(' gamma alpha = %8.6g\n',est.parm.gAlpha);
      fprintf(' gamma beta  = %8.6g\n',est.parm.gBeta);
      fprintf(' var log alpha  = %8.6g\n',est.parm.var(1,1));
      fprintf(' var log beta   = %8.6g\n',est.parm.var(2,2));
      for d=2:size(est.parm.var,2)
        fprintf(' condbeta     = %8.6g\n',est.parm.var(1,d));
      end
     case 'gamma'
      fprintf(' gamma alpha = %8.6g\n',est.parm.gAlpha);
      fprintf(' gamma beta  = %8.6g\n',est.parm.gBeta);
      fprintf(' alpha shape    = %8.6g\n',est.parm.ashape);
      fprintf(' beta shape     = %8.6g\n',est.parm.bshape);
      fprintf(' coeff          = %8.6g\n',est.parm.corr);
     case 'gamma-normal'
      fprintf(' gamma alpha = %8.6g\n',est.parm.gAlpha);
      fprintf(' gamma beta  = %8.6g\n',est.parm.gBeta);
      fprintf(' alpha shape    = %8.6g\n',est.parm.ashape);
      fprintf(' var log beta   = %8.6g\n',est.parm.var(2,2));
      fprintf(' coeff          = %8.6g\n',est.parm.corr);
     case 'mixed-normal'
      for m=1:length(est.parm)
        fprintf(' gamma alpha = %8.6g\n',est.parm(m).gAlpha);
        fprintf(' gamma beta  = %8.6g\n',est.parm(m).gBeta);
        fprintf(' var log alpha  = %8.6g\n',est.parm(m).var(1,1));
        fprintf(' var log beta   = %8.6g\n',est.parm(m).var(2,2));
        fprintf(' cov log a,b    = %8.6g\n',est.parm(m).var(1,2));
        fprintf(' corr log a,b   = %8.6g\n',est.parm(m).corr);
        fprintf(' p              = %8.6g\n',est.parm(m).p);        
      end
    end
    if (nargout>1)
      fprintf('gradient = ');
      for k=1:length(grad)
        fprintf(' %.4g ',grad(k));
        if(mod(k,3)==0)
          fprintf('\n         = ');
        end
      end
      fprintf('\n');
    end
  end % if (mod(neval,est.disp)==0)
end % fucntion annuityLike()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parts of the likelihood
function [like_i gi] = likelihood(parm,est,data)
% compute the likelihood and gradient for each person.  (note: not the
% log likelihood.  the likelihood.  this makes it convenient to allow
% mixed distributions)

  persistent ef_el el mef_el;
  
  lambda = est.lambda;      
  % initialize persistent vars
  if(isempty(ef_el) || size(ef_el,2)~=est.int.n ...
     || size(ef_el,1)~=data.N)
    if (any(data.lage<data.fage))
      error('last age is less than first age');
    end
    if (any(data.lage==data.fage))
      fprintf('WARNING: %d last age = first age\n',sum(data.lage== ...
                                                       data.fage));
      m = data.lage==data.fage;
      data.lage(m) = data.lage(m) + 0.5/365.25;
    end
    if(isfield(est,'fasthazard'))
      h = est.fasthazard;
    else
      h = 1;
    end
    ef_el =  exp(lambda*data.fage.^h) - ...
             exp(lambda*data.lage.^h);
    ef_el = ef_el*ones(1,est.int.n);
    el = (exp(lambda*data.lage.^h).*data.lage.^(h-1)*h)*ones(1,est.int.n);
    if(~isempty(data.mort))
      error('option data.mort no longer implemented');
      mef_el =  exp(lambda*data.mort.fage) - ...
                exp(lambda*data.mort.lage);
      mef_el = mef_el*ones(1,est.int.n);
    end
  end % if (isempty..)
  % unpack parameters
  %est.parm = transformParam(parm,'unpack','mle',est.dist);
  % get integration points
  [logAlpha alpha wp condmu condsig] = intPoints(parm,est,data);
  % calculate cutoffs
  if (nargout<2)
    [e pc] = calculateCuts(est,data,logAlpha,condmu,condsig);  
  else % need more stuff for gradient
    [e pc dc dpc cut_m_mu dscale dshape] = ...
        calculateCuts(est,data,logAlpha,condmu,condsig);
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if (isfield(est.cut(1),'alphaUncertainty') && est.cut(1).alphaUncertainty ...
      ~= 0)
    % m changes because of alpha uncertainty
    % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
    % compute by gauss-hermite quadrature
    n = est.int.n;
    [n xeps weps] = gqzero(n);
    xeps = xeps*(sqrt(2)*est.cut(1).alphaUncertainty);
    weps = weps/sqrt(pi);      
    %x = est.int.x*est.cut(1).alphaUncertainty;
    %w = est.int.w.*normpdf(est.int.x,0,1);
    sum(weps)
    if (size(xeps,2)==1) %  change to row vectors
      xeps = xeps';
      weps = weps';
    end
    likeA = zeros(size(ef_el));
    for j=1:length(wp)
      m = ~data.died;
      likeA(m,j)=likeA(m,j)+sum(exp((alpha(m,j)/lambda.*ef_el(m,j)) ...
                                    *exp(xeps)).* ...
                                (ones(sum(m),1)*weps),2);
      m = data.died;
      likeA(m,j)=likeA(m,j)+sum(exp((alpha(m,j)/lambda.*ef_el(m,j)) ...
                                    *exp(xeps)).* ...
                                ((alpha(m,j).*el(m,j))*exp(xeps)).* ...
                                (ones(sum(m),1)*weps),2);
    end      
    likeA = likeA.*(ones(data.N,1)*wp');
    likeMort = likeA;
  else % no alpha uncertainty
    likeA = exp(alpha/lambda.*ef_el); 
    likeA(data.died,:) = alpha(data.died,:).*el(data.died,:).* ...
      likeA(data.died,:);
    likeA = likeA.*(ones(data.N,1)*wp');
    likeMort = likeA; % mortality part of likelihood
  end % alpha uncertainty
  m = data.g==0;
  likeA(m,:) = pc(m,:,1);
  m = data.g==5;
  likeA(m,:) = (pc(m,:,2) - pc(m,:,1));
  m = data.g==10;
  likeA(m,:) = (1-pc(m,:,2));
  
  likeChoice = likeA; % choice part of likelihood
  
  lci= sum(likeChoice.*(ones(data.N,1)*wp'),2);
  lci(lci==0) = realmin;
  fprintf('likeChoice = %.16g\n',sum(log(lci)));
  lmi= sum(likeMort,2);
  lmi(lmi==0) = realmin;
  fprintf('likeMort = %.16g\n',sum(log(lmi)));

  likeA = likeMort.*likeChoice;

  like_i = sum(likeA,2);
  like_i(like_i==0) = realmin;

  fprintf('loglike = %.16g\n',sum(log(like_i(1:10))));

  for i=1:20 
    fprintf('%3d %.16g\n',i,log(like_i(i)));
  end

  if(sum(isnan(like_i)))
    fprintf('WARNING: %d like_i isnan\n',sum(isnan(like_i)));
  end
  
  if(nargout>1) % the gradient
    % derivatives of cutoffs were calculated above
    dldgb = zeros(data.N,est.int.n,length(est.parm.gBeta));
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
      % derivative wrt gamma beta
      m = data.g==0;
      dldgb(m,:,1) = -likeMort(m,:).*dpc(m,:,1);
      condvar = condsig^2;
      % derivative wrt sigma beta
      sigb = sqrt(est.parm.var(2,2));
      siga = sqrt(est.parm.var(1,1));
      dldsb(m,:) = dldgb(m,:,1).*cut_m_mu(m,:,1)./condvar *...
          sigb;
      m = data.g==5;
      dldgb(m,:,1) = likeMort(m,:).*(dpc(m,:,1)-dpc(m,:,2));
      dldsb(m,:) = likeMort(m,:).*( dpc(m,:,1).*cut_m_mu(m,:,1) - ...
                                    dpc(m,:,2).*cut_m_mu(m,:,2)) ...
          ./condvar * sigb;
      m = data.g==10;
      dldgb(m,:,1) = likeMort(m,:).*dpc(m,:,2);
      dldsb(m,:) = likeMort(m,:).*(dpc(m,:,2).*cut_m_mu(m,:,2)) ...
          ./condvar * sigb;
     case 'gamma'
      m = data.g==0;
      dldgb(m,:,1) = dscale(m,:,1);
      dldsb(m,:) = dshape(m,:,1);
      m = data.g==5;
      dldgb(m,:,1) = dscale(m,:,2)-dscale(m,:,1);
      dldsb(m,:) = dshape(m,:,2)-dshape(m,:,1);
      m = data.g==10;
      dldgb(m,:,1) = -dscale(m,:,2);
      dldsb(m,:) = -dshape(m,:,2);
      
      dldgb(:,:,1) = dldgb(:,:,1).*likeMort.*exp(condmu);
      dldsb = dldsb.*likeMort;
    end % switch(est.dist) 
    
    switch(est.dist)
     case 'normal'
      % derivative wrt sigma_alpha,beta
      m = data.g==0;
      dldcov(m,:) = (-ones(sum(m),1)*est.int.x' / ...
                     siga + ...  
                     cut_m_mu(m,:,1)./ ...
                     condvar*(est.parm.var(1,2)/...
                              est.parm.var(1,1))).* ...
          likeMort(m,:).*dpc(m,:,1);    
      m = data.g==5;
      dldcov(m,:) = ((-ones(sum(m),1)*est.int.x' / ...
                      siga + ...  
                      cut_m_mu(m,:,2)./ ...
                      condvar*(est.parm.var(1,2)) /...
                      est.parm.var(1,1)).* ...
                     dpc(m,:,2) - ...
                     (-ones(sum(m),1)*est.int.x' / ...
                      siga + ...  
                      cut_m_mu(m,:,1)./ ...
                      condvar*(est.parm.var(1,2) / ...
                               est.parm.var(1,1))).* ...
                     dpc(m,:,1)).*likeMort(m,:);
      m = data.g==10;
      dldcov(m,:) = -(-ones(sum(m),1)*est.int.x' / ...
                      siga + ...  
                      cut_m_mu(m,:,2)./ ...
                      condvar*(est.parm.var(1,2) / ...
                               est.parm.var(1,1))).* ...
          dpc(m,:,2).*likeMort(m,:);
      
      
     case {'gamma-normal','gamma'}
      dldcov = dldgb(:,:,1).*(ones(data.N,1)*log(est.int.x'));
     case {'normal-flex','uni-normal-beta'}
      dldcov(:,:) = 0; % not used
    end % switch(est.dist)

    dldga = zeros(size(dldgb));      
    switch(est.dist)
     case 'normal'
      m = data.g==0;
      % wrt gamma alpha
      dldga(m,:,1) = dpc(m,:,1).*dc(m,:,1);
      % wrt sigma alpha
      sa3 = est.parm.var(1,2)^2/siga^3;
      dldsa(m,:) = dpc(m,:,1).*((dc(m,:,1) + ...
                                 est.parm.var(1,2)/est.parm.var(1,1)).* ...
                                (ones(sum(m),1)*est.int.x') - ...
                                cut_m_mu(m,:,1)./condvar ...
                                * sa3);
      m = data.g==5;
      dldga(m,:,1) = (dpc(m,:,2).*dc(m,:,2) - ...
                      dpc(m,:,1).*dc(m,:,1));    
      dldsa(m,:) = dpc(m,:,2).*((dc(m,:,2) + ...
                                 est.parm.var(1,2)/est.parm.var(1,1)).* ...
                                (ones(sum(m),1)*est.int.x') - ...
                                cut_m_mu(m,:,2)./condvar ...
                                * sa3) ...
          - dpc(m,:,1).*((dc(m,:,1) + ...
                          est.parm.var(1,2)/est.parm.var(1,1)).* ...
                         (ones(sum(m),1)*est.int.x') - ...
                         cut_m_mu(m,:,1)./condvar ...
                         * sa3);
      
      m = data.g==10;
      dldga(m,:,1) = -dpc(m,:,2).*dc(m,:,2);
      dldsa(m,:) = -dpc(m,:,2).*((dc(m,:,2) + ...
                                  est.parm.var(1,2)/est.parm.var(1,1)).* ...
                                 (ones(sum(m),1)*est.int.x') - ...
                                 cut_m_mu(m,:,2)./condvar ...
                                 * sa3);

      dldga(:,:,1) = dldga(:,:,1).*likeMort;
      dldsa = dldsa.*likeMort;
     case 'uni-normal-beta'
      dldsa = zeros(data.N,est.int.n);
      m = data.g==0;
      % wrt gamma alpha
      dldga(m,:,1) = dpc(m,:,1).*dc(m,:,1);
      m = data.g==5;
      dldga(m,:,1) = (dpc(m,:,2).*dc(m,:,2) - ...
                      dpc(m,:,1).*dc(m,:,1));    
      m = data.g==10;
      dldga(m,:,1) = -dpc(m,:,2).*dc(m,:,2);
      dldga(:,:,1) = dldga(:,:,1).*likeMort;      
     case 'normal-flex'
      dla = ones(data.N,1)*est.int.x';
      m = data.g==0;
      % wrt gamma alpha
      dldga(m,:,1) = dpc(m,:,1).*dc(m,:,1);
      % wrt quasi-correlation 
      dldr(m,:,1) = dpc(m,:,1);
      degree = size(est.parm.var,2)-1;
      
      m = data.g==5;
      dldga(m,:,1) = (dpc(m,:,2).*dc(m,:,2) - ...
                      dpc(m,:,1).*dc(m,:,1));    
      dldr(m,:,1) = (dpc(m,:,2) - dpc(m,:,1));
      
      m = data.g==10;
      dldga(m,:,1) = -dpc(m,:,2).*dc(m,:,2);
      dldr(m,:,1) = -dpc(m,:,2);

      dldga(:,:,1) = dldga(:,:,1).*likeMort;

      dldr(:,:,1) = dldr(:,:,1).*likeMort;
      for d=2:degree
        dldr(:,:,d) = -dldr(:,:,1).*(dla.^d);
      end
      dldr(:,:,1) = -dldr(:,:,1).*(dla);
      % wrt sigma alpha
      dldsa = dldga(:,:,1).*(ones(data.N,1)*est.int.x');
     case {'gamma-normal'}
      dldsa = zeros(data.N,est.int.n);
      m = data.g==0;
      dldga(m,:,1) = dpc(m,:,1).*(dc(m,:,1)); % - est.parm.corr);
      m = data.g==5;
      dldga(m,:,1) = (dpc(m,:,2).*(dc(m,:,2)) - ... %est.parm.corr) - ...
                      dpc(m,:,1).*(dc(m,:,1))); % - est.parm.corr));    
      m = data.g==10;
      dldga(m,:,1) = -dpc(m,:,2).*(dc(m,:,2)); % - est.parm.corr);
      dldga(:,:,1) = dldga(:,:,1).*likeMort;
     case 'gamma'
      dldsa = zeros(data.N,est.int.n);
      m = data.g==0;
      dldga(m,:,1) = dpc(m,:,1).*(dc(m,:,1));
      m = data.g==5;
      dldga(m,:,1) = (dpc(m,:,2).*dc(m,:,2) - ...
                      dpc(m,:,1).*dc(m,:,1));    
      m = data.g==10;
      dldga(m,:,1) = -dpc(m,:,2).*dc(m,:,2);
      dldga(:,:,1) = dldga(:,:,1).*likeMort;
      
      dldga(:,:,1) = dldga(:,:,1) + dldgb(:,:,1)*est.parm.corr;
        
    end% switch(est.dist)
        
    % add the mortality part
    m = data.died==0;
    dldga(m,:,1) = dldga(m,:,1) + (likeChoice(m,:).*likeMort(m,:).* ...
                                   alpha(m,:)/lambda.*ef_el(m,:));
    m = data.died==1;
    dldga(m,:,1) = dldga(m,:,1) + likeMort(m,:).*likeChoice(m,:).* ...
        (1 + alpha(m,:)/lambda.*ef_el(m,:));
    switch (est.dist)
     case {'normal','normal-flex','uni-normal-beta'}
      m = data.died==0;
      dldsa(m,:) = dldsa(m,:) +  likeChoice(m,:).*likeMort(m,:).* ...
          (ones(sum(m),1)*est.int.x').*alpha(m,:)/lambda.*ef_el(m,:);
      m = data.died==1;
      dldsa(m,:) = dldsa(m,:) + likeChoice(m,:).*likeMort(m,:).* ...
          (ones(sum(m),1)*est.int.x').*(1 + alpha(m,:)/lambda.*ef_el(m,:));
     case {'gamma','gamma-normal'}
      dldsa = dldsa + likeChoice.*likeMort.* ...
              (ones(data.N,1)*(log(est.int.x)- ...
                               psi(est.parm.ashape))');
    end % switch(est.dist)
    
    % collect and organize
    grad = zeros(length(parm),1);
    gi = zeros(length(parm),data.N);
    j = 1;
    % multiply by x
    s = length(est.parm.gAlpha);
    gi(j:j+s-1,:) = ((sum(dldga(:,:,1),2))*ones(1,s) ...
                     .*data.alphaX)';
    j = j+s;
    
    s = length(est.parm.gBeta);
    gi(j:j+s-1,:) = ((sum(dldgb(:,:,1),2))*ones(1,s) ...
                     .*data.betaX)';
    j = j+s;

    gcovi = sum(dldcov,2);
    gcovi(like_i==realmin) = 0;
    switch (est.dist)
     case 'normal'
      % sigma alpha
      gi(j,:) = 0.5*(exp(0.5*parm(j))*(sum(dldsa,2)) + ...
                     gcovi*est.parm.var(1,2));
      j = j+1;
      % sigma beta
      gi(j,:) = 0.5*(exp(0.5*parm(j))*( sum(dldsb,2)) + ...
                     gcovi*est.parm.var(1,2));
      j = j+1;
      % correlation
      gi(j,:) = gcovi*2*exp(parm(j))/(exp(parm(j))+1)^2* ...
                sqrt(est.parm.var(1,1)*est.parm.var(2,2));
     case 'normal-flex'
      size(gcovi)
      % sigma alpha
      gi(j,:) = 0.5*(exp(0.5*parm(j))*(sum(dldsa,2)));
      j = j+1;
      % sigma beta
      gi(j,:) = 0.5*(exp(0.5*parm(j))*( sum(dldsb,2)));
      j = j+1;
      % quasi-correlation
      gi(j:(j+degree-1),:) = squeeze(sum(dldr,2))';
      j = j+degree;
     case 'uni-normal-beta'
      % sigma beta
      gi(j,:) = 0.5*(exp(0.5*parm(j))*( sum(dldsb,2)));
      j = j+1;
     case 'gamma-normal'
      % alpha shape
      gi(j,:) = est.parm.ashape * sum(dldsa,2);
      j = j+1;
      % sigma beta
      gi(j,:) = 0.5*(exp(0.5*parm(j))*( sum(dldsb,2)) + ...
                     gcovi*est.parm.var(1,2));
      j = j+1;
      % corr
      gi(j,:) = gcovi;      
     case 'gamma'
      % alpha shape
      gi(j,:) = est.parm.ashape * sum(dldsa,2);
      j = j+1;
      % beta shape
      gi(j,:) = est.parm.bshape * sum(dldsb,2);
      j = j+1;
      % corr
      gi(j,:) = gcovi;      
    end % switch(est.dist)
  end % if nargout>1
  
end % fucntion likelihood()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [like_i gi] = likelihoodMort(parm,est,data)
% compute the mortality likelihood and gradient for each person.  
% this make estimating the choose the middle model easy

  persistent ef_el el mef_el;
  
  lambda = est.lambda;      
  % initialize persistent vars
  if(isempty(ef_el) || size(ef_el,2)~=est.int.n ...
     || size(ef_el,1)~=data.N)
    if (any(data.lage<data.fage))
      error('last age is less than first age');
    end
    if (any(data.lage==data.fage))
      fprintf('WARNING: %d last age = first age\n',sum(data.lage== ...
                                                       data.fage));
      m = data.lage==data.fage;
      data.lage(m) = data.lage(m) + 0.5/365.25;
    end
    if(isfield(est,'fasthazard'))
      h = est.fasthazard;
    else
      h = 1;
    end
    ef_el =  exp(lambda*data.fage.^h) - ...
             exp(lambda*data.lage.^h);
    ef_el = ef_el*ones(1,est.int.n);
    el = (exp(lambda*data.lage.^h).*data.lage.^(h-1)*h)*ones(1,est.int.n);
    if(~isempty(data.mort))
      error('option data.mort no longer implemented');
      mef_el =  exp(lambda*data.mort.fage) - ...
                exp(lambda*data.mort.lage);
      mef_el = mef_el*ones(1,est.int.n);
    end
  end % if (isempty..)
  % unpack parameters (done in annuityLike())
  %est.parm = transformParam(parm,'unpack','mle',est.dist);
  % get integration points
  [logAlpha alpha wp condmu condsig] = intPoints(parm,est,data);
  % calculate cutoffs
  if (nargout<2)
    [e pc] = calculateCuts(est,data,logAlpha,condmu,condsig);  
  else % need more stuff for gradient
    [e pc dc dpc cut_m_mu dscale dshape] = ...
        calculateCuts(est,data,logAlpha,condmu,condsig);
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  likeA = exp(alpha/lambda.*ef_el); 
  likeA(data.died,:) = alpha(data.died,:).*el(data.died,:).* ...
      likeA(data.died,:);
  likeA = likeA.*(ones(data.N,1)*wp');
  likeMort = likeA;
  likeChoice = 1; % choice part of likelihood

  likeA = likeA.*likeChoice;
  
  like_i = sum(likeA,2);
  like_i(like_i==0) = realmin;
  if(sum(isnan(like_i)))
    fprintf('WARNING: %d like_i isnan\n',sum(isnan(like_i)));
  end
  
  if(nargout>1) % the gradient
    % add the mortality part
    m = data.died==0;
    dldga(m,:,1) = likeMort(m,:).* ...
        alpha(m,:)/lambda.*ef_el(m,:);
    m = data.died==1;
    dldga(m,:,1) = likeMort(m,:).* ...
        (1 + alpha(m,:)/lambda.*ef_el(m,:));
    dldsa = zeros(data.N,est.int.n);
    switch (est.dist)
     case {'normal','normal-flex','uni-normal-beta'}
      m = data.died==0;
      dldsa(m,:) = dldsa(m,:) +  likeMort(m,:).* ...
          (ones(sum(m),1)*est.int.x').*alpha(m,:)/lambda.*ef_el(m,:);
      m = data.died==1;
      dldsa(m,:) = dldsa(m,:) + likeMort(m,:).* ...
          (ones(sum(m),1)*est.int.x').*(1 + alpha(m,:)/lambda.*ef_el(m,:));
     case {'gamma','gamma-normal'}
      dldsa = dldsa + likeMort.* ...
              (ones(data.N,1)*(log(est.int.x)- ...
                               psi(est.parm.ashape))');
    end % switch(est.dist)
    
    % collect and organize
    grad = zeros(length(parm),1);
    gi = zeros(length(parm),data.N);
    j = 1;
    % multiply by x
    s = length(est.parm.gAlpha);
    gi(j:j+s-1,:) = ((sum(dldga(:,:,1),2))*ones(1,s) ...
                     .*data.alphaX)';
    j = j+s;
    
    s = length(est.parm.gBeta);
    gi(j:j+s-1,:) = 0;
    j = j+s;
    
    gcovi = zeros(data.N,1);
    switch (est.dist)
     case 'normal'
      % sigma alpha
      gi(j,:) = 0.5*(exp(0.5*parm(j))*(sum(dldsa,2)) + ...
                     gcovi*est.parm.var(1,2));
      j = j+1;
      % sigma beta
      gi(j,:) = 0; 
      j = j+1;
      % correlation
      gi(j,:) = gcovi*2*exp(parm(j))/(exp(parm(j))+1)^2* ...
                sqrt(est.parm.var(1,1)*est.parm.var(2,2));
     case 'normal-flex'
      % sigma alpha
      gi(j,:) = 0.5*(exp(0.5*parm(j))*(sum(dldsa,2)));
      j = j+1;
      % sigma beta
      gi(j,:) = zeros(1,data.N);
      j = j+1;
      % quasi-correlation
      gi(j:(j+degree-1),:) = 0;
      j = j+degree;
     case 'uni-normal-beta'
      % sigma beta
      gi(j,:) = 0;
      j = j+1;
     case 'gamma-normal'
      % alpha shape
      gi(j,:) = est.parm.ashape * sum(dldsa,2);
      j = j+1;
      % sigma beta
      gi(j,:) = 0;
      j = j+1;
      % corr
      gi(j,:) = gcovi;      
     case 'gamma'
      % alpha shape
      gi(j,:) = est.parm.ashape * sum(dldsa,2);
      j = j+1;
      % beta shape
      gi(j,:) = 0;
      j = j+1;
      % corr
      gi(j,:) = 0;      
    end % switch(est.dist)
  end % if nargout>1  
end % fucntion likelihoodMort()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [logAlpha alpha wp condmu condsig] = intPoints(parm,est,data)
% calculate integration points and weights
  switch (est.dist)
   case {'normal','normal-flex','uni-normal-beta'}
    m = est.parm.var==0;
    est.parm.var(m) = eps;
    m = isnan(est.parm.var) | ~isfinite(est.parm.var);
    est.parm.var(m) = 1;

    logAlpha = ones(data.N,1)*est.int.x'*sqrt(est.parm.var(1,1)) + ...
        data.alphaX*est.parm.gAlpha*ones(1,est.int.n);
    alpha = exp(logAlpha);
    alpha(alpha>log(realmax)) = log(realmax);
    wp = normpdf(est.int.x,0,1).*est.int.w;
    wp = wp/sum(wp);
    % conditional mean and variance of beta
    switch (est.dist)
     case {'normal','uni-normal-beta'} %..................................
      condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n) + ...
               est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (logAlpha - ...
                data.alphaX*est.parm.gAlpha*ones(1,est.int.n)); 
      condsig = sqrt(est.parm.var(2,2) -  ...
                     est.parm.var(1,2)^2/est.parm.var(1,1));
      if(~isreal(condsig) || condsig<100*sqrt(realmin))
        condsig = 100*sqrt(realmin);
        disp('WARNING: nearly perfectly correlated, adjusting condsig');
      end
     case 'normal-flex' %..................................................
      dla =  (logAlpha - data.alphaX*est.parm.gAlpha*ones(1,est.int.n)) ...
             /sqrt(est.parm.var(1,1));  
      condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n);
      for d = 2:size(est.parm.var,2)             
        condmu = condmu + est.parm.var(1,d)*dla.^(d-1);
      end
      condsig = sqrt(est.parm.var(2,2));
    end % switch
    %------------------------------------------------------------------
   case {'gamma','gamma-normal'}
    aScale = exp(data.alphaX*est.parm.gAlpha)*ones(1,est.int.n);
    alpha = (ones(data.N,1)*est.int.x').*aScale;
    logAlpha = log(alpha);
    wp = est.int.w.*est.int.x.^(est.parm.ashape-1) ... 
         / gamma(est.parm.ashape);
    % conditional dist of beta
    switch (est.dist) 
     case 'gamma' %..................................................
      condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n) + ...
               est.parm.corr * (ones(data.N,1)*log(est.int.x'));
      condsig = 0; % not used
     case 'gamma-normal' %..........................................
      condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n) + ...
               est.parm.corr * (ones(data.N,1)*log(est.int.x'));
      condsig = sqrt(est.parm.var(2,2));
    end % switch
    %-------------------------------------------------------------------
   case 'mixed-normal' 
    error('FIXME');
  end % switch(est.dist)
end % function intPoints()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [e pc dc dpc cut_m_mu dscale dshape] = ...
      calculateCuts(est,data,logAlpha,condmu,condsig)
  persistent un2all all2un dg;
  dx = 1.0e-4;
  if(isempty(all2un))
    all = [data.group logAlpha condmu data.alphaX data.betaX];
    un = unique(all,'rows');
    if(size(un,1)>100 || size(un,1)==data.N) % not worthwhile to do this
      all2un = ones(size(logAlpha,1),1);
      un2all = [];
    else
      un2all = zeros(size(un,1),size(all,1));
      all2un = zeros(size(un,1),1);
      for k=1:size(un,1)
        un2all(k,:) = sum(all==ones(size(all,1),1)*un(k,:),2)==size(all,2);
        all2un(k) = find(un2all(k,:),1);
      end
      if(~isequal(all(all2un,:),un))
        fprintf('oops\n');
        all(all2un)
        un2all = []; % not used
      end
      if(~isequal(un2all'*un,all))
        fprintf('oops2\n');
      end
     end
  end % if isempty(all2un)
  if (isempty(un2all))
    dg = data.group;
    cm = condmu;
    la = logAlpha;
  else
    dg = data.group(all2un);
    la = logAlpha(all2un,:);
    cm = condmu(all2un,:);
  end
  for g=1:data.ngroup
    m = dg==g;
    gla = la(m,:);    
    ue(m,:,1) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                                reshape(gla,size(gla,1)*est.int.n,1), ...
                                'linear','extrap'),size(gla,1),est.int.n); 
    ue(m,:,2) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                                reshape(gla,size(gla,1)*est.int.n,1), ...
                                'linear','extrap'),size(gla,1),est.int.n);
  end
  ue(isnan(ue)) = realmax;
  if (any(ue(:,:,1)>ue(:,:,2)))
    fprintf('cutoffs crossed\n');
    for k=1:size(ue,2)
      m = ue(:,k,1)>ue(:,k,2);
      ue(m,k,1) = 0.5*(ue(m,k,1)+ue(m,k,2));
      ue(m,k,2) = ue(m,k,1);
    end
  end
  switch (est.dist)
   case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
    upc(:,:,1) = normcdf(ue(:,:,1),cm,condsig*ones(size(cm)));
    upc(:,:,2) = normcdf(ue(:,:,2),cm,condsig*ones(size(cm)));
   case 'gamma'
    upc(:,:,1) = gamcdf(exp(ue(:,:,1)),est.parm.bshape, ...
                        exp(cm));
    upc(:,:,2) = gamcdf(exp(ue(:,:,2)),est.parm.bshape, ...
                        exp(cm));
  end % switch(est.dist)
  pc = zeros(data.N,est.int.n,2);
  e = pc;
  
  if (isempty(un2all))
    pc = upc;
    e = ue;
  else
    for d = 1:size(pc,3)
      pc(:,:,d) = un2all'*upc(:,:,d);
      e(:,:,d) = un2all'*ue(:,:,d);
    end
  end
  e(isnan(e) | e==realmax)=Inf;
  if(nargout>2) % return derivatives too
    for g=1:data.ngroup
      m = dg==g;   
      gla = la(m,:);    
      udc(m,:,1) = ... %ue(m,:,1) - ...
          reshape(interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                          reshape(gla*(1+dx),size(gla,1)*est.int.n,1), ...
                          'linear','extrap'),size(gla,1),est.int.n) ...
          -  reshape(interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                             reshape(gla*(1-dx),size(gla,1)*est.int.n,1), ...
                             'linear','extrap'),size(gla,1),est.int.n); 
      udc(m,:,2) = ... %ue(m,:,2) - ...
          reshape(interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                          reshape(gla*(1+dx),size(gla,1)*est.int.n,1), ...
                          'linear','extrap'),size(gla,1),est.int.n) ...
          - reshape(interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                            reshape(gla*(1-dx),size(gla,1)*est.int.n,1), ...
                            'linear','extrap'),size(gla,1),est.int.n); 
      udc(isnan(udc)) = 0;
      udc(isinf(udc)) = 0;
    end % for g=1:data.ngroup
    udc(:,:,1) = udc(:,:,1)./(2*dx*la);
    udc(:,:,2) = udc(:,:,2)./(2*dx*la);
    % cutoff minus mu
    ucut_m_mu(:,:,1) = ue(:,:,1)-cm;
    ucut_m_mu(:,:,2) = ue(:,:,2)-cm;
    ucut_m_mu(ue==Inf | ue==realmax) = 0;
    % derivatives of probabilities
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
      udpc(:,:,1) = normpdf(ue(:,:,1),cm,condsig*ones(size(cm)));
      udpc(:,:,2) = normpdf(ue(:,:,2),cm,condsig*ones(size(cm)));
     case 'gamma'
      m = ue>log(realmax);
      ue(m) = log(realmax);
      udpc(:,:,1) = gampdf(exp(ue(:,:,1)),est.parm.bshape*ones(size(cm)), ...
                           exp(cm));
      udpc(:,:,2) = gampdf(exp(ue(:,:,2)),est.parm.bshape*ones(size(cm)), ...
                           exp(cm));
      udshape(:,:,1) = dgamcdf_dshape(exp(ue(:,:,1)),est.parm.bshape*ones(size(cm)), ...
                                      exp(cm));
      udshape(:,:,2) = dgamcdf_dshape(exp(ue(:,:,2)),est.parm.bshape*ones(size(cm)), ...
                                      exp(cm));
      udscale(:,:,1) = dgamcdf_dscale(exp(ue(:,:,1)),est.parm.bshape*ones(size(cm)), ...
                                      exp(cm));
      udscale(:,:,2) = dgamcdf_dscale(exp(ue(:,:,2)),est.parm.bshape*ones(size(cm)), ...
                                      exp(cm));
    end % switch(est.dist)
    dpc = zeros(size(pc));
    cut_m_mu = dpc;
    dc = dpc;
    dscale = [];
    dshape = [];
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
      if(isempty(un2all))
        dc = udc;
        dpc = udpc;
        cut_m_mu = ucut_m_mu;
      else
        for d = 1:size(udpc,3)
          dc(:,:,d) = un2all'*udc(:,:,d);
          dpc(:,:,d) = un2all'*udpc(:,:,d);
          cut_m_mu(:,:,d) = un2all'*ucut_m_mu(:,:,d);
        end
      end
     case 'gamma'
      if(isempty(un2all))
        dc = udc;
        dpc = udpc;
        cut_m_mu = ucut_m_mu;
        dshape = udshape;
        dscale = udscale;
      else
        for d = 1:size(udpc,3)
          dc(:,:,d) = un2all'*(udc(:,:,d).*exp(ue(:,:,d)));
          dpc(:,:,d) = un2all'*udpc(:,:,d);
          dshape(:,:,d) = un2all'*udshape(:,:,d);
          dscale(:,:,d) = un2all'*udscale(:,:,d);
          cut_m_mu(:,:,d) = un2all'*ucut_m_mu(:,:,d);
        end
      end % if
    end % switch(est.dist)
  end % function calculateCuts()
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%$
% derivatives of gamma cdf
function dgdA = dgamcdf_dshape(x,a,t)
  dgdA = (gamcdf(x,a*(1+1e-8),t) - gamcdf(x,a*(1-1e-8),t)) ...
         ./ (a*2e-8);
end % function dgamcdf_dshape()

function dgdt = dgamcdf_dscale(x,a,t)
  dgdt = -(x.^a .* exp(-x./t))./(t.^(a+1).*gamma(a));
  if(any(isnan(dgdt)))
    dgdt = (gamcdf(x,a,(1+1e-8)*t) - gamcdf(x,a,(1-1e-8)*t)) ...
           ./ (t*2e-8);
  end
end % function dgamcdf_dscale()
