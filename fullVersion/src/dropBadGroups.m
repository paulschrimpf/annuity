function [dataOut cutOut cfgOut] = dropBadGroups(data,cut,cfg) 
  keep = [];
  for c=1:length(cut)
    bad = (cut(c).lb_orig(:,1)>cut(c).lb_orig(:,2) & ...
           isfinite(cut(c).lb_orig(:,1)) & ...
           isfinite(cut(c).lb_orig(:,2)));
    good = (cut(c).lb_orig(:,1)<cut(c).lb_orig(:,2) & ...
            isfinite(cut(c).lb_orig(:,1)) & ...
            isfinite(cut(c).lb_orig(:,2)));
    first = min(find(bad,1),find(good,1));
    last = max(find(bad,1,'last'),find(good,1,'last'));
    pct = sum(bad)/(last-first+1);
    if (pct<cfg.dropBadGroups)
      keep=[keep; c];
    end
  end
  cutOut = cut(keep');
  % config
  cfgOut = cfg;
  cfgOut.data.group = cfg.data.group(keep);
  cfgOut.cut.agemin = cfg.cut.agemin(keep);
  if (length(cfg.cut.rho)>1)
    cfgOut.cut.rho = cfg.cut.rho(keep);
  end
  if(length(cfg.cut.delta)>1)
    cfgOut.cut.delta = cfg.cut.delta(keep);
  end
  cfgOut.cut.z = cfg.cut.z(keep,:);
  %data
  kd = zeros(data.N,1);
  kd(:) = false;
  newgroup = zeros(data.N,1);
  ng = 1;
  for c=1:length(keep)
    kd = kd | (data.group==keep(c));
    newgroup(data.group==keep(c)) = ng;
    ng = ng+1;
  end
  dataOut.N = sum(kd);
  dataOut.g = data.g(kd);
  dataOut.died = data.died(kd);
  dataOut.male = data.male(kd);
  dataOut.year = data.year(kd);
  dataOut.quarter = data.quarter(kd);
  dataOut.cage = data.cage(kd);
  dataOut.t = data.t(kd);
  dataOut.group = newgroup(kd);
  dataOut.ngroup = length(keep);
  dataOut.betaX = data.betaX(kd,:);
  dataOut.alphaX = data.alphaX(kd,:);
  dataOut.lage = data.lage(kd);
  dataOut.fage = data.fage(kd);
  dataOut.agemin = data.agemin(kd);
  dataOut.baseage = data.baseage(kd);
  dataOut.maxAge = data.maxAge(kd);
  dataOut.name = data.name;
  
  keep = std(dataOut.betaX)>1.0e-4;
  keep(1) = 1;
  dataOut.betaX = dataOut.betaX(:,keep);
  dataOut.name.betaX = data.name.betaX(keep);
  keep = std(dataOut.alphaX)>1.0e-4;
  keep(1)=1;
  dataOut.alphaX = dataOut.alphaX(:,keep);    
  dataOut.name.alphaX = data.name.alphaX(keep);
  
  if(cfg.data.standardize) % standardize X's
    x = dataOut.betaX(:,2:size(dataOut.betaX,2));
    dataOut.betaX(:,2:size(dataOut.betaX,2))=(x-ones(size(x,1),1)*mean(x))./ ...
        (ones(size(x,1),1)*std(x));
    x = dataOut.alphaX(:,2:size(dataOut.alphaX,2));
    dataOut.alphaX(:,2:size(dataOut.alphaX,2))=(x-ones(size(x,1),1)*mean(x))./ ...
        (ones(size(x,1),1)*std(x));
  end   
  
  fprintf('-- DATA SUMMARY: --\n');
  fprintf(' %d observations\n',dataOut.N);
  fprintf(' %d groups\n',dataOut.ngroup);
  fprintf(' %6.3g%% death\n',100*sum(dataOut.died)/dataOut.N);
  for g=0:5:10
    fprintf(' %6.3g%% chose %d guarantee\n',100*sum(dataOut.g==g)/dataOut.N,g);
  end
  fprintf('-- END DATA SUMMARY --\n\n');
