function val = vcheck(ct,wt,q,z,delta,rho,beta,lg,gam,bgam,zeta)
  T = length(ct);
  g = zeros(T+1,1);                     
  for t = min(lg,T):-1:1,
    g(t) = z(t) + rho*g(t+1);
  end;    
  val = 0;
  vb = 0;
  pt = 1;
  qt = 1;
  at = wt(1);
  for t=1:T
    qt = pt*q(t);
    pt = pt*(1-q(t));
    if(pt==0)
      return;
    end
    if (abs(at-wt(t))>exp(log(eps)/2))
      fprintf('at=%20.16g wt=%20.16g\n',at,wt(t));
    end
    val = val + delta^(t-1)*(pt*util(ct(t),gam) + qt*beta*zeta^(t-1)*beq(wt(t)+g(t), ...
                                                  bgam));
    vb = vb + delta^(t-1)*zeta^(t-1)*qt*beta*beq(wt(t)+g(t),bgam);
    at = (at + z(t) - ct(t))/rho;
  end  
  at = (at+z(T)-ct(T))/rho;
  val = val+pt*delta^T*zeta^T*beta*beq(at,bgam);
  vb = vb + delta^T*zeta^T*pt*beta*beq(at,bgam);
%  fprintf('val = %.16g\n',val)
%  fprintf(' vb = %.16g\n',vb)
  %val = beta*beq(at,gam);
  %for t=T:-1:1
  %  val = q(t)*beta*beq(wt(t)+g(t),gam)+(1-q(t))*(util(ct(t),gam)+delta* ...
  %                                               val);
  %end
