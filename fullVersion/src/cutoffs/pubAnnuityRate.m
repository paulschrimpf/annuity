function  z = pubAnnuityRate(infl,r,q) 
% calculate public annuity annual payments
% the payment rate make a constant nominal annuity actuarially fair give
% inflation rate 'infl', interest factor 'r', and death hazard 'q'
  
  plive = (cumprod(1-q)); % probability alive at time t
  z = 1./(sum(plive.*(cumprod( r/(1+infl)*ones(size(q))) )));
