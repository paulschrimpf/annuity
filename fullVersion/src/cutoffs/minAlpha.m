function minAlpha(cfg)
% finds minimum alpha such that 5 year guarantee is ever chosen over 0
  
  % set parameters
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  valParam.gam  = cfg.cut.gam;      % CRRA coefficient
  valParam.bgam = cfg.cut.bgam;
  infl  = cfg.cut.inflation;% Inflation rate                               
  z_slope = cfg.cut.z; % paymenet rates
  valParam.delta = cfg.cut.delta;
  valParam.rho = cfg.cut.rho;
  if valParam.gam==1;
    valParam.gam=1.00000001; % If gamma=1, util is "undefined" %
  end;
  
  guar    = [0 5 10];  % guarantee options
  lambda = cfg.cut.lambda;
  base = cfg.cut.agemin-cfg.mort.baseage;
  valParam.T = cfg.cut.agemax-cfg.cut.agemin;
  inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate
                                                % factors    

  choices = 3; % number of choices
  % finding the cutoff values for beta
  bma = 50.0;
  btol = 1.0e-4;
  warning('off','MATLAB:divideByZero')
  %profile on;
  
  alo = -50;
  ahi = 0;
  g = [0 5 10];
  for c=1:choices-1,
    cp1 = c+1;
    if (cp1>choices)
      cc = 1;
      cp1 = 3;
    else 
      cc = c;
    end
    Z0 = w0*alpha2*z_slope(cc); % *** APPLY FORMULA OF ANNUITY PRICING ***
    Z0 = (Z0*ones(T,1)).*inf_factor; % convert nominal annuity to real
    Z1 = w0*alpha2*z_slope(cp1); % *** APPLY FORMULA OF ANNUITY PRICING ***
    Z1 = (Z1*ones(T,1)).*inf_factor; % convert nominal annuity to real
    amin = fzero(@(a) mindV(a,lambda,Z0,Z1,g(cc),g(cp1), ...
                            w0,alpha2,delta,rho,base,T,bgam),[alo ahi]);
    fprintf('%d-%d: amin = %.10g\n',g(cc),g(cp1),amin);
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dV = mindV(logAlpha,lambda,Z0,Z1,g0,g1, ...
                    w0,alpha2,delta,rho,base,T,gam)
  warning('assuming zeta = 1');
  alpha = exp(logAlpha);
  t = base:T-1;
  tp1 = t+1;
  part2  = exp(alpha/lambda*(1 - exp(lambda*(tp1))));
  part1  = exp(alpha/lambda*(1 - exp(lambda*(t  ))));
  m      = (part1 - part2)/part1(1); % probability of dying at time t
  part2 = part2/part1(1);
  m(part1==0 | isnan(m)) = 0; % at high t, both part1 and part2 are numerically 0.  
  dt = cumprod(delta*ones(size(t)));
  dZ = 0;
  dV = 0;
  for t=1:T-base
    sZ0 = w0*alpha2;
    for s=1:max(t-1,g0)
      sZ0 = sZ0+Z0(s)*rho^(s-t);
    end
    sZ1 = w0*alpha2;
    for s=1:max(t-1,g1)
      sZ1 = sZ1+Z1(s)*rho^(s-t);
    end
    dV = dV + dt(t)*m(t)*(beq(sZ0,gam)-beq(sZ1,gam));
  end
  dV = dV + delta*dt(T-base)*part2(T-base)*(beq(sZ0/rho,gam)-beq(sZ1/rho,gam));
 
% just for debugging
%  dV=mindVold(logAlpha,lambda,Z0,Z1,g0,g1, ...
%                   w0,alpha2,delta,rho,base,T)  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dV = mindVold(logAlpha,lambda,Z0,Z1,g0,g1, ...
                    w0,alpha2,delta,rho,base,T)
  alpha = exp(logAlpha);
  t = base:T-1;
  tp1 = t+1;
  part1  = exp(alpha/lambda*(1 - exp(lambda*(t  ))));
  part2  = exp(alpha/lambda*(1 - exp(lambda*(tp1))));
  m      = (part1 - part2)/part1(1); % probability of dying at time t
  part2 = part2/part1(1);
  m(part1==0 | isnan(m)) = 0; % at high t, both part1 and part2 are numerically 0.  
  dt = cumprod(delta*ones(size(t)));
  dZ = 0;
  dV = 0;
  for t=1:T-base
    dV = dV + dZ*dt(t)*m(t);
    dZ = dZ/rho + (Z0(t)-Z1(t))/rho;
  end
  dV = dV + delta*dZ/rho*dt(T-base)*part2(T-base);
  G0 = zeros(1,T-base);                     
  for t = g0:-1:1,
    G0(t) = Z0(t) + rho*G0(t+1);
  end
  G1 = zeros(1,T-base);                     
  for t = g1:-1:1,
    G1(t) = Z1(t) + rho*G1(t+1);
  end;    
  dV = dV + sum(dt.*m.*(G0-G1));
