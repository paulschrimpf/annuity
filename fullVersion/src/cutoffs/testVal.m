% script for comparing testing the value function.  v_func uses a grid
% method.  vf3.m uses c0 optimization.  they should give similar answers.
path(path,'oldbcutCalc');
clear v_func;
% define parameters
lambda = 0.15; %774486012628;
alpha = exp(-6)';
beta = 10; %exp(500);
global wmax;  %maximum wealth
wmax = 500;
global wstep; % step in wealth grid
wstep = 1;
global T;     % number of years
gam = 3;
bgam = 3;
global delta; % discount factor
delta = 1/1.03;
global minf;  % value of zero consumption
minf = -realmax;
global rho;   % 1/(1+riskless interest rate)
rho = 1/1.03;
zeta = 1/1.03;

w0 = 100; % starting wealth;
alpha2 = 0.2; % fraction annuitized
infl = 0.0462; % inflation
z_slope = [0.1029080, 0.1024765, 0.1003112]; % 60 female 6a
z_slope(3) = 0.1;
agemin = 60;
agemax = 100;
T = agemax - agemin;
base = 60;
t = (agemin-base):(agemax-base);
tp1 = t+1;
inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate
                                             % factors    

vp.gam = gam;   % relative risk aversion
vp.bgam = bgam;
vp.rho = rho;
vp.delta = delta;
vp.T = T;
vp.zeta = zeta;

lg = [0 5 5];
for a=1:length(alpha)
  galpha = alpha(a);
  part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1))));
  part1  = exp(galpha/lambda*(1 - exp(lambda*(t  ))));
  q      = (part1 - part2)./part1; % probability of dying at time t
                                   % conditional on living until t-1
                                   % q(i,t) is prob at time t for alpha(i) 
  q(part1==0) = 1; % at high t, both part1 and part2 are numerically 0.  
  vgrid= 0;
  vc = zeros(3,1);
  for b=1:length(beta)
    for c=1:3
      Z0 = w0*alpha2*z_slope(c); % *** APPLY FORMULA OF ANNUITY PRICING ***
      Z0 = (Z0*ones(T,1)).*inf_factor; % convert nominal annuity to real
%      [vgrid cgrid wgrid] = v_func(w0*(1-alpha2),Z0,q,beta(b),lg(c));
%      vgrid=vcheck(cgrid,wgrid,q,Z0,delta,rho,beta(b),lg(c),gam);
      [v ct wt] = vf3(w0*(1-alpha2),Z0,q,beta(b),lg(c),vp);
      %(v-vcheck(ct,wt,q,Z0,delta,rho,beta(b),lg(c),gam))/v
      fprintf(['g=%2d a=%g b=%g \n' ...
               '  vg=%20.16g\n   v=%20.16g\n'],lg(c),log(alpha(a)),log(beta(b)), ...
              vgrid,v);
      %[beta(b) c v]
      vc(c) = v;
      vc2(c) = vcheck(ct,wt,q,Z0,vp.delta,vp.rho,beta(b),lg(c),vp.gam,vp.bgam,vp.zeta);
    end
    [m i] = max(vc);
    %[m j] = max(vc2);
    fprintf('-%d-----------------------\n',i);
  end
end
