%% The cutoff calculation sometimes encounters numeric difficulties, when
%  this happens the cutoff will be recorded as either 50 of -Inf.  This
%  file corrects these mistakes by linear interpolation.  Numeric
%  problems are rare and can be fixed more rigorously.  We did this for
%  our baseline specification, but this interpolation strategy does
%  nearly as well and is far less labor intensive.  
function cut=cleanCut(cutin,cfg,data)
  cut = cutin;
  lb = cut(1).lb;
  t = whos('lb');
  if (strcmp(t.class,'double'))
    for g=1:data.ngroup
      for c=1:3
        k = find(cut(g).lb_orig(:,c)~=-Inf,1)-1; 
        cut(g).lb(1:k,c) = Inf;
      end
      warning('Messing with cutoffs');
      m = (cut(g).lb(:,1)==-Inf | cut(g).lb(:,1)==50);
      gd = (isfinite(cut(g).lb(:,1)) & cut(g).lb(:,1)~=50);
      if(any(m))
        cut(g).lb(m,1) = interp1(cut(g).la(gd),cut(g).lb(gd,1),cut(g).la(m), ...
                                 'linear','extrap');
      end
      m = (cut(g).lb(:,2)==-Inf | cut(g).lb(:,2)==50);
      gd = (isfinite(cut(g).lb(:,2)) & cut(g).lb(:,2)~=50);
      if(any(m))
        cut(g).lb(m,2) = interp1(cut(g).la(gd),cut(g).lb(gd,2),cut(g).la(m), ...
                                 'linear','extrap');    
      end
      m = cut(g).lb(:,1)>cut(g).lb(:,2);
      if(sum(m)>0)
        warning('Cutoffs cross');
        cut(g).lb(m,1) = 0.5*(cut(g).lb(m,1)+cut(g).lb(m,2));
        cut(g).lb(m,2) = cut(g).lb(m,1);
      end
      if(cfg.data.no10)
        cut(g).lb(:,2) = Inf;
      end   
    end
  else
    for g=1:data.ngroup
      for iw=1:length(cut(g).lb)
      for c=1:3
        k = find(cut(g).lb_orig{iw}(:,c)~=-Inf,1)-1; 
        cut(g).lb{iw}(1:k,c) = Inf;
      end
      warning('Messing with cutoffs');
      m = (cut(g).lb{iw}(:,1)==-Inf | cut(g).lb{iw}(:,1)==50);
      gd = (isfinite(cut(g).lb{iw}(:,1)) & cut(g).lb{iw}(:,1)~=50);
      if(any(m))
        cut(g).lb{iw}(m,1) = interp1(cut(g).la(gd),cut(g).lb{iw}(gd,1),cut(g).la(m), ...
                                 'linear','extrap');
      end
      m = (cut(g).lb{iw}(:,2)==-Inf | cut(g).lb{iw}(:,2)==50);
      gd = (isfinite(cut(g).lb{iw}(:,2)) & cut(g).lb{iw}(:,2)~=50);
      if(any(m))
        cut(g).lb{iw}(m,2) = interp1(cut(g).la(gd),cut(g).lb{iw}(gd,2),cut(g).la(m), ...
                                 'linear','extrap');    
      end
      m = cut(g).lb{iw}(:,1)>cut(g).lb{iw}(:,2);
      if(sum(m)>0)
        warning('Cutoffs cross');
        cut(g).lb{iw}(m,1) = 0.5*(cut(g).lb{iw}(m,1)+cut(g).lb{iw}(m,2));
        cut(g).lb{iw}(m,2) = cut(g).lb{iw}(m,1);
      end
      if(cfg.data.no10)
        cut(g).lb{iw}(:,2) = Inf;
      end   
    end
  end
end
