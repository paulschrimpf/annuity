function [out der] = transformParam(in,dir,obj,dist)
% transformation of mle struc to/from a vector.
% if dir = 'pack', take mle -> vector 
% if dir = 'unpack', take vector -> mle
% optionally returns the derivative of the transformation in der
  persistent nBetaX nAlphaX degree dgAlpha dvar dp twodh frac; 
  if (nargin<4)
    warning('distribution unspecified, setting to normal');
    dist = 'normal';
  end
  switch (obj)
   case {'mle','gmm'}
    if (strcmp(dir,'pack'))
      nBetaX = numel(in(1).gBeta);
      nAlphaX = numel(in(1).gAlpha);
      out = zeros(nBetaX+nAlphaX,1);
      out(1:nAlphaX) = in(1).gAlpha(:);
      out(nAlphaX+1:nAlphaX+nBetaX) = in(1).gBeta(:);
      j = nAlphaX+nBetaX+1;
      switch (dist) 
       case 'normal'
        out(j) = log(in.var(1,1));
        j = j+1;
        out(j) = log(in.var(2,2));
        j = j+1;
        in.corr = in.var(1,2)/sqrt(in.var(1,1)*in.var(2,2));
        out(j) = log((1+in.corr)/(1-in.corr));
        j = j+1;
       case 'normal-flex'
        out(j) = log(in.var(1,1));
        j = j+1;
        out(j) = log(in.var(2,2));
        j = j+1;
        degree =size(in.var,2)-1; 
        for d=2:(degree+1)
          out(j) = in.var(1,d);
          j = j+1;
        end
       case 'mixed-normal-varbeta'
        out(j) = log(in(1).var(1,1));
        j = j+1;
        for (m=1:degree)
          out(j) = log(in(m).var(2,2));
          j = j+1;
          in(m).corr = in(m).var(1,2)/sqrt(in(m).var(1,1)*in(m).var(2,2));
          out(j) = log((1+in(m).corr)/(1-in(m).corr));
          j = j+1;        
        end
        for m=1:(degree-1)
          out(j) = log(in(m).p/in(degree).p);
          j = j+1;
        end
       case 'mixed-normal'
        degree = length(in);
        out = [];
        for m=1:degree
          onew = transformParam(in(m),dir,obj,'normal');
          out = [out; onew];
        end
        j = length(out)+1;
        for m=1:(degree-1)
          out(j) = log(in(m).p/in(degree).p);
          j = j+1;
        end
       case 'uni-normal-beta'
        out(j) = log(in.var(2,2));
        j = j+1;
       case 'gamma'
        out(j) = log(in.ashape);
        j = j+1;
        out(j) = log(in.bshape);
        j = j+1; 
        out(j) = in.corr;
        j = j+1
       case 'gamma-normal'
        out(j) = log(in.ashape);
        j = j+1;
        out(j) = log(in.var(2,2));
        j = j+1; 
        out(j) = in.corr;
        j = j+1;
      end        
      
      dgAlpha = 0;
      dvar = false;
      dp = false;
      if (isfield(in,'dumb'))
        if (isfield(in.dumb,'gAlpha'));
          i = length(in.dumb.gAlpha);
          dgAlpha = i;
          out(j:j+i-1) = in.dumb.gAlpha;
          j = j+i;
        end
        if (isfield(in.dumb,'var'));
          dvar = true;
          out(j) = log(in.dumb.var);
          j = j+1;
        end
        if (isfield(in.dumb,'p'))
          dp = true;
          out(j) = log(in.dumb.p)/log(1+in.dumb.p);
          j = j+1;
        end
      end
      if (isfield(in,'twodh'))
        twodh = in.twodh;
        out(j:(j+length(twodh.p)-1)) = twodh.p;
        j = j+length(twodh.p);
      else
        twodh = [];
      end      
      if (isfield(in,'frac'))
        frac = in.frac;
      else
        frac = [];
      end      
    elseif (strcmp(dir,'unpack'))
      if(isempty(nBetaX))
        error(['transformParam must be called to pack at least once before' ...
               ' it can unpack\n.']);
      end
      out.gAlpha = in(1:nAlphaX);
      out.gBeta = in(nAlphaX+1:nAlphaX+nBetaX);
      j = nAlphaX+nBetaX+1;
      switch(dist) 
       case 'normal'
        out.var = zeros(2,2);
        out.var(1,1) = exp(in(j));
        j = j+1;
        out.var(2,2) = exp(in(j));
        j = j+1;
        out.corr = exp(in(j));
        out.corr = (out.corr-1)/(out.corr+1);
        out.var(1,2) = out.corr*sqrt(out.var(1,1)*out.var(2,2));
        j = j+1;
       case 'normal-flex'
        out.var = zeros(2,2);
        out.var(1,1) = exp(in(j));
        j = j+1;
        out.var(2,2) = exp(in(j));
        j = j+1;
        for d=2:(degree+1)
          out.var(1,d) = in(j);
          j = j+1;
        end
       case 'mixed-normal-varbeta'
        error('not implemented');
       case 'mixed-normal'
        j = 1;
        np = (length(in) - (degree-1))/degree;
        for m=1:degree
          if (m==1)
            out =  transformParam(in(j:(j+np-1)),dir,obj,'normal');
          else
            out(m) = transformParam(in(j:(j+np-1)),dir,obj,'normal');
          end
          j=j+np;
        end
        for m=1:(degree-1)
          out(m).p = exp(in(j));
          j = j+1;
        end
        out(degree).p = 1;
        sp = sum([out.p]);
        for m=1:degree
          out(m).p = out(m).p/sp;
        end
       case 'uni-normal-beta'
        out.var = zeros(2,2);
        out.var(1,1) = 1;
        out.var(2,2) = exp(in(j));
        j = j+1;
        out.corr = 0;
       case 'gamma'
        out.var = zeros(2,2);
        out.ashape = exp(in(j));
        j = j+1;
        out.bshape = exp(in(j));
        j = j+1;
        out.corr = in(j);
        j = j+1;
       case 'gamma-normal'
        out.var = zeros(2,2);
        out.ashape = exp(in(j));
        j = j+1;
        out.var(2,2) = exp(in(j));
        j = j+1;
        out.corr = in(j);
        j = j+1;
      end % switch(dist)
      if (~isempty(twodh))
        out.twodh = twodh;
        np = length(twodh.p);
        out.twodh.p = in(j:(j+np-1));
        j = j + np;        
        out.gAlpha = reshape(out.gAlpha,np,nAlphaX/np);
        out.gBeta = reshape(out.gBeta,np,nAlphaX/np);
      end
      if (~isempty(frac))
        out.frac = frac;
        np = numel(frac);
        out.gAlpha = reshape(out.gAlpha,np,nAlphaX/np);
        out.gBeta = reshape(out.gBeta,np,nAlphaX/np);
      end 
      if (nargout>1)
        der.gAlpha = zeros(nAlphaX,length(in));
        j = 1;
        for k=1:nAlphaX
          der.gAlpha(k,j) = 1;
          j = j+1;
        end
        der.gBeta = zeros(nBetaX,length(in));
        for k=1:nBetaX
          der.gBeta(k,j) = 1;
          j = j+1;
        end
        der.var = zeros(2,2,length(in));
        switch (dist)
         case 'normal'
          der.var(1,1,j) = out.var(1,1);
          j = j+1;
          der.var(2,2,j) = out.var(2,2);
          j = j+1;
          ex = exp(in(j));
          der.var(1,2,j) = 2*ex/(ex+1)^2*sqrt(out.var(2,2)*out.var(1,1));
          der.var(1,2,j-1) = out.var(1,2)*0.5;
          der.var(1,2,j-2) = out.var(1,2)*0.5;
          der.corr = zeros(1,length(in));
          der.corr(j) = 2*ex/(ex+1)^2;
         case 'normal-flex'
          der.var(1,1,j) = out.var(1,1);
          j = j+1;
          der.var(2,2,j) = out.var(2,2);
          j = j+1;
          ex = exp(in(j));
          for d=2:(degree+1)
            der.var(1,d,j) = 1;
          end
         case 'uni-normal-beta'
          der.var(2,2,j) = out.var(2,2);
          der.corr = 0;
          j = j+1;
         case 'gamma'
          der.ashape = zeros(1,length(in));
          der.ashape(j) = out.ashape;
          j = j+1;
          der.bshape = zeros(1,length(in));
          der.bshape(j) = out.bshape;
          j = j+1;
          der.corr = zeros(1,length(in));
          der.corr(j) = 1;
         case 'gamma-normal'
          der.ashape = zeros(1,length(in));
          der.ashape(j) = out.ashape;
          j = j+1;
          der.var(2,2,j) = out.var(2,2);
          j = j+1;
          der.corr = zeros(1,length(in));
          der.corr(j) = 1;
        end
      end
      
      if (dgAlpha>0)
        i = dgAlpha;
        out.dumb.gAlpha = in(j:j+i-1);
        j = j+i;
      end
      if (dvar)
        out.dumb.var = exp(in(j));
        j = j+1;
      end
      if (dp)
        out.dumb.p = exp(in(j));
        out.dumb.p = out.dumb.p/(out.dumb.p+1);
        j = j+1;
      end
    else
      error('dir = %s not recognized',dir);
    end;
    %---------------------------------------------------------------------
   case 'mlmort'
    warning('mlmort not supported');
    %---------------------------------------------------------------------
   case '2step'
    persistent gAlpha va;
    if (strcmp(dir,'pack'))
      nBetaX = length(in.gBeta);
      nAlphaX = 0;
      gAlpha = in.gAlpha;
      va = in.var(1,1);
      out = zeros(nBetaX+nAlphaX+2,1);
      out(1:nBetaX) = in.gBeta;
      j = nAlphaX+nBetaX+1;
      out(j) = log(in.var(2,2));
      j = j+1;
      in.corr = in.var(1,2)/sqrt(in.var(1,1)*in.var(2,2));
      out(j) = log((1+in.corr)/(1-in.corr));
    elseif (strcmp(dir,'unpack'))
      if(isempty(nBetaX))
        error(['transformParam must be called to pack at least once before' ...
               ' it can unpack\n.']);
      end
      out.gAlpha = gAlpha;
      out.gBeta = in(1:nBetaX);
      j = nAlphaX+nBetaX+1;
      out.var(1,1) = va;
      out.var(2,2) = exp(in(j));
      j = j+1;
      out.corr = exp(in(j));
      out.corr = (out.corr-1)/(out.corr+1);
      out.var(1,2) = out.corr*sqrt(out.var(1,1)*out.var(2,2));
      if (nargout>1)
        j = 1;
        der.gAlpha = zeros(size(gAlpha),length(in));
        der.gBeta = zeros(nBetaX,length(in));
        for k=1:nBetaX
          der.gBeta(k,j) = 1;
          j = j+1;
        end
        der.var = zeros(2,2,length(in));
        der.var(2,2,j) = out.var(2,2);
        j = j+1;
        ex = exp(in(j));
        der.var(1,2,j) = 2*ex/(ex+1)^2;
        der.var(1,2,j-1) = 0.5*sqrt(out.var(2,2));
        der.var(1,2,j-2) = 0.5*sqrt(out.var(1,1));
        der.corr = zeros(1,length(in));
        der.corr(j) = 2*ex/(ex+1)^2;
      end
    else
      error('dir = %s not recognized',dir);
    end;
   otherwise 
    error('unknown objective');
  end; % end switch
 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

