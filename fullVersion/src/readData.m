function [data cfg] = readData(cfg); 
% given configuration parameters, cfg, reads a data set and returns the
% data organized into the struc 'data'

  % open data file
  if (isempty(strfind(cfg.data.name,'.mat')))
    rawData = load(cfg.data.name,'-ascii');

    % select a subsample.
    cage = rawData(:,cfg.data.lcage);
    cyear = rawData(:,cfg.data.lcyear);
    male = rawData(:,cfg.data.lmale);
    dyear = rawData(:,cfg.data.ldyear);
    dead = (dyear ~= cfg.data.nodeathYear);
    dage = rawData(:,cfg.data.ldage);
    fage = rawData(:,cfg.data.lfage);
    mage = rawData(:,cfg.data.lmage);

    % drop people with missing values
    keep = zeros(size(rawData,1),1);
    for j=1:length(cfg.data.group)
      keep = keep | (floor(cage)==cfg.data.group(j).cage & ...
                     male==cfg.data.group(j).male);
    end
    if(cfg.data.deadOnly)
      keep = (keep & dead==1);
    end
    if(isfield(cfg.data,'okay') && cfg.data.okay>0)
      fprintf('WARNING: dropping if "okay"==%d\n',cfg.data.okeep);
      keep = (keep & rawData(:,cfg.data.okay)==cfg.data.okeep);
    end
    for k=1:length(cfg.data.lbetaX)
      if(cfg.data.lbetaX(k)>0)
        keep = keep & rawData(:,cfg.data.lbetaX(k))~=cfg.data.dropVal;
      end
    end
    for k=1:length(cfg.data.lalphaX)
      if(cfg.data.lalphaX(k)>0)
        keep = keep & rawData(:,cfg.data.lalphaX(k))~=cfg.data.dropVal;
      end
    end
    if (isfield(cfg.data,'lp'))
      for k=1:length(cfg.data.lp)
        keep = keep & rawData(:,cfg.data.lp(k))~=cfg.data.dropVal;
      end
    end
    if (isfield(cfg.data,'keepYear'))
      m = zeros(size(rawData,1),1);
      for k=1:length(cfg.data.keepYear)
        m = m | cyear == cfg.data.keepYear(k);
      end
      keep = keep & m;
    end
    
    if(isfield(cfg.data,'mortOnly'))
      for n=1:length(cage)
        mkp(n) = (~isempty(find(cage(n)==cfg.data.mortOnly.cage)) & ...
                  ~isempty(find(male(n)==cfg.data.mortOnly.male)));
      end
      mkp = mkp';
      if(isfield(cfg.data,'okay') && cfg.data.okay>0)
        mkp = (mkp & rawData(:,cfg.data.okay)==1);
      end
      for k=1:length(cfg.data.lalphaX)
        if(cfg.data.lalphaX(k)>0)
          mkp = mkp & rawData(:,cfg.data.lalphaX(k))~=cfg.data.dropVal;
        end
      end
      if (sum(mkp & keep)>0)
        error('mortOnly and group intersect');
      end
      mortData = rawData(mkp,:);
    else
      mortData = [];
    end
    rawData = rawData(keep,:);
    if(isfield(cfg.data,'obs'))
      rawData=rawData(cfg.data.skip+1:cfg.data.obs,:);
    end
    
    % make it log premium
    %rawData(:,6) = log(rawData(:,6));
    
    % number of observations
    data.N = size(rawData,1);
    % guarantee choice
    data.g = rawData(:,cfg.data.lguar);
    % construct:  fage = first age observed
    %             lage = last age observed
    %             died = indicator of whether died at lage 
    cage = rawData(:,cfg.data.lcage);
    cyear = rawData(:,cfg.data.lcyear);
    male = rawData(:,cfg.data.lmale);
    dyear = rawData(:,cfg.data.ldyear);
    dead = (dyear ~= cfg.data.nodeathYear);
    dage = rawData(:,cfg.data.ldage);
    fage = rawData(:,cfg.data.lfage);
    mage = rawData(:,cfg.data.lmage);
    data.died = (dyear ~= cfg.data.nodeathYear);
    male = rawData(:,cfg.data.lmale);
    data.male = male;
    data.year = cyear;
    data.quarter = rawData(:,cfg.data.lquarter);
    if(max(data.quarter)-min(data.quarter)~=3)
      warning('Range of quarter is not 4');
    end
    data.quarter = data.quarter - min(data.quarter)+1; % make sure it goes
                                                      % from 1 to 4.
    data.cage = cage;
    data.t = data.year + (data.quarter-1)/4; % time 

    if (~(isfield(cfg.data,'allDummies') && cfg.data.allDummies))
      %groups specified in config
      data.group = zeros(data.N,1);
      for j=1:length(cfg.data.group)
        data.group(floor(cage)==cfg.data.group(j).cage & ...
                   male==cfg.data.group(j).male) = j;
      end
      data.ngroup = length(cfg.data.group);
      data = createX(cfg,data,rawData);      
    end
    
    % create group indicators
    if (isfield(cfg.data,'lp'))
      data.group = zeros(data.N,1);  
      cfg.data.group = [];
      if (isfield(cfg.data,'lrate'))
        rate = 1./(1+rawData(:,cfg.data.lrate));
      else
        rate = ones(data.N,1)*cfg.cut.rho;
      end
      if (isfield(cfg.data,'linfl'))
        infl = rawData(:,cfg.data.linfl);
      else
        infl = ones(data.N,1)*cfg.cut.inflation;
      end

      for n=1:data.N
        % try to fit into existing group
        for j=1:length(cfg.data.group)
          if (sum(rawData(n,cfg.data.lp)==cfg.data.group(j).z)==3 && ...
              data.male(n)==cfg.data.group(j).male && ...
              cage(n)==cfg.data.group(j).cage && ...
              rate(n)==cfg.data.group(j).rate && ...
              infl(n)==cfg.data.group(j).infl)
            data.group(n) = j;
            break;
          end
        end
        if (data.group(n)==0) % append a new group
          newg.z = rawData(n,cfg.data.lp);
          newg.male = data.male(n);
          newg.cage = cage(n);
          newg.name = num2str(newg.cage);
          newg.year = data.year(n);
          newg.quarter = data.quarter(n);
          newg.rate = rate(n);
          newg.infl = infl(n);
          if(newg.male)
            newg.name = [newg.name ' Male '];
          else
            newg.name = [newg.name ' Female '];
          end
          if (isfield(cfg.data,'lexternal') && cfg.data.lexternal)
            newg.external = rawData(n,cfg.data.lexternal);
            if(newg.external)
              newg.name = [newg.name num2str(newg.year) ' External'];
            else
              newg.name = [newg.name num2str(newg.year) ' Internal'];
            end
          else
            newg.name = [newg.name num2str(newg.year) ' Q' ...
                         num2str(newg.quarter)];
          end
          cfg.data.group = [cfg.data.group, newg];
          data.group(n) = length(cfg.data.group);
        end
      end % loop over n
      data.ngroup = length(cfg.data.group);
      fprintf('%d Groups Created\n',data.ngroup);
      for j=1:data.ngroup
        cfg.cut.agemin(j) = cfg.data.group(j).cage;
        cfg.cut.z(j,:) = cfg.data.group(j).z;
        cfg.cut.rho(j) = cfg.data.group(j).rate;
        cfg.cut.inflation(j) = cfg.data.group(j).infl;
        if(isfield(cfg.cut,'deltaEQrho') && ...
           cfg.cut.deltaEQrho)
          cfg.cut.delta(j) = cfg.cut.rho(j);
        end
      end 
    end % if lp

    if (isfield(cfg.data,'allDummies') && cfg.data.allDummies)
      data = createX(cfg,data,rawData);
    end
    
    % constructing mortality timing variables
    baseage = zeros(data.N,1);
    for g = 1:data.ngroup
      baseage(data.group==g) = cfg.mort.baseage;
    end
    data.lage = dage.*data.died + mage.*(1-data.died) - baseage;
    data.fage = fage - baseage;
    data.agemin = cage - baseage;
    data.baseage = baseage;
    data.maxAge = mage - baseage;

    if(length(mortData))
      data.mort.N = size(mortData,1);
      data.mort.ngroup = 1;
      data.mort.group = ones(data.mort.N,1);
      cage = mortData(:,cfg.data.lcage);
      cyear = mortData(:,cfg.data.lcyear);
      dyear = mortData(:,cfg.data.ldyear);
      data.mort.died = (dyear ~= cfg.data.nodeathYear);
      male = mortData(:,cfg.data.lmale);
      data.mort.male = male;
      data.mort.year = cyear;
      data.mort.quarter = mortData(:,cfg.data.lquarter);
      if(max(data.mort.quarter)-min(data.mort.quarter)~=3)
        error('Range of quarter is not 4');
      end
      data.mort.quarter = data.mort.quarter - min(data.mort.quarter)+1; % make sure it goes
                                                         % from 1 to 4.
      data.mort.cage = cage;
      data.mort.t = data.mort.year + (data.mort.quarter-1)/4; % time 
      
      baseage = ones(data.mort.N,1)*cfg.mort.baseage;
      data.mort.lage = dage.*data.mort.died + ...
          mage.*(1-data.mort.died) - ...
          baseage; 
      data.mort.fage = fage - baseage;
      data.mort.agemin = cage - baseage;
      data.mort.baseage = baseage;
      data.mort.maxAge = mage - baseage;
      data.mort = createX(cfg,data.mort,mortData);
      if(cfg.data.mortOnly.cageDummies)
        for age=min(data.mort.cage)+1:max(data.mort.cage)
          data.alphaX = [data.alphaX data.cage==age];
          data.mort.alphaX = [data.mort.alphaX data.mort.cage==age];
          n.s = ['age=' num2str(age)];
          data.name.alphaX = [data.name.alphaX n];
        end
      end
    else
      data.mort = [];
    end
    
  else % fake data format is slightly different
    load(cfg.data.name);
    error('Fake data no longer supported\n');
    rawData=comp;
    data.N = size(rawData,1);
    data.fage = rawData(:,cfg.data.lcyear);
    data.lage = rawData(:,cfg.data.ldyear);
    data.died = rawData(:,-cfg.data.nodeathYear)==1;
    data.g = rawData(:,cfg.data.lguar);
    data.maxAge=data.lage; %not exactly ...
  end;
  
  if(cfg.data.no10)
    data.g(data.g==10) = 5;
  end
  
  if(cfg.data.standardize) % standardize X's
    x = data.betaX(:,2:size(data.betaX,2));
    data.betaX(:,2:size(data.betaX,2))=(x-ones(size(x,1),1)*mean(x))./ ...
        (ones(size(x,1),1)*std(x));
    if (~isempty(data.mort))
      X = [data.alphaX(:,2:size(data.alphaX,2)); ...
           data.mort.alphaX(:,2:size(data.mort.alphaX,2))];
      x = data.alphaX(:,2:size(data.alphaX,2));
      data.alphaX(:,2:size(data.alphaX,2))=(x-ones(size(x,1),1)*mean(X))./ ...
          (ones(size(x,1),1)*std(X));
      x = data.mort.alphaX(:,2:size(data.mort.alphaX,2));
      data.mort.alphaX(:,2:size(data.mort.alphaX,2))=(x-ones(size(x,1),1)*mean(X))./ ...
          (ones(size(x,1),1)*std(X));
    else
      x = data.alphaX(:,2:size(data.alphaX,2));
      data.alphaX(:,2:size(data.alphaX,2))=(x-ones(size(x,1),1)*mean(x))./ ...
        (ones(size(x,1),1)*std(x));
    end
  end       
  
  fprintf('-- DATA SUMMARY: --\n');
  fprintf(' %d observations\n',data.N);
  fprintf(' %d groups\n',data.ngroup);
  fprintf(' %6.3g%% death\n',100*sum(data.died)/data.N);
  for g=0:5:10
    fprintf(' %6.3g%% chose %d guarantee\n',100*sum(data.g==g)/data.N,g);
  end
  fprintf('-- END DATA SUMMARY --\n\n');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------
function data = createX(cfg,datain,rawData)
  % constructing x's
    data = datain;
    lbetaX = cfg.data.lbetaX(cfg.data.lbetaX>0);
    lalphaX = cfg.data.lalphaX(cfg.data.lalphaX>0);
    data.betaX = [ones(data.N,1) rawData(:,lbetaX)];
    data.name.betaX(1).s = 'Constant$_\beta$';
    n = 2;
    for k = 1:length(cfg.data.lbetaX)
      if (cfg.data.lbetaX(k)>0)
        data.name.betaX(n).s = cfg.data.betaXName(k).s;
        n = n+1;
      end
    end
    data.alphaX = [ones(data.N,1) rawData(:,lalphaX)];
    data.name.alphaX(1).s = 'Constant$_\alpha$';
    na = 2;
    for k = 1:length(cfg.data.lalphaX)
      if (cfg.data.lalphaX(k)>0)
        data.name.alphaX(na).s = cfg.data.alphaXName(k).s;
        na = na+1;
      end
    end
    nbx = length(lbetaX)+1;
    nax = length(lalphaX)+1;
    if (sum(cfg.data.lbetaX==-1)>0) % include group indicator and
                                    % interactions  as x
      for j=2:data.ngroup
        data.betaX = [data.betaX data.group==j];
        data.name.betaX(n).s = cfg.data.group(j).name;
        n = n+1;
      end
    end
    if (sum(cfg.data.lalphaX==-1)>0) % include group indicator as x
      for j=2:data.ngroup
        data.alphaX = [data.alphaX data.group==j];
        data.name.alphaX(na).s = cfg.data.group(j).name;
        na = na+1;
      end
    end

    % time trends
    if (sum(cfg.data.lbetaX==-2)>0)
      data.betaX = [data.betaX data.t];
      data.name.betaX(n).s = 'Time';
      n = n+1;
    end
    if (sum(cfg.data.lalphaX==-2)>0)
      data.alphaX = [data.alphaX data.t];
      data.name.alphaX(na).s = 'Time';
      na = na+1;
    end
    % year dummies
    if (sum(cfg.data.lbetaX==-3)>0)
      for(t=min(data.year)+1:max(data.year))  
        data.betaX = [data.betaX data.year==t];
        data.name.betaX(n).s = ['Year=' num2str(t)];
        n = n+1;
      end
    end
    if (sum(cfg.data.lalphaX==-3)>0)
      for(t=min(data.year)+1:max(data.year))  
        data.alphaX = [data.alphaX data.year==t];
        data.name.alphaX(na).s = ['Year=' num2str(t)];
        na =na+1;
      end
    end
    
 % END function createX() --------------------------------------------------
