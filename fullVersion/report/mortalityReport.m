function mortalityReport(data,est,cfg,prefix)
% create table and graphs summarizing the mortality estimates
   
  %----------------------------------------------------------------------
  % life expectancy and P(live)
  elife = zeros(3,1);
  Plive = zeros(3,2);
  centile = [.05 .50 .95];
  % do separately for each group
  if (data.ngroup>5)
    ngroup = 0;
    dg = zeros(data.N,1);
    group=[];
    for s = 0:1
      for cage = min(data.cage):max(data.cage)
        m = data.male==s & data.cage==cage;
        if (sum(m)>0)
          ngroup = ngroup+1;
          dg(m) = ngroup;
          newgroup.male = s;
          newgroup.age = cage;
          if(s)
            newgroup.name = ['Male ' num2str(cage)];
          else
            newgroup.name = ['Female ' num2str(cage)];
          end
          group = [group newgroup];
        end
      end
    end
  else
    dg = data.group;
    ngroup = data.ngroup;
    for g=1:ngroup
      group(g).male = cfg.data.group(g).male;
      group(g).age = cfg.data.group(g).cage;
      group(g).name = cfg.data.group(g).name;
    end
  end

  for k=1:ngroup
    m = dg==k;
    gdta = data;
    gdta.N = sum(m);
    gdta.fage = data.fage(m);
    gdta.lage = data.lage(m);
    gdta.died = data.died(m);
    gdta.agemin = data.agemin(m);
    gdta.g = data.g(m);
    gdta.maxAge = data.maxAge(m);
    gdta.alphaX = data.alphaX(m,:);
    gdta.betaX = data.betaX(m,:);
    gdta.group = data.group(m);
 
    % monte carlo integration make this easier
    logAlpha = [];
    dup = ceil(1e5/gdta.N);
    for i = 1:dup
      la = sampleAlphaBeta(data,est);
      logAlpha = [logAlpha; la];
    end
    if(isfield(est,'fasthazard'))
      h = est.fasthazard;
    else 
      h = 1;
    end
    alpha = exp(quantile(logAlpha,centile));
    lambda = est.lambda;
    t = (min(gdta.agemin)+[5 10]);
    Pobs = exp(alpha/lambda.*(1-exp(lambda*min(gdta.agemin).^h)));
    for j = 1:length(t)
      Plive(1:3,j) = 1-exp(alpha/lambda*(1-exp(lambda*t(j).^h))) ./ ... 
          Pobs;
    end
    dt = 0.1;
    t = (0.0:dt:200.0);
    elife(1:3) = sum( exp(kron(alpha'/lambda,(1 - exp(lambda*t.^h))))*dt,2);
    % now average over alpha
    if(0)
      alpha = exp(logAlpha);
      elife(6) = mean( sum( exp(kron(alpha/lambda,(1 - exp(lambda*t))))*dt, ...
                            2));
      Pobs = exp(alpha/lambda.*(1-exp(lambda*gdta.agemin)));
      t = min(gdta.agemin)+[5 10];
      Plive(6,1) = 1-mean(exp(alpha/lambda*(1-exp(lambda*t(1).^h)))./Pobs);
      Plive(6,2) = 1-mean(exp(alpha/lambda*(1-exp(lambda*t(2).^h)))./Pobs);
    end
    % write the table
    out = [prefix '/tables/elife' num2str(k) '.tex'];
    of = fopen(out,'w');
    fprintf(of,['& \\multicolumn{3}{c}{%s} \\\\ \\hline \\hline \n'],group(k).name);
    fprintf(of,['Percentile & Life Exp. & P(Live$<$5) & P(Live$<$10) \\\\' ...
                ' \\hline \n']);
    elife = elife + data.baseage(k);
    for c = 1:length(centile),
      fprintf(of,'%4g & %8.4g & %8.4g & %8.4g \\\\ \n',100*centile(c),elife(c), ...
              Plive(c,1),Plive(c,2));
    end;
    % add institute of actuaries data
    if (group(k).male)
      filename = '../../data/mortalityTables/male.txt';
    else 
      filename = '../../data/mortalityTables/female.txt';
    end
    if (exist(filename,'file'))
      mtable = load(filename);
      f = find(mtable(:,1) == group(k).age,1);
      l = size(mtable,1);
      fprintf(of,' & & & \\\\ \n');
      surv = cumprod(1-mtable(f:l,2));
      fprintf(of,'From Mortality Table & %8.4g & %8.4g & %8.4g \\\\ \n', ...
              sum(surv)+mtable(f,1),1-surv(5),1-surv(10));
    end
    fclose(of);


    %----------------------------------------------------------------------
    % estimated survival curve and curve from table
%     t=(mtable(f:l,1) - cfg.mort.baseage);
%     esurv = zeros(size(t'));
%     switch(est.dist)
%      case {'normal','normal-flex'}
%       wp = normpdf(est.int.x,0,1).*est.int.w;
%      case {'gamma','gamma-normal'}
%       wp = est.int.w.*est.int.x.^(est.parm.ashape-1) ... 
%            / gamma(est.parm.ashape);
%     end
%     for i=1:gdta.N
%       switch(est.dist)
%        case {'normal','normal-flex'}
%         alpha = exp( gdta.alphaX(i,:)*est.parm.gAlpha + ...
%                      est.int.x*sqrt(est.parm.var(1,1)) );
%        case {'gamma','gamma-normal'}
%         alpha = est.int.x * exp(gdta.alphaX(i,:)*est.parm.gAlpha);
%       end
%       s1 = (exp(alpha/lambda*(1-exp(lambda*t(1).^h)))) ...
%            *ones(size(t'));
%       surv = sum( exp(alpha/lambda*(1-exp(lambda*t'.^h))) ...
%                   .* (wp*ones(size(t'))) ./ s1, 1);
%       %surv = surv/surv(1); % this would do nothing
%       esurv = esurv+surv/(gdta.N);
%     end
%     tsurv = cumprod(1-mtable(f:l,2));
%     tsurv = tsurv/tsurv(1);
    
%     t = t + cfg.mort.baseage;
%     figure; % survival
%     plot(t,esurv,t,tsurv);
%     legend('estimated','mortality table');
%     ylabel('P(D>t)');
%     xlabel('Age');
%     title(group(k).name);
%     filename = sprintf('%s/figures/survivalVStable%d.eps',prefix,k);   
%     print(filename,'-depsc2');

    
    %----------------------------------------------------------------------
    % mortality densities and survival curves given alpha
    if(0)

    alpha = exp(ma + sa*dalpha);
    dt = 1.0;
    t = min(gdta.agemin):dt:40;
    s = zeros(length(alpha),length(t));
    d = s;
    for g=1:5
      s(g,:) = exp(alpha(g)/lambda*(1-exp(lambda*t)));
      d(g,:) = exp(alpha(g)/lambda*(1-exp(lambda*t))).*alpha(g).*exp(lambda*t);
      s(g,:) = s(g,:)/s(g,1);
      d(g,:) = d(g,:)/s(g,1);
    end
    figure;
    t = t+gdta.baseage(k);
    plot(t,s(1,:),'--',t,s(2,:),'.-',t,s(3,:),t,s(4,:),':',t,s(5,:),'d');
    ylabel('P(D>t)');
    xlabel('Age');
    legend(str(1).s,str(2).s,str(3).s,str(4).s,str(5).s);
    filename = [prefix '/figures/survivalPM' num2str(k) '.eps'];
    print(filename,'-depsc2');
    
    figure;
    plot(t,d(1,:),'--',t,d(2,:),'.-',t,d(3,:),t,d(4,:),':',t,d(5,:),'d');
    ylabel('Density');
    xlabel('Age');
    legend(str(1).s,str(2).s,str(3).s,str(4).s,str(5).s);
    filename = [prefix '/figures/mortDenPM' num2str(k) '.eps'];
    print(filename,'-depsc2');

    %----------------------------------------------------------------------
    % fit of hazard and survival curves
    
    % estimated f(d|date first observed) 
    t = min(gdta.agemin):40;
    edens = zeros(size(t));
    esurv = zeros(size(t));
    wp = normpdf(est.int.x,0,1).*est.int.w;
    for i=1:gdta.N
      alpha = exp( gdta.alphaX(i,:)*est.parm.gAlpha + ...
                   est.int.x*sqrt(est.parm.var(1,1)) );
      dens = sum( exp(alpha/lambda*(1-exp(lambda*t.^h))).*(alpha*exp(lambda*t.^h)) ...
                  .* (wp*ones(size(t))), 1);
      surv = sum( exp(alpha/lambda*(1-exp(lambda*t.^h))) ...
                  .* (wp*ones(size(t))), 1);
      dens = dens./surv;
      surv = surv/surv(1);
      %l = gdta.fage(i)+1;
      %m = gdta.maxAge(i)+1;
      edens = edens+dens/(gdta.N);
      esurv = esurv+surv/(gdta.N);
    end
    % observed
    clear odens osurv;
    lage = floor(gdta.lage);
    for j = 1:length(t)
      nd = sum( lage(gdta.died)==t(j) );
      nr = sum( gdta.fage<=t(j) & gdta.lage>=t(j)); 
      nr = max(nr,0.1); % to make sure it's not zero
      odens(j) = nd/nr;
      osurv(j) = 1 - sum(lage(gdta.died)<t(j))/sum(gdta.fage<=t(j));
    end
    osurv = cumprod(1-odens);
    
    figure; % density 
    lj = sum(odens>0)+1;
    t = t+data.baseage(k);
    plot(t(1:lj),edens(1:lj),t(1:lj),odens(1:lj));
    legend('estimated','observed');
    ylabel('Hazard');
    xlabel('Age');
    filename = [prefix '/figures/mortDenFit' num2str(k) '.eps'];
    print(filename,'-depsc2');
    
    figure; % survival
    plot(t(1:lj),esurv(1:lj),t(1:lj),osurv(1:lj));
    legend('estimated','observed');
    ylabel('P(D>t)');
    xlabel('Age');
    filename = [prefix '/figures/survivalFit' num2str(k) '.eps'];
    print(filename,'-depsc2');
    
    
    %----------------------------------------------------------------------
    % fit of density and survival curves conditional on guarantee choice
    
    if (~strcmp(cfg.est.objective,'mlmort'))
      % estimated f(d|date first observed) 
      t = min(gdta.agemin):40;
      edens = zeros(3,length(t));
      esurv = zeros(3,length(t));
      clear logAlpha condmu condsig e pc;
      
      logAlpha = ones(gdta.N,1)*est.int.x'*sqrt(est.parm.var(1,1)) + ...
          gdta.alphaX*est.parm.gAlpha*ones(1,est.int.n);
      condmu = gdta.betaX*est.parm.gBeta*ones(1,est.int.n,1) + ...
               est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (logAlpha - ...
                gdta.alphaX*est.parm.gAlpha*ones(1,est.int.n)); 
      condsig = sqrt(est.parm.var(2,2) -  ...
                     est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
                ones(gdta.N,est.int.n);            
      
      for g=1:gdta.ngroup
        m = gdta.group==g;
        if(sum(m)==0)
          continue;
        end;
        e(m,:,1) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                                   reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                   'linear','extrap'),sum(m),est.int.n); 
        e(m,:,2) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                                   reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                   'linear','extrap'),sum(m),est.int.n); 
      end
      pc(:,:,1) = normcdf(e(:,:,1),condmu,condsig);
      pc(:,:,2) = normcdf(e(:,:,2),condmu,condsig);
      pc(:,:,3) = 1 - pc(:,:,2);
      pc(:,:,2) = pc(:,:,2) - pc(:,:,1);
      for i=1:gdta.N
        alpha = exp( gdta.alphaX(i,:)*est.parm.gAlpha + ...
                     est.int.x*sqrt(est.parm.var(1,1)) );
        for c = 1:3
          wp = normpdf(est.int.x,0,1).*est.int.w.* ...
               reshape(pc(i,:,c),size(est.int.w));
          wp = wp/sum(wp);
          dens = sum( exp(alpha/lambda*(1-exp(lambda*t.^h))).*(alpha*exp(lambda*t.^h)) ...
                      .* (wp*ones(size(t))), 1);
          surv = sum( exp(alpha/lambda*(1-exp(lambda*t.^h))) ...
                      .* (wp*ones(size(t))), 1);
          dens = dens./surv;
          surv = surv/surv(1);
          %l = gdta.fage(i)+1;
          %m = gdta.maxAge(i)+1;
          edens(c,:) = edens(c,:)+dens/(gdta.N);
          esurv(c,:) = esurv(c,:)+surv/(gdta.N);
        end
      end
      % observed
      lage = floor(gdta.lage);
      lg = [0 5 10];
      clear osurv odens;
      for c=1:3
        m = gdta.g==lg(c);
        for j = 1:length(t)
          nd = sum( lage(gdta.died & m)==t(j) );
          nr = sum( gdta.fage(m)<=t(j) & gdta.lage(m)>=t(j)); 
          nr = max(nr,1); % to make sure it's not zero
          odens(c,j) = nd/nr;
        end
        osurv(c,:) = cumprod(1-odens(c,:));
      end
      
      t = t+data.baseage(k);
      for c=1:3
        figure; % density 
        lj = sum(odens(c,:)>0)+1;
        plot(t(1:lj),edens(c,1:lj),t(1:lj),odens(c,1:lj));
        legend('estimated','observed');
        ylabel('Hazard');
        xlabel('Age');
        filename = sprintf('%s/figures/mortDenFit%d_%d.eps',prefix,k,lg(c));
        print(filename,'-depsc2');
        
        figure; % survival
        plot(t(1:lj),esurv(c,1:lj),t(1:lj),osurv(c,1:lj));
        legend('estimated','observed');
        ylabel('P(D>t)');
        xlabel('Age');
        filename = sprintf('%s/figures/survivalFit%d_%d.eps',prefix,k,lg(c));   
        print(filename,'-depsc2');
      end
    end % end if (0)
    end % loop over groups
  end % if (~strcmp(cfg.est.objective,'mlmort'))
