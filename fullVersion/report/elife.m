function el = elife(parm,est,data)

  % unpack parameters
  k = length(est.parm.gAlpha);
  est.parm.gAlpha = parm(1:k);
  k = k+1;
  switch (est.dist)
   case 'normal'
    stdalpha = exp(parm(k));
    est.parm.var(1,1) = stdalpha^2;   
    logAlpha = ones(data.N,1)*est.int.x'*sqrt(est.parm.var(1,1)) + ...
        data.alphaX*est.parm.gAlpha*ones(1,est.int.n);
    alpha = exp(logAlpha);
    wp = normpdf(est.int.x,0,1).*est.int.w;
   case 'exponential'
    L = exp(parm(k));
    alpha = (ones(data.N,1)*est.int.x/L) .* ...
            exp(data.alphaX*est.parm.gAlpha*ones(1,est.int.n));
    wp = est.int.w';
    %fprintf('%g %g\n',1/L,sum(est.int.x/L .* est.int.w));
   case 'gamma'
    beta = exp(parm(k));
    k = k+1;
    A = exp(parm(k));
    alpha = (ones(data.N,1)*est.int.x/beta) .* ...
            exp(data.alphaX*est.parm.gAlpha*ones(1,est.int.n));
    wp = est.int.w.*est.int.x.^(A-1) /gamma(A);
    wp = wp';
    %fprintf('%g %g\n',A/beta,sum(wp'.*est.int.x/beta));
  end
  k = k+1;
  est.lambda = exp(parm(k));
  
  lambda = est.lambda;      
  
  dt = 1.0;
  t = 0:dt:100;
  el = 0;
  for i=1:data.N
    el = el +  sum(sum(exp(alpha(i,:)'/lambda* ...
                           (1-exp(lambda*t)))*dt,2).* ...
                   wp,1);
  end
  el = el/data.N+60
  
