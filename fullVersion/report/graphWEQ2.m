function graphWEQ2(est,data,cfg,cfdata,prefix)
  
  guar = 0:5:10; % guarantee lengths
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  nchoice = length(guar);
  disp = 10;
  lambda = est.lambda;
  logAlpha = log(cfdata.alpha);
  logBeta = log(cfdata.beta);
  
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end
  nbin = 5;
  x = logAlpha;
  agridAll = quantile(x,1/(nbin+1):1/(nbin+1):nbin/(nbin+1));
  x = logBeta;
  bgridAll = quantile(x,1/(nbin+1):1/(nbin+1):nbin/(nbin+1));
  nbin = 5;
  
  for gr=1:1 %data.ngroup
    x = logAlpha(cfdata.group==gr);
    agrid = sort([agridAll quantile(x,1/(nbin+1):1/(nbin+1):nbin/(nbin+ ...
                                                  1))]);
    x = logBeta(cfdata.group==gr);
    bgrid = sort([bgridAll quantile(x,1/(nbin+1):1/(nbin+1):nbin/(nbin+ ...
                                                  1))]);
    z_slope = cfg.cut.z(gr,:);
    agemin = cfg.cut.agemin(gr);
    T = agemax - agemin;
    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(gr);
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(gr);
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(gr);
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(gr);
    end
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                  % rate factors    
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
 
    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;

    for a=1:length(agrid)
      galpha = exp(agrid(a));
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
      
      q = (part1 - part2)./part1;
      q(part1==0) = 0;
      fr     = (part1(1:T) - part2(1:T))';          % density  
      for b=1:length(bgrid)
        e(1) = interp1(est.cut(gr).la,est.cut(gr).lb(:,1), ...
                       agrid(a),'linear','extrap');
        e(2) = interp1(est.cut(gr).la,est.cut(gr).lb(:,2), ...
                       agrid(a),'linear','extrap');
        c = 1*(bgrid(b)<=e(:,1)) + 2*(bgrid(b)>e(:,1) & bgrid(b)<e(:,2)) + ...
            3*(bgrid(b)>=e(:,2));
        %for c=1:3
        Z= w0*(alpha2*z_slope(c)); % for computing e(pay)
        Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                         % value
        val(gr,a,b) = vf3(w0,z_slope(c),q,exp(bgrid(b)),guar(c),vp);
        wlo = w0*(1-alpha2);
        whi = wlo + sum(Z);
        % wealth equivalent
        w(gr,a,b)= weq(val(gr,a,b),wlo,whi,q,exp(bgrid(b)),vp);
        [c gr a b]
        %end
        %[m i]=max(w(:,gr,a,b));
        %[i oc]
      end
    end
  end
  
  save weqTemp w val agrid bgrid;
end
