function fullAnnuity(data,est,cfg,prefix)
% create table with wealth equivalents with beta=0 and full
% annuitization 
   
  if(data.ngroup>1)
    error('fullAnnuity.m only works with 1 group');
  end

  if (~strcmp(est.dist,'normal'))
    return;
  end

  dbeta = [0 -10];
  frac = [unique([cfg.cut.fracAnnuitized]) 0.3:0.1:0.9 0.9999 ];
  alpha2 = cfg.cut.fracAnnuitized;

  [la lb] = sampleAlphaBeta(data,est);
  ma = mean(la);
  mb = mean(lb);
  
  logAlpha = ma;
  logBeta = [mb; -20];

  % things for the value function
%  global T gam delta rho;
  agemax = cfg.cut.agemax;
  agemin = cfg.cut.agemin;
  T = cfg.cut.agemax - cfg.cut.agemin;
  delta = cfg.cut.delta;
  rho = cfg.cut.rho;
  gam = cfg.cut.gam;
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  infl = cfg.cut.inflation;
  lambda = est.lambda;
  inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                % rate factors    
  rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
  base = cfg.mort.baseage;
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end


  t = (agemin-base):(agemax-base);
  tp1 = t+1;
  z_slope = cfg.cut.z(1,:);
  galpha = exp(logAlpha);
  part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
  part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));  
  q = (part1 - part2)./part1;
  q(part1==0) = 0;

  % results
  val = zeros(length(dbeta),length(frac));
  wEq = val;
  for b=1:length(dbeta)
    for f=1:length(frac)
      vp.rho = rho;
      vp.delta = delta;
      vp.T = T;
      vp.zeta = cfg.cut.zeta;
      vp.gam = gam;
      vp.bgam = cfg.cut.bgam;
      vp.pub = pub/(1-alpha2)*(1-frac(f));
      vp.infl = infl;
      vp.frac = frac(f);
      
      val(b,f)=vf3(w0,z_slope(2),q,exp(logBeta(b)),0,vp);
      wlo = w0*(1-frac(f));
      whi = 2*w0;
      wEq(b,f)=weq(val(b,f),wlo,whi,q,exp(logBeta(b)),vp);
    end
  end
  % print results
  out = fopen([prefix '/tables/weqFrac.tex'],'w');
  fprintf(out,['Fraction Annuitized & Mean $\\beta$ & $\\beta=0$ \\\\ \\hline' ...
               ' \n']);
  for f=1:length(frac)
    fprintf(out,' %.4g & %.4g & %.4g \\\\ \n', ...
            frac(f),wEq(1,f),wEq(2,f));
  end
  fclose(out);
  fprintf('     |  mean         0\n');
  fprintf('%4.2g |  %g   %g\n',[frac; wEq]);
  
end
