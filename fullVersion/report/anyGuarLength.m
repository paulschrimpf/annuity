% simulate model under symmetric info, allowing people to choose any
% guarantee length

function anyGuarLength(data,cfdata,est,cfg,prefix)
  
  % these comparisons are fine.  to minimize random numerical error, need to
  % find choices by calculating cutoffs.  there are lots of cutoffs, so
  % rather than doing it on the full alpha grid, use a grid consisting of
  % the quadrature points used in estimation.  
  lagrid = est.int.x*sqrt(est.parm.var(1,1)) + ...
           mean(data.alphaX*est.parm.gAlpha);
  ccfg = cfg;
  ccfg.cut.la = lagrid;
  guar = 0:25;
  cut = findSymCut(ccfg,est,cfdata,guar);
  % save the cutoffs just in case 
  eval(['save ' prefix  '/anyCut cut']);  
  %eval(['load ' prefix  '/anyCut cut']);  
  % plot the cutoffs 
  cmd = 'plot(cut.la,cut.lb(:,1)';
  for g=2:length(guar)
    cmd = [cmd ',cut.la,cut.lb(:,' num2str(g) ')'];
  end
  cmd = [cmd ');'];
  eval(cmd);
  filename = [prefix '/figures/anyCut.eps'];
  print(filename,'-depsc2');
  
  % calculate probability of each choice overall 
  prob = zeros(length(guar),4);
  if (est.dist ~= 'normal')
    error('only normal distribution implemented');
  end
  condmu = mean(data.betaX*est.parm.gBeta) + ...
           est.parm.var(1,2)/est.parm.var(1,1) * ... 
           (lagrid - mean(data.alphaX*est.parm.gAlpha)); 
  condsig = sqrt(est.parm.var(2,2) -  ...
                 est.parm.var(1,2)^2/est.parm.var(1,1));
  for a = 1:est.int.n
    wp = normpdf(est.int.x(a),0,1)*est.int.w(a);
    p = normcdf(cut.lb(a,:),condmu(a),condsig)';
    prob(2:length(guar),1) =  prob(2:length(guar),1) + (p(2:length(guar)) - ...
        p(1:(length(guar)-1)))*wp; 
    % find prob|eq choice 
    e(1) = interp1(est.cut(1).la,est.cut(1).lb(:,1), ...
                     lagrid(a),'linear','extrap');
    e(2) = interp1(est.cut(1).la,est.cut(1).lb(:,2), ...
                     lagrid(a),'linear','extrap');
    % 0 guar
    pc = normcdf(e(1),condmu(a),condsig);
    m = cut.lb(a,:) > e(1);
    p2 = p;
    p2(m) = pc;
    prob(2:length(guar),2) =  prob(2:length(guar),1) + (p2(2:length(guar)) - ...
        p2(1:(length(guar)-1))) / (pc)*wp; 
    % 5 guar
    m = cut.lb(a,:) < e(1);
    p2 = p;
    p2(m) = pc;
    m = cut.lb(a,:) > e(2);
    pc2 = normcdf(e(2),condmu(a),condsig);
    p2(m) = pc2;
    prob(2:length(guar),3) =  prob(2:length(guar),1) + (p2(2:length(guar)) - ...
        p2(1:(length(guar)-1))) / (pc2-pc)*wp; 
    % 10 guar
    m = cut.lb(a,:) < e(2);
    p2 = p;
    p2(m) = pc;
    prob(2:length(guar),4) =  prob(2:length(guar),1) + (p2(2:length(guar)) - ...
        p2(1:(length(guar)-1))) / (1-pc2)*wp;    
  end

  file = fopen([prefix '/tables/anyGuarProb.tex'],'w');
  for g=1:length(guar) 
    fprintf(file,' %d & %10.4g \\\\ \n',g-1,prob(g,:));
  end
  fprintf(file,' %d & %10.4g \\\\ \n',max(guar),1-sum(prob));
  fclose(file);
  [(0:25)' prob cut.lb(13,:)']

  % run simulations to find welfare effect
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  lambda = est.lambda;
  T = agemax - agemin;
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end

  for n=1:cfdata.N
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                  % rate factors    
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    galpha = exp(cfdata.logAlpha(n));
    part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
    part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));

  end
end % function anyGuarLength()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
function cut = findSymCut(cfg,est,cfdata,guar);
% calculates and saves cutoffs
  
  % set parameters
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  valParam.frac = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  valParam.gam  = cfg.cut.gam;      % CRRA coefficient
  valParam.bgam  = cfg.cut.bgam;      % CRRA coefficient
  valParam.zeta = cfg.cut.zeta;
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  valParam.pub = pub;
    
  if valParam.gam==1;
    valParam.gam=1.00000001; % If gamma=1, util is "undefined" %
  end;
  
  gbeta = cfg.cut.lambda;

  % determine annuity payment rates
  oc = cfdata.oc;
  for g=1:cfdata.ngroup
    meanPay(g) = 0.0; % mean payments made in equilibrium
    for n=1:cfdata.N
      if(cfdata.group(n)==g)
        meanPay(g) = meanPay(g) + cfdata.epay(n,oc(n));
      end
    end
    meanPay(g) = meanPay(g) / sum(cfdata.group==g);
  end

  choices = length(guar); % number of choices
  % finding the cutoff values for beta
  bmax = 50.0;
  btol = 1.0e-8;
  warning('off','MATLAB:divideByZ')
  %profile on;
  tic
  for group=1:size(cfg.cut.z,1)
    if (length(cfg.cut.rho)>1)
      valParam.rho = cfg.cut.rho(group);     % Riskless interest rate                       
    else 
      valParam.rho = cfg.cut.rho;
    end
    if (length(cfg.cut.delta)>1)
      valParam.delta = cfg.cut.delta(group);   % Utility discount rate                        
    else
      valParam.delta = cfg.cut.delta;   % Utility discount rate                        
    end
    if (length(cfg.cut.inflation)>1)
      valParam.infl = cfg.cut.inflation(group);   % inflation
    else
      valParam.infl = cfg.cut.inflation;   
    end

    cut(group).lambda = cfg.cut.lambda; % lambda
    cut(group).la = cfg.cut.la;
    agemin = cfg.cut.agemin(group); % Age
    galpha = exp(cut(group).la);
    galpha_length=length(galpha);
    valParam.T = agemax-agemin; % Determines number of periods in problem
    T = valParam.T;
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    part2  = exp(galpha/gbeta*(1 - exp(gbeta*(tp1))));
    part1  = exp(galpha/gbeta*(1 - exp(gbeta*(t  ))));
    q      = (part1 - part2)./part1; % probability of dying at time t
                                     % conditional on living until t-1
                                     % q(i,t) is prob at time t for alpha(i) 
    q(part1==0) = 1; % at high t, both part1 and part2 are numerically 0.  

    % form expected payment
    z_slope = mean(cfg.cut.z(g,:));
    Z= w0*(valParam.frac*z_slope); % for computing e(pay)
    Z = (Z*ones(T,1)).*valParam.infl; % convert nominal annuity to real payments
                                     % value                                           
    cumz = cumsum(Z.*cumprod(valParam.rho*ones(T,1)));
    for n=1:galpha_length
      fr = (part1(n,1:T) - part2(n,1:T))';          % density  
      for c = 1:choices
        if guar(c)>0,
          epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
              sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
        else
          epay(n,c) = sum(cumz.*fr);
        end
      end
      epay(n,:) = epay(n,:)/sum(fr);
    end

    bcut = zeros(galpha_length,choices);
    diff = zeros(galpha_length,choices);
    for ga = galpha_length:-1:1,
      zi = z_slope.*meanPay(g)./epay(ga,:);
      for c=1:choices,
        cp1 = c+1;
        if (cp1>choices)
          cc = 1;
          cp1 = 3;
        else 
          cc = c;
        end

        if(ga ==galpha_length)
          b0 = -200;
        else
          b0 = max(bcut(ga+1:galpha_length,cc));
        end;
        
        if (ga==1 || bcut(ga-1,cc)==0)
          b1 = bmax;
        else
          b1 = bmax; %bcut(ga-1,cc);
        end;
%        log(galpha(ga))
        dV0 = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b0),guar(cc), ...
                    guar(cp1),valParam);
        dV1 = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b1),guar(cc),guar(cp1),valParam);
        if (dV0*dV1>=0) 
          if (0 && abs(dV0)<1e-6)
            [bcut(ga,c) f]= fzero(@(b) vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b),guar(cc), ...
                                             guar(cp1),valParam),b0);
            fprintf('0: a=%.4g c=%d b=%.16g f=%.16g\n',log(galpha(ga)),c, ...
                    bcut(ga,c),f);
            continue;            
          elseif (abs(dV1)<1e-6)
            [bcut(ga,c) f]= fzero(@(b) vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b),guar(cc), ...
                                             guar(cp1),valParam),b1);
            fprintf('1: a=%.4g c=%d b=%.16g f=%.16g\n',log(galpha(ga)),c, ...
                    bcut(ga,c),f);
            continue;
          end
          b0 = 5.0;
          dV0 = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b0),guar(cc),guar(cp1),valParam);
          iter = 0;
          while(dV0<0 & iter<20),
            iter = iter +1;
            b0 = b0-10.0;
            dV0 = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b0),guar(cc),guar(cp1),valParam);
          end;
          iter = 0;
          while(dV1>0 && iter<100 && b1<709/4),
            iter = iter +1;
            b1 = b1*1.5;
            dV1 = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b1),guar(cc),guar(cp1),valParam);
          end;
          if (dV0*dV1>=0) 
            fprintf('WARNING: bcut not bracketed\n');
            fprintf('%d %g %g %g %g\n',[c dV0 dV1 b0 b1]);
            continue;
          end;
        end
        iter = 0;
        dV = 100;
        b = (b1+b0)/2;
        while ((b1 - b0) > btol && iter<100),
          iter = iter+1;
          b = (b1+b0)/2;
          dV = vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b),guar(cc),guar(cp1),valParam);
          if (dV*dV0>0) 
            b0 = b;
            dV0 = dV;
          elseif (dV*dV1>0),
            b1 = b;
            dV1 = dV;
          else
            b1 = b;
            b0 = b;
            dV1 = dV;
          end
        end
        %fprintf('%d: %d iterations\n',ga,iter);
        %b = (b1+b0)/2;
        %bcut(ga,c) = b;
        %if(ga ==galpha_length)
        %  b0 = -4;
        %else
        %  b0 = max(bcut(ga+1:galpha_length,cc));
        %end;
        bcut(ga,c) = fzero(@(b) vdiff(w0,zi(cc),zi(cp1),q(ga,:),exp(b),guar(cc), ...
                                      guar(cp1),valParam),b);
        fprintf('a=%.4g c=%d b=%.8g f=%.8g b2 = %.8g f2 = %.8g\n',log(galpha(ga)),c, ...
                bcut(ga,c),vdiff(w0,zi(cc),zi(cp1),q(ga,:), ...
                                 exp(bcut(ga,c)),guar(cc),guar(cp1),valParam),b,dV);
      end; % loop over c
      fprintf('Group %d of %d: point %d of %d finished\n', ...
              group,size(cfg.cut.z,1),ga,galpha_length);
      if (bcut(ga,1)>bcut(ga,2))
        fprintf('     logalpha=%g, 5 year never chosen\n', ...
                log(galpha(ga)));
      end
    end; % loop over ga
    la = log(galpha);
    lb = bcut;
    lb(lb==0) = -Inf;
    bcut = exp(lb);
    plot(la,lb(:,1),la,lb(:,2),la,lb(:,3));
    legend('cut1','cut2','cut3');
  
    bcut_orig=bcut;
    for i = 1:choices,
      ga = 1;
      while(bcut(ga,i)==0)
        ga = ga+1;
        if (ga > size(bcut,1)-1)
          fprintf('WARNING: unable to find cutoff for any alpha\n');
          ga = size(bcut,1)-1;
          break;
        end
      end
      if (ga>1);
        warning('bcut will contain extrapolations');
        la = log(galpha);
        bcut(1:(ga-1),i) = exp(lb(ga,i)+(la(1:(ga-1))- ...
                                         la(ga))*(lb(ga,i)-lb(ga+1, ... 
                                                  i))/(la(ga)-la(ga+1))); 
      end
      ga = galpha_length;
      while(bcut(ga,i)==0)
        ga = ga-1;
      end
      if (ga==0)
          continue;
      elseif (ga<galpha_length);
        warning('bcut will contain extrapolations');
        la = log(galpha);
        bcut((ga+1):galpha_length,i) = exp(lb(ga,i) + ...
                                           (la((ga+1):galpha_length)-la(ga))*(lb(ga,i)-lb(ga-1,i))/ ...
                                           (la(ga)-la(ga-1))); 
      end
    end
    
    lb = log(bcut);
    figure;
    plot(la,lb(:,1),la,lb(:,2));
    
    cut(group).lb = lb;
    cut(group).lb_orig = log(bcut_orig);
    cut(group).z = z_slope;
    cut(group).cfg = cfg; % store all config info
    cut(group).rho = valParam.rho;
    cut(group).delta = valParam.delta;
    cut(group).inflation = valParam.infl;
    cut(group).agemin = cfg.cut.agemin(group);
    cut(group).zeta = valParam.zeta;
    cut(group).public = cfg.cut.public;
  end % loop over groups
  %profile viewer
  toc
  %eval(['save ' cfg.cut.file ' cut']);
end % function findbcut()  


