function pdie(data,est,cfg,prefix);

%   if (data.ngroup>5)
%     ngroup = 0;
%     dg = zeros(data.N,1);
%     for s = 0:1
%       for cage = min(data.cage):max(data.cage)
%         m = data.male==s & data.cage==cage;
%         if (sum(m)>0)
%           ngroup = ngroup+1;
%           dg(m) = ngroup;
%         end
%       end
%     end
%   else
    dg = data.group;
    ngroup = data.ngroup;
%  end

  if(isfield(est,'fasthazard'))
    h = est.fasthazard;
  else 
    h = 1;
  end

  if (strcmp(cfg.est.objective,'mlmort'))
    if(~strcmp(est.dist,'normal'))
      error(['pdie with mlmort and distribution non-normal is not' ...
             ' implemented']);
    end
    logAlpha = ones(data.N,1)*est.int.x'*sqrt(est.parm.var(1,1)) + ...
        data.alphaX*est.parm.gAlpha*ones(1,est.int.n);
    alpha = exp(logAlpha);
    wp = normpdf(est.int.x,0,1).*est.int.w;
    lambda = est.lambda;      
    m = data.fage==data.maxAge;
    data.maxAge(m) = data.maxAge(m) + 0.25;
    ef_em = exp(lambda*data.fage.^h) - exp(lambda*(data.maxAge.^h)); 
    ef_em = ef_em * ones(1,est.int.n);
    condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n,1) + ...
             est.parm.var(1,2)/est.parm.var(1,1) * ... 
             (logAlpha - ...
              data.alphaX*est.parm.gAlpha*ones(1,est.int.n)); 
    condsig = sqrt(est.parm.var(2,2) -  ...
                   est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
              ones(data.N,est.int.n);            
    Pda = 1 - exp(alpha/lambda.*ef_em); % size N x int.n
    pdi = sum(Pda.*(ones(data.N,1)*wp'),2);
    pd(1) = mean(pdi);
    out = [prefix '/tables/pdie.tex'];
    of = fopen(out,'w');
    fprintf(of,'   & Observed & Estimated \\\\ \\hline \n');
    fprintf(of,'   & %.4g & %.4g \\\\ \n',sum(data.died)/data.N,pd(1));
    % by groups
    if(ngroup>1)
      for k=1:ngroup
        m = dg==k;
        fprintf(of,'\\hline \\multicolumn{3}{c}{%s} \\\\ \\hline \n',cfg.data.group(k).name);
        fprintf(of,' & %.4g & %.4g \\\\ \n', ...
                mean(data.died(m)),mean(pdi(m)));
      end
    end
    fclose(of);
    return;
  end

  x = transformParam(est.parm,'pack','gmm',est.dist);
  clear annuityGMM;
  [f g h gi m gmoments] = annuityGMM(x,est,data);
  pd(1) = 0;
  pd(2:4) =  m(2,3:5);
  pd(1) = pd(2)*m(2,1) + pd(3)*m(2,2) + pd(4)*(1-m(2,1) - m(2,2));

  out = [prefix '/tables/pdie.tex'];
  of = fopen(out,'w');
  fprintf(of,'Guarantee & Observed & Estimated \\\\ \\hline \n');
  fprintf(of,'All & %.4g & %.4g \\\\ \n',sum(data.died)/data.N,pd(1));
  j = 2;
  for g = 0:5:10,
    fprintf(of,'%d & %.4g & %.4g \\\\ \n',g, ...
            sum(data.died(data.g==g))/sum(data.g==g), ...
            pd(j));
    j = j+1;
  end;

  out = [prefix '/tables/prop.tex'];
  of2 = fopen(out,'w');
  fprintf(of2,'Guarantee & 0 & 5 & 10 \\\\ \\hline \n');
  fprintf(of2,'Observed ');
  fprintf(of2,'& %.4g ',[m(1,1:2) 1-m(1,1)-m(1,2)]);
  fprintf(of2,' \\\\ \n');
  fprintf(of2,'Estimated ');
  fprintf(of2,'& %.4g ',[m(2,1:2) 1-m(2,1)-m(2,2)]);
  fprintf(of2,' \\\\ \n');

  % by groups
  if(ngroup>1)
    % create plots of fits against year 
    % write overall fit for external & internal to table
    bigGroup = []; % groups based on cage and male and external
    minYear = 2000;
    maxYear = 0;
    for g=1:data.ngroup
      group = cfg.data.group(g);
      if (minYear>group.year)
        minYear = group.year;
      end
      if (maxYear<group.year)
        maxYear = group.year;
      end
      found =0;
      for G=1:length(bigGroup)
        if ((group.male == bigGroup(G).male) && ...
            (group.cage == bigGroup(G).cage) && ...
            (group.external==bigGroup(G).external))
          found =1;
          break;
        end
      end
      if (found==0)
        newg.male = group.male;
        newg.cage = group.cage; 
        newg.external=group.external;
        newg.name = num2str(newg.cage);
        if(newg.male)
          newg.name = [newg.name ' Male '];
        else
          newg.name = [newg.name ' Female '];
        end
        if(newg.external)
          newg.name = [newg.name 'External'];
        else
          newg.name = [newg.name 'Internal'];
        end
        bigGroup = [bigGroup, newg];
      end
    end
    time = minYear:1:(maxYear);
    for G=1:length(bigGroup)
      bigGroup(G).z = zeros(length(time),3);
      bigGroup(G).p = bigGroup(G).z;
      bigGroup(G).d = bigGroup(G).z;
      bigGroup(G).phat = bigGroup(G).p;
      bigGroup(G).dhat = bigGroup(G).d;      
      bigGroup(G).w = zeros(1,length(time));
      for g=1:data.ngroup
        group=cfg.data.group(g);
        if ((group.male == bigGroup(G).male) && ...
            (group.cage == bigGroup(G).cage) && ...
            (group.external==bigGroup(G).external))
          it = (time == group.year);
          if (sum(it)~=1)
            [min(time) max(time)]
            group.year + (group.quarter-1)/4;
          end
          if (bigGroup(G).z(it,:)~=[0 0 0])
            error('Double z');
          end
          bigGroup(G).z(it,:) = group.z;
          bigGroup(G).p(it,:) = [mean(data.g(data.group==g)==0) ...
                    mean(data.g(data.group==g)==5) ...
                    mean(data.g(data.group==g)==10)];
          bigGroup(G).d(it,:) = [mean(data.died(data.group==g & data.g==0)) ...
                    mean(data.died(data.group==g & data.g==5)) ...
                    mean(data.died(data.group==g & data.g==10))];
          bigGroup(G).w(it) = sum(data.group==g);
          bigGroup(G).phat(it,:) = [reshape(gmoments(g,2,1:2),1,2) ...
                    1-sum(gmoments(g,2,1:2))]; 
          bigGroup(G).dhat(it,:) = gmoments(g,2,3:5);
        end % if
      end % loop over g
      bigGroup(G).w = bigGroup(G).w/sum(bigGroup(G).w);
    end % loop over G
    
    % add to table
    for k=1:length(bigGroup)
      fprintf(of,'\\hline \\multicolumn{3}{c}{%s} \\\\ \\hline \n',bigGroup(k).name);
      j = 1;
      for g = 0:5:10,
        fprintf(of,'%d & %.4g & %.4g \\\\ \n',g, ...
                bigGroup(k).w*bigGroup(k).d(:,j), ...
                bigGroup(k).w*bigGroup(k).dhat(:,j));
        j = j+1;
      end
      fprintf(of2,'\\hline \\multicolumn{4}{c}{%s} \\\\ \\hline \n',bigGroup(k).name);
      fprintf(of2,'Observed ');
      fprintf(of2,'& %.4g ',bigGroup(k).w*bigGroup(k).p);
      fprintf(of2,' \\\\ \n');
      fprintf(of2,'Estimated ');
      fprintf(of2,'& %.4g ',bigGroup(k).w*bigGroup(k).phat);
      fprintf(of2,' \\\\ \n');

      % make graph showing year by year changes
      if (minYear<maxYear)
        figure;
        subplot(2,1,1),plot(time,bigGroup(k).p(:,1), ...
                            time,bigGroup(k).p(:,2),'--', ...
                            time,bigGroup(k).p(:,3),'-.', ...
                            time,bigGroup(k).phat(:,1), ...
                            time,bigGroup(k).phat(:,2),'--', ...
                            time,bigGroup(k).phat(:,3),'-.');
        legend('0 Observed','5 Observed','10 Observed', ...
             '0 Estimated','5 Estimated','10 Estimated');
        ylabel('Proportion');
        xlabel('Year');
        title(sprintf('%s Choice Proportions',bigGroup(k).name));
        subplot(2,1,2),plot(time,bigGroup(k).d(:,1), ...
                            time,bigGroup(k).d(:,2),'--', ...
                            time,bigGroup(k).d(:,3),'-.', ...
                            time,bigGroup(k).dhat(:,1), ...
                            time,bigGroup(k).dhat(:,2),'--', ...
                            time,bigGroup(k).dhat(:,3),'-.');
        legend('0 Observed','5 Observed','10 Observed', ...
               '0 Estimated','5 Estimated','10 Estimated');
        ylabel('Prob(Die)');
        xlabel('Year');
        title(sprintf('%s Conditional Death Probabilities', ...
                      bigGroup(k).name));
        filename = [prefix '/figures/yearFit' num2str(k) '.eps'];
        print(filename,'-depsc2');
      end % end if minYear<maxYear
    end % loop over k
  end % if ngroup>1
  fclose(of);
  fclose(of2);
