function reducedForm(data,cfg,prefix)
% creates table and graph of reduced form survival function estimates
  
% table of observed proportions
  filename = [prefix '/tables/rfPdie.tex'];
  of = fopen(filename,'w');
  fprintf(of,'Guarantee & \\%% Choose & \\%% Die \\\\ \\hline \n');
  for g=0:5:10
    fprintf(of,'%d years & %8.4g & %8.4g \\\\ \n',g,mean(data.g==g), ...
            mean(data.died(data.g==g)));
  end
  fclose(of);
  
  % table of sample size
  filename = [prefix '/tables/size.tex'];
  of = fopen(filename,'w');
  fprintf(of,'Group & N \\\\ \\hline \n');
  fprintf(of,' All & %d \\\\ \n',data.N);
  if (data.ngroup>1)
    ngroup = min(data.ngroup,5);
    for g=1:data.ngroup
      fprintf(of,' %s & %d \\\\ \n',cfg.data.group(g).name, ...
              sum(data.group==g));
    end
  end
  fclose(of);
  
  % reduced form estimates, done in stata
  of = fopen('temp.txt','w');
  for n=1:data.N
    fprintf(of,'%g\t',[data.fage(n),data.lage(n),data.died(n),data.g(n), ...
                       data.year(n),data.cage(n)]);
    fprintf(of,'\n');
  end
  fclose(of);
  of = fopen('temp.do','w');
  fprintf(of,['clear \n set mem 100m\n set more off \n' ...
              'infile fage lage died g year cage using temp.txt\n' ...
              'replace lage = lage+0.25 if lage==fage\n' ...
              'gen g5=0\n replace g5=1 if g==5\n' ...
              'gen g10=0\n replace g10=1 if g==10\n' ...
              'gen a65=0\n replace a65=1 if cage==65\n' ...
              ...              %'xi i.year\n' ...
              'label var g5 "5 year guarantee"\n' ...
              'label var g10 "10 year guarantee"\n' ...
              'label var a65 "age = 65"\n' ...
              ...              %'foreach var of varlist _I* {\n' ...
              ...              %' su year if `var''==1\n' ...
              ...              %' local y = r(mean)\n' ...
              ...              %' label var `var'' "year = `y''"\n' ...
              ...              %'}\n' ...
              'stset lage, failure(died==1) enter(fage)\n' ...
              ...              %'streg g5 g10 a65 _Iyear*, dist(gompertz)\n' ...
              'streg g5 g10 a65, dist(gompertz)\n' ...
              'mat b = get(_b)\n' ...
              'mat V = get(VCE)\n' ...
              'mata\n' ...
              ' beta = st_matrix("b")\n' ...
              ' names = st_matrixcolstripe("b")\n' ...
              ' V = st_matrix("V") \n' ...
              ' unlink("gompest.txt")\n' ...
              ' unlink("labels.txt")\n' ...
              ' of = fopen("gompest.txt","w")\n' ...
              ' of2 = fopen("labels.txt","w")\n' ...
              ' for (i=1;i<=cols(beta);i++) {\n' ...
              '   fwrite(of,sprintf("%%20.16g %%20.16g\\n",beta[i],' ...
              ' sqrt(V[i,i])))\n'  ...
              '   if (names[i,2]=="_cons") {\n' ...
              '     fwrite(of2,sprintf("Constant\\n"))\n' ...
              '   }\n' ...
              '   else {\n' ...
              '     fwrite(of2,sprintf("%%s\\n",st_varlabel(names[i,2])))\n' ...
              '   }\n' ...
              ' } \n fclose(of)\n fclose(of2)\n' ... 
              'end\n clear\n exit\n']);
  fclose(of);
  !stata -b do temp;
  in = fopen('labels.txt','r');
  k = 1;
  str(k).s = fgetl(in);
  while (str(k).s~=-1)
    k = k+1;
    str(k).s = fgetl(in);
  end
  fclose(in);
  str = str(1:k-1);
  k = length(str)-1;
  str(k).s = 'Constant';
  k = k+1;
  str(k).s = '$\lambda$';
  filename = [prefix '/tables/rfMort.tex'];
  of = fopen(filename,'w');
  fprintf(of,'Variable & Estimate & Std.Err.  \\\\ \\hline \n');
  load gompest.txt;
  table = gompest;
  for i =1:size(table,1)
    fprintf(of,'%s & %.4g & (%.4g) \\\\ \n',str(i).s,table(i,:));
  end
  % find the mean life expectancy
  if (1)
    if (any(data.cage==65))
      X = [data.g==5 data.g==10 data.fage==65];
    else
      X = [data.g==5 data.g==10];
    end
    for k=4:length(str)-2
      y = strread(str(k).s,'year = %d');
      X = [X data.year==y];
    end
    X = [X ones(data.N,1)];
    beta = table(1:length(str)-1,1);
    lambda = table(length(str),1);
    size(X)
    alpha = exp(X*beta);
    dt = 0.1;
    t = 0.0:dt:200;
    elife = sum( exp(kron(alpha/lambda,(1-exp(lambda*t)))) ...
                 *dt,2) + data.baseage;
    [min(elife) mean(elife) max(elife)];
    fprintf(of,['\\hline \\multicolumn{2}{l}{Mean Life Expectancy} &' ...
                ' %.4g \\\\ \n'],mean(elife));
  end
  fclose(of);
  
  %delete gompest.txt temp.do temp.txt temp.log labels.txt surv.txt;
  
  
  % reduced form survival curves
  %   dt = 1.0;
  %   t = 0:dt:40;
  %   s = zeros(3,length(t));
  %   alpha(1) = gompestAll(4,1);
  %   alpha(2) = gompestAll(4,1)+gompestAll(1,1);
  %   alpha(3) = gompestAll(4,1)+gompestAll(2,1);
  %   alpha = alpha + mean(gompestAll(3,1)*data.alphaX(2));
  %   alpha = exp(alpha);
  %   lambda = gompestAll(5,1);
  %   for g=1:3
  %     s(g,:) = exp(alpha(g)/lambda*(1-exp(lambda*t)));
  %   end
  %   figure;
  %   t = t+65;
  %   plot(t,s(1,:),'--',t,s(2,:),'.-',t,s(3,:));
  %   ylabel('P(S>t)');
  %   xlabel('Age');
  %   legend('0','5','10');
  %   filename = [prefix '/figures/rfSurvival.eps'];
  %   print(filename,'-depsc2');

  if(isfield(cfg.data,'lp'))
    bigGroup = []; % groups based on cage and male and external
    minYear = 2000;
    maxYear = 0;
    for g=1:data.ngroup
      group = cfg.data.group(g);
      if (minYear>group.year)
        minYear = group.year;
      end
      if (maxYear<group.year)
        maxYear = group.year;
      end
      found =0;
      for G=1:length(bigGroup)
        if ((group.male == bigGroup(G).male) && ...
            (group.cage == bigGroup(G).cage) && ...
            (group.external==bigGroup(G).external))
          found =1;
          break;
        end
      end
      if (found==0)
        newg.male = group.male;
        newg.cage = group.cage; 
        newg.external=group.external;
        newg.name = num2str(newg.cage);
        if(newg.male)
          newg.name = [newg.name ' Male '];
        else
          newg.name = [newg.name ' Female '];
        end
        if(newg.external)
          newg.name = [newg.name 'External'];
        else
          newg.name = [newg.name 'Internal'];
        end
        bigGroup = [bigGroup, newg];
      end
    end
    if(minYear==maxYear)
      return;
    end
    time = minYear:1:(maxYear);
    for G=1:length(bigGroup)
      bigGroup(G).z = zeros(length(time),3);
      bigGroup(G).p = bigGroup(G).z;
      for g=1:data.ngroup
        group=cfg.data.group(g);
        if ((group.male == bigGroup(G).male) && ...
            (group.cage == bigGroup(G).cage) && ...
            (group.external==bigGroup(G).external))
          it = (time == group.year);
          if (sum(it)~=1)
            [min(time) max(time)]
            group.year + (group.quarter-1)/4;
          end
          if (bigGroup(G).z(it,:)~=[0 0 0])
            error('Double z');
          end
          bigGroup(G).z(it,:) = group.z;
          bigGroup(G).p(it,:) = [mean(data.g(data.group==g)==0) ...
                    mean(data.g(data.group==g)==5) ...
                    mean(data.g(data.group==g)==10)];
        end
      end
      figure;
      subplot(2,1,1),plot(time,bigGroup(G).z(:,1), ...
                          time,bigGroup(G).z(:,2),'--', ...
                          time,bigGroup(G).z(:,3),'-.');
      legend('0','5','10');
      ylabel('Rate');
      xlabel('Year');
      title(sprintf('%s Payment Rates',bigGroup(G).name));
      subplot(2,1,2),plot(time,bigGroup(G).p(:,1), ...
                          time,bigGroup(G).p(:,2),'--', ...
                          time,bigGroup(G).p(:,3),'-.');
      legend('0','5','10');      
      ylabel('Proportion');
      xlabel('Year');
      title(sprintf('%s Choice Proportions',bigGroup(G).name));
      filename = [prefix '/figures/rateProp' num2str(G) '.eps'];
      print(filename,'-depsc2');
    end % loop over G
  end
end % end function reducedForm()
