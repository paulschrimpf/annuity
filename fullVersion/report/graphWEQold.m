function graphWEQ(est,data,cfg,cfdata,prefix)
  
  guar = 5;
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  nchoice = 3;
  lambda = est.lambda; 
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end

  [la lb] = sampleAlphaBeta(data,est);
  
  nbin = 50;
  q= quantile(la,[0.0, 1.0]);
  agrid = q(1):(q(2)-q(1))/(nbin-1):q(2);
  q= quantile(lb,[0.0, 1.0]);
  bgrid = q(1):(q(2)-q(1))/(nbin-1):q(2);
  
  f = zeros(nbin,nbin);

  gl = [0 5 10];

  for a=1:nbin
    z_slope = cfg.cut.z(1,:);
    agemin = cfg.cut.agemin(1);
    T = agemax - agemin;
    % parameters that optionally vary with group
    rho = cfg.cut.rho(1);
    delta = cfg.cut.delta(1);
    zeta = cfg.cut.zeta(1);
    infl = cfg.cut.inflation(1);
    
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                  % rate factors    
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    galpha = exp(agrid(a));
    part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
    part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    
    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    fr     = (part1(1:T) - part2(1:T))';          % density  

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;

    for b = 1:nbin
      % find which guarantee region we're in
      e(:,1) = interp1(est.cut(1).la,est.cut(1).lb(:,1), ...
                       agrid(a),'linear','extrap');
      e(:,2) = interp1(est.cut(1).la,est.cut(1).lb(:,2), ...
                       agrid(a),'linear','extrap');
      e(isnan(e)) = Inf;
      [e bgrid(b)]
      
      c = 1*(bgrid(b)<=e(:,1)) + 2*(bgrid(b)>e(:,1) & bgrid(b)<e(:,2)) + ...
          3*(bgrid(b)>=e(:,2));
      c
      pubrate = pubAnnuityRate(infl,rho,q);
      Z = w0*(alpha2*z_slope(c) + ... % formula for annuity pricing
              pub*pubrate);
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
      %                              % value
      v = vf3(w0,z_slope(c),q,exp(bgrid(b)),gl(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      f(a,b) = weq(v,wlo,whi,q,exp(bgrid(b)),vp);

      % symmetric info
      for c=1:nchoice
        Z = w0*alpha2*z_slope(c); % formula for annuity pricin
        Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
        cumz = cumsum(Z.*rdis);
        if(gl(c)>0) 
          epay(c) = cumz(gl(c))*sum(fr(1:gl(c))) + ...
            sum(cumz(gl(c)+1:T).*fr(gl(c)+1:T));
        else
          epay(c) = sum(cumz.*fr);
        end
      end
      epay = epay/sum(fr);
      zi = z_slope.*cfdata.s.epay(1,1)./epay;
      for c = 1:nchoice
        pubrate = pubAnnuityRate(infl,rho,q);
        Z = w0*(alpha2*zi(c)); % formula for annuity pricing
        Z = (Z*ones(T,1)).*inf_factor; 
        val = vf3(w0,zi(c),q,exp(bgrid(b)),gl(c),vp);
        wlo = w0*(1-alpha2);
        whi = wlo + sum(Z);
        % wealth equivalent
        sfc(c,a,b) =weq(val,wlo,whi,q,exp(bgrid(b)),vp);
      end
      sf(a,b) = max(sfc(:,a,b));
      
      % mandates
      % determine annuity payment rates
      for g=1:cfdata.ngroup
        totalPay(g) = 0.0; % total payments made in equilibrium
        for n=1:cfdata.N
          if (cfdata.group(n)==g)
            totalPay(g) = totalPay(g) + cfdata.epay(n,cfdata.oc(n));
          end
        end
        mand_zslope(g,:) = cfg.cut.z(g,:).* ...
            totalPay(g)./sum(cfdata.epay(cfdata.group==g,:),1);
      end
      for c = 1:nchoice,        % guarantee choice
        pubrate = pubAnnuityRate(infl,rho,q);
        Z = w0*(alpha2*mand_zslope(1,c)); % formula for annuity pricing
        Z = (Z*ones(T,1)).*inf_factor; 
        val = vf3(w0,mand_zslope(1,c),q,exp(bgrid(b)),gl(c),vp);
        wlo = w0*(1-alpha2);
        whi = wlo + sum(Z);
        % wealth equivalent
        mf(c,a,b) = weq(val,wlo,whi,q,exp(bgrid(b)),vp);
      end
      [a b]
    end
  end

  eval(['save ' prefix '/weqData agrid bgrid sf sfc mf f']);

  figure;
  surf(agrid,bgrid,f);
  xlabel('log \alpha');
  ylabel('log \beta');
  zlabel('Wealth Equivalent');
  colormap('gray'); 
  filename = [prefix '/figures/weqSurf.eps'];
  print(filename,'-depsc2');
  
  figure;
  % put cutoffs and distribution on same axes
  cut = est.cut;
  plot(la,lb,'.k',cut.la,cut.lb(:,1),'k',cut.la,cut.lb(:,2),'k');
  colormap('gray');
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  h1 = gca;
  set(h1,'DefaultLineLineWidth',2);
  h2 = axes('Position',get(h1,'Position'));
  [con h]=contour(agrid,bgrid,f);
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  set(h2,'Color','none');
  clabel(con,h);
  xlabel('log \alpha');
  ylabel('log \beta');
  zlabel('Wealth Equivalent');
  colormap('gray');
  filename = [prefix '/figures/weqContour.eps'];
  print(filename,'-depsc2');


  figure;
  % put cutoffs and distribution on same axes
  cut = est.cut;
  plot(la,lb,'.k',cut.la,cut.lb(:,1),'k',cut.la,cut.lb(:,2),'k');
  colormap('gray');
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  h1 = gca;
  set(h1,'DefaultLineLineWidth',2);
  h2 = axes('Position',get(h1,'Position'));
  [con h]=contour(agrid,bgrid,sf);
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  set(h2,'Color','none');
  clabel(con,h);
  xlabel('log \alpha');
  ylabel('log \beta');
  zlabel('Wealth Equivalent');
  colormap('gray');
  filename = [prefix '/figures/weqSym.eps'];
  print(filename,'-depsc2');

  for c=1:3
    figure;
    % put cutoffs and distribution on same axes
    cut = est.cut;
    plot(la,lb,'.k',cut.la,cut.lb(:,1),'k',cut.la,cut.lb(:,2),'k');
    colormap('gray');
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    h1 = gca;
    set(h1,'DefaultLineLineWidth',2);
    h2 = axes('Position',get(h1,'Position'));
    gmf = squeeze(mf(c,:,:));
    [con h]=contour(agrid,bgrid,gmf);
    axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
    set(h2,'Color','none');
    clabel(con,h);
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent');
    colormap('gray');
    filename = [prefix '/figures/weqM' num2str(gl(c)) '.eps'];
    print(filename,'-depsc2');
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % contours of differences
  figure;
  % put cutoffs and distribution on same axes
  cut = est.cut;
  plot(la,lb,'.k',cut.la,cut.lb(:,1),'k',cut.la,cut.lb(:,2),'k');
  colormap('gray');
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  h1 = gca;
  set(h1,'DefaultLineLineWidth',2);
  h2 = axes('Position',get(h1,'Position'));
  [con h]=contour(agrid,bgrid,sf-f);
  axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
  set(h2,'Color','none');
  clabel(con,h);
  xlabel('log \alpha');
  ylabel('log \beta');
  zlabel('Wealth Equivalent Difference');
  colormap('gray');
  filename = [prefix '/figures/weqSymDiff.eps'];
  print(filename,'-depsc2');

  for c=1:3
    figure;
    % put cutoffs and distribution on same axes
    cut = est.cut;
    plot(la,lb,'.k',cut.la,cut.lb(:,1),'k',cut.la,cut.lb(:,2),'k');
    colormap('gray');
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    h1 = gca;
    set(h1,'DefaultLineLineWidth',2);
    h2 = axes('Position',get(h1,'Position'));
    gmf = squeeze(mf(c,:,:));
    [con h]=contour(agrid,bgrid,gmf-f);
    axis([quantile(la,[0.0 1]) ...
        quantile(lb, [0.0 1]) ]);    
    set(h2,'Color','none');
    clabel(con,h);
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent Difference');
    colormap('gray');
    filename = [prefix '/figures/weqM' num2str(gl(c)) 'Diff.eps'];
    print(filename,'-depsc2');
  end


end
