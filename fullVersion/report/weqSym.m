%----------------------------------------------------------------------
function [str table] = weqSym(cfdata)
  oc = cfdata.oc;
  ow = zeros(cfdata.N,1);
  for i = 1:cfdata.N
    ow(i) = cfdata.weq(i,oc(i));
    op(i) = cfdata.epay(i,oc(i));
  end
  mepay = mean(op);
  soc = cfdata.s.oc;
  sow = zeros(cfdata.N,1);
  mR = sow;
  for i = 1:cfdata.N
    sow(i) = cfdata.s.weq(i,soc(i));
    mR(i) = cfdata.maxRisk(cfdata.group(i));
  end  

  k = 1;
  str(k).s = 'Maximal Amount at Risk';
  table(k,:) = ones(1,4)*mean(mR);
  k = k+1;
  str(k).s = 'Current (asymm. info)';
  table(k,:) = [mean(ow) mean(cfdata.weq(cfdata.g==0,1)) ...
                mean(cfdata.weq(cfdata.g==5,2)) ...
                mean(cfdata.weq(cfdata.g==10,3))];
  k = k+1;
  str(k).s = 'Symm. Info.';
  table(k,:) = [mean(sow) mean(sow(cfdata.g==0)) ...
                mean(sow(cfdata.g==5)) ...
                mean(sow(cfdata.g==10))];
  k = k+1;
  str(k).s = 'Relative Difference';
  ti = -([ow cfdata.weq] - [sow sow sow sow])./(mR*ones(1,4));
  table(k,:) = [mean(ti(:,1)) mean(ti(cfdata.g==0,2)) ...
                mean(ti(cfdata.g==5,3)) ...
                mean(ti(cfdata.g==10,4))];
  k = k+1;
end
%----------------------------------------------------------------------
