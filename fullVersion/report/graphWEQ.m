function graphWEQ(est,data,cfg,cfdata,prefix)
  
  guar = 5;
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  nchoice = 3;
  lambda = est.lambda; 
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end

  [la lb] = sampleAlphaBeta(data,est);
  
  nbinAll = 10; %100;
  nbinGr = 0;
  nbin = nbinAll + nbinGr;
  x = la;
  m = min(la);
  M = max(la);
  s = (M-m)/(nbinAll-1);
  agridAll = m:s:M;
  x = lb;
  m = min(lb);
  M = max(lb);
  s = (M-m)/(nbinAll-1);
  bgridAll = m:s:M;

  f = zeros(data.ngroup,nbin,nbin);

  gl = [0 5 10];
  if(1) 
  for gr=1:data.ngroup
    %x = la(data.group==gr);
    %a = quantile(x,1/(nbinGr+1):1/(nbinGr+1):nbinGr/(nbinGr+1));
    %x = lb(data.group==gr);
    %b = quantile(x,1/(nbinGr+1):1/(nbinGr+1):nbinGr/(nbinGr+1));
    %agrid(gr,:) = sort([ agridAll]);
    %bgrid(gr,:) = sort([ bgridAll]);
    m = min(la(data.group==gr));
    M = max(la(data.group==gr));
    s = (M-m)/(nbinAll-1);
    agrid(gr,:) =  m:s:M;
    m = min(lb(data.group==gr));
    M = max(lb(data.group==gr));
    s = (M-m)/(nbinAll-1);
    bgrid(gr,:) =  m:s:M;
    for a=1:nbin
      z_slope = cfg.cut.z(gr,:);
      agemin = cfg.cut.agemin(gr);
      T = agemax - agemin;
      % parameters that optionally vary with group
      rho = cfg.cut.rho(gr);
      delta = cfg.cut.delta(gr);
      zeta = cfg.cut.zeta;
      infl = cfg.cut.inflation(gr);
      
      inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                    % rate factors    
      rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
      base = cfg.mort.baseage;
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      galpha = exp(agrid(gr,a));
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    
      q = (part1 - part2)./part1;
      q(part1==0) = 0;
      fr     = (part1(1:T) - part2(1:T))';          % density  
      
      vp.rho = rho;
      vp.delta = delta;
      vp.T = T;
      vp.zeta = zeta;
      vp.gam = gam;
      vp.bgam = bgam;
      vp.pub = pub;
      vp.infl = infl;
      vp.frac = alpha2;
      
      for b = 1:nbin
        % find which guarantee region we're in
        e(:,1) = interp1(est.cut(gr).la,est.cut(gr).lb(:,1), ...
                         agrid(gr,a),'linear','extrap');
        e(:,2) = interp1(est.cut(gr).la,est.cut(gr).lb(:,2), ...
                         agrid(gr,a),'linear','extrap');
        e(isnan(e)) = Inf;
        [e bgrid(gr,b)]
        
        c = 1*(bgrid(gr,b)<=e(:,1)) + 2*(bgrid(gr,b)>e(:,1) & bgrid(gr,b)<e(:,2)) + ...
            3*(bgrid(gr,b)>=e(:,2));
        c
        pubrate = pubAnnuityRate(infl,rho,q);
        Z = w0*(alpha2*z_slope(c) + ... % formula for annuity pricing
                pub*pubrate);
        Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     %                              % value
        v = vf3(w0,z_slope(c),q,exp(bgrid(gr,b)),gl(c),vp);
        wlo = w0*(1-alpha2);
        whi = wlo + sum(Z);
        f(gr,a,b) = weq(v,wlo,whi,q,exp(bgrid(gr,b)),vp);

        % symmetric info
        for c=1:nchoice
          Z = w0*alpha2*z_slope(c); % formula for annuity pricin
          Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
          cumz = cumsum(Z.*rdis);
          if(gl(c)>0) 
            epay(c) = cumz(gl(c))*sum(fr(1:gl(c))) + ...
                      sum(cumz(gl(c)+1:T).*fr(gl(c)+1:T));
          else
            epay(c) = sum(cumz.*fr);
          end
        end
        epay = epay/sum(fr);
        zi = z_slope.*mean(cfdata.s.epay(cfdata.group==gr,1))./epay;
        for c = 1:nchoice
          pubrate = pubAnnuityRate(infl,rho,q);
          Z = w0*(alpha2*zi(c) + pub*pubrate); % formula for annuity pricing
          Z = (Z*ones(T,1)).*inf_factor; 
          val = vf3(w0,zi(c),q,exp(bgrid(gr,b)),gl(c),vp);
          wlo = w0*(1-alpha2);
          whi = wlo + sum(Z);
          % wealth equivalent
          sfc(gr,c,a,b) =weq(val,wlo,whi,q,exp(bgrid(gr,b)),vp);
        end
        sf(gr,a,b) = max(sfc(gr,:,a,b));
        
        % mandates
        % determine annuity payment rates
        totalPay(gr) = 0.0; % total payments made in equilibrium
        for n=1:cfdata.N
          if (cfdata.group(n)==gr)
            totalPay(gr) = totalPay(gr) + cfdata.epay(n,cfdata.oc(n));
          end
        end
        mand_zslope(gr,:) = cfg.cut.z(gr,:).* ...
            totalPay(gr)./sum(cfdata.epay(cfdata.group==gr,:),1);
        for c = 1:nchoice,        % guarantee choice
          Z = w0*(alpha2*mand_zslope(gr,c)); % formula for annuity pricing
          Z = (Z*ones(T,1)).*inf_factor; 
          val = vf3(w0,mand_zslope(gr,c),q,exp(bgrid(gr,b)),gl(c),vp);
          wlo = w0*(1-alpha2);
          whi = wlo + sum(Z);
          % wealth equivalent
          mf(gr,c,a,b) = weq(val,wlo,whi,q,exp(bgrid(gr,b)),vp);
        end
        [a b]
      end
    end
  end
  
  eval(['save ' prefix '/weqData agrid bgrid sf sfc mf f']);
  end
  eval(['load ' prefix '/weqData']);
  
  [loga logb] = sampleAlphaBeta(data,est);

  for gr=1:data.ngroup
    a = agrid(gr,:);
    b = bgrid(gr,:);
    la = loga(data.group==gr);
    lb = logb(data.group==gr);
    figure;
    surf(a,b,squeeze(f(gr,:,:)));
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent');
    colormap('gray'); 
    filename = [prefix '/figures/weqSurf' num2str(gr) '.eps'];
    print(filename,'-depsc2');
    
    figure;
    % put cutoffs and distribution on same axes
    cut = est.cut;
    plot(loga(data.group==gr),logb(data.group==gr),'.k', ...
         cut(gr).la,cut(gr).lb(:,1),'k',cut(gr).la,cut(gr).lb(:,2),'k');
    colormap('gray');
    axis([quantile(la, [0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    h1 = gca;
    set(h1,'DefaultLineLineWidth',2);
    h2 = axes('Position',get(h1,'Position'));
    [con h]=contour(a,b,squeeze(f(gr,:,:)));
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    set(h2,'Color','none');
    clabel(con,h);
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent');
    colormap('gray');
    filename = [prefix '/figures/weqContour' num2str(gr) '.eps'];
    print(filename,'-depsc2');
  
    
    figure;
    % put cutoffs and distribution on same axes
    cut = est.cut;
    plot(loga(data.group==gr),logb(data.group==gr),'.k', ...
         cut(gr).la,cut(gr).lb(:,1),'k',cut(gr).la,cut(gr).lb(:,2),'k');
    colormap('gray');
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    h1 = gca;
    set(h1,'DefaultLineLineWidth',2);
    h2 = axes('Position',get(h1,'Position'));
    [con h]=contour(a,b,squeeze(sf(gr,:,:)));
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    set(h2,'Color','none');
    clabel(con,h);
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent');
    colormap('gray');
    filename = [prefix '/figures/weqSym' num2str(gr) '.eps'];
    print(filename,'-depsc2');
    
    for c=1:3
      figure;
      % put cutoffs and distribution on same axes
      cut = est.cut;
      plot(loga(data.group==gr),logb(data.group==gr),'.k', ...
           cut(gr).la,cut(gr).lb(:,1),'k',cut(gr).la,cut(gr).lb(:,2),'k');
      colormap('gray');
      axis([quantile(la,[0.0 1]) ...
            quantile(lb, [0.0 1]) ]);    
      h1 = gca;
      set(h1,'DefaultLineLineWidth',2);
      h2 = axes('Position',get(h1,'Position'));
      gmf = squeeze(mf(gr,c,:,:));
      [con h]=contour(a,b,gmf);
      axis([quantile(la,[0.0 1]) ...
            quantile(lb, [0.0 1]) ]);    
      set(h2,'Color','none');
      clabel(con,h);
      xlabel('log \alpha');
      ylabel('log \beta');
      zlabel('Wealth Equivalent');
      colormap('gray');
      filename = [prefix '/figures/weqM' num2str(gr) '_' num2str(gl(c)) '.eps'];
      print(filename,'-depsc2');
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % contours of differences
    figure;
    % put cutoffs and distribution on same axes
    cut = est.cut;
    plot(loga(data.group==gr),logb(data.group==gr),'.k', ...
         cut(gr).la,cut(gr).lb(:,1),'k',cut(gr).la,cut(gr).lb(:,2),'k');
    colormap('gray');
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    h1 = gca;
    set(h1,'DefaultLineLineWidth',2);
    h2 = axes('Position',get(h1,'Position'));
    [con h]=contour(a,b,squeeze(sf(gr,:,:)-f(gr,:,:)));
    axis([quantile(la,[0.0 1]) ...
          quantile(lb, [0.0 1]) ]);    
    set(h2,'Color','none');
    clabel(con,h);
    xlabel('log \alpha');
    ylabel('log \beta');
    zlabel('Wealth Equivalent Difference');
    colormap('gray');
    filename = [prefix '/figures/weqSymDiff' num2str(gr) '.eps'];
    print(filename,'-depsc2');
    
    for c=1:3
      figure;
      % put cutoffs and distribution on same axes
      cut = est.cut;
      plot(loga(data.group==gr),logb(data.group==gr),'.k', ...
           cut(gr).la,cut(gr).lb(:,1),'k',cut(gr).la,cut(gr).lb(:,2),'k');
      colormap('gray');
      axis([quantile(la,[0.0 1]) ...
            quantile(lb, [0.0 1]) ]);    
      h1 = gca;
      set(h1,'DefaultLineLineWidth',2);
      h2 = axes('Position',get(h1,'Position'));
      gmf = squeeze(mf(gr,c,:,:));
      [con h]=contour(a,b,gmf-squeeze(f(gr,:,:)));
      axis([quantile(la,[0.0 1]) ...
            quantile(lb, [0.0 1]) ]);    
      set(h2,'Color','none');
      clabel(con,h);
      xlabel('log \alpha');
      ylabel('log \beta');
      zlabel('Wealth Equivalent Difference');
      colormap('gray');
      filename = [prefix '/figures/weqM' num2str(gr) '_' num2str(gl(c)) 'Diff.eps'];
      print(filename,'-depsc2');
    end
  end
  
end
