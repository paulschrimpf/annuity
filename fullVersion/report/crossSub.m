function [nosubData] = crossSub(data,est,cfdata,cfg,prefix)  
% Table for looking at cross subsidies.  
  
  % eliminate subsidies by scaling alpha
  nosubData = elimSub(cfdata,est,cfg);
  nosubData = runCF(nosubData,est,cfg);
  nosubData.oc = cfdata.oc;
  nosubData.g(nosubData.oc==1) = 0;
  nosubData.g(nosubData.oc==2) = 5;
  nosubData.g(nosubData.oc==3) = 10;
  
  % create tables, graphs, etc.
  mkdir(prefix,'nosub');
  nsprefix = [prefix '/nosub/'];
  mkdir(nsprefix,'tables');
  mkdir(nsprefix,'figures');

  cfTables(data,est,nosubData,cfg,nsprefix);  
  % graph the original and rescaled distributions
  out = [nsprefix '/figures/dist.eps'];
  la = log(cfdata.alpha);
  lb = log(cfdata.beta);
  nsla = log(nosubData.alpha);
  nslb = log(nosubData.beta);  
  figure;
  plot(la,lb,'.b',nsla,nslb,'.r')
  %axis([-5.5 -3.5 0 16]);
  xlabel('log(alpha)');
  ylabel('log(beta)');
  legend('original','without subsidies');
  print('-depsc2',out);
  
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------
function data = elimSub(dataIn,est,cfg)
  % eliminates cross choice subsidies by scaling alpha of 0 and 10 year
  % guarantee people
    
  TOL = 1.0e-10; % tolerance

  data = dataIn;
  laIn = log(dataIn.alpha);
  logBeta = log(dataIn.beta);
  % target epay
  epay5 = mean(dataIn.epay(dataIn.g==5,2));
  % get cutoffs
  for g=1:data.ngroup
    m = data.group==g;
    e(m,1) = interp1(est.cut(g).lb(:,1),est.cut(g).la, ...
                     logBeta(m),'linear','extrap');
    e(m,3) = interp1(est.cut(g).lb(:,2),est.cut(g).la, ...
                     logBeta(m),'linear','extrap');
  end
  for g=0:10:10 
    if (g==0)
      c = 1;
    else
      c = 3;
    end
    m = data.g==g;
    epay = mean(dataIn.epay(m,c));
    x = 1;
    if ((epay>epay5 && g==10) || ...
        (epay<epay5 && g==0)) % need to scale alpha away from cutoff
      xlo = 1;
      xhi = 2;
    else
      xlo = 0;
      xhi = 1;
    end
    la = (laIn - e(:,c))*xhi + e(:,c);
    data.alpha(m) = exp(la(m));
    ep = exppay(data,g,cfg);
    ephi = mean(ep(m));
    la = (laIn - e(:,c))*xlo + e(:,c);
    data.alpha(m) = exp(la(m));
    ep = exppay(data,g,cfg);
    eplo = mean(ep(m));
    if(~((eplo<epay5 && ephi>epay5) || ...
         (eplo>epay5 && ephi<epay5)))
      [xlo xhi]
      [eplo epay5 ephi]
      warning('not bracketed');
      if (xlo~=0 && xhi~=0)
        error('bad initial bracket');
      end
      x = 0; 
      la = (1+sign(laIn - e(:,c))*eps).*e(:,c); % make it just above or
                                               % below the cutoff
      data.alpha(m) = exp(la(m));
      ep = exppay(data,g,cfg);
      epay = mean(ep(m));
      continue;
    end
    data.alpha(m) = dataIn.alpha(m);
    its = 0;
    while(abs(epay-epay5)>TOL)
      x = 0.5*(xhi+xlo);
      la = (laIn - e(:,c))*x + e(:,c);
      data.alpha(m) = exp(la(m));
      ep = exppay(data,g,cfg);
      epay = mean(ep(m));
      if(epay>epay5)
        if(ephi>epay5)
          ephi = epay;
          xhi = x;
        else 
          eplo = epay;
          xlo = x;
        end
      else
        if(ephi<epay5)
          ephi = epay;
          xhi = x;
        else 
          eplo = epay;
          xlo = x;
        end
      end
      its = its+1;
    end
    [its x epay]
  end
%----------------------------------------------------------------------

%----------------------------------------------------------------------
function epay = exppay(data,g,cfg)
  % return expected payments from guarantee length g
  if (g==0)
    c = 1;
  elseif (g==5)
    c = 2;
  elseif (g==10)
    c = 3;
  end
  lambda = cfg.mort.lambda;
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  rho = cfg.cut.rho;
  delta = cfg.cut.delta;
  infl = cfg.cut.inflation;
  base = cfg.mort.baseage;
  for n=1:data.N;
    z_slope = cfg.cut.z(data.group(n),:);
    agemin = cfg.cut.agemin(data.group(n));
    T = agemax - agemin;
    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(data.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(data.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(data.group(n));
    end
    inf_factor = cumprod((1/(1+cfg.cut.inflation))*ones(T,1)); 
    rdis = cumprod(rho*ones(T,1)); 
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    galpha = data.alpha(n);
    part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1))));
    part1  = exp(galpha/lambda*(1 - exp(lambda*(t  ))));  
    fr     = (part1(1:T) - part2(1:T))';          % density  
    Z = w0*alpha2*z_slope(c); % formula for annuity pricing
    Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                   % value
    cumz = cumsum(Z.*rdis);
    if g>0,
      epay(n) = cumz(g)*sum(fr(1:g)) + ...
                sum(cumz(g+1:T).*fr(g+1:T));
    else
      epay(n) = sum(cumz.*fr);
    end;
    if (sum(fr)==0)
      epay(n) = 0.0;
    else
      epay(n) = epay(n)/sum(fr);
    end
  end    
%----------------------------------------------------------------------
  
