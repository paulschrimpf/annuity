function [dout] = crossSub2(data,est,cfdata,cfg,prefix)
% decompose cross subsidies by holding epdv of payments constant within
% each equilibrium guarantee choice
  
  dout = cfdata;
  %----------------------------------------------------------------------
  % simulate mandatory guarantee lengths
  
  spay = zeros(data.ngroup,3); % (group,guar)
  mand_z = zeros(data.ngroup,3,3);
  % determine annuity payment rates
  for g=1:dout.ngroup
    for c=1:3
      spay(g,c) = sum(dout.epay(dout.group'==g & ...
                                   dout.oc==c,c));
      mand_z(g,c,:) = cfg.cut.z(g,:).*spay(g,c)./ ...
          sum(dout.epay(dout.group'==g & ...
                          dout.oc==c,:),1);
    end
  end
  % simulate choices, wealth equivalents, etc

  guar = 0:5:10; % guarantee lengths
  
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
%  rho   = cfg.cut.rho;     % Riskless interest rate                       
%  delta = cfg.cut.delta;   % Utility discount rate                        
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end

  nchoice = length(guar);
  disp = 10;
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end
  lambda = est.lambda;
  logAlpha = log(cfdata.alpha);
  logBeta = log(cfdata.beta);

  warning('Off','MATLAB:divideByZero');
  for n=1:dout.N
    infl  = cfg.cut.inflation(dout.group(n));% Inflation rate                               
    rho = cfg.cut.rho(dout.group(n));
    delta = cfg.cut.delta(dout.group(n));
    zeta = cfg.cut.zeta(dout.group(n));
    agemin = cfg.cut.agemin(dout.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    galpha = exp(logAlpha(n));
    part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
    part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    
    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    fr     = (part1(1:T) - part2(1:T))';          % density  
    mand_z = cfg.cut.z(g,:).*cfdata.epay(n,cfdata.oc(n))./ ...
             cfdata.epay(n,:);

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.frac = alpha2;
    vp.infl = infl;

    for c = 1:nchoice,        % guarantee choice
      Z = w0*alpha2*mand_z(c); % formula for annuity pricing
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      dout.m.val(n,c) = vf3(w0,mand_z(c),q,dout.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      dout.m.weq(n,c) = weq(dout.m.val(n,c),wlo,whi,q,dout.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        dout.m.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        dout.m.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    dout.m.epay(n,:) = dout.m.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Mandate: Done with %d of %d\n',n,dout.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
  
  %----------------------------------------------------------------------
  % symmetric information
  % simulate choices, wealth equivalents, etc
  warning('Off','MATLAB:divideByZero');
  %sumPay = sum(totalPay);
  for n=1:dout.N
    infl = cfg.cut.inflation(dout.group(n));
    rho = cfg.cut.rho(dout.group(n));
    delta = cfg.cut.delta(dout.group(n));
    zeta = cfg.cut.zeta(dout.group(n));
    
    z_slope = cfg.cut.z(dout.group(n),:);
    agemin = cfg.cut.agemin(dout.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    galpha = exp(logAlpha(n));
    part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
    part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    
    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    fr     = (part1(1:T) - part2(1:T))';          % density  
    g = dout.group(n);
    c = dout.oc(n);
    zi = z_slope.*cfdata.epay(n,cfdata.oc(n))./ ...
         cfdata.epay(n,:);

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;

    for c = 1:nchoice,        % guarantee choice
      Z = w0*(alpha2*zi(c)); % formula for annuity pricing
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      dout.s.val(n,c) = vf3(w0,zi(c),q,dout.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      dout.s.weq(n,c) = weq(dout.s.val(n,c),wlo,whi,q,dout.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        dout.s.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        dout.s.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    dout.s.epay(n,:) = dout.s.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Symmetric: Done with %d of %d\n',n,dout.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
  [m soc] = max(dout.s.val,[],2);
  dout.s.oc = soc;
  for n =1:dout.N
    dout.s.g(n) = guar(soc(n));
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % create a table summarizing the results.
  % table that decomposes wealth equivalent into effect of info/price
  % change vs cross subsidy change.
  oc = cfdata.oc;
  ow = zeros(cfdata.N,1);
  op = ow;
  for i = 1:cfdata.N
    ow(i) = cfdata.weq(i,oc(i));
    op(i) = cfdata.epay(i,oc(i));
  end
  soc = cfdata.s.oc;
  sow = zeros(cfdata.N,1);
  sop = sow;
  for i = 1:cfdata.N
    sow(i) = cfdata.s.weq(i,soc(i));
    sop(i) = cfdata.s.epay(i,soc(i));
  end  

  xoc = dout.oc;
  xow = zeros(dout.N,1);
  xop = xow;
  for i = 1:dout.N
    xow(i) = dout.weq(i,xoc(i));
    xop(i) = dout.epay(i,xoc(i));
  end
  xsoc = dout.s.oc;
  xsow = zeros(dout.N,1);
  xsop = xsow;
  for i = 1:dout.N
    xsow(i) = dout.s.weq(i,xsoc(i));
    xsop(i) = dout.s.epay(i,xsoc(i));
  end  
  r = 1;
  isequal(dout.g,cfdata.g)
  str(r).s = 'Current (asymm. info)';
  table(r,:) = [mean(ow) mean(cfdata.weq(cfdata.g==0,1)) ...
                mean(cfdata.weq(cfdata.g==5,2)) ...
                mean(cfdata.weq(cfdata.g==10,3)) ...
                mean(xow) mean(dout.weq(dout.g==0,1)) ...
                mean(dout.weq(dout.g==5,2)) ...
                mean(dout.weq(dout.g==10,3)) ];
  r = r+1;
  str(r).s = ' ';
  table(r,:) = [mean(op) mean(cfdata.epay(cfdata.g==0,1)) ...
                mean(cfdata.epay(cfdata.g==5,2)) ...
                mean(cfdata.epay(cfdata.g==10,3)) ...
                mean(xop) mean(dout.epay(dout.g==0,1)) ...
                mean(dout.epay(dout.g==5,2)) ...
                mean(dout.epay(dout.g==10,3)) ];
  r = r+1;
  str(r).s = 'Symm. Info.';
  table(r,:) = [mean(sow) mean(sow(cfdata.g==0)) ...
                mean(sow(cfdata.g==5)) ...
                mean(sow(cfdata.g==10)) ...
                mean(xsow) mean(xsow(dout.g==0)) ...
                mean(xsow(dout.g==5)) ...
                mean(xsow(dout.g==10))];
  r = r+1;
  str(r).s = ' ';
  size(sop)
  size(xsop)
  table(r,:) = [mean(op) mean(sop(cfdata.g==0)) ...
                mean(sop(cfdata.g==5)) ...
                mean(sop(cfdata.g==10)) ...
                mean(xsop) mean(xsop(dout.g==0)) ...
                mean(xsop(dout.g==5)) ...
                mean(xsop(dout.g==10)) ];
  
  r = r+1;
  s = r;
  str(r).s = 'Mandated 0';
  r = r+1;
  str(r).s = ' ';
  r = r+1;
  str(r).s = 'Mandated 5';
  r = r+1;
  str(r).s = ' ';
  r = r+1;
  str(r).s = 'Mandated 10';
  r = r+1;
  str(r).s = ' ';
  r = s;
  for k=1:3
    table(r,:) = [mean(cfdata.m.weq(:,k)) ...
                    mean(cfdata.m.weq(cfdata.g==0,k)) ...
                    mean(cfdata.m.weq(cfdata.g==5,k)) ...
                    mean(cfdata.m.weq(cfdata.g==10,k)) ...
                    mean(dout.m.weq(:,k)) ...
                    mean(dout.m.weq(dout.g==0,k)) ...
                    mean(dout.m.weq(dout.g==5,k)) ...
                    mean(dout.m.weq(dout.g==10,k))];
    r = r+1;
    table(r,:) = [mean(cfdata.m.epay(:,k)) ...
                  mean(cfdata.m.epay(cfdata.g==0,k)) ...
                  mean(cfdata.m.epay(cfdata.g==5,k)) ...
                  mean(cfdata.m.epay(cfdata.g==10,k)) ...
                  mean(dout.m.epay(:,k)) ...
                  mean(dout.m.epay(dout.g==0,k)) ...
                  mean(dout.m.epay(dout.g==5,k)) ...
                  mean(dout.m.epay(dout.g==10,k))];
    r = r+1;
  end
  of = fopen([prefix '/tables/crossSub2.tex'],'w');
  fprintf(of,[' & \\multicolumn{8}{c}{Equilibrium Guarantee Choice} \\\\' ...
              ' \n']);
  fprintf(of,' & All & 0 & 5 & 10 & All & 0 & 5 & 10\\\\ \n');
  fprintf(of,[' & \\multicolumn{4}{c}{Allowing Cross Subsidies} & ' ...
          ' \\multicolumn{4}{c}{Holding E(Pay) Constant} \\\\ \n']);  
  for r = 1:length(str)
    if(mod(r,2)==0)
      fprintf(of,' & (%.4g) ',table(r,:));
      fprintf(of,'\\\\ \n');
    else
      fprintf(of,['%s & %.4g & %.4g & %.4g & %.4g ' ...
                  ' & %.4g & %.4g & %.4g & %.4g \\\\ \n'], ...
              str(r).s, table(r,:)); 
    end
  end
  fclose(of);
  
