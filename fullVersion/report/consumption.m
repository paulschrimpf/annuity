function consumption(data,est,cfg,prefix)
% creates plots of consumption and wealth profiles at mean alpha and
% beta+-1std with a five year guarantee

  % create plots for these deviations of alpha and beta
  dalpha =[0];
  dbeta = [-100 -2 -1 0 1 2];
  
  [la lb] = sampleAlphaBeta(data,est);
  
  ma = mean(la);
  sa = std(la);
  mb = mean(lb);
  sb = std(lb);
  
  logAlpha = ma + sa*dalpha;
  logBeta = mb + sb*dbeta;
  logBeta = max(log(eps),logBeta);
  logBeta = min(log(realmax),logBeta);
  
  % things for the value function
  %global T gam delta rho bgam;
  agemax = cfg.cut.agemax;
  agemin = cfg.cut.agemin;
  T = cfg.cut.agemax - cfg.cut.agemin;
  % assign observation weighted mean inflation, r, delta
  if(data.ngroup>1)
    w = zeros(size(cfg.cut.delta'));
    for g=1:length(w)
      w(g) = sum(data.group==g)/data.N;
    end
  end
  if(length(cfg.cut.delta)==1)
    delta = cfg.cut.delta;
  else
    delta = cfg.cut.delta*w;
  end
  if(length(cfg.cut.rho)==1)
    rho = cfg.cut.rho;
  else
    rho = cfg.cut.rho*w;
  end
  if(length(cfg.cut.inflation)==1)
    infl = cfg.cut.inflation;
  else
    infl = cfg.cut.inflation*w;
  end
  if(length(cfg.cut.zeta)==1)
    zeta = cfg.cut.zeta;
  else
    zeta = cfg.cut.zeta*w;
  end
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end

  
  gam = cfg.cut.gam;
  bgam = cfg.cut.bgam;
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  lambda = est.lambda;
  inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation
                                                % rate factors    
  rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
  base = cfg.mort.baseage;
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end

  t = (agemin-base):(agemax-base);
  tp1 = t+1;
  z_slope = cfg.cut.z(1,:);
  vp.rho = rho;
  vp.delta = delta;
  vp.T = T;
  vp.zeta = zeta;
  vp.gam = gam;
  vp.bgam = bgam;
  vp.pub = pub;
  vp.frac = alpha2;
  vp.infl = infl;

  % consumption and wealth arrays
  ct = zeros(length(dalpha),length(dbeta),T);
  wt = ct;

  cmdc = ''; 
  cmdw = '';
  styles = {'''-ok''','''-xk''','''-+k''','''-*k''', ...
            '''-sk''','''-dk''','''-vk''','''-pk''','''-hk'''};
  i = 1;
  j = 1;
  for a=1:length(dalpha)
    for b=1:length(dbeta)
        galpha = exp(logAlpha(a));
        part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
        part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
        
        q = (part1 - part2)./part1;
        q(part1==0) = 0;        
        pubrate = pubAnnuityRate(infl,rho,q);
        Z= w0*(alpha2*z_slope(2) + ... % formula for annuity pricing
               pub*pubrate);
        Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                       % value

        [val ct(a,b,:) wt(a,b,:)] = ...
            vf3(w0,z_slope(2),q,exp(logBeta(b)),5,vp);
        if (isempty(cmdc))
          cmdc = ['t,squeeze(ct(' num2str(a) ',' num2str(b) ',:)),' ...
                  styles{j}];
          cmdw = ['t,squeeze(wt(' num2str(a) ',' num2str(b) ',:)),' ...
                  styles{j}];
        else
          cmdc = [cmdc ',t,squeeze(ct(' num2str(a) ',' num2str(b) ',:)),' ...
                  styles{j}];
          cmdw = [cmdw ',t,squeeze(wt(' num2str(a) ',' num2str(b) ',:)),' ...
                  styles{j}];
        end
        if(dalpha(a)==0)
          s = 'Mean \alpha';
        else 
          s = [num2str(dalpha(a)) ' \alpha'];
        end
        if(dbeta(b)==0)
          s = [s ',Mean \beta'];
        elseif (dbeta(b)<-5)
          s = [s ',\beta = 0'];
        else
          s = [s ',' num2str(dbeta(b)) ' \beta'];
        end
        leg(i) = {s};
        i = i+1;
        j = j+1;
        if (j>length(styles))
          j = 1+mod(j,length(styles));
        end
    end
  end
  t = t(1:T)+base;  
  figure;
  eval(['subplot(2,1,1),plot(' cmdc ')']);
  ylabel('Consumption');
  xlabel('Age');
  title('Consumption');
  legend(leg,'Location','EastOutside');
  eval(['subplot(2,1,2),plot(' cmdw ')']);
  ylabel('Wealth');
  xlabel('Age');
  title('Wealth');
  legend(leg,'Location','EastOutside');
  filename = [prefix '/figures/ctwt.eps'];
  print(filename,'-depsc2');
  
  
