function pdieMortTable(data,cfg,prefix)
  
  % load mortality tables
  load ../../data/mortalityTables/male.txt;
  load ../../data/mortalityTables/female.txt;

  for i=1:data.N % loop over observations
    if(data.male(i))
      mtable = male;
    else
      mtable = female;
    end
    S = cumprod(1-mtable(:,2));
    t = mtable(:,1) - cfg.mort.baseage;
    if (data.fage(i)<0)
      penter=1;
    else
      penter = interp1(t,S,data.fage(i));
    end
    pexit = interp1(t,S,data.maxAge(i));
    pdie(i) = 1-pexit/penter;
%    if (isnan(pdie(i)))
%      [penter pexit data.fage(i) data.maxAge(i)]
%    end
  end
    
  mean(pdie)
    
