function bdta = resample(data,est) 
% draw a sample from data with replacement
  bdta.N = data.N;
  m = ceil(rand(data.N,1)*data.N);
  % sample x's nonparametrically
  bdta.male = data.male(m);
  bdta.ngroup = data.ngroup;
  bdta.betaX = data.betaX(m,:);
  bdta.name = data.name;
  bdta.group = data.group(m);
  bdta.alphaX = data.alphaX(m,:);
  bdta.year = data.year(m);
  bdta.quarter = data.quarter(m);
  bdta.cage = data.cage(m);
  bdta.t = data.t(m);
  % sample truncation points nonparametrically too
  bdta.baseage = data.baseage(m);
  bdta.maxAge = data.maxAge(m);
  bdta.agemin = data.agemin(m);
  bdta.fage = data.fage(m);
  if (nargin>1) % do parametric bootstrap
    % ?? should we be sampling alpha|truncation ??
    % yes, definitely
    la = sampleAlpha(bdta,est);
    % sample mortality
    [bdta.lage bdta.died] = sampleMort(bdta,la,est);
    % sample guarantee choices
    bdta.g = sampleGuar(bdta,la,est);
  else % nonparametric 
    bdta.g = data.g(m);
    bdta.died = data.died(m);
    bdta.lage = data.lage(m);
    if (~isempty(data.mort))
      error('mortality only data not supported')
    end
  end
  bdta.mort = data.mort;
  [mean(data.died) mean(data.g==0) mean(data.g==5) mean(data.g==10); ...
   mean(bdta.died) mean(bdta.g==0) mean(bdta.g==5) mean(bdta.g==10)]  
end % function resample()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function la = sampleAlpha(data,est);
% sample alpha|fage
  data
  size(data.alphaX)
  est
  est.parm
  size(est.parm.gAlpha)
  mu = data.alphaX*est.parm.gAlpha;
  la = zeros(size(mu));
  sd = sqrt(est.parm.var(1,1));
  l = est.lambda;
  for n=1:data.N
    a = randn()*sd + mu(n); % initial a
    f = data.fage(n);
    % use metropolis-hastings to sample from posterior
    s = exp(exp(a)/l*(1-exp(l*f)))* ...
        normpdf(a,mu(n),sd);
    accept = 0;
    for r=1:100 
      u = rand();
      anew = randn()*sd*2 + a;
      snew = exp(exp(anew)/l*(1-exp(l*f)))* ...
             normpdf(anew,mu(n),sd);
      if (u < snew/s)
        a = anew;
        s = snew;
        accept = accept+1;
      end
    end
    la(n) = a;
  end
  fprintf('acceptance rate = %f\n',accept/r);
end % function sampleAlpha

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [lage died] = sampleMort(data,la,est);
% sample mortality given alpha
  alpha = exp(la);
  c = exp(est.lambda);
  lage = -ones(data.N,1);
  for n=1:data.N
    while (lage(n)<data.fage(n))
      lage(n) = randraw('gompertz',[alpha(n) c], 1);
    end
  end
  died = lage<data.maxAge;
  lage(~died) = data.maxAge(~died);
end % function sampleMort

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function g = sampleGuar(data,la,est)
% sample guarantee choice given alpha

  % draw beta|alpha
  condmu = data.betaX*est.parm.gBeta + est.parm.var(1,2)/est.parm.var(1,1) * ... 
           (la-data.alphaX*est.parm.gAlpha); 
  condsig = sqrt(est.parm.var(2,2) - est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
            ones(data.N,1);             
  lb = condmu + randn(data.N,1).*condsig;

  g = -ones(data.N,1);
  % find choices
  for d=1:length(est.cut)
    e(:,1) = interp1(est.cut(d).la,est.cut(d).lb(:,1), ...
                     la,'linear','extrap');
    e(:,2) = interp1(est.cut(d).la,est.cut(d).lb(:,2), ...
                     la,'linear','extrap');
    e(isnan(e)) = Inf;
    oc = 1*(lb<=e(:,1)) + 2*(lb>e(:,1) & lb<e(:,2)) + ...
         3*(lb>=e(:,2));
    g(oc==1 & data.group==d) = 0;
    g(oc==2 & data.group==d) = 5;
    g(oc==3 & data.group==d) = 10;
  end
  if(any(g<0))
    error('crap');
  end
end % function sampleGuar
