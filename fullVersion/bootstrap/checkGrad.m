% 2 January 2007
% report gradient for each bootstrapped parameter estimates
clear all;
confs= {'confFemale60', ...
        'confFemale65', ...
        'confMale60', ...
        'confMale65'};

YEAR = 92;
SEPARATE = true; % separate by year
GAM = 3;
BGAM = 3;
SIMULATE = true;
USEOLDSIM = false;
OBJECTIVE = 'mle';
READLAMBDA = true;
FRAC = 0.2;
DIST = 'normal';
DEGREE = 2 ;
ZETA = 1;
ESTIMATE = true;
INCLUDEX = false;
FASTHAZARD = 1;
EXTERNAL = false;
PUBLICANNUITY = 0; % put this much wealth in a (alpha-specific)
                     % actuarially fair annuity
RATE = 0.0; % riskless interest rate, if don't want to use one in data
DUMB.p = 0.0; % this percent of people just pick the middle
DUMB.dmean = true; % dumb people have different mean alpha
DUMB.dsig = true; % dumbe people have different sigma alpha
DUMB.estP = false;

path(path,'cutoffs/');
path(path,'../src/');
path(path,'../src/cutoffs/');

for c = 1:length(confs) 
  clear est;
  clear annuityLike;
  clear step2Like;
  clear mortLike;
  clear transformParam;
  configName = confs{c};
  eval(['cfg = ' configName '();']);
  mortOnly = strcmp(OBJECTIVE,'mlmort'); 
  if (mortOnly)  % change config to just do mortality
    READLAMBDA = false;
  end
  cfg.est.objective = OBJECTIVE;
  cfg.est.opt.LargeScale = 'on';
  cfg.est.opt.DerivativeCheck = 'off';
  cfg.est.opt.GradObj = 'on';
  cfg.est.opt.MaxIter = 100000;

  switch(OBJECTIVE)
   case '2step'
    cfg.outprefix = [cfg.outprefix '_2step'];
   case 'mlmort'
    cfg.outprefix = [cfg.outprefix 'Mort'];
  end

  cfg.cut.gam = GAM;
  cfg.cut.bgam = BGAM;
  cfg.cut.fracAnnuitized = FRAC;
  cfg.est.dist = DIST;
  cfg.cut.zeta = ZETA;
  cfg.mort.fasthazard = FASTHAZARD;
  cfg.cut.fasthazard = FASTHAZARD;
  cfg.cut.public = PUBLICANNUITY;
  cfg.est.dumb = DUMB;
  
  
  if (SEPARATE) 
    cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
  else
    if (YEAR~=92)
      cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
    end
  end
  if (GAM~=3)
    cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
  end
  if (BGAM~=GAM)
    cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
  end
  if (FRAC~=0.2)
    cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
  end
  if (~strcmp(DIST,'normal'))
    cfg.outprefix = [cfg.outprefix,'_' DIST];
  end
  if(ZETA~=1)
    cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
  end
  if(INCLUDEX)
    cfg.outprefix = [cfg.outprefix, '_X'];
    cfg.data.lalphaX = [-1 5 7];
    cfg.data.alphaXName(2).s = 'Premium';
    cfg.data.alphaXName(3).s = 'Percqua';

    cfg.data.lbetaX = [-1 5 7];
    cfg.data.betaXName(2).s = 'Premium';
    cfg.data.betaXName(3).s = 'Percqua';
  end
  if(FASTHAZARD~=1)
    cfg.outprefix = [cfg.outprefix, '_t' num2str(FASTHAZARD)];
  end
  if(EXTERNAL)
    cfg.outprefix = [cfg.outprefix '_ext'];
    cfg.data.okay = 15;
    cfg.data.okeep = 1;
  end
  if(PUBLICANNUITY)
    cfg.outprefix = [cfg.outprefix '_pub' num2str(PUBLICANNUITY*100)];
  end
  if(exist('RATE','var') && RATE>0)
    cfg.outprefix = [cfg.outprefix '_r' num2str(RATE*100)];
    cfg.cut.rho = 1/(1+RATE);
    cfg.cut.delta = 1/(1+RATE);
    cfg.data = rmfield(cfg.data,'lrate');
  end
  if (DUMB.p>0 || DUMB.estP)
    if (DUMB.estP)
      cfg.outprefix = [cfg.outprefix '_dumbEst'];
    else
      cfg.outprefix = [cfg.outprefix '_dumb' num2str(DUMB.p*100)];
    end
  end
  
  cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');

  % read data
  if (SEPARATE) 
    switch YEAR
     case {92,95}
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9295.txt'];
     case {90,97} 
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_9097.txt']; 
     otherwise
      error(['no appropriate data set for separate and ' num2str(YEAR)]);
    end
      cfg.data.keepYear = 1900 + YEAR;
  else
    if (YEAR<100)
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_19' num2str(YEAR) ...
                       '.txt'];
    else
      cfg.data.name = ['/proj/paul/annuities/data/newCompB2_' num2str(YEAR) ...
                       '.txt'];
    end
    cfg.data.allDummies=false;
  end
  
  % load data
  [data cfg] = readData(cfg); % see readData for details of data struc
  
  % load bootstrap results
  load([cfg.outprefix 'Boot2']);
  
  % set the seed so we can replicate results
  rand('state',9457);

  for b=1:length(bootEst)
    clear annuityGMM annuityLike mortLike;
    bData = resample(data); 
    % create parm vector
    est = bootEst{b};
    k = length(est.parm.gAlpha);
    parm = zeros(k+2,1);
    parm(1:k) = est.parm.gAlpha;
    k = k+1;
    switch (est.dist)
     case {'normal','exponential'}
      parm(k) = log(sqrt(est.parm.var(1,1)));
      k = k+1;
     case 'gamma'
      parm(k) = log(sqrt(est.parm.var(1,1)));
      k = k+1;
      parm(k) = log(1);
      k = k+1;
     otherwise
      error('not implemented');
    end
    parm(k) = log(est.lambda);
    [mf(b) mg(b,:)] = mortLike(parm,bootEst{b},bData);
    nmg(c,b) = norm(mg(b,:));
    parm = transformParam(bootEst{b}.parm,'pack','gmm',bootEst{b}.dist);
    [f(b) g] = annuityLike(parm,bootEst{b},bData);
    ng(c,b) = norm(g);
  end
end  
