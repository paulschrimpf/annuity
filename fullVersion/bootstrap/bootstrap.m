function [bootEst bootMoments bootGrad] = bootstrap(cfg,est,data)
% nonparameter bootstrap of standard errors
  
  % set the seed so we can replicate results
  rand('state',122345678);

  truest = est;

  % main loop
  tic;
  %eval(['load ',cfg.outprefix,'Boot2']);
  %bootEstOld = bootEst;
  for b=1:cfg.nboot
    % clear likelihoods b/c they have persistent vars
    clear annuityGMM annuityLike mortLike;
    % resample data
    bData = resample(data,est); 
    cfg.outprefix = '/simPooled';
    writeAMPLdata(bData,cfg,est.cut);
    fprintf('Wrote a simulated data set\n');
    fprintf('calling ampltrial ...\n');
    tic
    [status output] = system('amplTrial pooled.ampl &> tee ampl.out');
    toc
    fprintf('finished %d estimates\n',b);
    output
    fclose(fid);
    perl('timestamp.pl')
    continue;
    est = truest; % initialize parameters at estimates
    % estimate lambda
    %[est.lambda g] = estimateLambda(cfg,est,bData);
    est.lambda = bootEstOld{b}.lambda;
    l(b) = est.lambda;
    % calculate cutoffs
    cfg.mort.lambda = est.lambda;
    %est.cut = loadcutoffs(cfg);
    est.cut = bootEstOld{b}.cut;
    est.cut = cleanCut(est.cut,cfg,bData);
    % estimate other parameters
    est = estimateModel(cfg,est,bData);
    % calculate moments (as sanity check)
    % store results
    pgmm = transformParam(est.parm,'pack','gmm',est.dist);
    [f g h gi moments] = annuityGMM(pgmm,est,bData);
    bootEst{b} = truest;
    bootEst{b}.lambda = l(b);
    bootMoments{b} = moments;
    bootGrad{b} = g;
    % print update
    fprintf('BOOTSTRAP REPORT: completed %d of %d repetitions\n',b, ...
            cfg.nboot);
    time = toc;
    h = floor(time/3600);
    m = floor((time-3600*h)/60);
    s = floor((time-3600*h-60*m));
    fprintf('                  elapsed time = %d:%d:%d \n',h,m,s);
  end
  %eval(['save ',cfg.outprefix,'Boot22 bootEst bootMoments bootGrad']);
  return;
end % function bootstrap()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [lambda g] = estimateLambda(cfg,est,data)
% estimate lambda from mortality data
  cfg.est.objective = 'mlmort';
  cfg.est.opt.LargeScale = 'off';
  cfg.est.opt
  
  % pack parameters
  objFunc = 'mortLike';
  % estimation can be unstable with low initial var
  est.parm.var(1,1) = 1;
  k = length(est.parm.gAlpha);
  parm = zeros(k+2,1);
  parm(1:k) = est.parm.gAlpha;
  k = k+1;
  switch (est.dist)
   case {'normal','exponential'}
    parm(k) = log(sqrt(est.parm.var(1,1)));
    k = k+1;
   case 'gamma'
    parm(k) = log(sqrt(est.parm.var(1,1)));
    k = k+1;
    parm(k) = log(1);
    k = k+1;
   otherwise
    error('not implemented');
  end
  parm(k) = log(est.lambda);
  
  % maximize the likelihood
  switch(cfg.est.optmethod)
   case 'fminunc'
    eval(['[parm fval exit output optgrad opthess] =' ...
          'fminunc(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt);']);
   case 'fminsearch'
    eval(['[parm fval exit output] = ' ...
          'fminsearch(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
   otherwise
    error('optimization method "%s" unrecognized.\n',cfg.est.optmethod);
  end

  % unpack parameters
  k = length(est.parm.gAlpha);
  est.parm.gAlpha = parm(1:k);
  k = k+1;
  stdalpha = exp(parm(k));
  est.parm.var(1,1) = stdalpha^2;
  k = k+1;
  est.lambda = exp(parm(k));
  
  lambda = est.lambda;
  g = optgrad';
  return;
end % function estimateLambda() 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [est] = estimateModel(cfg,estin,data)
% estimate the model
  cfg.est.objective = 'mle';
  objFunc = 'annuityLike';
  est = estin;
  
  % pack parameters
  parm = transformParam(est.parm,'pack',cfg.est.objective,cfg.est.dist);
  
  % maximize the likelihood
  switch(cfg.est.optmethod)
   case 'fminunc'
    eval(['[parm fval exit output optgrad opthess] =' ...
          'fminunc(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
   case 'fminsearch'
    eval(['[parm fval exit output] = ' ...
          'fminsearch(@(x) ' objFunc '(x,est,data),parm,cfg.est.opt)']);
   otherwise
    error('optimization method "%s" unrecognized.\n',cfg.est.optmethod);
  end
  
  % unpack parameters
  est.parm = transformParam(parm,'unpack',cfg.est.objective,est.dist);
  
  return
end % function estimateModel() 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
