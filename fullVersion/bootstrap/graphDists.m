% 15 December 2006
% create histograms of bootstrapped parameter distributions
clear;
for male = 0:1
  for age = 60:5:65 
    if (male) 
      g = 'm';
    else 
      g = 'f';
    end
    file = ['../results/' g num2str(age) '_just92BootPara'];
    load(file);
    
    % but bootstrapped values into a convenient matrix form
    params = {'gAlpha','gBeta','corr'};
    titles = {'\mu_\alpha','\mu_\beta','\rho', ...
              '\sigma_\alpha','\sigma_{\alpha \beta}','\sigma_\beta', ...
              '\lambda','p0 (observed-estimated)','p5 (observed-estimated)', ...
              'pdie|0 (obs-est)','pdie|5 (obs-est)','pdie|10 (obs-est)', ...
              'norm(grad)'};
    j = 1;
    for p=1:length(params)
      for b=1:length(bootEst)
        l(j,b) = bootEst{b}.parm.(params{p});
      end
      j = j+1;
    end
    for s1=1:2
      for s2=s1:2
        for b=1:length(bootEst)
          l(j,b) = bootEst{b}.parm.var(s1,s2);
        end
        if(s1==s2)
          l(j,:) = sqrt(l(j,:)); % make if std dev instead of var
        end
        j = j+1;
      end
    end
    for b=1:length(bootEst)
      l(j,b) = bootEst{b}.lambda;
    end
    j = j+1;
    for m=1:5
      for b=1:length(bootEst)
        l(j,b) = (bootMoments{b}(1,m)-bootMoments{b}(2,m));
      end
      j = j+1;
    end
      
    file = ['figs/' g num2str(age) 'bootDist.eps'];
    figure;
    for j=1:length(titles)
      subplot(4,4,j),hist(l(j,:))
      title(titles{j});
    end
    print(file,'-depsc2');
  end % loop over age
end % loop over male

