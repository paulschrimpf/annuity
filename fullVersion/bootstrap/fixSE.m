clear all;
path(path,'../src/');
path(path,'../src/cutoffs/');
c=1;
% loop over the four cells
for male=0:1
  for age=60:5:65
    clear annuityLike mortLike;
    if (male) % strings for writing filenames, etc 
      g = 'm';
    else 
      g = 'f';
    end
    % load the results
    file = ['../results/' g num2str(age) 'Mort_just92Results'];
    load(file);
    % load the data
    [data cfg] = readData(cfg); % see readData for details of data struc

    % pack parameters into a vector to pass to the likelihood function
    k = length(est.parm.gAlpha);
    pm = zeros(k+2,1);
    pm(1:k) = est.parm.gAlpha;
    k = k+1;
    pm(k) = log(sqrt(est.parm.var(1,1)));
    k = k+1;
    pm(k) = log(est.lambda);
    
    % call the first step likelihood
    [like grad hess gi] = mortLike(pm,est,data);
    % gi are the individual gradients
    gi = gi - ones(data.N,1)*sum(gi); % whether or not you subtract this
                                      % makes little difference.  it
                                      % is nearly zero.
    v = inv(gi'*gi); % one version of variance-covariance matrix
    se(c,1) = est.lambda*sqrt(v(k,k));
    
    % compute variance with numerical hessian approximation
    dx = 1.0e-8; % step size
    h = zeros(length(pm)); 
    for j=1:length(pm)
      p = pm;
      p(j) = pm(j)+dx;
      [like gp] = mortLike(p,est,data);
      p(j) = pm(j)-dx;
      [like gm] = mortLike(p,est,data);
      h(j,:) = (gp-gm)/(2*dx);
    end
    v2 = inv(h); % second version of variance-covariance matrix
    se(c,2) = est.lambda*sqrt(v2(k,k));

    % get step 2 standard errors
    % load results
    file = ['../results/' g num2str(age) '_just92Results'];
    load(file);
    clear tranformParam annuityLike; % these functions have some
                                     % persistent vars, so need to clear
                                     % them each time the data changes
    % transform parameters into a vector to pass to the likelihood
    parm = transformParam(est.parm,'pack',cfg.est.objective, ...
                          cfg.est.dist);
    % call the likelihood
    [like grad hess gi] = annuityLike(parm,est,data);
    gi = gi';
    % standard errors, outproduct of gradients 
    V(1,:,:) = inv(gi'*gi);
    gi = gi - ones(data.N,1)*sum(gi); % this makes little difference
                                      % b/c gradient is close to 0
    V(2,:,:) = inv(gi'*gi);
    
    % compute with numerical hessian approximation
    dx = 1.0e-8;
    h = zeros(length(parm));
    for j=1:length(parm)
      p = parm;
      p(j) = parm(j)+dx;
      [like gp] = annuityLike(p,est,data);
      p(j) = parm(j)-dx;
      [like gm] = annuityLike(p,est,data);
      h(j,:) = (gp-gm)/(2*dx);
    end
    V(4,:,:) = inv(h); % inverse hessian
    V(3,:,:) = inv(h)*gi'*gi*inv(h); % sandwich version
    
    % get the derivative of the transformation from the vector of
    % parameters to their natural form
    [est.parm der] = transformParam(parm,'unpack',cfg.est.objective, ...
                                    est.dist);
    % check derivative of transformation
    dx = 1e-8;
    fprintf('%2s %10s %10s %10s\n','p','parm','analytic','numeric');
    for p=1:length(parm)
      pm = parm;
      pm(p) = parm(p) - dx;
      est2.parm = transformParam(pm,'unpack',cfg.est.objective, ...
                                 est.dist);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'gAlpha',der.gAlpha(p), ...
              (est.parm.gAlpha - est2.parm.gAlpha)/dx);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'gBeta',der.gBeta(p), ...
              (est.parm.gBeta - est2.parm.gBeta)/dx);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'var(1,1)',der.var(1,1,p), ...
              (est.parm.var(1,1) - est2.parm.var(1,1))/dx);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'cov',der.var(1,2,p), ...
              (est.parm.var(1,2) - est2.parm.var(1,2))/dx);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'var(2,2)',der.var(2,2,p), ...
              (est.parm.var(2,2) - est2.parm.var(2,2))/dx);
      fprintf('%2d %10s %10.6g %10.6g\n',p,'corr',der.corr(p), ...
              (est.parm.corr - est2.parm.corr)/dx);
      fprintf('------------------------------------------------\n');
    end
    
    % multiple V by derivative of transformation to get std errors
    % V(last,:,:) method will end up being used for saved std. errors
    % this is the inv(hess) one right now
    for s=1:size(V,1)
      v = squeeze(V(s,:,:));
      std.gAlpha = sqrt(diag(der.gAlpha*v*der.gAlpha'));
      tmp = std.gAlpha;
      std.gBeta = sqrt(diag(der.gBeta*v*der.gBeta'));
      tmp = [tmp; std.gBeta];
      jac = reshape(der.var(1,1,:),1,length(parm));
      std.var(1,1) = sqrt(jac*v*jac');
      tmp = [tmp; std.var(1,1)];
      jac = reshape(der.var(1,2,:),1,length(parm));
      std.var(1,2) = sqrt(jac*v*jac');
      tmp = [tmp; std.var(1,2)];
      jac = reshape(der.var(2,2,:),1,length(parm));
      std.var(2,2) = sqrt(jac*v*jac');
      tmp = [tmp; std.var(2,2)];
      std.corr = sqrt(der.corr*v*der.corr');
      tmp = [tmp; std.corr];
      est.parm.std = std;          
      if (s==1)
        SE = tmp;
      else 
        SE = [SE tmp];
      end
    end
    est.parm.std.lambda = se(c,2);
    if (isfield(est.parm.std,'stdDev'))
      % remove bootstrapped std errors of stdDeviations if they exist 
      rmfield(est.parm.std,'stdDev');
    end
    
    eval(['save ',cfg.outprefix,'Results est cfg bootEst']);
    
    % run estReport
    cd ../report;
    i = findstr('/',cfg.outprefix);
    prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
    estReport(data,est,prefix);
    cd ../bootstrap;
    
    stdErr(c,:,:) = SE;
    c = c+1;
  end
end


