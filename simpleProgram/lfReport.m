function lfReport(data,est,cfg,cfdata,lfdata);
%  
  wa = zeros(size(cfdata.g));
  for i=1:cfdata.N
    wa(i) = cfdata.weq(i,cfdata.oc(i));
  end
  fprintf('average welfare under asym = %g\n',mean(wa));
  fprintf('  scaled so that w0 = 100, = %g\n',mean(wa./cfdata.w0)*100);
  [ws cs] = max(cfdata.s.weq,[],2);
  [wsl csl] = max(lfdata.s.weq,[],2);
  [wal cal] = max(lfdata.m.weq,[],2);
  fprintf('---- Choice portions ---- \n');
  fprintf('Asym.     '); fprintf(' %3.2f',mean(cfdata.oc==1),mean(cfdata.oc==2), ...
                              mean(cfdata.oc==3));
  fprintf('\n');
  fprintf('Asym.leq  '); 
  fprintf(' %3.2f',mean(cal==1),mean(cal==2),mean(cal==3));
  fprintf('\n');
  fprintf('Sym.lneq  '); 
  fprintf(' %3.2f',mean(csl==1),mean(csl==2),mean(csl==3));
  fprintf('\n');
  fprintf('Sym.leq   ');
  fprintf(' %3.2f',mean(cs==1),mean(cs==2),mean(cs==3));
  fprintf('\n');

  fprintf('--- Average welfare --- \n');
  fprintf('Asym     %.2f\n', mean(wa));
  fprintf('Asym.leq %.2f\n', mean(wal));
  fprintf('Sym.lneq %.2f\n', mean(wsl));
  fprintf('Sym.leq  %.2f\n', mean(ws));
  
  pa = pal = psl = ps = wa;
  for i=1:cfdata.N
    pa(i) = cfdata.epay(i,cfdata.oc(i));
    pal(i) = lfdata.m.epay(i,cal(i));
    ps(i) = cfdata.s.epay(i,cs(i));
    psl(i) = lfdata.s.epay(i,csl(i));
  end 
  fprintf('--- E[payments] ---\n');
  fprintf('Asym     %.2f\n', mean(pa));
  fprintf('Asym.leq %.2f\n', mean(pal));
  fprintf('Sym.lneq %.2f\n', mean(psl));
  fprintf('Sym.leq  %.2f\n', mean(ps));
  
  
end