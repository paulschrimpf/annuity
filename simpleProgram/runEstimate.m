%% Main script for estimating model and subsequent calculations
clear all;

%% Set options.  These options are modified to obtain the robustness
% results in Table VII.  Right now, they are set to obtain our baseline
% estimates.  
confs= {'confPooled'}; % additional options -- see confPooled.m for explanation
DATA_PATH = '/home/paul/econ/annuity/data/';  % path to directory containing all data
YEAR = 92; % year of prices to use
SEPARATE = true; % separate by year
GAM = 3;  % relative risk aversion coefficient for consumption
BGAM = 3; % relative risk aversion coefficient for wealth at death
SIMULATE = true; % whether simulations should be run after estimating
USEOLDSIM = false; % whether to use saved simulation results instead of re-running
OBJECTIVE = 'mle'; % type of estimation, valid options are:
                   %  'mlmort' = just estimate  lambda and distribution
                   %      of alpha from mortality data (as in step(1) of
                   %      README.txt
                   %  'mle' = full estimation as described in README.txt
                   %      and the paper
LAMBDA = 0.1102206337978725; % the step 1 estimate of lambda -- if
                             % running step 1 should be commented out
FRAC = 0.2;  % fraction of wealth annuitized
DIST = 'normal'; % distribution of log alpha and log beta, choices are:
                 % 'normal' = joint normal (baseline results)
                 % 'gamma' = alpha gamma distribution, beta|alpha gamma
                 % 'gamma-normal' = alpha gamma, beta|alpha log normal
ZETA = 1; % additional discounting of utility from wealth at death
ESTIMATE = false; % whether to run estimation or just load results
INCLUDEX = false; % wheteher to include covariates as shifters in mean of
                  % alpha and beta
FASTHAZARD = 1; % modification to Gompertz hazard rate -- see footnote d
                % of table VII
EXTERNAL = false; % whether to use alternate sample of people with
                  % external funds
PUBLICANNUITY = 0; % put this much wealth in a (alpha-specific)
                   % actuarially fair annuity
%RATE = 0.0; % riskless interest rate, if don't want to use one in data
%INFL = 0.0; % inflation, if don't want to use one in data
DUMB.p = 0.0; % this percent of people just pick the middle
DUMB.dmean = false; % dumb people have different mean alpha
DUMB.dsig = false; % dumbe people have different sigma alpha
DUMB.estP = false; % try to estimate p?

RAHETERO = false;  % whether to allow heterogeneity in risk aversion
NINT = 24; % number of integration points
EXTRALAGRID = (-6.505:0.01:-4.505)'; % grid on which to compute guarantee cutoffs
path(path,'./cutoffs'); 

for c = 1:length(confs)
  %% Manage options -- check them and modify certain data structures accordingly
  clear est;
  clear transformParam;
  configName = confs{c};
  eval(['cfg = ' configName '();']);
  mortOnly = strcmp(OBJECTIVE,'mlmort'); 
  cfg.est.objective = OBJECTIVE;

  switch(OBJECTIVE)
   case '2step'
    cfg.outprefix = [cfg.outprefix '_2step'];
   case 'mlmort'
    cfg.outprefix = [cfg.outprefix 'Mort'];
  end

  cfg.cut.gam = GAM;
  cfg.cut.bgam = BGAM;
  cfg.cut.fracAnnuitized = FRAC;
  cfg.est.dist = DIST;
  cfg.cut.zeta = ZETA;
  cfg.mort.fasthazard = FASTHAZARD;
  cfg.cut.fasthazard = FASTHAZARD;
  cfg.cut.public = PUBLICANNUITY;
  cfg.est.dumb = DUMB;
  cfg.cut.alphaUncertainty = 0;
  cfg.cut.alphaBias = 1;

  
  if (SEPARATE) 
    cfg.outprefix = [cfg.outprefix '_just' num2str(YEAR)];
  else
    if (YEAR~=92)
      cfg.outprefix = [cfg.outprefix '_' num2str(YEAR)];
    end
  end
  if (GAM~=3)
    cfg.outprefix = [cfg.outprefix 'g' num2str(GAM)];
  end
  if (BGAM~=GAM)
    cfg.outprefix = [cfg.outprefix '_bg' num2str(BGAM)];
  end
  if (FRAC~=0.2)
    cfg.outprefix = [cfg.outprefix '_frac' num2str(100*FRAC)];
  end
  if (~strcmp(DIST,'normal'))
    cfg.outprefix = [cfg.outprefix,'_' DIST];
  end
  if(ZETA~=1)
    cfg.outprefix = [cfg.outprefix,'_z' num2str(1/ZETA-1)];
  end
  if(INCLUDEX)
    cfg.outprefix = [cfg.outprefix, '_X'];
    cfg.data.lalphaX = [-1 5 7];
    cfg.data.alphaXName(2).s = 'Premium';
    cfg.data.alphaXName(3).s = 'Percqua';

    cfg.data.lbetaX = [-1 5 7];
    cfg.data.betaXName(2).s = 'Premium';
    cfg.data.betaXName(3).s = 'Percqua';
  end
  if(FASTHAZARD~=1)
    cfg.outprefix = [cfg.outprefix, '_t' num2str(FASTHAZARD)];
  end
  if(EXTERNAL)
    cfg.outprefix = [cfg.outprefix '_ext'];
    cfg.data.okay = 15;
    cfg.data.okeep = 1;
  end
  if(PUBLICANNUITY)
    cfg.outprefix = [cfg.outprefix '_pub' num2str(PUBLICANNUITY*100)];
  end
  if(exist('RATE','var') && RATE>0)
    cfg.outprefix = [cfg.outprefix '_r' num2str(RATE*100)];
    cfg.cut.rho = 1/(1+RATE);
    cfg.cut.delta = 1/(1+RATE);
    cfg.data = rmfield(cfg.data,'lrate');
  end
  if (DUMB.p>0 || DUMB.estP)
    if (DUMB.estP)
      cfg.outprefix = [cfg.outprefix '_dumbEst'];
    else
      cfg.outprefix = [cfg.outprefix '_dumb' num2str(DUMB.p*100)];
    end
  end
  
  if (RAHETERO)
    file = regexprep(cfg.outprefix,'\.(\d)','p$1');
    scfg = cfg;
    load([file 'Results']);
    cfg = scfg;
    clear scfg;
    cfg.cut.raHetero.beta = exp(est.parm.gBeta);
    cfg.outprefix = [cfg.outprefix '_raHetero'];
  end

  cfg.outprefix = regexprep(cfg.outprefix,'\.(\d)','p$1');

  if (exist('LAMBDA','var'))
    %cfg.outprefix = [cfg.outprefix '_lambda' num2str(LAMBDA)];    
    cfg.mort.lambda=LAMBDA;
    cfg.cut.lambda = cfg.mort.lambda;
  end

  % read data
  if (SEPARATE) 
    switch YEAR
     case {92,95}
      cfg.data.name = [DATA_PATH  'newCompB2_9295.txt'];
     case {90,97} 
      cfg.data.name = [DATA_PATH 'newCompB2_9097.txt']; 
     otherwise
      error(['no appropriate data set for separate and ' num2str(YEAR)]);
    end
      cfg.data.keepYear = 1900 + YEAR;
  else
    if (YEAR<100)
      cfg.data.name = [DATA_PATH 'newCompB2_19' num2str(YEAR) ...
                       '.txt'];
    else
      cfg.data.name = [DATA_PATH 'newCompB2_' num2str(YEAR) ...
                       '.txt'];
    end
    cfg.data.allDummies=false;
  end
  
  % load data
  [data cfg] = readData(cfg); % see readData for details of data struc
  if (~strcmp(cfg.est.objective,'mlmort'))
    %cfg.cut.redo = true;
    % load cutoffs
    if (EXTRALAGRID)
      cfg.cut.la = sort([cfg.cut.la; EXTRALAGRID]);
    end
    cfg.cut.lambda = cfg.mort.lambda;
    cut = loadcutoffs(cfg);
    which cleanCut
    cut = cleanCut(cut,cfg,data);
    est.cut = cut;
  else
    est.cut.la = zeros(2,1);
    est.cut.lb = zeros(2,2);
  end
  
  est.lambda = cfg.mort.lambda;
  est.fasthazard = cfg.mort.fasthazard;
  est.inc = cfg.mort.inc;
  est.disp = cfg.est.disp;
  est.extraMoments = cfg.est.extraMoments;
  est.dist = cfg.est.dist;
  est.dumb = cfg.est.dumb;
  
  % initialize integration points
  cfg.est.nint = NINT;
  switch(est.dist)
   case {'normal'}
    est.int = gaussChebyshevInt(cfg.est.nint);
   case {'gamma','gamma-normal'}
    [est.int.x est.int.w] = gaussLaguerre(cfg.est.nint,0);
    est.int.n = cfg.est.nint;
  end

  %% estimate lambda  -- step (1) of README.txt
  % estimate lambda
  if (ESTIMATE && (~exist('LAMBDA','var') || strcmp(OBJECTIVE,'mlmort')))
    writeAMPLdata(data,cfg,est.cut,1);
    [status output] = system(['amplTrial mortLikePooled.ampl | tee' ...
                        ' mort.out']);
  end
  % load results
  try 
    load('mort.lambda'); 
  catch
    error(['Failed to load "mort.lambda" This file can be created by running ' ...
           'estimation with OBJECTIVE=mlmort']);
  end
  est.lambda = mort;
  amplEst = load('est.out');
  ei = [1 2 3 4];
  for j = 1:4
    est.parm.gAlpha(ei(j)) = amplEst(j);
  end
  j = 5;
  assert(abs(est.lambda-amplEst(j))<1e-10);
  j = j+1;
  est.parm.var(1,1) = amplEst(j)^2;
  
  %% Calculate cutoffs -- step (2) in README.txt
  % to save time, keep old lambda if it has changed by less than 1e-8;
  if (abs(cfg.mort.lambda-est.lambda) < 1e-8) 
    est.lambda = cfg.mort.lambda;
  end
  cfg.mort.lambda = est.lambda;
  cfg.cut.lambda = est.lambda;
  est.cut = loadcutoffs(cfg);
  est.cut = cleanCut(est.cut,cfg,data);

  %% Write initial values for joint estimation -- step (3.i)
  fid = fopen('initial.ampl','w');
  d = 0;
  for j=1:4
    fprintf(fid,'let muA[%d] := %.16g;\n',j,est.parm.gAlpha(ei(j)));
    % initial value for muB - make midway between cutoffs 
    k = find(est.parm.gAlpha(ei(j)) < est.cut(ei(j)).la,1);
    est.parm.gBeta(ei(j)) = 0.5*(est.cut(ei(j)).lb(k,1)+ ...
                                 est.cut(ei(j)).lb(k,2));
    d = d + est.cut(ei(j)).lb(k,1) - est.cut(ei(j)).lb(k,2);
    fprintf(fid,'let muB[%d] := %.16g;\n',j,est.parm.gBeta(ei(j)));
  end
  d = abs(d)/4;
  % set variance of beta so that about 80% are in middle region
  est.parm.var(2,2) = (d/(2*norminv(0.9)))^2;
  fprintf(fid,'let sigB := %.16g;\n',sqrt(est.parm.var(2,2)));
  fprintf(fid,'let sigA := %.16g;\n',sqrt(est.parm.var(1,1)));
  fclose(fid);

  %% Do joint estimation -- step 3.ii
  % estimate other parameters
  writeAMPLdata(data,cfg,est.cut); % write data with new cutoffs
  fprintf('calling ampltrial ...\n');
  if (ESTIMATE)
    if (strcmp(DIST,'normal'))
      [status output] = system('amplTrial pooled.ampl | tee ampl.out');
    elseif (strcmp(DIST,'gamma'))
      [status output] = system('amplTrial pooledgamma.ampl | tee ampl.out');
    elseif (strcmp(DIST,'gamma-normal'))
      [status output] = system('amplTrial pooledGN.ampl | tee ampl.out');
    end
    % pick best set of estimates -- ampl program runs estimation
    % beginning from many different initial values -- the results are
    % saved in pooled.out.  A perl program picks out the results with the
    % highest likelihood value and writes them in a format convenient for
    % loading into Matlab.  (This is admittedly a bit convoluted, and we
    % apologize for the mix-mash of languages) 
    system('perl writeEstimates.pl pooled.out');
  end

  % load estimates 
  shortPrefix = 'pooled';
  amplEst = load([shortPrefix '.out']);
  amplLike= amplEst(1);
  est.parm.gAlpha = zeros(size(data.alphaX,2),1);
  est.parm.gBeta = zeros(size(data.betaX,2),1);
  j = 2;
  for k=1:4
    est.parm.gAlpha(ei(k)) = amplEst(j);
    j = j+1;
  end
  est.parm.var(1,1) = amplEst(j)^2;
  j = j+1; 
  for k=1:4
    est.parm.gBeta(ei(k)) = amplEst(j);
    j = j+1;
  end
  est.parm.var(2,2) = amplEst(j)^2;
  j = j+1;
  est.parm.corr = amplEst(j);  
  est.parm.var(1,2) = est.parm.corr*sqrt(est.parm.var(1,1)* ...
                                         est.parm.var(2,2));
  
  % save results
  % run counterfactuals, create tables, etc.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  i = findstr('/',cfg.outprefix);
  prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix));
  
  %% run counterfactuals
  % reduced form
  if (mortOnly)
    cfnsim = 0;
  else
    cfnsim = 5000; % number of simulations for computing welfare costs, etc
  end
  
  % create directories for output
  mkdir(prefix);
  mkdir(prefix,'tables');
  mkdir(prefix,'figures');
  %eval(['delete ' prefix '/tables/*']);
  %eval(['delete ' prefix '/figures/*']);
  %eval(['delete ' prefix '/*']);
  if(cfg.mort.lambda~=est.lambda)
    warning('cfg lambda ~= est.lambda, using est.lambda');
    cfg.mort.lambda = est.lambda;
  end
    
  % convert data.alphaX and data.betaX into group dummies 
  for g=1:length(cfg.data.group)
    x(:,g) = data.group==g;
    data.name.alphaX(g).s = cfg.data.group(g).name;
    data.name.betaX(g).s = cfg.data.group(g).name;
  end
  data.alphaX(:,1:4) = x;
  data.betaX(:,1:4) = x;

  % table of estimates  
  est.parm.std.lambda = 0;
  est.parm.std.gAlpha = zeros(size(est.parm.gAlpha));
  est.parm.std.gBeta = zeros(size(est.parm.gBeta));
  est.parm.std.var = zeros(size(est.parm.var));
  est.parm.std.corr = 0;
  estReport(data,est,prefix);
    
  %% counterfactuals
  if(cfnsim)
    % run simulations
    cfdata = simCF(data,est,cfg,cfnsim,prefix,true,true,USEOLDSIM);
    % create tables
    cfTables(data,est,cfdata,cfg,prefix);
    % create tables and figures showing fit of estimates
    fit(data,est,cfdata,cfg,prefix);
    % print on screen the numbers that go in the robustness table VII
    robustTable(data,est,cfg,cfdata);
  end
  
  %% new counterfactuals looking at loading factors
  lfdata = cfLoadFactors(cfdata, est,cfg);

end
