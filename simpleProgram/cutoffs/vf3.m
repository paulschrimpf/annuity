function [res Ct Wt] = vf3(w0,payrate,q,Beta,guar_per,valParam);
% w0 is starting wealth
% Z(t) is annuity at time t
% q(t) is probably of death
% Beta is vector of bequest weights
% guar_per are the guarantee periods

  % unpack valParam structure
  T = valParam.T;
  gam = valParam.gam;
  bgam = valParam.bgam;
  delta = valParam.delta;
  rho = valParam.rho;
  zeta = valParam.zeta;
  pub = valParam.pub;
  infl = valParam.infl;
  frac = valParam.frac;

  % compute payments
  Z = w0*frac*payrate*cumprod(1/(1+infl)*ones(T,1));
  % computes PV of guarantee payments
  g = zeros(T+1,1);                     
  for t = guar_per:-1:1,
    g(t) = Z(t) + rho*g(t+1);
  end;    
  % add public annuity
  if(pub>0)
    if(pub+frac>1)
      error('public + private fraction annuitized > 1');
    end
    pubrate = pubAnnuityRate(infl,rho,q);
    Z = Z + (w0*pub*pubrate)*cumprod(1/(1+infl)*ones(T,1));
  end
  w0 = w0*(1-frac-pub);
  
  Nq = size(q,1);
  Nb = length(Beta);

  persistent opt ctol;
  if (isempty(opt)),
      opt = optimset('TolX',1.0e-4);
      ctol = 1.0e-10;
  end;
  if(delta~=rho)
    error('assumes delta==rho');
  end
  for ib = 1:Nb,
    if(Beta(ib)<1e-100)
        Beta(ib) = 1e-100;
    elseif (Beta(ib)>1e100)
        Beta(ib) = 1e100;
    end
    for iq = 1:Nq,  
      if (q(iq,1)==1) % going to die instantly, optimum is to just bequeath
                      % it all
         V(ib,iq) = Beta(ib)*beq(w0+g(1),bgam);
         Ct = zeros(1,T);
         Wt = w0*ones(1,T);
         continue;
      end

      if (bgam==0)
        % check if satiation consumption feasible
        w = w0;
        a = cumprod(1-q(iq,:));
        m(1) = q(iq,1);
        for t=2:T
          m(t) = q(iq,t)*a(t-1);
        end
        Ct = ones(1,T)*Beta(ib)^(-1/gam);
        w = (w-Ct(1)+Z(t))/rho;
        for t=2:T
          if (a(t)==0)
            break;
          end
          w = (w-Ct(t)+Z(t))/rho;
          if (w<0)
            break;
          end
        end
        if(w>0)
          Wt(1)=w0;
          v=0;
          for t=1:T
            if (a(t)==0)
              break;
            end        
            v = v + delta^(t-1)*(a(t)*util(Ct(t),gam)+ ...
                                 m(t)*Beta(ib)*beq(Wt(t)+g(t),bgam));
            Wt(t+1) = (Wt(t)-Ct(t)+Z(t))/rho;
          end
          if(a(T)>0)
            v = v + delta^(T+1)*a(T)*Beta(ib)*beq(Wt(T+1),bgam);
          end
          V(ib,iq)=v;        
          %fprintf('satiation consumption feasible %.16g\n',v);
          continue;
        end
      end
 
      %[c vfmin] = fminsearch(@(x) -vc0(x,w0,rho,q(iq,:)',Z,delta,gam, ...
      %                                    Beta(ib),g,bgam,zeta),1);
      %fprintf(' fmin: %.16g %.16g\n',c,vfmin);
      chi = (w0+Z(1));
      clo = 1e-250;
      [c vfmin] = fminbnd(@(x) -vc0(x,w0,rho,q(iq,:)',Z,delta,gam, ...
                                    Beta(ib),g,bgam,zeta),clo,chi);
      vfmin = -vfmin;

      cmid = (chi+clo)*0.5;
      its = 0;
      [vhi cth]= vc0(chi,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
      [vmid ctm]= vc0(cmid,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
      [vlo ctl]= vc0(clo,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam, ...
                     zeta);
      its = 0;
      while(vmid==-realmax || vmid==vhi)
        chi = cmid;
        vhi = vmid;
        cmid = (chi+clo)*0.5;
        vmid = vc0(cmid,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
        its = its+1;
        if (its>500)
          log(Beta(ib))
          [chi cmid clo]
          [vhi vmid vlo]
          log(gam)
          error('doh hi')
        end
      end;
      its = 0;
      while(vmid<=vhi)
        cmid = chi; vmid = vhi;
        chi = chi*2;
        if (chi>w0+Z(1))
          c = w0+Z(1);
          clo = c;
          chi = c;
          v = vc0(c,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
          break;
        end;
        vhi = vc0(chi,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
        its = its+1;
        if (its>100)
          log(Beta(ib))
          error('doh hi2')
        end
      end; 
      it = 0;
      while(vmid<=vlo)
        chi = cmid; vhi=vmid;
        cmid = (chi+clo)*0.5;
        vmid = vc0(cmid,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
        its = its+1;
        if (its>100)
          error('doh lo')
        end
      end
      if (chi~=clo && (vmid <= vlo || vmid <= vhi))
        fprintf('%20.16g ',[vlo vmid vhi]);
        fprintf('\n');
        fprintf('%20.16g ',[clo cmid chi]);  
        error('maximum not bracketed');
      end;
      v = (vhi+vlo)*0.5;
      c = (chi+clo)*0.5;
      its = 0;
      while(its==0 || chi-clo>ctol)
        if(chi-cmid>cmid-clo)
          c = (chi+cmid)*0.5;
        else
          c = (cmid + clo)*0.5;
        end;
        v = vc0(c,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
        if (v<vmid)
          if(c<cmid)
            clo=c; vlo=v;
          else
            chi=c; vhi=v;
          end;
        elseif(v>=vmid),
          if(c<cmid),
            chi=cmid; vhi=vmid;
            cmid=c; vmid=v;
          else
            clo=cmid; vlo=vmid;
            cmid=c; vmid=v;
          end;
        else
          [clo cmid chi c]
          [vlo vmid vhi v]
          gam
          bgam
          Beta(ib)
          fprintf('CRAP\n');
%          pause();
        end;            
        its = its+1;
        if(its>500)
          [clo cmid chi] 
          chi - clo
          ctol
          [vlo vmid vhi]
          error('too many iterations');
        end
      end;
      its;
      if (vmid>vfmin)
        V(ib,iq)=vmid;
      else
        V(ib,iq)=vfmin;
      end
%      fprintf(' mine: %.16g %.16g\n----------------------\n',c,v);
      

      if(nargout>1) 
        [val Ct Wt] = vc0(cmid,w0,rho,q(iq,:)',Z,delta,gam,Beta(ib),g,bgam,zeta);
        Wt = Wt(1:T);
      end;
    end;
  end;      
  res = V;
end
