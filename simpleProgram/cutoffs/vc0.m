%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [val Ct Wt] = vc0(c0,w0,rho,q,z,delta,gam,beta,g,bgam,zeta)
  if (~isreal(c0))
     val = -realmax;
     return;
  end;
  T = length(q)-1;
  [val Ct Wt] = vfc1(c0,w0,rho,q(1:T),z,delta,gam,beta,g(1:T),bgam,zeta);
  if (val<-1.0e300)
    val = -realmax;
  end
  return;

  % legacy  matlab version.  the c code is identical, but executes about
  % 100 times faster.
  val = 0;
  ct = c0;
  at = w0;
  pt = (1-q(1));  
  qt = q(1);
  lt = pt*ct^(-gam);
  Ct = zeros(T,1);
  Wt = zeros(T,1);
  for t=1:(T-1),
    Wt(t) = at;
    Ct(t) = ct;
    val = val+delta^(t-1)*(pt*util(ct,gam) + qt*beta*beq(at+g(t),bgam));
    if (~isreal(val))
      [t val ct at]
      [ptp1 lt]
    end
%      fprintf('%d %18.16g\n',t,val);
    at = (at+z(t)-ct)/rho;
    qt = pt*q(t+1);
    ltp1 = -qt*delta^(t)*beta*(at+g(t+1))^(-bgam) + lt*rho;
    if (at<0 || ~isreal(at) || ltp1<0 || ~isfinite(at))
        val = -realmax;
        return;
    end;
    ptp1 = pt*(1-q(t+1));
    ctp1 = ct* ( delta*(1-q(t+1))*lt/ltp1 )^(1/gam);
    ct = ctp1;
    lt = ltp1;
    pt = ptp1;
    if (pt==0) % no chance of living this long, return val up to this
               % point
      %fprintf('%d ',t);
      %[val c]
      return;
    end;    
  end;
  t = T;
  Wt(T) = at;
  Ct(T) = ct;
  val = val+delta^(T-1)*(pt*util(ct,gam) + qt*beta*beq(at+g(t),gam));
  %fprintf('%d %18.16g\n',t,val);
  at = (at+z(t)-ct)/rho;
  if (at<0 || ~isreal(at))
      val = -realmax;
      return;
  end;
  val = val+pt*delta^T*beta*beq(at,gam);
  %vc = vfc1(c0,w0,rho,q(1:T),z,delta,gam,beta,g(1:T));
  %if(abs(val-vc)/val>1.0e-12)
  %    fprintf('vals different %.16g %.16g\n',val,vc);
  %end
  
%  fprintf('%d %18.16g\n',t,val);
%  if(nargout>1)
%    fprintf('mfinal lt = %18.16g\n',lt*rho);
%    fprintf('m du/dwT  = %18.16g\n',delta^T*pt*beta*at^(-gam));
%  end
