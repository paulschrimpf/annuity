%% utility from wealth at death function
function res = beq(w,gam);
  if (gam==0)
    res = w;
  elseif (gam==1)
    res = log(w);
  else
    res = ((w.^(1-gam))-1)/(1-gam);
  end
end
