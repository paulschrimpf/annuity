#include <math.h>
#include "mex.h"
#define realmax 1.0e307
double  util(double c,double g) 
{
  return (pow(c,1.0-g)-1.0)/(1.0-g);
}

double beq(double c, double g) 
{ 
  if(g==0)
    return (c);
  else if(g==1)
    return (log(c));
  else
    return (pow(c,1.0-g)-1.0)/(1.0-g);
}

void mexFunction
( 
 int nlhs, /* number of left hand side arguments */
 mxArray *plhs[], /* pointer to lhs*/
 int nrhs, /* number of rhs arguments*/
 const mxArray *prhs[]) /* pointer to rhs arguments*/
{
  int T; /* number of time periods*/
  int t; /* loop indices*/
  
  /* input pointers*/
  double *c0; /* consumption at time 0*/
  double *w0; /* initial wealth*/
  double *rho; /* 1/(1+r)*/
  double *q; /* probability of death*/
  double *z; /* annuity payment*/
  double *gam; /* rra*/
  double *bgam; /* rra*/
  double *beta; /* bequest weight*/
  double *delta; /* discount rate*/
  double *g; /* sum of guaranteed payments if die at t*/
  double *zeta; /* additional bequest discount factor */
  
  /* output pointers*/
  double *val; /* value*/
  double *c;  /* consumption vector*/
  double *w;  /* wealth vector*/

  /* calculation variables*/
  double pt,ptp1; /* probability of living til t*/
  double qt; /* prob of dying at t*/
  double lt,ltp1; /* lagrange multiplier*/
  double dt; /* delta^t */
  double zt; /* zeta^t */
  double wb;

  if(nrhs!=11) {
    mexErrMsgTxt("Must have 11 arguments\n");
  }
#ifdef DEBUG 
  printf("Getting pointers ...\n");
#endif
  /* assign input arg pointers*/
  c0 = mxGetPr(prhs[0]);
  w0 = mxGetPr(prhs[1]);
  rho = mxGetPr(prhs[2]);
  q = mxGetPr(prhs[3]);
  T = mxGetM(prhs[3]);
  if(1!=mxGetN(prhs[3])) {
    mexErrMsgTxt("q: Dimension mismatch.\n");
  }
  z = mxGetPr(prhs[4]);
  if(T!=mxGetM(prhs[4]) || 1!=mxGetN(prhs[4])) {
    printf("%d %d %d\n",mxGetM(prhs[4]),mxGetN(prhs[4]),T);
    mexErrMsgTxt("z: Dimension mismatch.\n");
  }
  delta = mxGetPr(prhs[5]);
  gam = mxGetPr(prhs[6]);
  beta = mxGetPr(prhs[7]);
  g = mxGetPr(prhs[8]);
  if(T!=mxGetM(prhs[8]) || 1!=mxGetN(prhs[8])) {
    printf("%d %d %d\n",mxGetM(prhs[8]),mxGetN(prhs[8]),T);
    mexErrMsgTxt("g: Dimension mismatch.\n");
  }
  bgam = mxGetPr(prhs[9]);
  zeta = mxGetPr(prhs[10]);
#ifdef DEBUG 
  printf("Finished getting input pointers\n");
#endif
  
  /* allocate the return value*/
  plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
  val = mxGetPr(plhs[0]);
  if(nlhs > 1) {
    plhs[1] = mxCreateDoubleMatrix(T,1,mxREAL);
    c = mxGetPr(plhs[1]);
  }
  else c = calloc(T,sizeof(double));    
  if(nlhs > 2) {
    plhs[2] = mxCreateDoubleMatrix(T,1,mxREAL);
    w = mxGetPr(plhs[2]);
  }
  else w = calloc(T,sizeof(double));
#ifdef DEBUG 
  printf("Finished getting output pointers\n");
#endif
  w[0] = *w0;
  c[0] = *c0;
  qt = q[0];
  pt = 1.0 - q[0];
  lt = pt*pow(*c0,-(*gam));
  *val = 0.0;
  dt = 1.0;
  zt = 1.0;
  wb = *beta;
  for(t=0;t<T-1;t++) {
#ifdef DEBUG
    printf("Beginning time %d\n",t);
#endif
    w[t+1] = (w[t]+z[t]-c[t])/(*rho);
    if (w[t+1]<0) {
      c[t] = w[t]+z[t];
      w[t+1]=0;
    }
#ifdef DEBUG
    printf("  assigned w[t+1]\n");
#endif
    *val += dt*(pt*util(c[t],*gam) + qt*zt*wb*beq(w[t]+g[t],*bgam));
#ifdef DEBUG
    printf("  added to val\n");
#endif
    qt = pt*q[t+1];
    dt *= (*delta);
    zt *= (*zeta);
    /*if (w[t+1]=0) ltp1 = 0; */
    /*else ltp1 = */
    ltp1 = -qt*dt*zt*wb*pow(w[t+1]+g[t+1],-(*bgam)) + lt*(*rho);
#ifdef DEBUG
    printf("  assigned ltp1\n");
#endif
    if (ltp1<0) {
      *val = -realmax;
      /*      printf("ltp1<0\n"); */
      break;
    }
    ptp1 = pt*(1.0-q[t+1]);
    if (ptp1==0) break;  /* dead */
    c[t+1] = c[t]* pow((*delta)*(1-q[t+1])*lt/ltp1,1./(*gam));
#ifdef DEBUG
    printf("  assigned c[t+1]\n");
#endif
    lt = ltp1;
    pt = ptp1;
#ifdef DEBUG
    printf("Done with time %d\n",t);
#endif
  }
  if(t==T-1) {
    lt = (w[t]+z[t]-c[t])/(*rho);
    if(lt<0) {
      c[t] = w[t]+z[t];
      *val += dt*(pt*util(c[t],*gam) + qt*zt*wb*beq(w[t]+g[t],*bgam));
    }
    else {
      *val += dt*(pt*util(c[t],*gam) + qt*zt*wb*beq(w[t]+g[t],*bgam));
      dt *= (*delta);
      zt *= (*zeta);
      *val += pt*dt*zt*wb*beq(lt,*bgam);
    }
  }   
#ifdef DEBUG 
  printf("Just need to free w and c\n");
#endif
  if(nlhs<3) free(w);
  if(nlhs<2) free(c);
  return;
}

