function cut = findbcut(cfg);
% calculates and saves cutoffs -- cutoffs in beta given alpha and gamma
  
  % set parameters
  agemax = cfg.cut.agemax; % Maximal age alive
  valParam.frac = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  valParam.gam  = cfg.cut.gam;      % CRRA coefficient
  valParam.bgam  = cfg.cut.bgam;      % CRRA coefficient
  z_slope = cfg.cut.z; % paymenet rates
  valParam.zeta = cfg.cut.zeta;
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end

  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  valParam.pub = pub;
    
  if valParam.gam==1;
    valParam.gam=1.00000001; % If gamma=1, util is "undefined" %
  end;
  
  guar    = [0 5 10];  % guarantee options
  gbeta = cfg.cut.lambda;

  choices = length(guar); % number of choices
  % finding the cutoff values for beta
  bmax = 50.0;
  btol = 1.0e-8;
  warning('off','MATLAB:divideByZ')
  %profile on;
  tic
  for group=1:size(cfg.cut.z,1)
  for iw=1:length(cfg.cut.w0)
    w0 = cfg.cut.w0(iw);
    z_slope = cfg.cut.z(group,:);
    if (length(cfg.cut.rho)>1)
      valParam.rho = cfg.cut.rho(group);     % Riskless interest rate                       
    else 
      valParam.rho = cfg.cut.rho;
    end
    if (length(cfg.cut.delta)>1)
      valParam.delta = cfg.cut.delta(group);   % Utility discount rate                        
    else
      valParam.delta = cfg.cut.delta;   % Utility discount rate                        
    end
    if (length(cfg.cut.inflation)>1)
      valParam.infl = cfg.cut.inflation(group);   % inflation
    else
      valParam.infl = cfg.cut.inflation;   
    end

    cut(group).lambda = cfg.cut.lambda; % lambda
    cut(group).la = cfg.cut.la;
    agemin = cfg.cut.agemin(group); % Age
    galpha = exp(cut(group).la);
    galpha_length=length(galpha);
    valParam.T = agemax-agemin; % Determines number of periods in problem
    T = valParam.T;
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    part2  = exp(galpha/gbeta*(1 - exp(gbeta*(tp1.^h))));
    part1  = exp(galpha/gbeta*(1 - exp(gbeta*(t.^h  ))));
    q      = (part1 - part2)./part1; % probability of dying at time t
                                     % conditional on living until t-1
                                     % q(i,t) is prob at time t for alpha(i) 
    q(part1==0) = 1; % at high t, both part1 and part2 are numerically 0.  
    bcut = zeros(galpha_length,choices);
    diff = zeros(galpha_length,choices);
    q(1,:)
    for ga = galpha_length:-1:1,
      for c=1:choices,
        cp1 = c+1;
        if (cp1>choices)
          cc = 1;
          cp1 = 3;
        else 
          cc = c;
        end

        if(ga ==galpha_length)
          b0 = 0;
        else
          b0 = max(bcut(ga+1:galpha_length,cc));
        end;
        
        if (ga==1 || bcut(ga-1,cc)==0)
          b1 = bmax;
        else
          b1 = bmax; %bcut(ga-1,cc);
        end;
%        log(galpha(ga))
        dV0 = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b0),guar(cc), ...
                    guar(cp1),valParam);
        dV1 = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b1),guar(cc),guar(cp1),valParam);
        if (dV0*dV1>=0) 
          if (0 &&abs(dV0)<1e-6)
            [bcut(ga,c) f]= fzero(@(b) vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b),guar(cc), ...
                                             guar(cp1),valParam),b0);
            fprintf('0: a=%.4g c=%d b=%.16g f=%.16g\n',log(galpha(ga)),c, ...
                    bcut(ga,c),f);
            continue;            
          elseif (0 && abs(dV1)<1e-6)
            [bcut(ga,c) f]= fzero(@(b) vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b),guar(cc), ...
                                             guar(cp1),valParam),b1);
            fprintf('1: a=%.4g c=%d b=%.16g f=%.16g\n',log(galpha(ga)),c, ...
                    bcut(ga,c),f);
            continue;
          end
          b0 = 5.0;
          dV0 = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b0),guar(cc),guar(cp1),valParam);
          iter = 0;
          while(dV0<0 && iter<20),
            iter = iter +1;
            b0 = b0-10.0;
            dV0 = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b0),guar(cc),guar(cp1),valParam);
          end;
          iter = 0;
          while(dV1>0 && iter<100 && b1<709/4),
            iter = iter +1;
            b1 = b1*1.5;
            dV1 = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b1),guar(cc),guar(cp1),valParam);
          end;
          if (dV0*dV1>=0) 
            fprintf('WARNING: bcut not bracketed\n');
            fprintf('%d %g %g %g %g\n',[c dV0 dV1 b0 b1]);
            continue;
          end;
        end
        iter = 0;
        dV = 100;
        b = (b1+b0)/2;
        while ((b1 - b0) > btol && iter<100),
          iter = iter+1;
          b = (b1+b0)/2;
          dV = vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b),guar(cc),guar(cp1),valParam);
          if (dV*dV0>0) 
            b0 = b;
            dV0 = dV;
          elseif (dV*dV1>0),
            b1 = b;
            dV1 = dV;
          else
            b1 = b;
            b0 = b;
            dV1 = dV;
          end
        end
        %fprintf('%d: %d iterations\n',ga,iter);
        %b = (b1+b0)/2;
        %bcut(ga,c) = b;
        %if(ga ==galpha_length)
        %  b0 = -4;
        %else
        %  b0 = max(bcut(ga+1:galpha_length,cc));
        %end;
        bcut(ga,c) = fzero(@(b) vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:),exp(b),guar(cc), ...
                                      guar(cp1),valParam),b);
        fprintf('a=%.4g c=%d b=%.8g f=%.8g b2 = %.8g f2 = %.8g\n',log(galpha(ga)),c, ...
                bcut(ga,c),vdiff(w0,z_slope(cc),z_slope(cp1),q(ga,:), ...
                                 exp(bcut(ga,c)),guar(cc),guar(cp1),valParam),b,dV);
      end; % loop over c
      fprintf('Group %d of %d: point %d of %d finished\n', ...
              group,size(cfg.cut.z,1),ga,galpha_length);
      if (bcut(ga,1)>bcut(ga,2))
        fprintf('     logalpha=%g, 5 year never chosen\n', ...
                log(galpha(ga)));
      end
    end; % loop over ga
    la = log(galpha);
    lb = bcut;
    lb(lb==0) = -Inf;
    bcut = exp(lb);
    plot(la,lb(:,1),la,lb(:,2),la,lb(:,3));
    legend('cut1','cut2','cut3');
  
    bcut_orig=bcut;
    for i = 1:choices,
      ga = 1;
      while(bcut(ga,i)==0)
        ga = ga+1;
        if (ga > size(bcut,1)-1)
          fprintf('WARNING: unable to find cutoff for any alpha\n');
          ga = size(bcut,1)-1;
          break;
        end
      end
      if (ga>1);
        warning('bcut will contain extrapolations');
        la = log(galpha);
        bcut(1:(ga-1),i) = exp(lb(ga,i)+(la(1:(ga-1))- ...
                                         la(ga))*(lb(ga,i)-lb(ga+1, ... 
                                                  i))/(la(ga)-la(ga+1))); 
      end
      ga = galpha_length;
      while(bcut(ga,i)==0)
        ga = ga-1;
      end
      if (ga<galpha_length);
        warning('bcut will contain extrapolations');
        la = log(galpha);
        bcut((ga+1):galpha_length,i) = exp(lb(ga,i) + ...
                                           (la((ga+1):galpha_length)-la(ga))*(lb(ga,i)-lb(ga-1,i))/ ...
                                           (la(ga)-la(ga-1))); 
      end
    end 
    lb = log(bcut);
    figure;
    plot(la,lb(:,1),la,lb(:,2));
    
    if (length(cfg.cut.w0)>1)
      cut(group).lb{iw} = lb;
      cut(group).lb_orig{iw} = log(bcut_orig);
    else
      cut(group).lb = lb;
      cut(group).lb_orig = log(bcut_orig);
    end
    cut(group).z = z_slope;
    cut(group).cfg = cfg; % store all config info
    cut(group).rho = valParam.rho;
    cut(group).delta = valParam.delta;
    cut(group).inflation = valParam.infl;
    cut(group).agemin = cfg.cut.agemin(group);
    cut(group).zeta = valParam.zeta;
    cut(group).public = cfg.cut.public;
    cut(group).w0 = w0;
    cut(group).wp = cfg.cut.wp;
  end % loop over groups
  end; % loop over w0
  cut
  %profile viewer
  toc
  %eval(['save ' cfg.cut.file ' cut']);
end % function findbcut()  


