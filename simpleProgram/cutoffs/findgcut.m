function cut = findgcut(cfg);
% calculates and saves cutoffs -- cutoffs in gamma given alpha and beta

  if (~isfield(cfg.cut,'raHetero'))
    error('cfg.cut.raHetero is not a field');
  end
  
  valParam.beta = cfg.cut.raHetero.beta;
  if (isfield(cfg.cut.raHetero,'cons') )
    % these say which coeff of risk aversion we're varying
    valParam.conRA = cfg.cut.raHetero.cons;
  else
    valParam.conRA  = 1;
  end
  valParam.bgam = cfg.cut.bgam;
  valParam.gam = cfg.cut.gam;
  if (isfield(cfg.cut.raHetero,'beq') )
    valParam.beqRA = cfg.cut.raHetero.beq;
  else
    valParam.beqRA = 1;
  end

  % set parameters
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  valParam.frac = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  
  z_slope = cfg.cut.z; % paymenet rates
  valParam.zeta = cfg.cut.zeta;
  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  valParam.pub = pub;
      
  guar    = [0 5 10];  % guarantee options
  gbeta = cfg.cut.lambda;

  choices = 3; % number of choices
  % finding the cutoff values for beta
  bmax = log(100.0);
  btol = 1.0e-8;
  warning('off','MATLAB:divideByZ')
  %profile on;
  tic
  for group=1:size(cfg.cut.z,1)
    z_slope = cfg.cut.z(group,:);
    if (length(cfg.cut.rho)>1)
      valParam.rho = cfg.cut.rho(group);     % Riskless interest rate                       
    else 
      valParam.rho = cfg.cut.rho;
    end
    if (length(cfg.cut.delta)>1)
      valParam.delta = cfg.cut.delta(group);   % Utility discount rate                        
    else
      valParam.delta = cfg.cut.delta;   % Utility discount rate                        
    end
    if (length(cfg.cut.inflation)>1)
      valParam.infl = cfg.cut.inflation(group);   % inflation
    else
      valParam.infl = cfg.cut.inflation;   
    end
    if (length(cfg.cut.raHetero.beta)>1)
      valParam.beta = cfg.cut.raHetero.beta(group);
    end % else beta set already
    
    cut(group).lambda = cfg.cut.lambda; % lambda
    cut(group).la = cfg.cut.la;
    agemin = cfg.cut.agemin(group); % Age
    galpha = exp(cut(group).la);
    galpha_length=length(galpha);
    valParam.T = agemax-agemin; % Determines number of periods in problem
    T = valParam.T;
    base = cfg.mort.baseage;
    t = (agemin-base):(agemax-base);
    tp1 = t+1;
    part2  = exp(galpha/gbeta*(1 - exp(gbeta*(tp1))));
    part1  = exp(galpha/gbeta*(1 - exp(gbeta*(t  ))));
    q      = (part1 - part2)./part1; % probability of dying at time t
                                     % conditional on living until t-1
                                     % q(i,t) is prob at time t for alpha(i) 
    q(part1==0) = 1; % at high t, both part1 and part2 are numerically 0.  
    gcut = zeros(galpha_length,choices);
    diff = zeros(galpha_length,choices);
    ga = round(0.6*galpha_length);
    k = 1;
    cut(group).grid.la = cut(group).la
    glo = 0.3;
    ghi = 50;
    gTry = glo:0.2:ghi;
    cut(group).grid.g = gTry;
    for ga = galpha_length:-1:1,
      for c=1:choices,
        cp1 = c+1;
        if (cp1>choices)
          cc = 3;
          cp1 = 1;
        else 
          cc = c;
        end
        gTry = glo:0.2:ghi;
        clear dV;
        for k=1:length(gTry);
          g = gTry(k);
          [dV(k,1) v0 v1]= vdiff_g(w0,z_slope(cc),z_slope(cp1),q(ga,:),log(g),guar(cc), ...
                            guar(cp1),valParam);
          cut(group).grid.val(c,ga,k) = v0;
        end
        [dV0 i0] = min(dV);
        g0 = log(gTry(i0));
        [dV1 i1] = max(dV);
        g1 = log(gTry(i1));
        if (dV0*dV1>0) 
          fprintf('need to refine initial bracket\n');
%          [dV gTry'];
%          gTry = glo:0.2:(ghi*5);
%          for k=1:length(gTry);
%            g = gTry(k);
%            dV(k,1) = vdiff_g(w0,z_slope(cc),z_slope(cp1),q(ga,:),log(g),guar(cc), ...
%                              guar(cp1),valParam);
%          end
          if (dV0*dV1>0) 
            fprintf('failed to find initial bracket\n');
            fprintf(' ga = %d, log a = %g, c = %d\n',ga,log(galpha(ga)),c);
            continue;
          end        
        end
        iter = 0;
        dV = 100;
        b = (g1+g0)/2;
        while (abs(g1 - g0) > btol && iter<100),
          iter = iter+1;
          g = (g1+g0)/2;
          dV = vdiff_g(w0,z_slope(cc),z_slope(cp1),q(ga,:),g,guar(cc),guar(cp1),valParam);
          if (dV*dV0>0 || (mod(iter,2) && dV==0))
            g0 = g;
            dV0 = dV;
          else %if (dV*dV1>0 || (mod(iter,2)==0 && dV==0)),
            g1 = g;
            dV1 = dV;
          end
        end
        g = (g1+g0)/2;
        gcut(ga,c) = g;
        fprintf('a=%.4g c=%d g=%g f=%.8g, iter = %d\n',log(galpha(ga)),c, ...
                gcut(ga,c),vdiff_g(w0,z_slope(cc),z_slope(cp1),q(ga,:), ...
                                   gcut(ga,c),guar(cc),guar(cp1),valParam),iter);
      end; % loop over c
      fprintf('Group %d of %d: point %d of %d finished\n', ...
              group,size(cfg.cut.z,1),ga,galpha_length);
      if (gcut(ga,1)>gcut(ga,2))
        fprintf('     logalpha=%g, 5 year never chosen\n', ...
                log(galpha(ga)));
      end
    end; % loop over ga
    la = log(galpha);
    lb = gcut;
    lb(lb==0) = -Inf;
    gcut = exp(lb);
    plot(la,lb(:,1),la,lb(:,2),la,lb(:,3));
    legend('cut1','cut2','cut3');
  
    gcut_orig=gcut;
    for i = 1:choices,
      ga = 1;
      while(gcut(ga,i)==0)
        ga = ga+1;
        if (ga > size(gcut,1)-1)
          fprintf('WARNING: unable to find cutoff for any alpha\n');
          ga = size(gcut,1)-1;
          break;
        end
      end
      if (ga>1);
        warning('gcut will contain extrapolations');
        la = log(galpha);
        gcut(1:(ga-1),i) = exp(lb(ga,i)+(la(1:(ga-1))- ...
                                         la(ga))*(lb(ga,i)-lb(ga+1, ... 
                                                  i))/(la(ga)-la(ga+1))); 
      end
      ga = galpha_length;
      while(gcut(ga,i)==0)
        ga = ga-1;
      end
      if (ga<galpha_length);
        warning('gcut will contain extrapolations');
        la = log(galpha);
        gcut((ga+1):galpha_length,i) = exp(lb(ga,i) + ...
                                           (la((ga+1):galpha_length)-la(ga))*(lb(ga,i)-lb(ga-1,i))/ ...
                                           (la(ga)-la(ga-1))); 
      end
    end
    
    lb = log(gcut);
    figure;
    plot(la,lb(:,1),la,lb(:,2));
    
    cut(group).lb = lb;
    cut(group).lb_orig = log(gcut_orig);
    cut(group).z = z_slope;
    cut(group).cfg = cfg; % store all config info
    cut(group).rho = valParam.rho;
    cut(group).delta = valParam.delta;
    cut(group).inflation = valParam.infl;
    cut(group).agemin = cfg.cut.agemin(group);
    cut(group).zeta = valParam.zeta;
    cut(group).public = cfg.cut.public;
    %cut(group).lb = [];
    cut(group).beta = valParam.beta;
    cut(group).w0 = w0;
    cut(group).wp = 1;
  end % loop over groups
  %profile viewer
  toc
  %eval(['save ' cfg.cut.file ' cut']);
end % function findgcut()  


