function [dV V0 V1] = vdiff_g(w0,Z0,Z1,q,loggamma,g0,g1,valParam)
  b = valParam.beta;
  g = exp(loggamma);
  if (g==1)
    g = 1 + 1e-8;
  end
  if (valParam.conRA)
    valParam.gam = g;
  end
  if (valParam.beqRA)
    valParam.bgam = g;
  end
  
  [V0 c0 wt0]= vf3(w0,Z0,q,b,g0,valParam);
  [V1 c1 wt1] = vf3(w0,Z1,q,b,g1,valParam);
  v0c1 = vCheck(c1,wt1,Z0,q,g0,b,valParam,w0);

    if (v0c1>V0)
       %fprintf('%20.16g  ',[c1(1) c0(1) V0 v0c1]);
       %fprintf('\n');
       %warning('vfunc 0 inaccuracy');
       V0 = v0c1;
    end;
    v1c0 = vCheck(c0,wt0,Z1,q,g1,b,valParam,w0);
    if (v1c0>V1)
       %fprintf('%20.16g  ',[c1(1) c0(1) V1 v1c0]);
       %fprintf('\n');
       %warning('vfunc 1 inaccuracy');
       V1 = v1c0;
    end;    
    dV = V0 - V1;
end  % function vdiff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
function val = vCheck(Ct,Wt,z,q,guar,beta,valParam,w0)
  rho = valParam.rho;
  gam = valParam.gam;
  bgam = valParam.bgam;
  delta = valParam.delta;
  zeta = valParam.zeta;
  pub = valParam.pub;
  infl = valParam.infl;
  frac = valParam.frac;

  T = length(q)-1;
  g = zeros(T,1);

  
  % compute payments
  Z = w0*frac*z*cumprod(1/(1+infl)*ones(T,1));;
  % computes PV of guarantee payments
  g = zeros(T+1,1);                     
  for t = guar:-1:1,
    g(t) = Z(t) + rho*g(t+1);
  end;    
  % add public annuity
  if(pub>0)
    if(pub+frac>1)
      error('public + private fraction annuitized > 1');
    end
    pubrate = pubAnnuityRate(infl,rho,q);
    Z = Z + (w0*pub*pubrate)*cumprod(1/(1+infl)*ones(T,1));
  end
  w0 = w0*(1-frac-pub);

  for t = guar:-1:1,
    g(t) = Z(t) + rho*g(t+1);
  end;    
  val = vc0(Ct(1),Wt(1),rho,q',Z,delta,gam,beta,g,bgam,zeta);
  if(val==0)
    val = -realmax;
  end;
  return;
end % function vCheck
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

