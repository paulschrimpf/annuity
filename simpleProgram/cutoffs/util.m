% utility from consumption function
function res = util(c,gam);
  res = ((c.^(1-gam))-1)/(1-gam); 
end
