#!/bin/perl

my $file;
my $first = 1;
my $line;
my $max;
my $maxline;
my @fields;


my $numArgs = $#ARGV + 1;
if ($numArgs != 1) {
    die "one command line argument required\n";
}
$file = $ARGV[0];
open OUT,">$file";
$file = $file.".csv";
`cp pooled.csv $file`;
$file = "pooled.csv";

print "working on $file\n";
open IN, "<$file";
# skip three header lines
$_ = <IN>;
$_ = <IN>;
$_ = <IN>;

# find maximum likelihood value
$max = -99999999999;
$maxline = "";
while ($line = <IN>) {
    $line =~ m/([\-.0-9]+),/;
    if ($1 > $max) {
        $max = $1;
        $maxline = $line;
    }
}
@fields = split(/\,/,$maxline);
# print parameter values 
print OUT "$fields[0]\n"; # loglike
my $j; # mu alpha
for($j=2;$j<6;$j++) {
    print OUT  "$fields[$j]\n";
}
print OUT "$fields[7]\n"; #sigA
for($j=9;$j<13;$j++) { # mu beta
    print OUT  "$fields[$j]\n";
}
print "$#fields\n";
print OUT "$fields[14]\n"; # sigB
print OUT "$fields[16]\n"; # corr
#print OUT "$fields[17]\n"; # lambda
for($j = 18;$j<=$#fields;$j+=2) {
    print OUT "$fields[$j]\n";
}

close(OUT);
close(IN);

