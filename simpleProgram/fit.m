function fit(data,est,cfdata,cfg,prefix)
  x = transformParam(est.parm,'pack','gmm',est.dist);
  out = [prefix '/tables/fit.tex'];
  of = fopen(out,'w');
  for gr=1:data.ngroup
    if (mod(gr,2)==1) 
      % print group names
      i = strfind(cfg.data.group(gr).name,'1992');
      name = cfg.data.group(gr).name(1:i-1);
      fprintf(of,'%s &',name);
      i = strfind(cfg.data.group(gr+1).name,'1992');
      name = cfg.data.group(gr+1).name(1:i-1);
      fprintf(of,' %s \\\\ \\hline ',name);
      fprintf(of,' \\begin{tabular}{c|cccc}\n');
    else
      fprintf(of,' & ');
      fprintf(of,' \\begin{tabular}{cccc}\n');
    end
    
    clear annuityGMM gData;
    % only use data from this group
    gData = groupData(data,gr);
    [f g h gi m] = annuityGMM(x,est,gData);
    pd(1) = 0;
    pd(2:4) =  m(2,3:5);
    pd(1) = pd(2)*m(2,1) + pd(3)*m(2,2) + pd(4)*(1-m(2,1) - m(2,2));
    
    if(mod(gr,2)==1) 
      fprintf(of,' & ');
    end
    fprintf(of,'\\multicolumn{4}{c}{Guarantee Length} \\\\ \n');
    if(mod(gr,2)==1) 
      fprintf(of,' & ');
    end
    fprintf(of,'All & 0 & 5 & 10 \\\\ \\hline \n');
    if(mod(gr,2)==1) 
      fprintf(of,' & ');
    end
    fprintf(of,['\\multicolumn{4}{c}{Proportion} \\\\ ' ...
                '\n']);
    if(mod(gr,2)==1) 
      fprintf(of,'Observed & ');
    end
    fprintf(of,'--- ');
    fprintf(of,'& %.4g ',[m(1,1:2) 1-m(1,1)-m(1,2)]);
    fprintf(of,'\\\\ \n');
    if(mod(gr,2)==1) 
      fprintf(of,'Estimated &');
    end
    fprintf(of,' --- ');
    fprintf(of,'& %.4g ',[m(2,1:2) 1-m(2,1)-m(2,2)]);
    fprintf(of,'\\\\ \n');
    if(mod(gr,2)==1) 
      fprintf(of,' & ');
    end
    fprintf(of,['\\multicolumn{4}{c}{Probability of Death} \\\\ \n']);
    if(mod(gr,2)==1) 
      fprintf(of,'Observed &');
    end
    fprintf(of,' %.4g',mean(gData.died));
    for g=0:5:10
      fprintf(of,'& %.4g ',mean(gData.died(gData.g==g)));
    end
    fprintf(of,'\\\\ \n ');
    if(mod(gr,2)==1) 
      fprintf(of,'Estimated &');
    end
    fprintf(of,' %.4g',pd(1));
    for j=2:4
      fprintf(of,'& %.4g ',pd(j));
    end
    fprintf(of,'\\\\ \n ');
    if(mod(gr,2)==1) 
      fprintf(of,' &');
    end
    fprintf(of,[' \\multicolumn{4}{c}{Expected Payment} \\\\ \n']);
    gi = cfdata.group==gr;
    oc = cfdata.oc;
    op = zeros(cfdata.N,1);
    for i = 1:cfdata.N
      op(i) = cfdata.epay(i,oc(i));
    end
    if ~isequal(size(gi),size(cfdata.g))
      gi = gi';
    end    
    table(1,:) = [mean(op(gi)) mean(cfdata.epay(cfdata.g==0 & gi,1)) ...
                  mean(cfdata.epay(cfdata.g==5 & gi,2)) ...
                  mean(cfdata.epay(cfdata.g==10 & gi,3))];
    if(mod(gr,2)==1) 
      fprintf(of,' &');
    end
    fprintf(of,' %.4g',table(1,1));
    for j=2:4
      fprintf(of,'& %.4g ',table(1,j));
    end
    fprintf(of,'\\\\ \n');
    if(mod(gr,2)==1) 
      fprintf(of,['\\multicolumn{5}{l}{Interest rate such that company' ...
                  ' breaks even = %.4g} \\\\ \n'],evenInterest(cfdata,cfg));
    else
      fprintf(of,['\\multicolumn{4}{l}{Interest rate such that company' ...
                  ' breaks even = %.4g} \\\\ \n'],evenInterest(cfdata,cfg));      
    end
    fprintf(of,'\\end{tabular} \n');
    if(mod(gr,2)==0)
      fprintf(of,' \\\\ \hline ');
    end
  end
  fclose(of);
  
end

