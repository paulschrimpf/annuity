%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Counterfactuals that look other loading factors. In the published paper, we
% compare the observed asymmetric info where loading factors are unequal across
% guarantee lengths with symmetric information where we equalize loading
% factors. Here we simulate two additional setups. First, we look at
% symmetric information with the same unequal loading factors as in the
% observed data. Second, we calculate choices under asymmetric info with
% loading factors that would be equal if people chose their guarantees
% randomly. The second setup has two downsides: observed loading factors will
% not be equal because of selection and insurance profits may be greater or
% lower due to selection
function cfdata = cfLoadFactors(cfdataIn,est,cfg)
  cfdata=cfdataIn;

  %global T bgam gam delta rho;
  guar = 0:5:10; % guarantee lengths
  w0   = cfg.cut.w0;       % Starting wealth (almost always = 100)        
  agemax = cfg.cut.agemax; % Maximal age alive
  alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
  gam  = cfg.cut.gam;      % CRRA coefficient
  bgam  = cfg.cut.bgam;      % CRRA coefficient
  infl  = cfg.cut.inflation;% Inflation rate                               
  rho   = cfg.cut.rho;     % Riskless interest rate                       
  delta = cfg.cut.delta;   % Utility discount rate                        
  zeta = cfg.cut.zeta;
  nchoice = length(guar);
  disp = 10;
  lambda = est.lambda;
  logAlpha = log(cfdata.alpha);
  if (isfield(cfg.cut,'raHetero'));
    logBeta = log(cfdata.gam);
  else
    logBeta = log(cfdata.beta);
  end


  if(isfield(cfg.cut,'public'))
    pub = cfg.cut.public;
  else 
    pub = 0;
  end
  if (isfield(cfg.mort,'fasthazard'))
    h = cfg.mort.fasthazard;
  else 
    h = 1;
  end


  %%----------------------------------------------------------------------
  % symmetric information, unequal loading factors
  % simulate choices, wealth equivalents, etc
  warning('Off','MATLAB:divideByZero');
  % calculate average payments by contract under asym info
  meanPay = zeros(cfdata.ngroup,max(cfdata.oc));
  for g=1:cfdata.ngroup
    for c=1:size(meanPay,2)
      meanPay(g,c) = mean(cfdata.epay(cfdata.oc==c & cfdata.group==g,c));
    end
  end
  meanPay
  
  for n=1:cfdata.N
    if(isfield(cfdata,'w0'))
      w0 = cfdata.w0(n);
    end
    if isfield(cfdata,'gam') && length(cfdata.gam)>1
      gam = cfdata.gam(n);
      bgam = cfdata.bgam(n);
    end
    if isfield(cfdata,'frac')
      alpha2 = cfdata.frac(n);
    end

    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(cfdata.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(cfdata.group(n));
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(cfdata.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(cfdata.group(n));
    end

    z_slope = cfg.cut.z(cfdata.group(n),:);
    agemin = cfg.cut.agemin(cfdata.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    galpha = exp(logAlpha(n));
    if isfield(cfdata,'alphaUncertainty');
      t1 = (agemin-base):(agemax-base+1);
      % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
      % compute by gauss-hermite quadrature
      n = 200; % make it very precise
      [n x w] = gqzero(n);
      x = x*(sqrt(2)*cfdata.alphaUncertainty);
      w = w/sqrt(pi);
      if (size(x,2)==1) %  change to row5D vectors
        x = x';
        w = w';
      end
      j = 1;
      for t=agemin-base:agemax-base+1;
        Mt(:,j) = sum(exp(galpha*exp(x)/lambda*(1 - exp(lambda*(t^h)))) ...
                      .*(ones(size(galpha))*w),2);
        j = j+1;
      end
      L = size(Mt,2);
      part1 = Mt(:,1:L-1);
      part2 = Mt(:,2:L);
    else 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    end    

    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    if (isfield(cfdata,'alphaBias') && cfdata.alphaBias ~= 1)
      mu = cfdata.alphaX(n,:)*est.parm.gAlpha;
      logAHat(n) = mu + cfdata.alphaBias*(logAlpha(n)-mu);
      alph = exp(logAHat(n));
      part22  = exp(alph/lambda*(1 - exp(lambda*(tp1.^h))));
      part12  = exp(alph/lambda*(1 - exp(lambda*(t.^h  ))));     
      qPerceived = (part12 - part22)./part12;
      qPerceived(part12==0) = 0;
    else
      logAHat(n) = logAlpha(n);
      qPerceived = q;
    end

    fr     = (part1(1:T) - part2(1:T))';          % density  
    g = cfdata.group(n);
    if isfield(cfdata,'frac')
      wa = cfdata.w0.*cfdata.frac;
    else
      wa = w0*ones(size(cfdata.group));
    end
    % annuity payment rates  
    for c=1:length(z_slope)
      zi(c) = z_slope(c).*meanPay(g,c)/cfdata.epay(n,c) ...
              * wa(n)/mean(wa(cfdata.group==g));
    end
    clear wa;
    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.frac = alpha2;
    vp.infl = infl;
    
    for c = 1:nchoice,        % guarantee choice
      Z = w0*(alpha2*zi(c)); % formula for annuity pricing
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cfdata.s.val(n,c) = vf3(w0,zi(c),qPerceived,cfdata.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      cfdata.s.weq(n,c) = weq(cfdata.s.val(n,c),wlo,whi,qPerceived,cfdata.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        cfdata.s.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        cfdata.s.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    cfdata.s.epay(n,:) = cfdata.s.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('Symmetric: Done with %d of %d\n',n,cfdata.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
  [m soc] = max(cfdata.s.val,[],2);
  cfdata.s.oc = soc;
  for n =1:cfdata.N
    cfdata.s.g(n) = guar(soc(n));
  end
  
  %%----------------------------------------------------------------------
  % asymmetric info, loading factors that would be equal if people choose
  % randomly 
  totalMeanPay = zeros(size(meanPay,1),1);
  eqZ = zeros(size(meanPay));
  for g=1:size(meanPay,1)
    for c=1:size(meanPay,2)
      if any(cfdata.oc==c & cfdata.group==g)
        totalMeanPay(g) = totalMeanPay(g) + ...
            meanPay(g,c)*mean(cfdata.oc(cfdata.group==g)==c);
      end
    end
    for c=1:size(meanPay,2)
      eqZ(g,c) = cfg.cut.z(g,c)*totalMeanPay(g)/mean(cfdata.epay(cfdata.group==g,c));
    end
  end
  
  for n=1:cfdata.N
    if(isfield(cfdata,'w0'))
      w0 = cfdata.w0(n);
    end
    if isfield(cfdata,'gam') && length(cfdata.gam)>1
      gam = cfdata.gam(n);
      bgam = cfdata.bgam(n);
    end
    if isfield(cfdata,'frac')
      alpha2 = cfdata.frac(n);
    end

    % parameters that optionally vary with group
    if (length(cfg.cut.rho)>1)
      rho = cfg.cut.rho(cfdata.group(n));
    end
    if (length(cfg.cut.delta)>1)
      delta = cfg.cut.delta(cfdata.group(n));
    end
    if (length(cfg.cut.zeta)>1)
      zeta = cfg.cut.zeta(cfdata.group(n));
    end
    if (length(cfg.cut.inflation)>1)
      infl = cfg.cut.inflation(cfdata.group(n));
    end

    agemin = cfg.cut.agemin(cfdata.group(n));
    T = agemax - agemin;
    inf_factor = cumprod((1/(1+infl))*ones(T,1)); % Vector of inflation rate factors
    rdis = cumprod(rho*ones(T,1)); % vector of interest discount factors
    base = cfg.mort.baseage;
    galpha = exp(logAlpha(n));
    if isfield(cfdata,'alphaUncertainty');
      t1 = (agemin-base):(agemax-base+1);
      % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
      % compute by gauss-hermite quadrature
      n = 200; % make it very precise
      [n x w] = gqzero(n);
      x = x*(sqrt(2)*cfdata.alphaUncertainty);
      w = w/sqrt(pi);
      if (size(x,2)==1) %  change to row5D vectors
        x = x';
        w = w';
      end
      j = 1;
      for t=agemin-base:agemax-base+1;
        Mt(:,j) = sum(exp(galpha*exp(x)/lambda*(1 - exp(lambda*(t^h)))) ...
                      .*(ones(size(galpha))*w),2);
        j = j+1;
      end
      L = size(Mt,2);
      part1 = Mt(:,1:L-1);
      part2 = Mt(:,2:L);
    else 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));
    end    

    q = (part1 - part2)./part1;
    q(part1==0) = 0;
    if (isfield(cfdata,'alphaBias') && cfdata.alphaBias ~= 1)
      mu = cfdata.alphaX(n,:)*est.parm.gAlpha;
      logAHat(n) = mu + cfdata.alphaBias*(logAlpha(n)-mu);
      alph = exp(logAHat(n));
      part22  = exp(alph/lambda*(1 - exp(lambda*(tp1.^h))));
      part12  = exp(alph/lambda*(1 - exp(lambda*(t.^h  ))));     
      qPerceived = (part12 - part22)./part12;
      qPerceived(part12==0) = 0;
    else
      logAHat(n)  = logAlpha(n);
      qPerceived = q;
    end

    fr     = (part1(1:T) - part2(1:T))';          % density  

    vp.rho = rho;
    vp.delta = delta;
    vp.T = T;
    vp.zeta = zeta;
    vp.gam = gam;
    vp.bgam = bgam;
    vp.pub = pub;
    vp.infl = infl;
    vp.frac = alpha2;

    pubrate = pubAnnuityRate(infl,rho,q);
    for c = 1:nchoice,        % guarantee choice
      Z = w0*(alpha2*eqZ(cfdata.group(n),c));
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cfdata.m.val(n,c) = vf3(w0,eqZ(cfdata.group(n),c),qPerceived, ...
                              cfdata.beta(n),guar(c),vp);
      wlo = w0*(1-alpha2);
      whi = wlo + sum(Z);
      % wealth equivalent
      cfdata.m.weq(n,c) = weq(cfdata.m.val(n,c),wlo,whi,qPerceived,cfdata.beta(n),vp);
      cumz = cumsum(Z.*rdis);
      % expected payment
      if guar(c)>0,
        cfdata.m.epay(n,c) = cumz(guar(c))*sum(fr(1:guar(c))) + ...
            sum(cumz(guar(c)+1:T).*fr(guar(c)+1:T));
      else
        cfdata.m.epay(n,c) = sum(cumz.*fr);
      end;    
    end;   % loop over choices
    cfdata.m.epay(n,:) = cfdata.m.epay(n,:)/sum(fr);
    if (mod(n,disp)==0)
      fprintf('"Equal" loadings: Done with %d of %d\n',n,cfdata.N)
    end;
  end; % loop over n
  warning('On','MATLAB:divideByZero');
  
end % function cfLoadFactors()

