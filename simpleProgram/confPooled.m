%% Returns a data structure containing configuration information
function cfg = config()
cfg.est.mortResults = '../results/pooledResults'; % only used for 2step objective
cfg.est.objective = 'mle'; % Objective function, options are mle, gmm, or
                           % mlmort (for only mortality estimation), 2step
cfg.nboot = 50;  % number of bootstrap replications
cfg.est.optmethod = 'fminunc';  % deprecated -- optimization is now down
                                % in ampl
% minimization options -- deprecated
cfg.est.opt = optimset('TolX',1.0e-8, ... % x tolerance
                       'TolFun',1.0e-8, ... % function tolerance
                       'MaxFunEval',1000, ... % max func evals
                       'GradObj','On', ... % whether analytic grads 
                       'DerivativeCheck','On', ... % wheteher to
                        ...                        % numerically check
                        ...                        % derivatives
                       'MaxIter',10000, ...
                       'Hessian','Off',...
                       'Display','On',...
                       'LargeScale','Off'); 
cfg.est.extraMoments = false; % deprecated -- include E(x|g), E(x|die) in objective
                              % function 
cfg.est.efficientGMM = false; % deprecated
cfg.est.nint = 24; % number of integration points
cfg.est.disp = 5; % deprecated -- display level, higher = more frequent information
% constraints for fmincon -- deprecated
cfg.est.min.gAlpha = [-6; -.1];
cfg.est.max.gAlpha = [-3; .1];
cfg.est.min.gBeta = [5; -.3];
cfg.est.max.gBeta = [15; .3];
cfg.est.min.var = [1.0e-4 -0.95e-4; -0.95e-4 1.0e-4];
cfg.est.max.var = [1 3; 3 10];
cfg.est.dist='normal';

cfg.outprefix = '../results/pooled'; % prefix for all output files

%% DATA STUFF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data file name 
cfg.data.name = '/proj/paul/annuities/data/newCompB2_1992.txt';
% data.l**** = location (column) in data file of variables
cfg.data.lcyear = 14; % year at which at = cage
cfg.data.lquarter = 13; % 
cfg.data.lexternal = 15; % location of external indicator
cfg.data.lcage = 4;  % see previous
cfg.data.lfage = 19;  % age first observed (jan 1, 1998)
cfg.data.ldage = 20;  % age at death
cfg.data.lmage = 21;  % age last observerd (Dec 31, 2005)
cfg.data.lmale = 6;  % location of male
cfg.data.ldyear = 8; % location of year of death
cfg.data.nodeathYear = 0; % dyear = this if did not die
cfg.data.lguar = 3;  % location of guarantee choice
cfg.data.lastyear = 2005; % last year of observation
cfg.data.firstyear= 1998; % first year of observation
cfg.data.deadOnly = false;
cfg.data.no10 = false;
cfg.data.okay = 12; % keep only people with okay=okeep (okay is a variable in
                    % this column of the data file)
cfg.data.okeep= 0; % see previous
cfg.data.lbetaX = [-1 ]; % x's in beta.  set to [] if no x's, -1 means
                        % group indicators
cfg.data.betaXName(2).s='External'; % names for coefficients used when writing tables
cfg.data.betaXName(3).s='New External';
%cfg.data.betaXName(4).s='DOB';
cfg.data.lalphaX = [-1]; % x's in alpha.  set to [] if no x's
cfg.data.alphaXName(2).s='External';
cfg.data.alphaXName(3).s='New External';
%cfg.data.alphaXName(4).s='DOB';
cfg.data.dropVal = -999; % drop observations with x = -999
cfg.data.standardize = true; % set to true if want to standardize x's
%cfg.data.skip=100; % skip this many
%cfg.data.obs = 10000; % only use this many observations
cfg.data.allDummies=true;
% group definitions
cfg.data.group(1).male = 0;
cfg.data.group(1).cage = 60;
cfg.data.group(1).name = '60 Female';
cfg.data.group(2).male = 1;
cfg.data.group(2).cage = 65;
cfg.data.group(2).name = '65 Male';
cfg.data.group(3).male = 0;
cfg.data.group(3).cage = 65;
cfg.data.group(3).name = '65 Female';
cfg.data.group(4).male = 1;
cfg.data.group(4).cage = 60;
cfg.data.group(4).name = '60 Male';

% additional data use only for mortality
%cfg.data.mortOnly.male = 0;
%cfg.data.mortOnly.cage = [1:59 61:100];
%cfg.data.mortOnly.cageDummies = false;

% the next three parameters will overide specs in cutoff section
cfg.data.lp = [9 10 11]; % payment rate locations
cfg.data.lrate = 16;       % real interest rate location
cfg.data.linfl = 17;       % inflation rate location

%% MORTALITY STUFF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg.mort.lambda = 0; % initial value of lambda 
cfg.mort.inc = 1; % increment of time observations
cfg.mort.baseage = 60; % base age ie P(live>=baseage)=1
cfg.mort.fasthazard = 1; % set to h if want log hazard a/l e^[(t-t0)^h],
                         % 1 is usual gompertz

%% CUTOFFS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg.cut.file = 'cutoffs/cut.mat'; % file with cutoffs; if the file
                                  % doesn't exist, then assumes you want
                                  % new cutoffs created
cfg.cut.w0 = 100; % wealth
cfg.cut.wp = 1;
cfg.cut.agemin = [60]; % first age
cfg.cut.agemax = 100; % last age
cfg.cut.fracAnnuitized = 0.2; % fraction of wealth annuitized
cfg.cut.gam = 3.0; % CRRA coefficient
cfg.cut.bgam = 3.0; % CRRA coefficient
cfg.cut.lambda = cfg.mort.lambda; % gompertz parameter
cfg.cut.inflation = .0462; % inflation rate (average 1900-1998) (if not
                           % read from data)
cfg.cut.rho = 1/1.05; % riskless interest rate (if not read from data)
cfg.cut.delta = 1/1.05; % utility discount rate
cfg.cut.deltaEQrho = true;
%cfg.cut.z = [0.1059, 0.1052, 0.0949]; % payment rates
cfg.cut.z(1,:) = [0.1029080, 0.1024765, 0.1003112]; % payment rates if
                                                    % not read from data
cfg.cut.la = (-10:0.1:0)'; % log alpha grid
cfg.cut.bmax = 50.0; % max log beta
cfg.cut.btol = 1.0e-4; % convergence tolerance
cfg.cut.fasthazard = cfg.mort.fasthazard;
