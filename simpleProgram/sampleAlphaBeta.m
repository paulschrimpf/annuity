function [logAlpha logBeta w0] = sampleAlphaBeta(data,est)

  if (~isfield(est,'w0') || length(est.w0)==1) && ~isfield(est.parm,'twodh') ...
        && (~isfield(est.parm,'frac') || numel(est.parm.frac)==1)    
    if (size(data.alphaX,2) ~= size(est.parm.gAlpha,1) )
      est.parm.gAlpha = est.parm.gAlpha';
    end
    if (size(data.betaX,2) ~= size(est.parm.gBeta,1) )
      est.parm.gBeta = est.parm.gBeta';
    end

    switch(est.dist)
     case {'normal','uni-normal-beta'}
      logAlpha = data.alphaX*est.parm.gAlpha + ... 
          randn(data.N,1)*sqrt(est.parm.var(1,1));
      condmu = data.betaX*est.parm.gBeta + est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (logAlpha-data.alphaX*est.parm.gAlpha); 
      condsig = sqrt(est.parm.var(2,2) - est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
                ones(data.N,1);             
      logBeta = condmu + randn(data.N,1).*condsig;
     case 'normal-flex'
      logAlpha = data.alphaX*est.parm.gAlpha + ... 
          randn(data.N,1)*sqrt(est.parm.var(1,1));
      dla =  (logAlpha - data.alphaX*est.parm.gAlpha) ...
             /sqrt(est.parm.var(1,1)); 
      condmu = data.betaX*est.parm.gBeta;
      for d = 2:size(est.parm.var,2)             
        condmu = condmu + est.parm.var(1,d)*dla.^(d-1);
      end
      condsig = sqrt(est.parm.var(2,2));
      logBeta = condmu + randn(data.N,1).*condsig;
     case 'gamma'
      alpha = gamrnd(est.parm.ashape,exp(data.alphaX*est.parm.gAlpha));
      logAlpha = log(alpha);
      condmu = data.betaX*est.parm.gBeta + ...
               est.parm.corr*(logAlpha-data.alphaX*est.parm.gAlpha);
      beta = gamrnd(est.parm.bshape,exp(condmu));
      logBeta = log(beta);     
     case 'gamma-normal'
      alpha = gamrnd(est.parm.ashape,exp(data.alphaX*est.parm.gAlpha));
      logAlpha = log(alpha);
      condmu = data.betaX*est.parm.gBeta + ...
               est.parm.corr*(logAlpha-data.alphaX*est.parm.gAlpha);
      logBeta = condmu + randn(data.N,1)*sqrt(est.parm.var(2,2));    
     case 'mixed-normal'
      cut = [0 cumsum([est.parm.p])];
      u = rand(data.N,1);
      logAlpha = ones(data.N,1)*-999;
      logBeta = logAlpha;
      for m=1:length(est.parm)
        % this is slightly inefficient, but doesn't need to be fast anyway
        la = data.alphaX*est.parm(m).gAlpha + ... 
             randn(data.N,1)*sqrt(est.parm(m).var(1,1));
        condmu = data.betaX*est.parm(m).gBeta + est.parm(m).var(1,2)/est.parm(m).var(1,1) * ... 
               (la-data.alphaX*est.parm(m).gAlpha); 
        condsig = sqrt(est.parm(m).var(2,2) - est.parm(m).var(1,2)^2/est.parm(m).var(1,1)) * ...
                  ones(data.N,1);             
        lb = condmu + randn(data.N,1).*condsig;
        ind = (u > cut(m)) & (u<=cut(m+1));
        logAlpha(ind) = la(ind);
        logBeta(ind) = lb(ind);
      end
      if(any(logAlpha)==-999)
        error('failed to assign some logAlpha');
      end
    end % switch  
  elseif isfield(est.parm,'twodh')
    cdf = [0 cumsum(est.parm.twodh.p)];
    u = rand(data.N,1);
    for w=1:length(est.parm.twodh.gamma);
      m = u<cdf(w+1) & u>=cdf(w);
      la = data.alphaX*est.parm.gAlpha(w,:)' + ... 
          randn(data.N,1)*sqrt(est.parm.var(1,1));
      condmu = data.betaX*est.parm.gBeta(w,:)' + est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (la-data.alphaX*est.parm.gAlpha(w,:)'); 
      condsig = sqrt(est.parm.var(2,2) - est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
                ones(data.N,1);             
      lb = condmu + randn(data.N,1).*condsig;
      logAlpha(m) = la(m);
      logBeta(m) = lb(m);
      w0(m) = est.parm.twodh.gamma(w);
    end
  elseif isfield(est.parm,'frac')
    cdf = [0 cumsum(est.wp)];
    u = rand(data.N,1);
    assert(numel(est.wp)==numel(est.parm.frac));
    for w=1:length(est.wp);
      m = u<cdf(w+1) & u>=cdf(w);
      la = data.alphaX*est.parm.gAlpha(w,:)' + ... 
          randn(data.N,1)*sqrt(est.parm.var(1,1));
      condmu = data.betaX*est.parm.gBeta(w,:)' + est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (la-data.alphaX*est.parm.gAlpha(w,:)'); 
      condsig = sqrt(est.parm.var(2,2) - est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
                ones(data.N,1);             
      lb = condmu + randn(data.N,1).*condsig;
      logAlpha(m) = la(m);
      logBeta(m) = lb(m);
      w0(m) = est.parm.frac(w);    
    end
  elseif isfield(est,'wp') 
    cdf = [0 cumsum(est.wp)];
    u = rand(data.N,1);
    for w=1:length(est.w0);
      m = u<cdf(w+1) & u>=cdf(w);
      la = data.alphaX*est.parm.gAlpha(w,:)' + ... 
          randn(data.N,1)*sqrt(est.parm.var(1,1));
      condmu = data.betaX*est.parm.gBeta(w,:)' + est.parm.var(1,2)/est.parm.var(1,1) * ... 
               (la-data.alphaX*est.parm.gAlpha(w,:)'); 
      condsig = sqrt(est.parm.var(2,2) - est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
                ones(data.N,1);             
      lb = condmu + randn(data.N,1).*condsig;
      logAlpha(m) = la(m);
      logBeta(m) = lb(m);
      w0(m) = est.w0(w);
    end
  end

end % function sampleAlphaBeta    
