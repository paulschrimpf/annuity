function [fval grad hess gi moments mg] = annuityGMM(parm,est,data)
  est.parm = transformParam(parm,'unpack','gmm',est.dist);

  if(isfield(est,'dumb') && est.dumb.p>0)
    dest = est;
    dest.dumb.p = 0;
    [fval grad hess gi moments mg] = annuityGMM(parm,dest,data);
    if(isfield(est.parm,'dumb'))
      if (isfield(est.parm.dumb,'gAlpha'))
        dest.parm.gAlpha = est.parm.dumb.gAlpha;
      end
      if (isfield(est.parm.dumb,'var'))
        dest.parm.var(1,1) = est.parm.dumb.var;
      end
      if (isfield(est.parm.dumb,'p'))
        est.dumb.p = est.parm.dumb.p;
      end
    end 
    dparm = transformParam(dest.parm,'pack','gmm',dest.dist);
    [dfval dgrad dhess dgi dmoments dmg] = annuityGMM(dparm,dest,data);
    fprintf(['WARNING: GMM will not return correct obj function value with' ...
             ' dumb\n']);
    moments
    dmoments
    moments(2,1:2) = moments(2,1:2)*(1-est.dumb.p);
    moments(2,2) = moments(2,2) + est.dumb.p;
    pg = [dmoments(2,1:2) 1-sum(dmoments(2,1:2))]
    moments(2,4) = moments(2,4)*(1-est.dumb.p) + sum(dmoments(2,3:5).*pg)* ...
       est.dumb.p;
    % restore state of transformParam
    parm = transformParam(est.parm,'pack','gmm',dest.dist);
    return
  end
  
  est.parm.var(1,1)
  switch (est.dist)
   case {'normal','normal-flex','uni-normal-beta'}
    logAlpha = ones(data.N,1)*est.int.x'*sqrt(est.parm.var(1,1)) + ...
        data.alphaX*est.parm.gAlpha*ones(1,est.int.n);
    alpha = exp(logAlpha);
    alpha(alpha>log(realmax)) = log(realmax);
    wp = normpdf(est.int.x,0,1).*est.int.w;
   case {'gamma','gamma-normal'}
    aScale = exp(data.alphaX*est.parm.gAlpha)*ones(1,est.int.n);
    alpha = (ones(data.N,1)*est.int.x').*aScale;
    logAlpha = log(alpha);
    wp = est.int.w.*est.int.x.^(est.parm.ashape-1) ... 
         / gamma(est.parm.ashape);
    fprintf('%.16g =?= %.16g\n',aScale(1,1)*est.parm.ashape, ...
            sum(wp.*est.int.x.*aScale(1,1)'));
   case 'mixed-normal'
    nmix = length(est.parm);
    fval = 0;
    for m=1:nmix
      em = est;
      em.dist = 'normal';
      em.parm = []; em.parm=est.parm(m);
      pm = transformParam(em.parm,'pack','mle','normal');
      [f grad hess gi mm mmg] = annuityGMM(pm,em,data);
      if (m==1)
        moments = mm*est.parm(m).p;
        mg = mmg*est.parm(m).p;
      else
        moments = moments + est.parm(m).p*mm;
        mg = mg + mmg*est.parm(m).p;
      end
    end
    return
  end        
  lambda = est.lambda;      

  persistent ef_em neval oPdg oPg oNd oMX;
  if(isempty(ef_em) || size(ef_em,2)~=est.int.n ...
     || size(ef_em,1)~=data.N)
    neval = 0;
    m = data.fage==data.maxAge;
    data.maxAge(m) = data.maxAge(m) + 0.25;
    if(isfield(est,'fasthazard'))
      h = est.fasthazard;
    else
      h = 1;
    end
    ef_em = exp(lambda*data.fage.^h) - exp(lambda*data.maxAge.^h); 
    ef_em = ef_em * ones(1,est.int.n);
    oPg(1) = mean(data.g==0);
    oPg(2) = mean(data.g==5);
    % P(d|g)
    oPdg(1) = mean(data.died(data.g==0));
    oPdg(2) = mean(data.died(data.g==5));
    oPdg(3) = mean(data.died(data.g==10));
    oNd(1) = sum(data.g==0);
    oNd(2) = sum(data.g==5);
    oNd(3) = sum(data.g==10);
    if(est.extraMoments)
      warning(['"extraMoments" option assumes that data.betaX = data.alphaX' ...
               ' = [const x]\n']);
      % E(X*g)
      oMX(1) = mean(data.alphaX(:,2).*(data.g==0));
      oMX(2) = mean(data.alphaX(:,2).*(data.g==5));
      % E(X*dead*g)
      oMX(3) = mean((data.g==0 & data.died==1).* ...
                    data.alphaX(:,2));
      oMX(4) = mean((data.g==5 & data.died==1).* ...
                    data.alphaX(:,2));
      oMX(5) = mean((data.g==10 & data.died==1).* ...
                    data.alphaX(:,2));
    end
    if (nargout>=6)
      mg = zeros([data.ngroup 2 5]);
      for g=1:data.ngroup
        mg(g,1,:) = [mean(data.g(data.group==g)==0) ...
                     mean(data.g(data.group==g)==5) ...
                     mean(data.died(data.group==g & data.g==0)) ...
                     mean(data.died(data.group==g & data.g==5)) ...
                     mean(data.died(data.group==g & data.g==10))];
      end
    end
  end % if isempty(ef_em)
  
  switch(est.dist)
   case {'normal','uni-normal-beta'}
    condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n,1) + ...
             est.parm.var(1,2)/est.parm.var(1,1) * ... 
             (logAlpha - ...
              data.alphaX*est.parm.gAlpha*ones(1,est.int.n)); 
    condsig = sqrt(est.parm.var(2,2) -  ...
                   est.parm.var(1,2)^2/est.parm.var(1,1)) * ...
              ones(data.N,est.int.n);          
   case 'normal-flex'
    dla =  (logAlpha - data.alphaX*est.parm.gAlpha*ones(1,est.int.n)) ...
           /sqrt(est.parm.var(1,1));  
    condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n);
    for d = 2:size(est.parm.var,2)             
      condmu = condmu + est.parm.var(1,d)*dla.^(d-1);
    end
    condsig = sqrt(est.parm.var(2,2));
   case {'gamma','gamma-normal'}
    condmu = data.betaX*est.parm.gBeta*ones(1,est.int.n) + ...
             est.parm.corr * (ones(data.N,1)*log(est.int.x'));
    condsig = sqrt(est.parm.var(2,2));
  end
  lb = est.cut(1).lb;
  t = whos('lb');
  if (strcmp(t.class,'cell'))
    for g=1:data.ngroup
      for w=1:length(est.cut(g).lb)
        m = data.group==g;
        e(m,:,1,w) = reshape(interp1(est.cut(g).la,est.cut(g).lb{w}(:,1), ...
                                   reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                   'linear','extrap'),sum(m),est.int.n); 
        e(m,:,2,w) = reshape(interp1(est.cut(g).la,est.cut(g).lb{w}(:,2), ...
                                   reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                   'linear','extrap'),sum(m),est.int.n); 
      end
    end
    e(isnan(e)) = Inf;
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
      pc = zeros(data.N,est.int.n,2);
      if (isempty(est.cut(1).wp))
        est.cut(1).wp = ones(size(est.cut(1).twodh.gamma))/ ...
            numel(est.cut(1).twodh.gamma);
      end
      for w=1:length(est.cut(1).lb)
        pc(:,:,1) =  pc(:,:,1) + ...
            normcdf(e(:,:,1),condmu,condsig)*est.cut(1).wp(w);
        pc(:,:,2) = pc(:,:,2) + ...
            normcdf(e(:,:,2),condmu,condsig)*est.cut(1).wp(w);
      end
     case 'gamma'
      pc = zeros(data.N,est.int.n,2);
      for w=1:length(est.cut(1).lb)
        pc(:,:,1) =  pc(:,:,1) + ...
            gamcdf(exp(e(:,:,1)),est.parm.bshape,exp(condmu))*est.cut(1).wp(w);
        pc(:,:,2) = pc(:,:,2) + ...
            gamcdf(exp(e(:,:,2)),est.parm.bshape,exp(condmu))*est.cut(1).wp(w);
      end
    end
  else
    for g=1:data.ngroup
      m = data.group==g;
      e(m,:,1) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,1), ...
                                 reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                 'linear','extrap'),sum(m),est.int.n); 
      e(m,:,2) = reshape(interp1(est.cut(g).la,est.cut(g).lb(:,2), ...
                                 reshape(logAlpha(m,:),sum(m)*est.int.n,1), ...
                                 'linear','extrap'),sum(m),est.int.n); 
      e(isnan(e)) = Inf;
    end
    switch(est.dist)
     case {'normal','gamma-normal','normal-flex','uni-normal-beta'}
      pc(:,:,1) = normcdf(e(:,:,1),condmu,condsig);
      pc(:,:,2) = normcdf(e(:,:,2),condmu,condsig);
     case 'gamma'
      pc(:,:,1) = gamcdf(exp(e(:,:,1)),est.parm.bshape,exp(condmu));
      pc(:,:,2) = gamcdf(exp(e(:,:,2)),est.parm.bshape,exp(condmu));
    end
  end
  wp = wp/sum(wp);
  % Prob(g|a)
  Pga(:,:,1) = pc(:,:,1).*(ones(data.N,1)*wp');
  Pga(:,:,2) = (pc(:,:,2)-pc(:,:,1)).*(ones(data.N,1)*wp');
  Pga(:,:,3) = (1-pc(:,:,2)).*(ones(data.N,1)*wp');
  
  % Prob(g)
  Pgi = sum(Pga,2); % size N x 3
  % Prob(d|a)
  if (isfield(est.cut(1),'alphaUncertainty') && est.cut(1).alphaUncertainty ...
      ~= 0)
    % m changes because of alpha uncertainty
    % psit=  int(exp(exp(e)*lambda(t)) *phi(e) de 
    % compute by gauss-hermite quadrature
    n = est.int.n; 
    [n x w] = gqzero(n);
    x = x*(sqrt(2)*est.cut(1).alphaUncertainty);
    w = w/sqrt(pi);
    if (size(x,2)==1) %  change to row vectors
      x = x';
      w = w';
    end
    j = 1;
    Pda = zeros(size(ef_em));
    for j=1:n
      Pda = Pda + (1 - exp(alpha*exp(x(j))/lambda.*ef_em))*w(j);
    end
  else % alphaUncertainty
    Pda = 1 - exp(alpha/lambda.*ef_em); % size N x int.n
  end % alphaUncertainty
  % Prob(d&g)
  % FIXME: should there be m's here?  I think not.  Maybe that's why it ...
  %     seemed identified.
  %m = data.g==0;
  %Pdg(1) = mean(sum( Pda(m,:).*Pga(m,:,1),2 )./Pgi(m,1));
  %m = data.g==5;
  %Pdg(2) = mean(sum( Pda(m,:).*Pga(m,:,2),2 )./Pgi(m,2));
  %m = data.g==10;
  %Pdg(3) = mean(sum( Pda(m,:).*Pga(m,:,3),2 )./Pgi(m,3));
  %Pg = reshape(mean(Pgi,1),1,3); % size 1 x 3
  Pgi(Pgi==0) = realmin;
  Pdg(1) = mean(sum( Pda.*Pga(:,:,1),2 )./Pgi(:,1));
  Pdg(2) = mean(sum( Pda.*Pga(:,:,2),2 )./Pgi(:,2));
  Pdg(3) = mean(sum( Pda.*Pga(:,:,3),2 )./Pgi(:,3));
  
  Pg = reshape(mean(Pgi,1),1,3); % size 1 x 3
  
  if(nargout>=6)
    for g=1:data.ngroup
      m = data.group==g;
      mg(g,2,:) = [mean(Pgi(m,1:2),1) ...
                   mean(sum( Pda(m,:).*Pga(m,:,1),2 )./Pgi(m,1)) ...
                   mean(sum( Pda(m,:).*Pga(m,:,2),2 )./Pgi(m,2)) ...
                   mean(sum( Pda(m,:).*Pga(m,:,3),2 )./Pgi(m,3))];
    end
  end
  
  fval = sum((Pg(1:2)-oPg).^2) + sum((oPdg-Pdg).^2);

  if (est.extraMoments)
    % E(X*g)
    EX(1:2) = mean(squeeze(Pgi(:,:,1:2)).*(data.alphaX(:,2)*ones(1,2)));
    % E(x*g*dead)
    EX(3) = mean( sum(Pda.*Pga(:,:,1),2).*data.alphaX(:,2) );
    EX(4) = mean( sum(Pda.*Pga(:,:,2),2).*data.alphaX(:,2) );
    EX(5) = mean( sum(Pda.*Pga(:,:,3),2).*data.alphaX(:,2) );
    
    fval = fval + sum((EX-oMX).^2);
  else
    EX = [];
  end
  
  neval = neval + 1;
  moments = [oPg oPdg oMX; Pg(1:2) Pdg EX];
  if(mod(neval,est.disp)==0)
    fprintf('observed estimated\n')
    for(i=1:size(moments,2))
      fprintf('%8.4g %8.4g\n',moments(1,i),moments(2,i));
    end;
    
    fprintf('f = %20.16g\n',fval);
    fprintf('parameter values:\n');
    fprintf(' mean log alpha = %8.6g\n',est.parm.gAlpha);
    fprintf(' mean log beta  = %8.6g\n',est.parm.gBeta);
    fprintf(' var log alpha  = %8.6g\n',est.parm.var(1,1));
    fprintf(' var log beta   = %8.6g\n',est.parm.var(2,2));
    fprintf(' cov log a,b    = %8.6g\n',est.parm.var(1,2));
    fprintf(' corr log a,b   = %8.6g\n',est.parm.corr);
  end

  grad = [];
  hess = [];
  gi = [];
  
end % end function annuityGMM()
