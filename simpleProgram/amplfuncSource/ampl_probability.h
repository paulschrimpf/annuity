#ifndef AMPL_PROBABILITY_H
#define AMPL_PROBABILITY_H

#include "funcadd.h"

double ampl_normcdf(arglist *al);
double ampl_normpdf(arglist *al);
double ampl_gampdf(arglist *al);
double ampl_gamcdf(arglist *al);

#endif
