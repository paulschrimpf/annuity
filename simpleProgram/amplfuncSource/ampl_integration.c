#include <math.h>/* for sqrt */
#include <assert.h>
#include <stdlib.h>
#include "funcadd.h"/* includes "stdio1.h" */

#include "integration.h"

double ampl_gaucheb(arglist *al)
{ /* ampl interface to gaucheb */
  /* 3 inputs: 1st = integration point to return 
               2nd = 0 for x, 1 for w
               3rd = # of points for rule */
  static int nCurrent = 0; /* current # of points stored */
  static VECTOR x,w; /* points and weights */
  int n,i;
  AmplExports *ae = al->AE; /* to allow error msg printing */
  if (al->derivs || al->hes) {
    printf("ERROR: ampl_gaucheb()\n"
           "  derivative and hessian not available\n");
    exit(-1);
  }
  n = (int) al->ra[2];
  if (n!=nCurrent) { /* need to recompute points and weights */
    if(nCurrent>0) {
      free_vector(&x); 
      free_vector(&w);
    }
    nCurrent = n;
    vector(&x,nCurrent);
    vector(&w,nCurrent);
    gaucheb(x,w);
    for (i=1;i<=nCurrent;i++) { 
      /* make it go from -inf to +inf */
      w.a[i] *= 2.0/sqrt(1.0-(x.a[i]*x.a[i]));
      x.a[i] = log(1.0-x.a[i]) - log(1.0+x.a[i]);
    }
  }
  i = (int) al->ra[0];
  if (i<1 || i>n) {
    printf("ERROR: ampl_gaucheb()\n"
           "  i=%d out of range\n",i);
    exit(-1);
  }
  if ((int) al->ra[1]) return w.a[i];
  else return x.a[i];
}

double ampl_gaulag(arglist *al)
{ /* ampl interface to gaulag */
  /* 3 inputs: 1st = integration point to return 
               2nd = 0 for x, 1 for w
               n = # of points for rule */
  static int nCurrent = 0; /* current # of points stored */
  static VECTOR x,w; /* points and weights */
  int n,i;
  AmplExports *ae = al->AE; /* to allow error msg printing */
  if (al->derivs || al->hes) {
    printf("ERROR: ampl_gaulag()\n"
	   "  derivative and hessian not available\n");
    exit(-1);
  }
  n = (int) al->ra[2];
  if (n!=nCurrent) { /* need to recompute points and weights */
    if(nCurrent>0) {
      free_vector(&x); 
      free_vector(&w);
    }
    nCurrent = n;
    vector(&x,nCurrent);
    vector(&w,nCurrent);
    gaulag(x,w,0);
    for(i=1;i<=nCurrent;i++) {
      w.a[i] *= exp(x.a[i]); /* so it works for int f(x) dx instead of 
				int f(x) e^{-x} dx */
    }
  }
  i = (int) al->ra[0];
  if (i<1 || i>n) {
    printf("ERROR: ampl_gaulag()\n"
	   "  i=%d out of range\n",i);
    exit(-1);
  }
  if ((int) al->ra[1]) return w.a[i];
  else return x.a[i];
}
