#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "funcadd.h"/* includes "stdio1.h" */

#include "probability.h"
#ifndef __linux__
# define __linux__ /* trick fortran.h */
#endif
#include "fortran.h" /* to allow calling of fortran digami function */

/* use gsl library for psi (aka polygamma functions) */
#include <gsl/gsl_sf_psi.h>
#include <gsl/gsl_sf_gamma.h>

double ampl_normcdf(arglist *al) 
{ /* ampl interface to normcdf */
  double x = al->ra[0];
  double cdf;
  cdf = normcdf(x,0,1);
  if (al->derivs || al->hes) al->derivs[0] = normpdf(x,0,1);
  if (al->hes) al->hes[0] = -al->derivs[0]*x;
  return(cdf);
}

double ampl_normpdf(arglist *al) 
{ /* ampl interface to normcdf */
  double x = al->ra[0];
  double pdf;
  pdf = normpdf(x,0,1);
  if (al->derivs || al->hes) al->derivs[0] = -pdf*x;
  if (al->hes) al->hes[0] = -pdf + al->derivs[0]*x;
  return(pdf);
}

double ampl_gampdf(arglist *al)
{ 
  /* Returns the pdf of a gamma distribution evaluated at x. a is
 shape parameter and b inverse scale.  such that mean=a/b and
 var=a/(b*b). That is 
   P(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b)
 for x>0. */
  AmplExports *ae = al->AE; /* to allow error msg printing */

  double x,a,b; /* inputs */
  double pdf; /* output */

  x = al->ra[0];
  a = al->ra[1];
  b = al->ra[2];
  pdf = gampdf(x,a,b);
  if (al->derivs || al->hes) {
    double psia,am1x,lxlb,abmx; /* intermediate variables */
    am1x = (a-1)/x-b;
    al->derivs[0] = am1x*pdf;
    psia = gsl_sf_psi(a);
    lxlb = log(x)+log(b);
    al->derivs[1] = pdf*(log(x)+log(b)-psia);
    abmx = a/b-x;
    al->derivs[2] = abmx*pdf;
    if (al->hes) { /* i,j element of hessian goes into hes[i+j(j+1)/2] */
      /* dx^2 i=j=0 */
      al->hes[0] = am1x*al->derivs[0] - (a-1)*pdf/(x*x);
      /* dxda i=0,j=1*/
      al->hes[1] = am1x*al->derivs[1]+pdf/x;
      /* dxdb i=0,j=2 */
      al->hes[3] = am1x*al->derivs[2]-pdf;
      /* da^2 i=j=1 */
      al->hes[2] = al->derivs[1]*(lxlb-psia) - gsl_sf_psi_1(a)*pdf;
      /* dadb i=1,j=2 */
      al->hes[4] = abmx*al->derivs[1]+pdf/b;
      /* db^2 i=j=2 */
      al->hes[5] = abmx*al->derivs[2]-a*pdf/(b*b);
    }
  }
  return(pdf);
}

double ampl_gamcdf(arglist *al)
{  
  /* Returns the cdf of a gamma distribution evaluated at x. a is
 shape parameter and b inverse scale.  such that mean=a/b and
 var=a/(b*b). That is 
   pdf(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b)
 for x>0, so
   cdf(x) = incompleteGamma(a,x*b)
 where we define incompleteGamma(a,x) as
   1/Gamma(a) int_0^x e^{-t} t^(a-1) dt

   Note that the derivatives (esp wrt a) are highly nonstandard.  The
  fortran routin DIGAMI (AS 187 at http://lib.stat.cmu.edu/apstat/187)
  is used.
  */
  AmplExports *ae = al->AE; /* to allow error msg printing */

  void FORTRAN_NAME(digami)(double *d,const double *x,const double *p,
			    const double *gplog, const double *gp1log,
			    const double *psip, const double *psip1,
			    const double *psidp,const double *psidp1, 
			    const double *ifault);
  /* output d, size 6: d[0] = d igam(x,p)/dx
                       d[1] = d2igam(x,p)/dx2
                       d[2] = d igam(x,p)/dp
                       d[3] = d2igam(x,p)/dp2
                       d[4] = d2igam(x,p)/dxdp
                       d[5] = igam(x,p) 
            ifault = 1 if convergence failed
    inputs : x,p
            gplog = log(gamma(p))
	    gp1log = log(gamma(p+1)) = log(p) + log(gamma(p)
            psip = psi(p)
	    psip1 = psi(p+1) = psi(p) + 1/p
            psidp = trigamma(p) = psi'(p)
	    psidp1 = trigamma(p+1) = trigamma(p) + p^-2
  */

  double x,a,b; /* inputs */
  double cdf; /* output */
  /* arguments for fortran routine */
  double d[6];
  double xb;
  double gplog,gp1log,psip,psip1,psidp,psidp1;
  double ifault;

  x = al->ra[0];
  a = al->ra[1];
  b = al->ra[2];

  xb = x*b;
  gplog = gsl_sf_lngamma(a);
  gp1log = gplog+log(a);
  psip = gsl_sf_psi(a);
  psip1 = psip+1/a;
  psidp = gsl_sf_psi_1(a);
  psidp1 = psidp + 1/(a*a);
  /* call fortran routine */
  FORTRAN_NAME(digami)(d, &xb, &a, &gplog, &gp1log, &psip, 
		       &psip1, &psidp, &psidp1,&ifault);
  if(ifault) printf("WARNING: ampl_gamcdf()\n"
	 "  error code returned by DIGAMI, result may be inaccurate\n");
  cdf = d[5];
  if (al->derivs) {
    /* wrt x */
    al->derivs[0] = d[0]*b; 
    /* wrt a */
    al->derivs[1] = d[2];
    /* wrt b */
    al->derivs[2] = d[0]*x; 
  }
  if (al->hes) { /* i,j element of hessian goes into hes[i+j(j+1)/2] */
    /* dx^2 i=j=0 */
    al->hes[0] = b*b*d[1];
    /* dxda i=0,j=1*/
    al->hes[1] = b*d[4];
    /* dxdb i=0,j=2 */
    al->hes[3] = d[0]+x*b*d[1];
    /* da^2 i=j=1 */
    al->hes[2] = d[3];
    /* dadb i=1,j=2 */
    al->hes[4] = x*d[4];
    /* db^2 i=j=2 */
    al->hes[5] = x*x*d[1];
  }
  return(cdf);
}


