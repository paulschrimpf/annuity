#include <stdlib.h>/* for atoi */
#include <math.h>/* for sqrt */
#include <assert.h>
#include "funcadd.h"/* includes "stdio1.h" */

#include "types.h"
#include "probability.h"
#include "ampl_probability.h"
#include "ampl_integration.h"

int main() {
  arglist al;
  AmplExports *ae = al.AE; /* to allow error msg printing */
  double ra[10],derivs[3],hes[6];
  double d[3],h[6],x[3],dh[3];
  double y;
  double dx = 1e-4;
  double yh,yl;
  int i,j;
  
  al.ra = ra;
  al.derivs=derivs;
  
  al.n = 1;
  al.ra[0] = 10; // x
  al.ra[1] = 50.; // a
  al.ra[2] = 1/exp(10); // binv
  al.hes = hes;

  y=ampl_gampdf(&al);
  for(i=0;i<3;i++) x[i] = al.ra[i];
  y = ampl_gampdf(&al);
  al.ra = x;
  for(i=0;i<3;i++) {
    x[i] = ra[i]*(1+dx);
    yh = ampl_gampdf(&al);
    for(j=0;j<3;j++) dh[j] = al.derivs[j];
    x[i] = ra[i]*(1-dx);
    yl = ampl_gampdf(&al);
    d[i] = (yh-yl)/(2*dx);
    x[i] = ra[i];
    for(j=i;j<3;j++) {
      h[i+j*(j+1)/2] = (dh[j]-al.derivs[j])/(2*dx*x[i]);
    }
  }

  al.hes = hes;
  y=ampl_gamcdf(&al);
  
  for(i=0;i<3;i++) x[i] = al.ra[i];
  y = ampl_gamcdf(&al);
  al.ra = x;
  for(i=0;i<3;i++) {
    x[i] = ra[i]*(1+dx);
    yh = ampl_gamcdf(&al);
    for(j=0;j<3;j++) dh[j] = al.derivs[j];
    x[i] = ra[i]*(1-dx);
    yl = ampl_gamcdf(&al);
    d[i] = (yh-yl)/(2*dx*x[i]);
    x[i] = ra[i];
    for(j=i;j<3;j++) {
      h[i+j*(j+1)/2] = (dh[j]-al.derivs[j])/(2*dx*x[i]);
    }
  }
  y = ampl_gamcdf(&al);
  yl=yh;
    
  return(0);
}
