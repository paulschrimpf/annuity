#ifndef AMPL_INTEGRATION_H
#define AMPL_INTEGRATION_H

#include "funcadd.h"

double ampl_gaucheb(arglist *al);
double ampl_gaulag(arglist *al);

#endif

