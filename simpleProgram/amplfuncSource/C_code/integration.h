#ifndef INTEGRATION_H
#define INTEGRATION_H
#include "types.h"

double qsimp(double (*func)(double,int),double a,double b,double eps,int i);
double qromb(double (*func)(double,int),double a, double b,double eps,int i);
double trapzd(double (*func)(double,int),double a,double b,int n,int i);
double qgaus16(double (*func)(double,int),double a,double b,int i);
double qgaus8(double (*func)(double,int),double a,double b,int i);
double adaptive_quad_16_8(double (*func)(double,int),double a,double b,double eps,int i);
void gauleg(VECTOR x,VECTOR w,double x1,double x2);
void gaulag(VECTOR x,VECTOR w,double alf);
void gauher(VECTOR x,VECTOR w);
void gaujac(VECTOR x,VECTOR w,double alf,double bet);
void gaucheb(VECTOR x, VECTOR w);

#endif /*INTEGRATION_H*/
