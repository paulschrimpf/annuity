#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "types.h"
#define NR_END 1
#define FREE_ARG char*

void nrerror(char error_text[])
     /* Numerical Recipes standard error handler */
{
  fprintf(stderr,"Numerical Recipes run-time error...\n");
  fprintf(stderr,"%s\n",error_text);
  fprintf(stderr,"...now exiting to system...\n");
  exit(1);
}

void vector(VECTOR *vec, int n)
     /* allocate a double vector with subscript range v[1..n] */
{
  vec->a=(double *)malloc((size_t) ((n+NR_END)*sizeof(double)));
  if (!vec->a) nrerror("allocation failure in vector()");
  vec->d1=n;
  return;
}

void ivector(IVECTOR *vec, int n)
     /* allocate a double vector with subscript range v[1..n] */
{
  vec->a=(int *)malloc((size_t) ((n+NR_END)*sizeof(int)));
  if (!vec->a) nrerror("allocation failure in vector()");
  vec->d1=n;
  return;
}

/* VECTOR *get_vec_mat(MATRIX *mat, int d1, int d2)  */
/*      // if d1==0 put mat(:,d2) into vector , if d2==0, put mat(d1,:) into vector */
/* { */
/*   VECTOR *out; */
/*   int i; */
/*   out=(VECTOR *)malloc(sizeof(VECTOR)); */
/*   if (d1==0) { */
/*     vector(out,mat->d1); */
/*     for(i=1;i<=mat->d1;i++) */
/*       out->a[i]=mat->a[i][d2]; */
/*   } */
/*   else if (d2==0) { */
/*    vector(out,mat->d2); */
/*    for(i=1;i<=mat->d2;i++) */
/*      out->a[i]=mat->a[d1][i]; */
/*   } */
/*   else {nrerror("no 0 in get");} */
/*   out->f=1; */
/*   return(out); */
/* } */

/* VECTOR *get_vec_ten(TENSOR *arr, int d1, int d2, int d3)  */
/* { */
/*   VECTOR *out; */
/*   int i; */
/*   out=(VECTOR *)malloc(sizeof(VECTOR)); */
/*   if (d1==0) { */
/*     vector(out,arr->d1); */
/*     for(i=1;i<=arr->d1;i++) */
/*       out->a[i]=arr->a[i][d2][d3]; */
/*   } */
/*   else if (d2==0) { */
/*     vector(out,arr->d2); */
/*     for(i=1;i<=arr->d2;i++) */
/*       out->a[i]=arr->a[d1][i][d3]; */
/*   } */
/*   else if (d3==0) { */
/*     vector(out,arr->d3); */
/*     for(i=1;i<=arr->d3;i++) */
/*       out->a[i]=arr->a[d1][d2][i]; */
/*   } */
/*   else {nrerror("no 0 in get");}    */
/*   out->f=1; */
/*   return(out); */
/* } */

/* VECTOR *get_vec_ten4(TENSOR4 *arr, int d1, int d2, int d3, int d4)  */
/* { */
/*   VECTOR *out; */
/*   int i; */
/*   out=(VECTOR *)malloc(sizeof(VECTOR)); */
/*   if (d1==0) { */
/*     vector(out,arr->d1); */
/*     for(i=1;i<=arr->d1;i++) */
/*       out->a[i]=arr->a[i][d2][d3][d4]; */
/*   } */
/*   else if (d2==0) { */
/*     vector(out,arr->d2); */
/*     for(i=1;i<=arr->d2;i++) */
/*       out->a[i]=arr->a[d1][i][d3][d4]; */
/*   } */
/*   else if (d3==0) { */
/*     vector(out,arr->d3); */
/*     for(i=1;i<=arr->d3;i++) */
/*       out->a[i]=arr->a[d1][d2][i][d4]; */
/*   } */
/*   else if (d4==0) { */
/*     vector(out,arr->d4); */
/*     for(i=1;i<=arr->d4;i++) */
/*       out->a[i]=arr->a[d1][d2][d3][i]; */
/*   } */
/*   else {nrerror("no 0 in get");}    */
/*   out->f=1; */
/*   return(out); */
/* } */

/* VECTOR *get_vec_ten5(TENSOR5 *arr, int d1, int d2, int d3, int d4, int d5)  */
/* { */
/*   VECTOR *out; */
/*   int i; */
/*   out=(VECTOR *)malloc(sizeof(VECTOR)); */
/*   if (d1==0) { */
/*     vector(out,arr->d1); */
/*     for(i=1;i<=arr->d1;i++) */
/*       out->a[i]=arr->a[i][d2][d3][d4][d5]; */
/*   } */
/*   else if (d2==0) { */
/*     vector(out,arr->d2); */
/*     for(i=1;i<=arr->d2;i++) */
/*       out->a[i]=arr->a[d1][i][d3][d4][d5]; */
/*   } */
/*   else if (d3==0) { */
/*     vector(out,arr->d3); */
/*     for(i=1;i<=arr->d3;i++) */
/*       out->a[i]=arr->a[d1][d2][i][d4][d5]; */
/*   } */
/*   else if (d4==0) { */
/*     vector(out,arr->d4); */
/*     for(i=1;i<=arr->d4;i++) */
/*       out->a[i]=arr->a[d1][d2][d3][i][d5]; */
/*   } */
/*   else if (d5==0) { */
/*     vector(out,arr->d5); */
/*     for(i=1;i<=arr->d5;i++) */
/*       out->a[i]=arr->a[d1][d2][d3][d4][i]; */
/*   } */
/*   else {nrerror("no 0 in get");}    */
/*   out->f=1; */
/*   return(out); */
/* } */


void matrix(MATRIX *mat, int nr, int nc)
     /* allocate a double matrix with subscript range m[1..nr][1..nc] */
{
  int i;  
  /* allocate pointers to rows */
  mat->a=(double **) malloc((size_t)((nr+NR_END)*sizeof(double*)));
  if (!mat->a) nrerror("allocation failure 1 in matrix()");
  /* allocate rows and set pointers to them */
  mat->a[1]=(double *) malloc((size_t)((nr*nc+NR_END)*sizeof(double)));
  if (!mat->a[1]) nrerror("allocation failure 2 in matrix()");
  for(i=2;i<=nr;i++) mat->a[i]=mat->a[i-1]+nc;
  mat->d1=nr;
  mat->d2=nc;
  /* return pointer to array of pointers to rows */
  return;
}

void imatrix(IMATRIX *mat, int nr, int nc)
     /* allocate a double matrix with subscript range m[1..nr][1..nc] */
{
  int i;
  /* allocate pointers to rows */
  mat->a=(int **) malloc((size_t)((nr+NR_END)*sizeof(int*)));
  if (!mat->a) nrerror("allocation failure 1 in matrix()");
  /* allocate rows and set pointers to them */
  mat->a[1]=(int *) malloc((size_t)((nr*nc+NR_END)*sizeof(int)));
  if (!mat->a[1]) nrerror("allocation failure 2 in matrix()");
  for(i=2;i<=nr;i++) mat->a[i]=mat->a[i-1]+nc;
  mat->d1=nr;
  mat->d2=nc;
  /* return pointer to array of pointers to rows */
  return;
}

void tensor(TENSOR *ten, int nr, int nc, int nb)
     /* allocate a float 3tensor with range t[nrl..nrh][ncl..nch][ndl..ndh] */
{
  long i,j;
  /* allocate pointers to pointers to rows */
  ten->a=(double ***) malloc((size_t)((nr+NR_END)*sizeof(double**)));
  if (!ten->a) nrerror("allocation failure 1 in f3tensor()");
  /* allocate pointers to rows and set pointers to them */
  ten->a[1]=(double **) malloc((size_t)((nr*nc+NR_END)*sizeof(double*)));
  if (!ten->a[1]) nrerror("allocation failure 2 in f3tensor()");
  /* allocate rows and set pointers to them */
  ten->a[1][1]=(double *) malloc((size_t)((nr*nc*nb+NR_END)*sizeof(double)));
  if (!ten->a[1][1]) nrerror("allocation failure 3 in f3tensor()");
  for(j=2;j<=nc;j++) ten->a[1][j]=ten->a[1][j-1]+nb;
  for(i=2;i<=nr;i++) {
    ten->a[i]=ten->a[i-1]+nc;
    ten->a[i][1]=ten->a[i-1][1]+nc*nb;
    for(j=2;j<=nc;j++) ten->a[i][j]=ten->a[i][j-1]+nb;
  }
  ten->d1=nr;
  ten->d2=nc;
  ten->d3=nb;
  return;
}

void free_ivector(IVECTOR *vec)
     /* free an int vector allocated with ivector() */
{
  free((FREE_ARG) (vec->a));
  vec->a=NULL;
  vec->d1=0;
}

void free_vector(VECTOR *vec)
     /* free an int vector allocated with ivector() */
{
  free((FREE_ARG) (vec->a));
  vec->a=NULL;
  vec->d1=0;
}

void free_imatrix(IMATRIX *mat)
     /* free a int matrix allocated by matrix() */
{
  free((FREE_ARG) (mat->a[1]));
  free((FREE_ARG) (mat->a));
  mat->a=NULL;
  mat->d1=0;
  mat->d2=0;
}

void free_matrix(MATRIX *mat)
     /* free a int matrix allocated by matrix() */
{
  free((FREE_ARG) (mat->a[1]));
  free((FREE_ARG) (mat->a));
  mat->a=NULL;
  mat->d1=0;
  mat->d2=0;
}

void free_tensor(TENSOR *ten)
     /* free a tensor allocated by tensor() */
{
  free((FREE_ARG) (ten->a[1][1]));
  free((FREE_ARG) (ten->a[1]));
  free((FREE_ARG) (ten->a));
  ten->a=NULL;
  ten->d1=0;
  ten->d2=0;
  ten->d3=0;
}

void tensor4(TENSOR4 *arr, int dim1, int dim2, int dim3, int dim4)
     // allocate a double 4 dimensional array 
{
  long i,j,k;
  arr->a=(double ****) malloc((size_t)((dim1+NR_END)*sizeof(double***)));
  if(!arr->a) nrerror("allocation failure 1 in array4");
  for(k=1;k<=dim1;k++) {
    arr->a[k]=(double ***) malloc((size_t)((dim2+NR_END)*sizeof(double**)));
    if(!arr->a[k]) nrerror("allocation failure 2 in array4");
    /* allocate pointers to last dim and set pointers to them */
    arr->a[k][1]=(double **) malloc((size_t)((dim2*dim3+NR_END)*sizeof(double*)));
    if (!arr->a[k][1]) nrerror("allocation failure 3 in array4");
    /* allocate last dim and set pointers to them */
    arr->a[k][1][1]=(double *) malloc((size_t)((dim2*dim3*dim4+NR_END)*sizeof(double)));
    if (!arr->a[k][1][1]) nrerror("allocation failure 4 in array4");
    for(j=2;j<=dim3;j++) arr->a[k][1][j]=arr->a[k][1][j-1]+dim4;
    for(i=2;i<=dim2;i++) {
      arr->a[k][i]=arr->a[k][i-1]+dim3;
      arr->a[k][i][1]=arr->a[k][i-1][1]+dim3*dim4;
      for(j=2;j<=dim3;j++) arr->a[k][i][j]=arr->a[k][i][j-1]+dim4;
    }
  }
  arr->d1=dim1;
  arr->d2=dim2;
  arr->d3=dim3;
  arr->d4=dim4;
  return;
}

void free_tensor4(TENSOR4 *arr)
{
  int k;
  for(k=1;k<=arr->d1;k++) {
    free((FREE_ARG) (arr->a[k][1][1]));
    free((FREE_ARG) (arr->a[k][1]));
    free((FREE_ARG) (arr->a[k]));
  }
  free((FREE_ARG) arr->a);
  arr->a=NULL;
  arr->d1=arr->d2=arr->d3=arr->d4=0;
  return;
}

void tensor5(TENSOR5 *arr, int dim1, int dim2, int dim3, int dim4, int dim5)
     // allocate a 5 dimensional array 
{
  long i,j,k,n;
  arr->a=(double *****) malloc((size_t)((dim1+NR_END)*sizeof(double****)));
  if(!arr->a) nrerror("allocation failure 1 in array5");
  for(n=1;n<=dim1;n++) {
      arr->a[n]=(double ****) malloc((size_t)((dim2+NR_END)*sizeof(double***)));
      if(!arr->a[n]) nrerror("allocation failure 2 in array5");
      for(k=1;k<=dim2;k++) {
	arr->a[n][k]=(double ***) malloc((size_t)((dim3+NR_END)*sizeof(double**)));
	if(!arr->a[n][k]) nrerror("allocation failure 2 in array4");
	/* allocate pointers to last dim and set pointers to them */
	arr->a[n][k][1]=(double **) malloc((size_t)((dim3*dim4+NR_END)*sizeof(double*)));
	if (!arr->a[n][k][1]) nrerror("allocation failure 3 in array4");
	/* allocate last dim and set pointers to them */
	arr->a[n][k][1][1]=(double *) malloc((size_t)((dim3*dim4*dim5+NR_END)*sizeof(double)));
	if (!arr->a[n][k][1][1]) nrerror("allocation failure 4 in array4");
	for(j=2;j<=dim4;j++) arr->a[n][k][1][j]=arr->a[n][k][1][j-1]+dim4;
	for(i=2;i<=dim3;i++) {
	  arr->a[n][k][i]=arr->a[n][k][i-1]+dim4;
	  arr->a[n][k][i][1]=arr->a[n][k][i-1][1]+dim4*dim5;
	  for(j=2;j<=dim4;j++) arr->a[n][k][i][j]=arr->a[n][k][i][j-1]+dim5;
	}
      }
  }
  arr->d1=dim1;
  arr->d2=dim2;
  arr->d3=dim3;
  arr->d4=dim4;
  arr->d5=dim5;
  return;
}

void free_tensor5(TENSOR5 *arr)
{
  int k,n;
  for(n=1;n<=arr->d1;n++) {
    for(k=1;k<=arr->d1;k++) {
      free((FREE_ARG) (arr->a[n][k][1][1]));
      free((FREE_ARG) (arr->a[n][k][1]));
      free((FREE_ARG) (arr->a[n][k]));
    }
    free((FREE_ARG) (arr->a[n]));
  }
  free((FREE_ARG) arr->a);
  arr->a=NULL;
  arr->d1=arr->d2=arr->d3=arr->d4=arr->d4=0;
  return;
}
