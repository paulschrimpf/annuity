#ifndef PROBABILITY_H
#define PROBABILITY_H
#include "types.h"

////////////////////////////////PROBABILITY/DENSITY FUNCTIONS ///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double normpdf(const double x,const double mu,const double varin);
double gampdf(const double x,const double a,const double b);
// Returns the pdf of a gamma distribution evaluated at x. a is shape parameter and b inverse scale. 
// such that mean=a/b and var=a/(b*b). That is P(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0.
double exppdf(const double x,const double a);
// PDF OF EXPONENTIAL DISTRIBUTION P(x|a) = (1/a)*exp(-x/a)
double chi2pdf(const double x,const double df);
// chi squared
double mixnormpdf(const double x,const VECTOR mu,const VECTOR var,const VECTOR p);
// mixtures of normals
double mixnormpdf_matrix(const double x,const MATRIX mu,const MATRIX var,const MATRIX p,const int r, const int c);
double mixnormpdf_tensor(const double x,const TENSOR mu,const TENSOR var,const TENSOR p,const int d1, const int d2,const int d3);

//////////////////////////////// LOGS OF PROBABILITY/DENSITY FUNCTIONS //////////////////////////////////////////////////
double ln_normpdf(const double x,const double mu,const double varin);
double ln_gampdf(const double x,const double a,const double b);
double ln_exppdf(const double x,const double a);
double ln_dirichpdf(const VECTOR x,const VECTOR a);

////////////////////////////////// CUMULATIVE DISTRIBUTION FUNCTIONS  ///////////////////////////////////////////////////
double normcdf(const double x,const double mu,const double var);
double gamcdf(const double x,const double a,const double b);
// Gives me the gamma cdf such that mean=a/b and var=a/(b*b)
// That is F(x) = integral(0,x) of [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0 and a>=1.
double expcdf(const double x,const double a);
// CDF OF EXPONENTIAL DISTRIBUTION P(x|a) = (1/a)*exp(-x/a)
double chi2cdf(const double x,const double df);
double mixnormcdf(const double x,const VECTOR mu,const VECTOR var,const VECTOR p);
double mixnormcdf_matrix(const double x,const MATRIX mu,const MATRIX var,const MATRIX p,const int d1,const int d2);
double mixnormcdf_tensor(const double x,const TENSOR mu,const TENSOR var,const TENSOR p,const int d1,const int d2,const int d3);

////////////////////////// INVERSE OF CUMULATIVE DISTRIBUTION FUNCTIONS  ////////////////////////////////////////////////
double invnormcdf(const double p,const double mu,const double var);
// Produces the normal deviate Z corresponding to a given lower tail area of P; Z is accurate to about 1 part in 10**16.
double invgamcdf(const double p,const double a,const double b);
double invexpcdf(const double p,const double a);
double invchi2cdf(const double p,const double df);
double invmixnormcdf(const double p,const VECTOR mu,const VECTOR var,const VECTOR we);

#endif /* PROBABILITY_H*/
