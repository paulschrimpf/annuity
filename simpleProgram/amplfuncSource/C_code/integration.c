#include <math.h>
#include "types.h"

#define JMAX 50
#define JMAXP (JMAX+1)
#define K 5
#define PIM4 0.7511255444649425
#define MAXIT 100
#define EPS 3.0e-14

double _trapzd(double (*func)(double,int),double a,double b,int n,int i);
double _gammln_int(double xx);

double qsimp(double (*func)(double,int),double a,double b,double eps,int i)
{
    int j;
    double s,st,ost=0.0,os=0.0;
    for (j=1;j<=JMAX;j++) {
	st=_trapzd(func,a,b,j,i);
	s=(4.0*st-ost)/3.0;
	if (j > 5)
	    if (fabs(s-os)<eps*fabs(os) || (s == 0.0&&os == 0.0)) return s;
	os=s;
	ost=st;
    }
    nrerror("Too many steps in routine qsimp");
    return 0.0;
}

double qromb(double (*func)(double,int),double a,double b,double eps,int i)
{
    void _polint_int(double xa[],double ya[],int n,double x,double *y,double *dy);
    double _trapzd(double (*func)(double,int), double a,double b,int n,int i);
    double ss,dss;
    double s[JMAXP],h[JMAXP+1];
    int j;
    h[1]=1.0;
    for (j=1;j<=JMAX;j++) {
	s[j]=_trapzd(func,a,b,j,i);
	if (j >= K) {
	    _polint_int(&h[j-K],&s[j-K],K,0.0,&ss,&dss);
	    if (fabs(dss) <= eps*fabs(ss)) return ss;
	}
	h[j+1]=0.25*h[j];
    }
    nrerror("Too many steps in routine qromb");
    return 0.0;
}

double _trapzd(double (*func)(double,int), double a,double b,int n,int i)
{
    double x,tnm,sum,del;
    static double s;
    int it,j;
    if (n == 1) return (s=0.5*(b-a)*((*func)(a,i)+(*func)(b,i)));
    else {
	for (it=1,j=1;j<n-1;j++) it <<= 1;
	tnm=it;
	del=(b-a)/tnm;
	x=a+0.5*del;
	for (sum=0.0,j=1;j<=it;j++,x+=del) sum += (*func)(x,i);
	s=0.5*(s+(b-a)*sum/tnm);
	return s;
    }
}

double qgaus16(double (*func)(double,int),double a,double b,int i)
{
    int j;
    double xr,xm,dx,s;
    static double x[]={0.0,0.09501250983764,0.28160355077926,0.45801677765723,0.61787624440264,
		       0.75540440835500,0.86563120238783,0.94457502307323,0.98940093499165};
    static double w[]={0.0,0.18945061045507,0.18260341504492,0.16915651939500, 
		       0.14959598881658,0.12462897125554,0.09515851168249,0.06225352393865,0.02715245941175};
    xm=0.5*(b+a);
    xr=0.5*(b-a);
    s=0;
    for (j=1;j<=8;j++) {
	dx=xr*x[j];
	s += w[j]*((*func)(xm+dx,i)+(*func)(xm-dx,i));
    }
    return s *= xr;
}

double qgaus8(double (*func)(double,int),double a,double b,int i)
{
    int j;
    double xr,xm,dx,s;
    static double x[]={0.0,0.18343464249565,0.52553240991633,0.79666647741363,0.96028985649754};
    static double w[]={0.0,0.36268378337836,0.31370664587789,0.22238103445337,0.10122853629038};
    xm=0.5*(b+a);
    xr=0.5*(b-a);
    s=0;
    for (j=1;j<=4;j++) {
	dx=xr*x[j];
	s += w[j]*((*func)(xm+dx,i)+(*func)(xm-dx,i));
    }
    return s *= xr;
}

double adaptive_quad_16_8(double (*func)(double,int), double a, double b, double eps, int i)
{
    double res,g8,g16,middle_point;
    double left_area,right_area,diff;
    middle_point=0.5*(b+a);
    g8 = qgaus8(func,a,b,i);
    g16 = qgaus16(func,a,b,i);
    diff = fabs(g16-g8);
    if ((diff<eps*fabs(g8))||((g16==0.0) && g8==0.0)||(fabs(a-middle_point)<=0.01)) res = g16;
    else {
	left_area = adaptive_quad_16_8(func,a,middle_point,eps,i);
	right_area = adaptive_quad_16_8(func,middle_point,b,eps,i);
	res = left_area + right_area;
    }
    return res;
}

void gauleg(VECTOR x,VECTOR w,double lo,double hi)
{
    int m,j,i;
    double z1,z,xm,xl,pp,p3,p2,p1;
    m=(x.d1+1)/2;
    xm=0.5*(hi+lo);
    xl=0.5*(hi-lo);
    for (i=1;i<=m;i++) {
	z=cos(PI*(i-0.25)/(x.d1+0.5));
	do {
	    p1=1.0;
	    p2=0.0;
	    for (j=1;j<=x.d1;j++) {
		p3=p2;
		p2=p1;
		p1=((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
	    }
	    pp=x.d1*(z*p1-p2)/(z*z-1.0);
	    z1=z;
	    z=z1-p1/pp;
	} while (fabs(z-z1) > EPS);
	x.a[i]=xm-xl*z;
	x.a[x.d1+1-i]=xm+xl*z;
	w.a[i]=2.0*xl/((1.0-z*z)*pp*pp);
	w.a[x.d1+1-i]=w.a[i];
    }
}

void gaulag(VECTOR x,VECTOR w,double alf)
{
    int i,its,j;
    double ai;
    double p1,p2,p3,pp,z,z1;
    for (i=1;i<=x.d1;i++) {
	if (i == 1) z=(1.0+alf)*(3.0+0.92*alf)/(1.0+2.4*x.d1+1.8*alf);
	else if (i == 2) z += (15.0+6.25*alf)/(1.0+0.9*alf+2.5*x.d1);
	else {
	    ai=i-2;
	    z += ((1.0+2.55*ai)/(1.9*ai)+1.26*ai*alf/
		  (1.0+3.5*ai))*(z-x.a[i-2])/(1.0+0.3*alf);
	}
	for (its=1;its<=MAXIT;its++) {
	    p1=1.0;
	    p2=0.0;
	    for (j=1;j<=x.d1;j++) {
		p3=p2;
		p2=p1;
		p1=((2*j-1+alf-z)*p2-(j-1+alf)*p3)/j;
	    }
	    pp=(x.d1*p1-(x.d1+alf)*p2)/z;
	    z1=z;
	    z=z1-p1/pp;
	    if (fabs(z-z1) <= EPS) break;
	}
	if (its > MAXIT) nrerror("too many iterations in gaulag");
	x.a[i]=z;
	w.a[i] = -exp(_gammln_int(alf+x.d1)-_gammln_int((double)x.d1))/(pp*x.d1*p2);
    }
}

void gauher(VECTOR x,VECTOR w)
{
    int i,its,j,m;
    double p1,p2,p3,pp,z,z1;
    m=(x.d1+1)/2;
    for (i=1;i<=m;i++) {
	if (i == 1) z=sqrt((double)(2*x.d1+1))-1.85575*pow((double)(2*x.d1+1),-0.166666666666667);
	else if (i == 2) z -= 1.14*pow((double)x.d1,0.426)/z;
	else if (i == 3) z=1.86*z-0.86*x.a[1];
	else if (i == 4) z=1.91*z-0.91*x.a[2];
	else z=2.0*z-x.a[i-2];
	for (its=1;its<=MAXIT;its++) {
	    p1=PIM4;
	    p2=0.0;
	    for (j=1;j<=x.d1;j++) {
		p3=p2;
		p2=p1;
		p1=z*sqrt(2.0/j)*p2-sqrt(((double)(j-1))/j)*p3;
	    }
	    pp=sqrt((double)2*x.d1)*p2;
	    z1=z;
	    z=z1-p1/pp;
	    if (fabs(z-z1) <= EPS) break;
	}
	if (its > MAXIT) nrerror("too many iterations in gauher");
	x.a[i]=z;
	x.a[x.d1+1-i] = -z;
	w.a[i]=2.0/(pp*pp);
	w.a[x.d1+1-i]=w.a[i];
    }
}

void gaujac(VECTOR x,VECTOR w,double alf,double bet)
{
    int i,its,j;
    double alfbet,an,bn,r1,r2,r3;
    double a,b,c,p1,p2,p3,pp,temp,z,z1;
    for (i=1;i<=x.d1;i++) {
	if (i == 1) {
	    an=alf/x.d1;
	    bn=bet/x.d1;
	    r1=(1.0+alf)*(2.78/(4.0+x.d1*x.d1)+0.768*an/x.d1);
	    r2=1.0+1.48*an+0.96*bn+0.452*an*an+0.83*an*bn;
	    z=1.0-r1/r2;
	} else if (i == 2) {
	    r1=(4.1+alf)/((1.0+alf)*(1.0+0.156*alf));
	    r2=1.0+0.06*(x.d1-8.0)*(1.0+0.12*alf)/x.d1;
	    r3=1.0+0.012*bet*(1.0+0.25*fabs(alf))/x.d1;
	    z -= (1.0-z)*r1*r2*r3;
	} else if (i == 3) {
	    r1=(1.67+0.28*alf)/(1.0+0.37*alf);
	    r2=1.0+0.22*(x.d1-8.0)/x.d1;
	    r3=1.0+8.0*bet/((6.28+bet)*x.d1*x.d1);
	    z -= (x.a[1]-z)*r1*r2*r3;
	} else if (i == x.d1-1) {
	    r1=(1.0+0.235*bet)/(0.766+0.119*bet);
	    r2=1.0/(1.0+0.639*(x.d1-4.0)/(1.0+0.71*(x.d1-4.0)));
	    r3=1.0/(1.0+20.0*alf/((7.5+alf)*x.d1*x.d1));
	    z += (z-x.a[x.d1-3])*r1*r2*r3;
	} else if (i == x.d1) {
	    r1=(1.0+0.37*bet)/(1.67+0.28*bet);
	    r2=1.0/(1.0+0.22*(x.d1-8.0)/x.d1);
	    r3=1.0/(1.0+8.0*alf/((6.28+alf)*x.d1*x.d1));
	    z += (z-x.a[x.d1-2])*r1*r2*r3;
	} else z=3.0*x.a[i-1]-3.0*x.a[i-2]+x.a[i-3];
	alfbet=alf+bet;
	for (its=1;its<=MAXIT;its++) {
	    temp=2.0+alfbet;
	    p1=(alf-bet+temp*z)/2.0;
	    p2=1.0;
	    for (j=2;j<=x.d1;j++) {
		p3=p2;
		p2=p1;
		temp=2*j+alfbet;
		a=2*j*(j+alfbet)*(temp-2.0);
		b=(temp-1.0)*(alf*alf-bet*bet+temp*(temp-2.0)*z);
		c=2.0*(j-1+alf)*(j-1+bet)*temp;
		p1=(b*p2-c*p3)/a;
	    }
	    pp=(x.d1*(alf-bet-temp*z)*p1+2.0*(x.d1+alf)*(x.d1+bet)*p2)/(temp*(1.0-z*z));
	    z1=z;
	    z=z1-p1/pp;
	    if (fabs(z-z1) <= EPS) break;
	}
	if (its > MAXIT) nrerror("too many iterations in gaujac");
	x.a[i]=z;
	w.a[i]=exp(_gammln_int(alf+x.d1)+_gammln_int(bet+x.d1)-_gammln_int(x.d1+1.0)-
		 _gammln_int(x.d1+alfbet+1.0))*temp*pow(2.0,alfbet)/(pp*p2);
    }
}

void _polint_int(double xa[], double ya[], int n, double x, double *y, double *dy)
{
    int i,m,ns=1;
    double den,dif,dift,ho,hp,w;
    VECTOR c,d;
    dif=fabs(x-xa[1]);
    vector(&c,n);
    vector(&d,n);
    for (i=1;i<=n;i++) {
	if ( (dift=fabs(x-xa[i])) < dif) {
	    ns=i;
	    dif=dift;
	}
	c.a[i]=ya[i];
	d.a[i]=ya[i];
    }
    *y=ya[ns--];
    for (m=1;m<n;m++) {
	for (i=1;i<=n-m;i++) {
	    ho=xa[i]-x;
	    hp=xa[i+m]-x;
	    w=c.a[i+1]-d.a[i];
	    if ( (den=ho-hp) == 0.0) nrerror("Error in routine polint");
	    den=w/den;
	    d.a[i]=hp*den;
	    c.a[i]=ho*den;
	}
	*y += (*dy=(2*ns < (n-m) ? c.a[ns+1] : d.a[ns--]));
    }
    free_vector(&d);
    free_vector(&c);
}

void gaucheb(VECTOR x, VECTOR w)
{
  int i;
  for(i=1;i<=x.d1;i++) {
    x.a[i]=cos( PI*(((double) i)-0.5)/((double) x.d1) );
    w.a[i]=PI/((double) x.d1);
  }
}

double _gammln_int(double xx)
{
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677,
			  24.01409824083091,-1.231739572450155,
			  0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}

#undef PIM4
#undef JMAX
#undef JMAXP
#undef K
#undef MAXIT
#undef EPS

