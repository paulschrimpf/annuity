#ifndef TYPES_H
#define TYPES_H

#define SMALL 1.0e-250
#define BIG 1.0e250
#define PI 3.141592653589793238462643383279502884197
#define ROOT2PI  2.506628274631000502415765284811
#define LOGROOT2PI 0.918938533204672780563271317078
#define EPSILON 1.110326e-16

typedef struct {
  double *a;
  int d1;
} VECTOR;

typedef struct {
  int *a;
  int d1;
} IVECTOR;

typedef struct {
  double **a;
  int d1;
  int d2;
} MATRIX;

typedef struct {
  int **a;
  int d1;
  int d2;
} IMATRIX;

typedef struct {
  double ***a;
  int d1;
  int d2;
  int d3;
} TENSOR;

typedef struct {
  double ****a;
  int d1;
  int d2;
  int d3;
  int d4;
} TENSOR4;

typedef struct {
  double *****a;
  int d1;
  int d2;
  int d3;
  int d4;
  int d5;
} TENSOR5;
/*
static double dmaxarg1,dmaxarg2;
#define MAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ? (dmaxarg1) : (dmaxarg2))
static double dminarg1,dminarg2;
#define MIN(a,b) (dminarg1=(a),dminarg2=(b),(dminarg1) < (dminarg2) ? (dminarg1) : (dminarg2))
static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ? (imaxarg1) : (imaxarg2))
static int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ? (iminarg1) : (iminarg2))
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
*/
void nrerror(char error_text[]);
/* Errror handler routine*/
void vector(VECTOR *vec, int n);
/* allocate a double vector with subscript range v[1..n] */
void ivector(IVECTOR *vec, int n);
/* allocate a double vector with subscript range v[1..n] */
void matrix(MATRIX *mat, int nr, int nc);
/* allocate a double matrix with subscript range m[1..nr][1..nc] */
void imatrix(IMATRIX *mat, int nr, int nc);
/* allocate an integer matrix with subscript range m[1..nr][1..nc] */
void tensor(TENSOR *ten, int nr, int nc, int nb);
/* allocate a double 3 dimensional matrix with subscript range m[1..nr][1..nc][1..nb] */
void free_ivector(IVECTOR *vec);
/* free an int vector allocated with ivector() */
void free_vector(VECTOR *vec);
/* free an int vector allocated with ivector() */
void free_imatrix(IMATRIX *mat);
/* free a int matrix allocated by matrix() */
void free_matrix(MATRIX *mat);
/* free a double matrix allocated by matrix() */
void free_tensor(TENSOR *ten);
/* free a tensor allocated by tensor() */
void tensor4(TENSOR4 *a, int d1, int d2, int d3, int d4);
void free_tensor4(TENSOR4 *a);
void tensor5(TENSOR5 *a, int d1, int d2, int d3, int d4, int d5);
void free_tensor5(TENSOR5 *a);


#endif /* TYPES_H */

