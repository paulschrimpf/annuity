#include <math.h>
#include "types.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////PROBABILITY/DENSITY FUNCTIONS ///////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double normpdf(const double x,const double mu,const double varin)
{
  double arg,var;
  var = sqrt(varin)<SMALL ? SMALL:varin;
  if (var<0.0) nrerror("variance has to be positive: normpdf");
  arg = -0.5*(x-mu)*(x-mu)/var;
  arg += -LOGROOT2PI - 0.5*log(var);
  return(exp(arg));
}

double gampdf(const double x,const double a,const double b)
{ // Returns the pdf of a gamma distribution evaluated at x. a is shape parameter and b inverse scale. 
  // such that mean=a/b and var=a/(b*b). That is P(x) = [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0.
  double p;
  double _gammln(double);
  if (a<0.0) nrerror("a has to be positive: gampdf");
  if (b<0.0) nrerror("b has to be positive: gampdf");
  if (x>0.0) {
    p=(a-1.0)*log(x)-(x*b)-_gammln(a)+a*log(b);
    return(exp(p));
  }
  else if ((x==0.0) && (a<1.0)) return(BIG);
  else if ((x==0.0) && (a==1.0)) return(b);
  else if ((x==0.0) && (a>1.0)) return(0.0);
  else if (x<0.0) return(0.0);
  nrerror("gampdf failed"); // should never get here
  return(-1.0);
}

double exppdf(const double x,const double a)
{ // PDF OF EXPONENTIAL DISTRIBUTION P(x|a) = (1/a)*exp(-x/a)
  if (a<0.0) nrerror("a has to be positive: exppdf");
  return(exp(-x/a)/a);
}

double chi2pdf(const double x,const double df)
{ 
  if (df<0.0) nrerror("Degrees of freedom have to be positive: chi2pdf");
  return(gampdf(x,0.5*df,0.5));
}

#define MNP \
    for(i=1;i<=d;i++) { \
	if (var.AI<0.0) nrerror("variance has to be positive: mixnormpdf"); \
	if (p.AI<0.0) nrerror("weight have to be positive: mixnormpdf"); \
	check+=p.AI; \
	pdf+=p.AI*normpdf(x,mu.AI,var.AI); \
    } \
    if (fabs(1.0-check)>1.0e-8) nrerror("weights have to add to one: mixnormpdf"); \
    return(pdf); 

double mixnormpdf(const double x,const VECTOR mu,const VECTOR var,const VECTOR p)
{ // mu(1:nmix)
  double pdf=0.0,check=0.0;
  int i,d;
  d=mu.d1;
#define AI a[i]
  MNP
#undef AI
}

double mixnormpdf_matrix(const double x,const MATRIX mu,const MATRIX var,const MATRIX p,const int r, const int c)
{ // mu(1:nmix)
  double pdf=0.0,check=0.0;
  int i,d;
  if (r==0) {
    d=mu.d1;
#define AI a[i][c]
    MNP
#undef AI
  }
  else if(c==0) {
    d=mu.d2;
#define AI a[r][i]
    MNP
#undef AI
  }
  else {
    nrerror("mixnormpdf_matrix failed"); 
    return(-BIG);
  }
}

double mixnormpdf_tensor(const double x,const TENSOR mu,const TENSOR var,const TENSOR p,const int d1, const int d2,const int d3)
{ // mu(1:nmix)
  double pdf=0.0,check=0.0;
  int i,d;
  if (d1==0) {
    d=mu.d1;
#define AI a[i][d2][d3]
    MNP
#undef AI
  }
  else if(d2==0) {
    d=mu.d2;
#define AI a[d1][i][d3]
    MNP
#undef AI
  }
  else if(d3==0) {
    d=mu.d3;
#define AI a[d1][d2][i]
      MNP
#undef AI
  }
  else {
    nrerror("mixnormpdf_tensor failed"); 
    return(-BIG);
  }
}
#undef MNP

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// LOGS OF PROBABILITY/DENSITY FUNCTIONS //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double ln_normpdf(const double x,const double mu,const double varin)
{
  double arg,var;
  var = sqrt(varin)<SMALL ? SMALL:varin;
  if (var<0.0) nrerror("variance has to be positive: ln_normpdf");
  arg = -0.5*(x-mu)*(x-mu)/var;
  arg += -LOGROOT2PI - 0.5*log(var);
  return(arg);
}

double ln_gampdf(const double x,const double a,const double b)
{
  double p;
  double _gammln(double);
  if (a<0.0) nrerror("a has to be positive: ln_gampdf");
  if (b<0.0) nrerror("b has to be positive: ln_gampdf");
  if (x>0.0) {
    p=(a-1.0)*log(x)-(x*b)-_gammln(a)+a*log(b);
    return(p);
  } 
  else if ((x==0.0) && (a<1.0)) return(log(BIG));
  else if ((x==0.0) && (a==1.0)) return(log(b));
  else if ((x==0.0) && (a>1.0)) return(-BIG);
  nrerror("ln_gampdf failed"); // should never get here
  return(-1.0);
}

double ln_exppdf(const double x,const double a)
{ 
  if (a<0.0) nrerror("a has to be positive: ln_exppdf");
  return((-x/a)-log(a));
}

double ln_dirichpdf(const VECTOR x,const VECTOR a)
{
  double aux,sa=0.0;
  int j;
  double _gammln(double);
  for (j=1;j<=a.d1;j++) sa += a.a[j];
  aux = _gammln(sa);
  for (j=1;j<=a.d1;j++) aux += ( ((a.a[j]-1.0)*log(x.a[j]))-_gammln(a.a[j]) );
  return(aux);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// CUMULATIVE DISTRIBUTION FUNCTIONS  ///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define P0 220.2068679123761
#define P1 221.2135961699311
#define P2 112.0792914978709
#define	P3 33.91286607838300
#define P4 6.373962203531650
#define P5 .7003830644436881
#define P6 .03526249659989109 
#define Q0 440.4137358247522
#define Q1 793.8265125199484
#define Q2 637.3336333788311
#define Q3 296.5642487796737
#define Q4 86.78073220294608
#define	Q5 16.06417757920695
#define Q6 1.755667163182642
#define Q7 .08838834764831844
#define CUTOFF  7.071
double normcdf(const double x,const double mu,const double var)
{
  double z,zabs,p,arg,logpdf,std;
  if (var<0.0) nrerror("variance has to be positive: normcdf");
  z=x-mu;
  zabs=fabs(z);  
  if (zabs<SMALL) return(0.5);
  std = sqrt(var);
  if (std<SMALL) return z>0.0 ? 1.0:0.0;
  zabs/=std;
  if (z/std>37.0) return(1.0);
  if (z/std<-37.0) return(0.0);
  arg = -0.5*zabs*zabs;
  logpdf = -LOGROOT2PI - log(std) + arg;
  if (zabs < CUTOFF) 
    p = arg + log(((((((P6*zabs + P5)*zabs + P4)*zabs + P3)*zabs + P2)*zabs 
		    + P1)*zabs + P0)) - log((((((((Q7*zabs + Q6)*zabs + Q5)*zabs + Q4)*zabs
						+ Q3)*zabs + Q2)*zabs + Q1)*zabs + Q0));
  else
    p = logpdf - log((zabs + 1.0/(zabs + 2.0/(zabs + 3.0/(zabs + 4.0/(zabs + 0.65))))));
  p = exp(p);
  return z < 0.0 ? p:1.0-p;
}
#undef P0
#undef P1
#undef P2
#undef P3
#undef P4
#undef P5
#undef P6
#undef Q0
#undef Q1
#undef Q2
#undef Q3
#undef Q4
#undef Q5
#undef Q6
#undef Q7
#undef CUTOFF 

double gamcdf(const double x,const double a,const double b)
{ // Gives me the gamma cdf such that mean=a/b and var=a/(b*b)
  // That is F(x) = integral(0,x) of [(b^a)/Gamma(a)] * [x^(a-1)] * exp(-x*b) for x>0 and a>=1.
  double p;
  double _gammp(double, double);
  if (a<0.0) nrerror("a has to be positive: gamcdf");
  if (b<0.0) nrerror("b has to be positive: gamcdf");
  p=_gammp(a,x*b);
  return p>1.0 ? 1.0:p;
}

double expcdf(const double x,const double a)
{ // CDF OF EXPONENTIAL DISTRIBUTION P(x|a) = (1/a)*exp(-x/a)
  if (a<0.0) nrerror("a has to be positive: expcdf");
  return(1.0-exp(-x/a));
}

double chi2cdf(const double x,const double df)
{ 
  if (df<0.0) nrerror("Degrees of freedom have to be positive: chi2cdf");
  return(gamcdf(x,0.5*df,0.5));
}

#define MNCDF \
  for(i=1;i<=d;i++) { \
    if (var.AI<0.0) nrerror("variance has to be positive: mixnormcdf"); \
    if (p.AI<0.0) nrerror("weight have to be positive: mixnormcdf"); \
    check+=p.AI; \
    cdf+=p.AI*normcdf(x,mu.AI,var.AI); \
  } \
  if (fabs(1.0-check)>1.0e-8) nrerror("weights have to add to one: mixnormcdf"); \
  return(cdf);

double mixnormcdf(const double x,const VECTOR mu,const VECTOR var,const VECTOR p)
{ 
  double cdf=0.0,check=0.0;
  int i,d;
  d = mu.d1;
#define AI a[i]  
  MNCDF
#undef AI
}

double mixnormcdf_matrix(const double x,const MATRIX mu,const MATRIX var,const MATRIX p,const int d1,const int d2)
{ 
  double cdf=0.0,check=0.0;
  int i,d;
  if (d1==0) {
    d = mu.d1;
#define AI a[i][d2]  
  MNCDF
#undef AI
  }
  else if (d2==0) {
    d = mu.d2;
#define AI a[d1][i]  
  MNCDF
#undef AI
  }
  else {
    nrerror("mixnormcdf_matrix failed"); 
    return(-BIG);
  }
}

double mixnormcdf_tensor(const double x,const TENSOR mu,const TENSOR var,const TENSOR p,const int d1,const int d2,const int d3)
{ 
  double cdf=0.0,check=0.0;
  int i,d;
  if (d1==0) {
    d = mu.d1;
#define AI a[i][d2][d3]  
  MNCDF
#undef AI
  }
  else if (d2==0) {
    d = mu.d2;
#define AI a[d1][i][d3]  
  MNCDF
#undef AI
  }
  else if (d3==0) {
    d = mu.d3;
#define AI a[d1][d2][i]  
  MNCDF
#undef AI
  }
  else {
    nrerror("mixnormcdf_matrix failed"); 
    return(-BIG);
  }
}
#undef MNCDF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////// INVERSE OF CUMULATIVE DISTRIBUTION FUNCTIONS  ////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double invnormcdf(const double p,const double mu,const double var)
{ //Produces the normal deviate Z corresponding to a given lower tail area of P; Z is accurate to about 1 part in 10**16.
    double q,r,inv;
    double SPLIT1 = 0.425,SPLIT2 = 5.0,CONST1 = 0.180625,CONST2 = 1.6;
    // Coefficients for P close to 0.5
    double A0=3.3871328727963666080,A1=1.3314166789178437745e+2,A2=1.9715909503065514427e+3;
    double A3=1.3731693765509461125e+4,A4=4.5921953931549871457e+4,A5=6.7265770927008700853e+4;
    double A6=3.3430575583588128105e+4,A7=2.5090809287301226727e+3,B1=4.2313330701600911252e+1;
    double B2=6.8718700749205790830e+2,B3=5.3941960214247511077e+3,B4=2.1213794301586595867e+4;
    double B5=3.9307895800092710610e+4,B6=2.8729085735721942674e+4,B7=5.2264952788528545610e+3;
    // Coefficients for P not close to 0, 0.5 or 1.
    double C0=1.42343711074968357734,C1=4.63033784615654529590,C2=5.76949722146069140550;
    double C3=3.64784832476320460504,C4=1.27045825245236838258,C5=2.41780725177450611770e-1;
    double C6=2.27238449892691845833e-2,C7=7.74545014278341407640e-4,D1=2.05319162663775882187;
    double D2=1.67638483018380384940,D3=6.89767334985100004550e-1,D4=1.48103976427480074590e-1;
    double D5=1.51986665636164571966e-2,D6=5.47593808499534494600e-4,D7=1.05075007164441684324e-9;
    // Coefficients for P near 0 or 1.
    double E0=6.65790464350110377720,E1=5.46378491116411436990,E2=1.78482653991729133580;
    double E3=2.96560571828504891230e-1,E4=2.65321895265761230930e-2,E5=1.24266094738807843860e-3;
    double E6=2.71155556874348757815e-5,E7=2.01033439929228813265e-7,F1=5.99832206555887937690e-1;
    double F2=1.36929880922735805310e-1,F3=1.48753612908506148525e-2,F4=7.86869131145613259100e-4;
    double F5=1.84631831751005468180e-5,F6=1.42151175831644588870e-7,F7=2.04426310338993978564e-15;
    if ((p>1.0) || (p<0.0)) nrerror("p has to be between zero and one: invnormcdf");
    if (var<0.0) nrerror("variance has to be positive: invnormcdf");
    q=p-0.5;
    if (fabs(q)<SPLIT1) {
	r = CONST1 - (q*q);
	inv=q*(((((((A7*r+A6)*r+A5)*r+A4)*r+A3)*r+A2)*r+A1)*r+A0)/(((((((B7*r+B6)*r+B5)*r+B4)*r+B3)*r+B2)*r+B1)*r+1.0);
	inv*=sqrt(var);
	inv+=mu;
	return(inv);
    }
    else {
	r = q<0.0 ? p:1.0-p;
	r=sqrt(-log(r));
	if (r<SPLIT2) {
	    r-=CONST2;
	    inv=(((((((C7*r+C6)*r+C5)*r+C4)*r+C3)*r+C2)*r+C1)*r+C0)/(((((((D7*r+D6)*r+D5)*r+D4)*r+D3)*r+D2)*r+D1)*r+1.0);
	}
	else {
	    r-=SPLIT2;
	    inv=(((((((E7*r+E6)*r+E5)*r+E4)*r+E3)*r+E2)*r+E1)*r+E0)/(((((((F7*r+F6)*r+F5)*r+F4)*r+F3)*r+F2)*r+F1)*r+1.0);
	}
	if (q<0.0) inv=-inv;
	inv*=sqrt(var);
	inv+=mu;
	return(inv);
    }
}

double invgamcdf(const double p,const double a,const double b)
{
    double xk,mn,v,temp,sigma,mu,h,xnew;
    if ((p<0.0) || (p>1.0)) nrerror("p has to be between 0 and 1: invgamcdf");
    if (a<0.0) nrerror("a has to be positive: invgamcdf");
    if (b<0.0) nrerror("b has to be positive: invgamcdf");
    if (p==0.0) return(0.0);
    if (p==1.0) return(BIG);
    mn=a*b;
    v=mn*b;
    temp=log(v+(mn*mn));
    mu=2.0*log(mn)-0.5*temp;
    sigma=-2.0*log(mn)+temp;
    xk=exp(invnormcdf(p,mu,sigma));
    h=1.0;
    do {
	h=(gamcdf(xk,a,b)-p)/gampdf(xk,a,b);
	xnew = xk - h;
	if (xnew<0.0) {
	    xnew = xk/10.0;
	    h = xk-xnew;
	}
	xk=xnew;
    } while ( (fabs(h)>sqrt(EPSILON)*fabs(xnew)) && (fabs(h)>sqrt((EPSILON))) );
    return(xk);
}

double invexpcdf(const double p,const double a)
{
    if ((p<0.0) || (p>1.0)) nrerror("p has to be between 0 and 1: invexpcdf");
    if (a<0.0) nrerror("a has to be positive: invexpcdf");
    return(log(1.0-p)*a);
}

double invchi2cdf(const double p,const double df)
{
    if ((p<0.0) || (p>1.0)) nrerror("p has to be between 0 and 1: invchi2cdf");
    if (df<0.0) nrerror("Degrees of freedom have to be positive: invchi2cdf");
    return(invgamcdf(p,df/2.0,0.5));
}
	
double invmixnormcdf(const double p,const VECTOR mu,const VECTOR var,const VECTOR we)
{
    double x1,x2,f1,f2,dx,inv,cdf=0.0;
    int i;
    if ((p<0.0) || (p>1.0)) nrerror("p has to be between 0 and 1: invmixnormcdf");
    for(i=1;i<=mu.d1;i++) {
	if (var.a[i]<0.0) nrerror("variance has to be positive: invmixnormcdf");
	if (we.a[i]<0.0) nrerror("weight have to be positive: invmixnormcdf");
	cdf+=we.a[i];
    }
    if (cdf!=1.0) nrerror("weights have to add to one: invmixnormcdf");
    if (p==0.0) return(0.0);
    if (p==1.0) return(BIG);
    x1 = -BIG;
    x2 = 0.0;
    f1 = mixnormcdf(x1,mu,var,we) - p;
    f2 = mixnormcdf(x2,mu,var,we) - p;
    if (f2==0.0) return(x2);
    if (f1*f2>0.0) {
	f1=f2;
	x1 = 0.0;
	x2 = BIG;
    }
    if (f1<0.0) {
	inv=x1;
	dx=x2-x1;
    }
    else {
	inv=x2;
	dx=x1-x2;
    }
    do {
	dx=dx*0.5;
	x2=inv+dx;
	f2=mixnormcdf(x2,mu,var,we) - p;	
	if (f2<=0.0) inv=x2;
    } while (!((fabs(dx)<0.0000000001) || (f2==0.0)));
    return(inv);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// Gamma Auxiliary Functions ////////////////////////////////////////////
double _gammln(double xx)
{
    double x,y,tmp,ser;
    static double cof[6]={76.18009172947146,-86.50532032941677,24.01409824083091,-1.231739572450155,
			  0.1208650973866179e-2,-0.5395239384953e-5};
    int j;
    y=x=xx;
    tmp=x+5.5;
    tmp -= (x+0.5)*log(tmp);
    ser=1.000000000190015;
    for (j=0;j<=5;j++) ser += cof[j]/++y;
    return -tmp+log(2.5066282746310005*ser/x);
}

double _gammp(double a,double x)
{
    void _gcf(double *gammcf, double a, double x, double *gln);
    void _gser(double *gamser, double a, double x, double *gln);
    double gamser,gammcf,gln;
    if (x < 0.0 || a <= 0.0) nrerror("Invalid arguments in routine gammp");
    if (x < (a+1.0)) {
	_gser(&gamser,a,x,&gln);
	return gamser;
    } else {
	_gcf(&gammcf,a,x,&gln);
	return 1.0-gammcf;
    }
}

#define ITMAX 1000
void _gser(double *gamser,double a,double x,double *gln)
{
    double _gammln(double xx);
    int n;
    double sum,del,ap;
    *gln=_gammln(a);
    if (x <= 0.0) {
	if (x < 0.0) nrerror("x less than 0 in routine gser");
	*gamser=0.0;
	return;
    } else {
	ap=a;
	del=sum=1.0/a;
	for (n=1;n<=ITMAX;n++) {
	    ++ap;
	    del *= x/ap;
	    sum += del;
	    if (fabs(del) < fabs(sum)*EPSILON) {
		*gamser=sum*exp(-x+a*log(x)-(*gln));
		return;
	    }
	}
	nrerror("a too large, ITMAX too small in routine gser");
	return;
    }
}

#define FPMIN SMALL/EPSILON
void _gcf(double *gammcf, double a, double x, double *gln)
{
    double _gammln(double xx);
    int i;
    double an,b,c,d,del,h;
    *gln=_gammln(a);
    b=x+1.0-a;
    c=1.0/FPMIN;
    d=1.0/b;
    h=d;
    for (i=1;i<=ITMAX;i++) {
	an = -i*(i-a);
	b += 2.0;
	d=an*d+b;
	if (fabs(d) < FPMIN) d=FPMIN;
	c=b+an/c;
	if (fabs(c) < FPMIN) c=FPMIN;
	d=1.0/d;
	del=d*c;
	h *= del;
	if (fabs(del-1.0) < EPSILON) break;
    }
    if (i > ITMAX) nrerror("a too large, ITMAX too small in gcf");
    *gammcf=exp(-x+a*log(x)-(*gln))*h;
}
#undef FPMIN
#undef ITMAX
