#include <stdlib.h>/* for atoi */
#include <math.h>/* for sqrt */
#include <assert.h>
#include "funcadd.h"/* includes "stdio1.h" */

#include "types.h"
#include "ampl_probability.h"
#include "ampl_integration.h"

/* cutoff related variables */
static int nCut=0;
static VECTOR logA;
static MATRIX logB;

#define BUFFER 10000 /* size of temporary buffer */

static void setCut(arglist *al) 
{
  AmplExports *ae = al->AE; /* to allow error msg printing */
  int nAlloc;
  FILE *fid; 
  double *tempA,**tempB;
  int i,j,ngrid,k;
  if (al->n<4) { /* read cutoffs from file */
    /* note: when ampl passes model to a solver, the dll will be
       recalled.  static variables will no longer be in memory.  Therefore,
       we need to save things to a simple text file and communicate
       with it */
    printf("setCut(): reading cutoffs from file...\n");
    fid = fopen("cut.tmp","r");
    fscanf(fid,"%d",&nCut);
    printf("          %d cutoffs expected\n",nCut);
    tempB = malloc((size_t) nCut*sizeof(double*));
    nAlloc=BUFFER;
    tempA = malloc((size_t) nAlloc*sizeof(double));
    for(j=0;j<nCut;j++) tempB[j]=malloc((size_t) nAlloc*sizeof(double));
    i=0;
    while(1==fscanf(fid,"%lf",tempA+i)) {
      for(j=0;j<nCut;j++) fscanf(fid,"%lf",tempB[j]+i);
      i++;
      if(i>=nAlloc) {
        printf("reallocating ...\n");
	nAlloc+=BUFFER;
	realloc(tempA,(size_t) nAlloc*sizeof(double));
	for(j=0;j<nCut;j++) realloc(tempB[j],(size_t) nAlloc*sizeof(double));
        printf("finished reallocating\n");
      }
    }
    fclose(fid);
    printf("        %d grid points read\n",i);
    vector(&logA,i);
    matrix(&logB,i,nCut);
    printf("        allocated logA and logB\n");
    for(i=1;i<=logA.d1;i++) {
      logA.a[i] = tempA[i-1];
      for(j=0;j<nCut;j++) logB.a[i][j+1] = tempB[j][i-1];
    }
    free(tempA);
    printf("        freed tempA\n");
    for(j=0;j<nCut;j++) free(tempB[j]);
    printf("        freed tempB\n");
  } else {
    if (nCut) { /* nCut already set, check for consistency */
      if (nCut != (int) al->ra[0]) {
	fprintf(stderr,"ERROR: setCut()\n"
	       "  cannot change nCut\n");
      }
    } else {
      nCut = (int) al->ra[0];
      /* al->n = 2 + ngrid + nCut*ngrid */
      ngrid = (al->n - 2)/2;
      printf("setCut(): setting up cutoffs ...\n"
	     "          %d cutoffs\n"
	     "          %d grid points\n",nCut,ngrid);
      vector(&logA,ngrid);
      matrix(&logB,ngrid,nCut);
    }
    ngrid = logA.d1;
    j = al->ra[al->n-1]; /* cutoff we're setting */
    k = 1; /* index in al->ra */
    for(i=1;i<=ngrid;i++) logA.a[i] = al->ra[k++];
    for(i=1;i<=ngrid;i++) logB.a[i][j] = al->ra[k++];
    if (j==nCut) {
      printf("          saving cutoffs in cut.tmp\n");
      fid = fopen("cut.tmp","w");
      fprintf(fid,"%d\n",nCut);
      for(i=1;i<=ngrid;i++) {
	fprintf(fid,"%25.16g ",logA.a[i]);
	for(j=1;j<=nCut;j++) fprintf(fid,"%25.16g ",logB.a[i][j]);
	fprintf(fid,"\n");
      }
      fclose(fid);
    }
  }
}
#undef BUFFER

static double cutoff(arglist *al) 
{ /* linearly interpolate to calculate cutoff */
  AmplExports *ae = al->AE; /* to allow error msg printing */
  int i,j,k;
  int h,l;
  int c;
  double la,cut;
  double slope,swap;
  if (nCut==0) setCut(al); /* read cutoffs from file */
  /* force logA to be increasing */
  if (logA.a[1]>logA.a[2]) {
    for(i=1,j=logA.d1;i<j;i++,j--) {
      swap = logA.a[i]; logA.a[i] = logA.a[j]; logA.a[j] = swap;
      for(k=1;k<=logB.d2;k++) 
	swap = logB.a[i][k]; logB.a[i][k] = logB.a[j][k]; logB.a[j][k] = swap;
    }
  }
  la = al->ra[0];
  c = al->ra[1];
  /* find where la fits in the table */
  /* table small enough that naive method isn't too bad */
  if (la<=logA.a[1]) {
    l = 1; h =2;
  } else if(la>=logA.a[logA.d1]) {
    h = logA.d1; l=h-1;
  } else {
    h = 1;
    while(la>logA.a[h]) h++;
    l = h-1;
    if(!(la>logA.a[l] && la <= logA.a[h])) {
      printf("logA grid problem: %g %g %g\n",logA.a[l],la,logA.a[h]);
    }
  }
  slope = (logB.a[h][c]-logB.a[l][c])/(logA.a[h]-logA.a[l]);
  cut = slope*(la-logA.a[l])+logB.a[l][c];
  if (al->derivs) { /* derivative requested */
    al->derivs[0] = slope;
    al->derivs[1] = 0;
  } 
  if (al->hes) { /* hessian requested */
    for(i=0;i<3;i++) al->hes[i] = 0;
  }
  return(cut);
}

void funcadd(AmplExports *ae)
{
  /* Insert calls on addfunc here... */
  
  /* Arg 3, called type, must satisfy 0 <= type <= 6:
   * type&1 == 0:0,2,4,6==> force all arguments to be numeric.
   * type&1 == 1:1,3,5==> pass both symbolic and numeric arguments.
   * type&6 == 0:0,1==> the function is real valued.
   * type&6 == 2:2,3==> the function is char * valued; static storage
       suffices: AMPL copies the return value.
       * type&6 == 4:4,5==> the function is random (real valued).
       * type&6 == 6: 6==> random, real valued, pass nargs real args,
       *0 <= nargs <= 2.
       *
       * Arg 4, called nargs, is interpretted as follows:
       *>=  0 ==> the function has exactly nargs arguments
       *<= -1 ==> the function has >= -(nargs+1) arguments.
       *
       * Arg 5, called funcinfo, is copied without change to the arglist
       *structure passed to the function; funcinfo is for the
       *function to use or ignore as it sees fit.
       */

  /* Solvers quietly ignore kth, sginv, and rncall, since */
  /* kth and sginv are symbolic (i.e., char* valued) and  */
  /* rncall is specified as random.  Thus kth, sginv, and */
  /* rncall may not appear nonlinearly in declarations in */
  /* an AMPL model. */
  
  addfunc("normcdf", (rfunc)ampl_normcdf, 0, 1, 0);
  addfunc("normpdf", (rfunc)ampl_normpdf, 0, 1, 0);
  addfunc("gamcdf", (rfunc)ampl_gamcdf, 0, 3, 0);
  addfunc("gampdf", (rfunc)ampl_gampdf, 0, 3, 0);
  addfunc("gaucheb", (rfunc)ampl_gaucheb, 0, 3, 0);
  addfunc("gaulag", (rfunc)ampl_gaulag, 0, 3, 0);
  addfunc("setCut", (rfunc)setCut, 0, -2, 0);
  addfunc("cutoff", (rfunc)cutoff, 0, 2, 0);

  /* at_end() and at_reset() calls could appear here, too. */
}
