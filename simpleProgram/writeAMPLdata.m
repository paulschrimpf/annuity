function writeAMPLdata(data,cfg,cut,mort)
  if (~exist('REALMAX'))
    REALMAX=realmax
  end
  if (nargin<4) 
    mort = 0;
  end
% writes AMPL format data file
  if(length(data)==1) 
    i = findstr('/',cfg.outprefix);
    prefix = cfg.outprefix(i(length(i))+1:length(cfg.outprefix))
    fid = fopen([prefix '.dat'],'w');
    
    fprintf(fid,'param nObs := %d;\n',data.N);
    vars = {'fage','lage','died','g'};
    for v=1:length(vars)
      writeVar(fid,data.(vars{v}),vars{v});
    end
    
    if (length(cfg.data.group)>1)
      writeVar(fid,data.group,'group');
    end
    % now cutoffs
    fprintf(fid,'param cutN := %d;\n',length(cut(1).la));
    writeVar(fid,cut(1).la,'cutLogA');
    fprintf(fid,'param cutLogB := ');
    if (length(cut)==1)
      for i=1:size(cut.lb,1)
        for j=1:size(cut.lb,2)
          if (isinf(cut.lb(i,j))) 
%            fprintf(['WARNING: writeAMPLdata()\n' ...
%                     '  %s(%d) = inf, writing %g instead\n'],...
%                    'cut lb',i,REALMAX);
            cut.lb(i,j) = REALMAX;
          end
          fprintf(fid,'%d,%d %25.16g\n',i,j,cut.lb(i,j));
        end
      end
      fprintf(fid,';\n');
    else 
      lb = cut(1).lb;
      t = whos('lb');
      if (strcmp(t.class,'double'))
        for c=1:length(cut);
          for i=1:size(cut(c).lb,1)
            for j=1:size(cut(c).lb,2)
              if (isinf(cut(c).lb(i,j))) 
                %              fprintf(['WARNING: writeAMPLdata()\n' ...
                %                       '  %s(%d) = inf, writing %g instead\n'],...
                %                      'cut lb',i,REALMAX);
                cut(c).lb(i,j) = REALMAX;
              end
              fprintf(fid,'%d,%d,%d %25.16g\n',i,j,c,cut(c).lb(i,j));
            end
          end
        end
      else 
        for c=1:length(cut);
          for iw=1:length(cut(c).lb)
            for i=1:size(cut(c).lb{iw},1)
              for j=1:size(cut(c).lb{iw},2)
                if (isinf(cut(c).lb{iw}(i,j))) 
                  %              fprintf(['WARNING: writeAMPLdata()\n' ...
                  %                       '  %s(%d) = inf, writing %g instead\n'],...
                  %                      'cut lb',i,REALMAX);
                  cut(c).lb{iw}(i,j) = REALMAX;
                end
                fprintf(fid,'%d,%d,%d,%d %25.16g\n',i,j,c,iw,cut(c).lb{iw}(i,j));
              end
            end
          end        
        end
      end
      fprintf(fid,';\n');
    end
    if (~mort)
      fprintf(fid,'param lambda :=%25.16g;\n',cfg.mort.lambda);
      fprintf(fid,'param h := %.16g;\n',cfg.mort.fasthazard);
    end

    if (any(data.alphaX~=data.betaX))
      error('betaX and alphaX assumed to be the same\n');
    end
    
    if (size(data.alphaX,2)>data.ngroup)
      nX = size(data.alphaX,2) - data.ngroup;
      fprintf(fid,'param nX := %d;\n',nX);
      fprintf(fid,'param X := \n');
      for i=1:data.N
        for k=1:nX 
          fprintf(fid,'%d,%d %.16g\n',i,k,data.alphaX(i,k+data.ngroup));
        end
      end
      fprintf(fid,';\n');
    end
    lb = cut(1).lb;
    t = whos('lb');
    if (strcmp(t.class,'cell'))
      fprintf(fid,'param pw :=\n');
      for k=1:length(cut(1).wp)
        fprintf(fid,'%d   %.16g\n',k,cut(1).wp(k));
      end
      fprintf(fid,';\n');
    end
    
    fclose(fid);
    fid = fopen('settings.ampl','w');
    fprintf(fid,['data ' prefix '.dat\n']);
    fclose(fid);
  else % length of data != 1
    i = findstr('/',cfg{1}.outprefix);
    prefix = cfg{1}.outprefix(i(length(i))+1:length(cfg{1}.outprefix))
    fid = fopen([prefix '.dat'],'w');

    n = 0
    for c=1:length(data)
      n = n+data{c}.N;
    end
    fprintf(fid,'param nObs := %d;\n',n);
    vars = {'fage','lage','died','g'};
    for v=1:length(vars)
      vec = [];
      for d=1:length(data)
        vec = [vec; data{d}.(vars{v})];
      end
      writeVar(fid,vec,vars{v});
    end
    vec = [];
    for d=1:length(data)
      vec = [vec; ones(data{d}.N,1)*d];
    end
    writeVar(fid,vec,'group');
    
    % now cutoffs
    fprintf(fid,'param cutN := %d;\n',length(cut{1}.la));
    fprintf(fid,'param cutLogA := ');
    for c=1:length(cut)
      if (c==1) 
        for i=1:size(cut{c}.la,1);
          fprintf(fid,'%d  %25.16g\n',i,cut{c}.la(i));
        end
      else 
        assert(max(abs(cut{c}.la - cut{1}.la))<1e-6);
      end
    end
    fprintf(fid,';\n');
    fprintf(fid,'param cutLogB := ');
    for c=1:length(cut);
      for i=1:size(cut{c}.lb,1)
        for j=1:size(cut{c}.lb,2)
          if (isinf(cut{c}.lb(i,j))) 
%            fprintf(['WARNING: writeAMPLdata()\n' ...
%                     '  %s(%d) = inf, writing %g instead\n'],...
%                    'cut lb',i,REALMAX);
            cut{c}.lb(i,j) = REALMAX;
          end
          fprintf(fid,'%d,%d,%d %25.16g\n',i,j,c,cut{c}.lb(i,j));
        end
      end
    end
    fprintf(fid,';\n');
    
    if (~mort)
      fprintf(fid,'param lambda := %.16g;\n',cfg{1}.mort.lambda);
      for d=2:length(cfg) 
        assert(cfg{1}.mort.lambda==cfg{d}.mort.lambda);
      end
      fprintf(fid,';\n');
      for d=2:length(cfg)
        assert(cfg{1}.mort.fasthazard==cfg{d}.mort.fasthazard);
      end
      fprintf(fid,'param h := %.16g;\n',cfg{1}.mort.fasthazard);
    end
        
    fclose(fid);
    fid = fopen('settings.ampl','w');
    fprintf(fid,['data ' prefix '.dat\n']);
    fclose(fid);
  end % if (length(data)==1) ... else ... 
end
  
function writeVar(fid,var,varName)
  fprintf(fid,'param %s := ',varName);
  for i=1:length(var)
    if (isinf(var(i))) 
%      fprintf(['WARNING: writeAMPLdata()\n' ...
%               '  %s(%d) = inf, writing %g instead\n'],...
%              varName,i,REALMAX);
      var(i) = REALMAX;
    end
    fprintf(fid,' %d %25.16g\n',i,var(i));
  end
  fprintf(fid,';\n');
end
  
