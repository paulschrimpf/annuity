function int = gaussChebyshevInt(nint);
% returns guass-chebyshev integration points and weights transformed to
% work on -inf,inf
  int.n = nint;
  int.x = zeros(nint,1);
  int.w = zeros(nint,1);
  for i=1:nint,
    int.x(i) = cos(pi*(i - 0.5)/nint);
    int.w(i) = pi/nint * 2.0/sqrt(1.0 - int.x(i)^2);
    int.x(i) = log((1.0-int.x(i))/(1.0+int.x(i)));
  end;

