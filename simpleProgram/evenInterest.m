function r = evenInterest(cfdata,cfg)
  % find interest rate such that company breaks even
    
  rlo = 0.0;
  rhi = 2.0;
  r = fzero(@(x) etrans(x,cfdata,cfg),[rlo rhi]);
  r = 1/r - 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function diff = etrans(rho,data,cfg)
  % return expected payments - w0*alpha2
    if (isfield(cfg.mort,'fasthazard'))
      h = cfg.mort.fasthazard;
    else 
      h = 1;
    end
    lambda = cfg.mort.lambda;
    if (numel(cfg.cut.w0)>1)
      w0   = cfg.cut.w0*cfg.cut.wp';       % Starting wealth (almost
                                           % always = 100)        
    else
      w0 = cfg.cut.w0;
    end
    agemax = cfg.cut.agemax; % Maximal age alive
    if (numel(cfg.cut.fracAnnuitized)>1)
      alpha2 = cfg.cut.fracAnnuitized*cfg.cut.wp';
    else
      alpha2 = cfg.cut.fracAnnuitized; % Fraction of wealth annuitized 
    end
    base = cfg.mort.baseage;
    for n=1:data.N;
      g = data.g(n);
      if (g==0)
        c = 1;
      elseif (g==5)
        c = 2;
      elseif (g==10)
        c = 3;
      end
      z_slope = cfg.cut.z(data.group(n),:);
      agemin = cfg.cut.agemin(data.group(n));
      T = agemax - agemin;
      % parameters that optionally vary with group
      if (length(cfg.cut.inflation)>1)
        infl = cfg.cut.inflation(data.group(n));
      else
        infl = cfg.cut.inflation;
      end
      inf_factor = cumprod((1/(1+infl))*ones(T,1)); 
      rdis = cumprod(rho*ones(T,1)); 
      t = (agemin-base):(agemax-base);
      tp1 = t+1;
      galpha = data.alpha(n);
      part2  = exp(galpha/lambda*(1 - exp(lambda*(tp1.^h))));
      part1  = exp(galpha/lambda*(1 - exp(lambda*(t.^h  ))));  
      fr     = (part1(1:T) - part2(1:T))';          % density  
      Z = w0*alpha2*z_slope(c); % formula for annuity pricing
      Z = (Z*ones(T,1)).*inf_factor; % convert nominal annuity to real payments
                                     % value
      cumz = cumsum(Z.*rdis);
      if g>0,
        epay(n) = cumz(g)*sum(fr(1:g)) + ...
                  sum(cumz(g+1:T).*fr(g+1:T));
      else
        epay(n) = sum(cumz.*fr);
      end;
      if (sum(fr)==0)
        epay(n) = 0.0;
      else
        epay(n) = epay(n)/sum(fr);
      end
    end %loop over n
    diff = mean(epay)-w0*alpha2;
%----------------------------------------------------------------------

  
