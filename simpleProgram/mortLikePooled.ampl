# ampl command file for step 1 estimation
load amplfunc.dll; # load library of external functions
model mortLikePooled.mod; # model file
include "settings.ampl";
#data pooled.dat; # data file
option solver snopt; # set the solver
option snopt_options 'timing 1 outlev=3'; # some solver options

# intial values
let minSig := 1e-10;
let sigA := 0.1;
let{k in 1..nG} muA[k] := -6;
let lambda :=   0.00001;
solve; 
# display results
display _solve_time;
display logLikelihood,lambda,muA,sigA;

for{k in 1..nG} {
printf "%d\n", k;
printf "%25.16g\n", muA[k];
}
printf "%25.16g\n", lambda;
printf "%25.16g\n", sigA;

